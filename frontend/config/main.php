<?php
$params = array_merge(
  require(__DIR__ . '/../../common/config/params.php'),
  require(__DIR__ . '/params.php')
);

$config = [
  'id' => 'app-frontend',

  'name' => 'Наименование проекта',

  'basePath' => dirname(__DIR__),

  'bootstrap' => ['seo', 'log', 'userCounter'],

  'language' => 'ru-RU',

  'sourceLanguage' => 'ru-RU',

  'controllerNamespace' => 'frontend\controllers',

  'components' => [

    'seo' => [
      'class' => \common\components\Seo::class
    ],

    'userCounter' => [
      'class' => 'frontend\components\UserCounter',
    ],

    'request' => [
      // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
      'cookieValidationKey' => 'tObyj990YYKKOAVibBsMOmxWTpuWyKzi',
    ],

    'urlManager' => [
      'showScriptName' => false,
      'enablePrettyUrl' => true,
      'rules' => [
        '/profile/customer/feed/?'=>'/profile/customer/feed/index',
        '/profile/customer/personal/?'=>'/profile/customer/personal/index',
        '/profile/customer/company/?'=>'/profile/customer/company/index',
        '/profile/customer/projects/?'=>'/profile/customer/projects/index',
        '/profile/customer/reviews/?'=>'/profile/customer/reviews/index',
        '/profile/customer/settings/?'=>'/profile/customer/settings/index',

        '/tasks/<id:\d+>/view'=>'/tasks/view/index',
        '/tasks/<id:\d+>/log'=>'/tasks/log/index',
        '/tasks/<id:\d+>/files'=>'/tasks/files/index',

        /**
         * Private profiles
         */
        '/profile/customer/tasks/?'=>'/profile/customer/tasks/index',
        '/profile/customer/tasks/<id:\d+>/view'=>'/profile/customer/tasks/view',
        '/profile/customer/tasks/<id:\d+>/log'=>'/profile/customer/tasks/log',
        '/profile/customer/tasks/<id:\d+>/participants'=>'/profile/customer/tasks/participants',

        '/profile/business/feed/?'=>'/profile/business/feed/index',
        '/profile/business/personal/?'=>'/profile/business/personal/index',
        '/profile/business/cv/?'=>'/profile/business/cv/index',
        '/profile/business/portfolio/?'=>'/profile/business/portfolio/index',
        '/profile/business/reviews/?'=>'/profile/business/reviews/index',
        '/profile/business/settings/?'=>'/profile/business/settings/index',
        '/profile/business/tasks/?'=>'/profile/business/tasks/index',

        /**
         * Public profiles
         */
        '/user/<id:\d+>/business/personal/?'=>'/user/business/personal/index',
        '/user/<id:\d+>/business/tasks/?'=>'/user/business/tasks/index',
        '/user/<id:\d+>/business/cv/?'=>'/user/business/cv/index',
        '/user/<id:\d+>/business/portfolio/?'=>'/user/business/portfolio/index',
        '/user/<id:\d+>/business/reviews/?'=>'/user/business/reviews/index',

        //-----
        '<controller:[\w-]+>/<id:\d+>'=>'<controller>/view',
        '<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>'=>'<controller>/<action>',
        '<controller:[\w-]+>/<action:[\w-]+>'=>'<controller>/<action>',
      ],
    ],

    'user' => [
      'identityClass' => 'common\models\User',
      'enableAutoLogin' => true,
    ],

    'log' => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets' => [
        [
          'class' => 'yii\log\FileTarget',
          'levels' => ['error', 'warning'],
        ],
      ],
    ],

    'errorHandler' => [
      'errorAction' => 'site/error',
    ],

  ],

  'params' => $params,
];

/*if (YII_DEBUG) {
  // configuration adjustments for 'dev' environment
  $config['bootstrap'][] = 'debug';
  $config['modules']['debug'] = [
    'class' => 'yii\debug\Module',
    'allowedIPs' => ['127.0.0.1', '::1']
  ];

  $config['bootstrap'][] = 'gii';
  $config['modules']['gii'] = [
    'class' => 'yii\gii\Module',
  ];
}*/
return $config;