<div class="t-background-img"><img src="/img/profile/1.jpg"></div>
<main class="t-page_profile">
  <h1 class="white">Баланс</h1>
  <div class="t-tabs">
    <select class="only-mobyle t-big-select js-big-select">
      <option value="/profile/business/subscription">Подписка</option>
      <option value="/profile/business/bonus" selected>Бонусы</option>
    </select>
    <a href="/profile/business/subscription">Подписка</a>
    <a href="/profile/business/bonus" class="active">Бонусы</a>
  </div>
  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>
    <div class="t-work-area_blur__box t-balance t-balance_bigMargin">
      <h3>На счёте</h3>
      <p class="t-balance__money t-balance__money_smallMargin">Бонусов TU — <?=\aiur\helpers\user\BalanceHelper::getCakes($user->id)?></p>
      <p class="t-balance__money">Очков — <?=\aiur\helpers\user\BalanceHelper::getPoints($user->id)?></p>
      <p class="t-balance__info">Бонусы TU выдаются заказчиком в качестве привилегий и могут быть использованы для покупки специальных опций.<br>
        Очки могут быть использованы для продления подписки</p>
    </div>
    <select class="only-mobyle t-big-select t-big-select_no-border js-big-select">
      <option selected value="/src/profile_developer/bonus.html">Все опции</option>
      <option value="/src/profile_developer/bonus.html?bonus=1">Активные</option>
      <option value="/src/profile_developer/bonus.html?bonus=2">Доступные</option>
      <option value="/src/profile_developer/bonus.html?bonus=3">Стандартные</option>
      <option value="/src/profile_developer/bonus.html?bonus=4">Особые</option>
    </select>
    <div class="t-tabs_task">
      <ul>
        <li><a href="/src/profile_developer/bonus.html" class="active">Все опции</a></li>
        <li><a href="/src/profile_developer/bonus.html?bonus=1">Активные</a></li>
        <li><a href="/src/profile_developer/bonus.html?bonus=2">Доступные</a></li>
        <li><a href="/src/profile_developer/bonus.html?bonus=3">Стандартные</a></li>
        <li><a href="/src/profile_developer/bonus.html?bonus=4">Особые</a></li>
      </ul>
    </div>
    <div class="t-bonus-list">
      <form class="t-work-area_blur__box t-work-area_blur__box_30 t-bonus">
        <p class="t-bonus__type">Стандартная</p>
        <p class="t-bonus__name">Название опции</p>
        <p>Краткое описание опции</p>
        <p class="t-bonus__name">Стоимость активации</p>
        <p>2 TU</p>
        <button class="t-btn js-activate">Активировать</button>
      </form>
      <form class="t-work-area_blur__box t-work-area_blur__box_30 t-bonus">
        <p class="t-bonus__type">Стандартная</p>
        <p class="t-bonus__name">Название опции</p>
        <p>Краткое описание опции</p>
        <p class="t-bonus__name">Стоимость активации</p>
        <p>3 TU</p>
        <button class="t-btn js-activate" disabled>Активировать</button>
      </form>
      <form class="t-work-area_blur__box t-work-area_blur__box_30 t-bonus">
        <p class="t-bonus__type">Особая</p>
        <p class="t-bonus__name">Название опции</p>
        <p>Краткое описание опции</p>
        <p class="t-bonus__name">Стоимость активации</p>
        <p>3 TU</p>
        <button class="t-btn js-activate" disabled>Активировать</button>
      </form>
      <form class="t-work-area_blur__box t-work-area_blur__box_30 t-bonus">
        <p class="t-bonus__type">Стандартная</p>
        <p class="t-bonus__name">Название опции</p>
        <p>Краткое описание опции</p>
        <p class="t-bonus__name">Стоимость активации</p>
        <p>2 TU</p>
        <p class="t-bonus__duration" >Приобретена навсегда</p>
      </form>
      <form class="t-work-area_blur__box t-work-area_blur__box_30 t-bonus">
        <p class="t-bonus__type">Стандартная</p>
        <p class="t-bonus__name">Название опции</p>
        <p>Краткое описание опции</p>
        <p class="t-bonus__name">Стоимость активации</p>
        <p>2 TU</p>
        <p class="t-bonus__duration" >Истекает 30.04.18</p>
      </form>
    </div>
  </div>
</main>