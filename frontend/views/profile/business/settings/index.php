<div class="t-background-img"><img src="/img/profile/1.jpg"></div>
<main class="t-page_profile">
  <h1 class="white">Профиль</h1>
  <div class="t-tabs">
      <select class="only-mobyle t-big-select js-big-select">
          <option value="/profile/business/personal">Личные данные</option>
          <option value="/profile/business/cv">Резюме</option>
          <option value="/profile/business/portfolio">Портфолио</option>
          <option value="/profile/business/reviews">Отзывы</option>
          <option selected value="/profile/business/settings">Настройки</option>
      </select>
    <a href="/profile/business/personal">Личные данные</a>
    <a href="/profile/business/cv">Резюме</a>
    <a href="/profile/business/portfolio">Портфолио</a>
    <a href="/profile/business/reviews">Отзывы</a>
    <a href="/profile/business/settings" class="active">Настройки</a>
  </div>
    <div class="t-loadLine">
        <p>Заполненность профиля —<span class="js-percent">84%</span></p>
        <div class="t-loadLine__box">
            <div data-width="84" data-percent="8" class="js-percent_line"></div>
            <div class="js-perspective"></div>
        </div>
    </div>
  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>

    <?if(empty($user->password_hash)):?>
      <?=\frontend\widgets\UserSettingsNewPassword::widget()?>
    <?else:?>
      <?=\frontend\widgets\UserSettingsResetPassword::widget()?>
    <?endif;?>

    <h2>Уведомления</h2>
    <div class="t-work-area_blur__box">
      <form id="js-noticeUpdate" action="/profile/business/settings/update" method="POST">
        <div class="t-input t-input_30"><span>На электронную почту</span>
          <input type="checkbox" name="fd[email_important]" id="notice_i" disabled class="checkbox">
          <label for="notice_i">Важные уведомления</label>
          <input type="checkbox" name="fd[email_new_article]" id="notice_n" <?if($settings->email_new_article):?>checked<?endif;?> class="checkbox">
          <label for="notice_n">При появлении новости или статьи</label>
          <input type="checkbox" name="fd[email_deny_task]" id="notice_e" <?if($settings->email_deny_task):?>checked<?endif;?> class="checkbox">
          <label for="notice_e">При подтверждении или отклонении задачи модератором</label>
        </div>
        <div class="t-input t-input_30"><span>На телефон</span>
          <input type="checkbox" name="fd[phone_important]" id="notice_ip" <?if($settings->phone_important):?>checked<?endif;?> class="checkbox">
          <label for="notice_ip">Важные уведомления</label>
          <input type="checkbox" name="fd[phone_new_article]" id="notice_np" <?if($settings->phone_new_article):?>checked<?endif;?> class="checkbox">
          <label for="notice_np">При появлении новости или статьи</label>
          <input type="checkbox" name="fd[phone_deny_task]" id="notice_ep" <?if($settings->phone_deny_task):?>checked<?endif;?> class="checkbox">
          <label for="notice_ep">При подтверждении или отклонении задачи модератором</label>
        </div>
      </form>
    </div>
  </div>
</main>