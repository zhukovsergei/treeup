<div class="t-background-img"><img src="/img/profile/1.jpg"></div>
<main class="t-page_profile">
    <h1 class="white">Профиль </h1>
    <div class="t-tabs">

        <select class="only-mobyle t-big-select js-big-select">
          <option value="/profile/business/personal">Личные данные</option>
          <option value="/profile/business/cv">Резюме</option>
          <option selected value="/profile/business/portfolio">Портфолио</option>
          <option value="/profile/business/reviews">Отзывы</option>
          <option value="/profile/business/settings">Настройки</option>
        </select>
        <a href="/profile/business/personal">Личные данные</a>
        <a href="/profile/business/cv">Резюме</a>
        <a href="/profile/business/portfolio" class="active">Портфолио</a>
        <a href="/profile/business/reviews">Отзывы</a>
        <a href="/profile/business/settings">Настройки</a>
    </div>
    <div class="t-loadLine">
        <p>Заполненность профиля —<span class="js-percent">84%</span></p>
        <div class="t-loadLine__box">
            <div data-width="84" data-percent="8" class="js-percent_line"></div>
            <div class="js-perspective"></div>
        </div>
    </div>
    <div class="t-work-area t-work-area_blur">
        <div class="t-work-area__background"></div>
        <div class="t-portfolio js-blocked">
            <?foreach($rows as $row):?>
              <form class="t-portfolio-box t-work-area_blur__box js-blocked" action="/profile/business/portfolio/update" method="post">
                  <input type="hidden" name="fd[deleted_blocks]" value="">
                  <input type="hidden" name="fd[deleted_files]" value="">
                <div class="t-input">
                  <span class="hidden js-hidden_box">Название проекта</span>
                  <input name="fd[name]" value="<?=$row->name?>" disabled class="validate t-portfolio-box__name js-showButton_input">
                </div>
                <div class="t-input">
                  <span class="hidden js-hidden_box">Описание</span>
                  <textarea name="fd[text]" disabled class="validate"><?=$row->text?></textarea>
                </div>

                <?foreach($row->blocks as $block):?>
                  <?=\frontend\widgets\PortfolioBlock::widget(['block' => $block])?>
                <?endforeach;?>

                <div class="t-addPortfolioBlock hidden js-hidden_box">
                  <h3>Добавить блок</h3>
                  <a href="" class="t-addPortfolioBlock__img js-addPortfolio" data-type="image">
                    <svg width="28" height="22" viewBox="0 0 28 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <path fill="#BDBDBD" d="M 21 3.33331L 24.5 3.33331C 24.5 3.33331 28 3.33331 28 6.66669L 28 18.6667C 28 18.6667 28 22 24.5 22L 3.5 22C 3.5 22 0 22 0 18.6667L 0 6.66669C 0 3.33331 3.5 3.33331 3.5 3.33331L 7 3.33331L 10.5 0L 17.5 0L 21 3.33331ZM 14 17C 16.7617 17 19 14.7614 19 12C 19 9.23859 16.7617 7 14 7C 11.2383 7 9 9.23859 9 12C 9 14.7614 11.2383 17 14 17Z"></path>
                    </svg>
                  </a>
                  <a href="" class="t-addPortfolioBlock__video js-addPortfolio" data-type="video">
                    <svg width="24" height="24" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <path fill="#BDBDBD" d="M 6.00095 1.60982C 11.7389 -1.70394 19.0771 0.26236 22.3902 6.00092C 25.7043 11.7391 23.737 19.0766 17.9985 22.3905C 12.2604 25.7039 4.92246 23.7373 1.60984 17.9988C -1.70398 12.2604 0.262403 4.92252 6.00095 1.60982ZM 16.3191 11.5123L 10.0367 7.84817C 9.46416 7.51392 9.00238 7.78114 9.00552 8.44338L 9.03775 15.7163C 9.04059 16.3784 9.50804 16.6479 10.0822 16.3163L 16.3167 12.7168C 16.8903 12.3857 16.8915 11.8466 16.3191 11.5123Z"></path>
                    </svg>
                  </a>
                  <a href="" class="t-addPortfolioBlock__audio js-addPortfolio" data-type="audio">
                    <svg width="22" height="24" viewBox="0 0 22 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <path id="path_block_audio'+count+'" fill-rule="evenodd" d="M 14.2191 0C 14.5703 0 14.8732 0.125029 15.1294 0.375086C 15.3842 0.625057 15.5122 0.921301 15.5122 1.26338L 15.5122 22.7371C 15.5122 23.079 15.3842 23.3752 15.128 23.6253C 14.8732 23.8748 14.5703 24 14.2191 24C 13.8693 24 13.5663 23.8748 13.3102 23.6253L 6.58416 17.0528L 1.29315 17.0528C 0.943349 17.0528 0.640376 16.9276 0.384225 16.6775C 0.128075 16.4276 0 16.1315 0 15.7895L 0 8.21062C 0 7.86854 0.128075 7.57264 0.384225 7.32232C 0.638998 7.07244 0.943349 6.94741 1.29315 6.94741L 6.58416 6.94741L 13.3102 0.375086C 13.5663 0.125029 13.8693 0 14.2191 0ZM 19.8117 7.25606C 19.2182 6.75052 18.3285 6.82186 17.8231 7.41547C 17.3191 8.00908 17.3907 8.90013 17.9828 9.40568C 18.7169 10.0311 19.1796 10.9601 19.1796 12C 19.1796 13.0399 18.7169 13.9688 17.9828 14.5943C 17.3907 15.0999 17.3191 15.9909 17.8231 16.5844C 18.3285 17.1781 19.2182 17.2495 19.8117 16.7439C 21.1489 15.604 22 13.9006 22 12C 22 10.0994 21.1489 8.39597 19.8117 7.25606Z"></path>
                    </svg>
                  </a>
                </div>
                <div class="t-btnBox">
                  <a class="t-btn js-edit js-hidden_box" href="#edit" data-blocked="blocked">Изменить</a>
                  <a href="#del" class="t-del js-del js-hidden_box" data-type="portfolio">
                    <svg width="10" height="15" viewBox="0 0 10 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <path fill="#CCCCCC" d="M 5.0001 0C 5.55283 0 6.0001 0.447693 6.0001 1L 6.0001 2L 9.0001 2C 9.55283 2 10.0001 2.44769 10.0001 3C 10.0001 3.55231 9.55283 4 9.0001 4L 5.0001 4L 1.0001 4C 0.447362 4 9.62968e-05 3.55231 9.62968e-05 3C 9.62968e-05 2.44769 0.447362 2 1.0001 2L 4.0001 2L 4.0001 1C 4.0001 0.447693 4.44736 0 5.0001 0ZM 0.250096 5.3382C 0.440526 5.12317 0.712987 5 1.0001 5L 9.0001 5C 9.28721 5 9.55967 5.12317 9.7501 5.3382C 9.93955 5.55322 10.0284 5.83942 9.99228 6.12402L 8.99228 14.124C 8.92978 14.6245 8.504 15 8.0001 15L 2.0001 15C 1.49619 15 1.07041 14.6245 1.00791 14.124L 0.0079088 6.12402C -0.028224 5.83942 0.0606432 5.55322 0.250096 5.3382ZM 2.13291 7L 2.88291 13L 7.11728 13L 7.86728 7L 2.13291 7Z"></path>
                    </svg>
                  </a>
                  <div class="js-hidden_box hidden">
                    <input type="hidden" name="id" value="<?=$row->id?>">
                    <button class="t-btn t-btn_120 js-add_info" disabled data-blocked="blocked">Сохранить</button>
                    <a href="#cancel" class="t-btn t-btn_border js-clear-form t-btn_120" data-blocked="blocked">Отмена</a>
                  </div>
                </div>
              </form>
            <?endforeach;?>
            <div class="t-addBox t-addBox_full"><a href="#add" data-type="portfolio" data-blocked="blocked" class="t-btn js-addProject">Добавить проект</a></div>
        </div>
    </div>
</main>


<div class="t-templates">
    <form class="t-portfolio-box t-work-area_blur__box js-blocked" data-type="portfolio" data-type="project" action="/profile/business/portfolio/create" method="post">
        <input type="hidden" name="fd[deleted_blocks]" value="">
        <input type="hidden" name="fd[deleted_files]" value="">
        <div class="t-input"><span class="js-hidden_box">Название проекта</span>
            <input name="fd[name]" value="" class="validate t-portfolio-box__name js-showButton_input">
        </div>
        <div class="t-input"><span class="js-hidden_box">Описание</span>
            <textarea name="fd[text]" class="validate"></textarea>
        </div>
        <div class="t-addPortfolioBlock js-hidden_box">
            <h3>Добавить блок</h3>
            <a href="" class="t-addPortfolioBlock__img js-addPortfolio" data-type="image">
                <svg width="28" height="22" viewBox="0 0 28 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <path fill="#BDBDBD" d="M 21 3.33331L 24.5 3.33331C 24.5 3.33331 28 3.33331 28 6.66669L 28 18.6667C 28 18.6667 28 22 24.5 22L 3.5 22C 3.5 22 0 22 0 18.6667L 0 6.66669C 0 3.33331 3.5 3.33331 3.5 3.33331L 7 3.33331L 10.5 0L 17.5 0L 21 3.33331ZM 14 17C 16.7617 17 19 14.7614 19 12C 19 9.23859 16.7617 7 14 7C 11.2383 7 9 9.23859 9 12C 9 14.7614 11.2383 17 14 17Z"></path>
                </svg>
            </a>
            <a href="" class="t-addPortfolioBlock__video js-addPortfolio" data-type="video">
                <svg width="24" height="24" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <path fill="#BDBDBD" d="M 6.00095 1.60982C 11.7389 -1.70394 19.0771 0.26236 22.3902 6.00092C 25.7043 11.7391 23.737 19.0766 17.9985 22.3905C 12.2604 25.7039 4.92246 23.7373 1.60984 17.9988C -1.70398 12.2604 0.262403 4.92252 6.00095 1.60982ZM 16.3191 11.5123L 10.0367 7.84817C 9.46416 7.51392 9.00238 7.78114 9.00552 8.44338L 9.03775 15.7163C 9.04059 16.3784 9.50804 16.6479 10.0822 16.3163L 16.3167 12.7168C 16.8903 12.3857 16.8915 11.8466 16.3191 11.5123Z"></path>
                </svg>
            </a>
            <a href="" class="t-addPortfolioBlock__audio js-addPortfolio" data-type="audio">
                <svg width="22" height="24" viewBox="0 0 22 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <path id="path_block_audio'+count+'" fill-rule="evenodd" d="M 14.2191 0C 14.5703 0 14.8732 0.125029 15.1294 0.375086C 15.3842 0.625057 15.5122 0.921301 15.5122 1.26338L 15.5122 22.7371C 15.5122 23.079 15.3842 23.3752 15.128 23.6253C 14.8732 23.8748 14.5703 24 14.2191 24C 13.8693 24 13.5663 23.8748 13.3102 23.6253L 6.58416 17.0528L 1.29315 17.0528C 0.943349 17.0528 0.640376 16.9276 0.384225 16.6775C 0.128075 16.4276 0 16.1315 0 15.7895L 0 8.21062C 0 7.86854 0.128075 7.57264 0.384225 7.32232C 0.638998 7.07244 0.943349 6.94741 1.29315 6.94741L 6.58416 6.94741L 13.3102 0.375086C 13.5663 0.125029 13.8693 0 14.2191 0ZM 19.8117 7.25606C 19.2182 6.75052 18.3285 6.82186 17.8231 7.41547C 17.3191 8.00908 17.3907 8.90013 17.9828 9.40568C 18.7169 10.0311 19.1796 10.9601 19.1796 12C 19.1796 13.0399 18.7169 13.9688 17.9828 14.5943C 17.3907 15.0999 17.3191 15.9909 17.8231 16.5844C 18.3285 17.1781 19.2182 17.2495 19.8117 16.7439C 21.1489 15.604 22 13.9006 22 12C 22 10.0994 21.1489 8.39597 19.8117 7.25606Z"></path>
                </svg>
            </a>
        </div>
        <div class="t-btnBox">
            <a class="t-btn js-edit js-hidden_box hidden" href="#edit" data-blocked="blocked">Изменить</a>
            <a href="#del" class="t-del js-del js-hidden_box hidden" data-type="portfolio">
                <svg width="10" height="15" viewBox="0 0 10 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <path fill="#CCCCCC" d="M 5.0001 0C 5.55283 0 6.0001 0.447693 6.0001 1L 6.0001 2L 9.0001 2C 9.55283 2 10.0001 2.44769 10.0001 3C 10.0001 3.55231 9.55283 4 9.0001 4L 5.0001 4L 1.0001 4C 0.447362 4 9.62968e-05 3.55231 9.62968e-05 3C 9.62968e-05 2.44769 0.447362 2 1.0001 2L 4.0001 2L 4.0001 1C 4.0001 0.447693 4.44736 0 5.0001 0ZM 0.250096 5.3382C 0.440526 5.12317 0.712987 5 1.0001 5L 9.0001 5C 9.28721 5 9.55967 5.12317 9.7501 5.3382C 9.93955 5.55322 10.0284 5.83942 9.99228 6.12402L 8.99228 14.124C 8.92978 14.6245 8.504 15 8.0001 15L 2.0001 15C 1.49619 15 1.07041 14.6245 1.00791 14.124L 0.0079088 6.12402C -0.028224 5.83942 0.0606432 5.55322 0.250096 5.3382ZM 2.13291 7L 2.88291 13L 7.11728 13L 7.86728 7L 2.13291 7Z"></path>
                </svg>
            </a>
            <div class="js-hidden_box">
                <input type="hidden" name="id" value="">
                <button class="t-btn t-btn_120 js-add_info" disabled data-blocked="blocked">Сохранить</button>
                <a href="#cancel" class="t-btn t-btn_border js-clear-form t-btn_120" data-blocked="blocked" data-del="on">Отмена</a>
            </div>
        </div>
    </form>

    <div class="t-portfolio-box__block" data-type="image">
        <input type="hidden" name="fd[block_id]" value="">
        <div class="t-input">
            <span class="js-hidden_box">Название блока</span>
            <input name="fd[block_name]" value="" class="validate t-portfolio-box__name_block">
        </div>
        <div class="t-input">
            <span class="js-hidden_box">Описание блока</span>
            <input name="fd[block_text]" value="" class="validate">
        </div>
        <div class="t-input_file-list js-file-list_img" >
            <div class="t-input_file">
                <label class="js-hidden_box t-fileLoad t-fileLoad_add js-load_img">
                    <input type="file" name="img" class="t-addFile">
                </label>
            </div>
        </div>
        <a href="#del" class="t-btn t-btn_border js-delBlock js-hidden_box">Удалить блок</a>
    </div>
    <div class="t-portfolio-box__block" data-type="video">
        <input type="hidden" name="fd[block_id]" value="">
        <div class="t-input">
            <span class="js-hidden_box">Название блока</span>
            <input name="fd[block_name]" value="" class="validate t-portfolio-box__name_block">
        </div>
        <div class="t-input">
            <span class="js-hidden_box">Описание блока</span>
            <input name="fd[block_text]" value="" class="validate">
        </div>
        <div class="t-input_file-list">
            <div class="t-input js-hidden_box">
                <a class="t-btn_img js-add_video" href="#addLink">Добавить ссылку на видео</a>
            </div>
        </div>
        <a href="#del" class="t-btn t-btn_border js-delBlock js-hidden_box">Удалить блок</a>
    </div>
    <div class="t-portfolio-box__block" data-type="audio">
        <input type="hidden" name="fd[block_id]" value="">
        <div class="t-input">
            <span class="js-hidden_box">Название блока</span>
            <input name="fd[block_name]" value="" class="validate t-portfolio-box__name_block">
        </div>
        <div class="t-input">
            <span class="js-hidden_box">Описание блока</span>
            <input name="fd[block_text]" value="" class="validate">
        </div>
        <div class="t-input_file-list">
            <div class="t-load-file t-load-file_audio">
                <ul class="js-file-list">
                    <li>
                        <div class="t-load-file__controls js-hidden_box">
                            <label class="js-load_audio t-btn_img">Добавить аудио-файл
                                <input type="file" name="audio">
                            </label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <a href="#del" class="t-btn t-btn_border js-delBlock js-hidden_box">Удалить блок</a>
    </div>

</div>