<div class="t-background-img"><img src="/img/profile/1.jpg"></div>
<main class="t-page_profile">
  <h1 class="white">Профиль</h1>
  <div class="t-tabs">
      <select class="only-mobyle t-big-select js-big-select">
          <option value="/profile/business/personal">Личные данные</option>
          <option value="/profile/business/cv">Резюме</option>
          <option value="/profile/business/portfolio">Портфолио</option>
          <option selected value="/profile/business/reviews">Отзывы</option>
          <option value="/profile/business/settings">Настройки</option>
      </select>
    <a href="/profile/business/personal">Личные данные</a>
    <a href="/profile/business/cv">Резюме</a>
    <a href="/profile/business/portfolio">Портфолио</a>
    <a href="/profile/business/reviews" class="active">Отзывы</a>
    <a href="/profile/business/settings">Настройки</a>
  </div>
    <div class="t-loadLine">
        <p>Заполненность профиля —<span class="js-percent">84%</span></p>
        <div class="t-loadLine__box">
            <div data-width="84" data-percent="8" class="js-percent_line"></div>
            <div class="js-perspective"></div>
        </div>
    </div>
  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>
      <select class="only-mobyle t-big-select t-big-select_no-border js-big-select">
          <option selected value="/profile/business/reviews">Все отзывы</option>
          <option value="/profile/business/reviews?reviews=1">Отзывы обо мне</option>
          <option value="/profile/business/reviews?reviews=2">Отзывы от меня</option>
      </select>
      <div class="t-tabs_task">
          <ul>
              <li><a href="/profile/business/reviews" class="active">Все отзывы</a></li>
              <li><a href="/profile/business/reviews?reviews=1">Отзывы обо мне</a></li>
              <li><a href="/profile/business/reviews?reviews=2">Отзывы от меня</a></li>
          </ul>
      </div>
    <div class="t-reviews-list">
      <?foreach($reviews as $review):?>
        <div class="t-review">
          <div class="t-review__personal-info">
            <div class="t-review__avatar"><img src="/img/face.jpg"></div>
            <div class="t-review__name"><span>Я</span><?=$review->date_add?></div>
          </div>
          <div class="t-work-area_blur__box">
            <div class="t-review__type">О задаче</div>
            <div class="t-review__task">Создать сайт булочной</div>
            <div class="t-review__text"><?=$review->text?></div>
          </div>
        </div>
      <?endforeach;?>
    </div>
  </div>
</main>