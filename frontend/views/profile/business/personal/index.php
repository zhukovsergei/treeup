<div class="t-background-img"><img src="/img/profile/1.jpg"></div>
<main class="t-page_profile">
  <h1 class="white">Профиль</h1>
  <div class="t-tabs">
      <select class="only-mobyle t-big-select js-big-select">
          <option selected value="/profile/business/personal">Личные данные</option>
          <option value="/profile/business/cv">Резюме</option>
          <option value="/profile/business/portfolio">Портфолио</option>
          <option value="/profile/business/reviews">Отзывы</option>
          <option value="/profile/business/settings">Настройки</option>
      </select>
    <a href="/profile/business/personal" class="active">Личные данные</a>
    <a href="/profile/business/cv">Резюме</a>
    <a href="/profile/business/portfolio">Портфолио</a>
    <a href="/profile/business/reviews">Отзывы</a>
    <a href="/profile/business/settings">Настройки</a>
  </div>
    <div class="t-loadLine">
        <p>Заполненность профиля —<span class="js-percent">84%</span></p>
        <div class="t-loadLine__box">
            <div data-width="84" data-percent="8" class="js-percent_line"></div>
            <div class="js-perspective"></div>
        </div>
    </div>
  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>
    <h2>Личные данные</h2>
    <div class="t-work-area_blur__box">
      <form id="personal_info" action="/profile/business/personal/save-personal" method="POST" class="js_ajaxForm">
        <div class="t-avatar">
          <img src="<?=\aiur\helpers\user\LogoHelper::getUserLogoUrl($user)?>" data-input="setAvatar">
          <div class="hidden js-hidden_box">
            <p>Аватар профиля</p>
            <label for="setAvatar"><span class="t-btn">Выбрать файл</span>
              <input id="setAvatar" type="file" name="file" data-value="<?=\aiur\helpers\user\LogoHelper::getUserLogoUrl($user)?>">
            </label><a href="#del" class="t-btn t-btn_border js-delAvatar">Удалить</a>
          </div>
        </div>
        <div class="t-input-box">
          <div class="t-input t-input_30"><span>Имя</span>
            <input type="text" name="fd[name]" value="<?=$user->profile->name?>" disabled class="validate">
          </div>
          <div class="t-input t-input_30"><span>Фамилия</span>
            <input type="text" name="fd[surname]" value="<?=$user->profile->surname?>" disabled class="validate">
          </div>
          <div class="t-input t-input_30"><span>Отчество</span>
            <input type="text" name="fd[patronymic]" value="<?=$user->profile->patronymic?>" disabled>
          </div>
          <div class="t-input t-input_30"><span>Дата рождения</span>
            <input type="hidden" name="fd[birthday]" value="<?=$user->profile->birthday?>">
            <div class="js-datepicker t-input_date disabled js-disabled" data-max="true"></div>
          </div>
          <div class="t-input t-input_30"><span>Город проживания</span>
            <select name="fd[town_id]" disabled class="js-selectSearch">
              <?foreach($towns as $town):?>
                <option <?if($user->profile->town_id === $town->id):?>selected<?endif;?> value="<?=$town->id?>"><?=$town->name?></option>
              <?endforeach;?>
            </select>
          </div>
          <div class="t-input t-input_30"><span>Электронная почта</span>
            <input type="email" name="fd[email]" value="<?=$user->profile->email?>" disabled class="validate">
          </div>
          <div class="t-input t-input_30"><span>Номер телефона</span>
            <input type="text" name="fd[phone]" value="<?=$user->profile->phone?>" placeholder="+7" disabled class="validate">
          </div>
        </div>
        <div class="t-btnBox"><a href="#edit" class="t-btn js-edit js-hidden_box">Изменить</a>
          <div class="hidden js-hidden_box">
              <input type="hidden" name="fd[remove_image]" value="0">
            <button class="t-btn">Сохранить</button><a href="#clearForm" class="t-btn t-btn_border js-clear-form">Отменить редактирование</a>
          </div>
        </div>
      </form>
    </div>
    <h2>Ссылки на другие профили</h2>
    <div class="t-work-area_blur__box">
      <form id="social_info" action="/profile/business/personal/save-social" method="POST" class="js_ajaxForm">
        <div class="t-input-box">
          <div class="t-input t-input_30"><span>Skype</span>
            <input type="text" name="fd[skype]" value="<?=$user->profile->skype?>" disabled>
          </div>
          <div class="t-input t-input_30"><span>Telegram</span>
            <input type="text" name="fd[telegram]" value="<?=$user->profile->telegram?>" disabled>
          </div>
          <div class="t-input t-input_30"><span>ICQ</span>
            <input type="text" name="fd[icq]" value="<?=$user->profile->icq?>" disabled>
          </div>
          <div class="t-input t-input_30"><span>ВКонтакте</span>
            <input type="text" name="fd[vk]" value="<?=$user->profile->vk?>" disabled>
          </div>
          <div class="t-input t-input_30"><span>Facebook</span>
            <input type="text" name="fd[fb]" value="<?=$user->profile->fb?>" disabled>
          </div>
          <div class="t-input t-input_30"><span>Google+</span>
            <input type="text" name="fd[google]" value="<?=$user->profile->google?>" disabled>
          </div>
        </div>
        <div class="t-btnBox"><a href="#edit" class="t-btn js-edit js-hidden_box">Изменить</a>
          <div class="hidden js-hidden_box">
            <button class="t-btn">Сохранить</button><a href="#clearForm" class="t-btn t-btn_border js-clear-form">Отменить редактирование</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</main>