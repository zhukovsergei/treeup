<div class="t-background-img"><img src="/img/profile/1.jpg"></div>
<main class="t-page_profile">
  <h1 class="white">Баланс</h1>
  <div class="t-tabs">
    <select class="only-mobyle t-big-select js-big-select">
      <option value="/profile/business/subscription" selected>Подписка</option>
      <option value="/profile/business/bonus">Бонусы</option>
    </select>
    <a href="/profile/business/subscription" class="active">Подписка</a>
    <a href="/profile/business/bonus">Бонусы</a>
  </div>
  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>
    <div class="t-work-area_blur__box t-balance">
      <?if($user->isSubscriber()):?>
        <h3>Подписка не активна</h3>
        <p>Истекает <?=$user->subscription_till?></p>
      <?else:?>
        <h3>Подписка не активна</h3>
      <?endif;?>

    </div>
    <div class="t-work-area_blur__box">
      <form id="pay_subscription" action="" method="POST">
        <h2 class="black">Активация подписки</h2>
        <div class="t-input">
          <span>Способ оплаты</span>
          <input type="radio" name="fd[pay]" id="pay_bonus_subscription" class="radio" checked>
          <label for="pay_bonus_subscription">Очки <span>— доступно <?=\aiur\helpers\user\BalanceHelper::getPoints($user->id)?></span></label>
          <input type="radio" name="fd[pay]" id="pay_card_subscription" class="radio">
          <label for="pay_card_subscription">Оплата банковской картой</label>
          <input type="radio" name="fd[pay]" id="pay_yandex_subscription" class="radio">
          <label for="pay_yandex_subscription">Яндекс.Деньги</label>
        </div>
        <div class="t-input">
          <span>Продолжительность</span>
          <p class="t-balance__money js-durationEnd"></p>
          <ul class="t-balance__duration">
            <li>1</li>
            <li>2</li>
            <li>3</li>
            <li>4</li>
            <li>5</li>
            <li>6</li>
            <li>7</li>
            <li>8</li>
            <li>9</li>
            <li>10</li>
            <li>11</li>
            <li>12</li>
          </ul>
          <input type="hidden" name="fd[duration]" value="1">
          <div id="js-sliderDuration" class="t-balance__sliderDuration"></div>
        </div>
        <div class="t-input">
          <span>Стоимость</span>
          <p>20 000 очков</p>
        </div>
        <div class="t-btnBox">
          <button class="t-btn">Оплатить</button>
        </div>
      </form>
    </div>
  </div>
</main>