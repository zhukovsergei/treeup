<?php

use aiur\helpers\user\BalanceAffordOfServicesHelper;
use aiur\helpers\user\BalanceHelper;
?>
<div class="t-background-img"><img src="/img/profile/1.jpg"></div>
<main class="t-page_profile">
  <h1 class="white">Баланс</h1>
  <div class="t-tabs">
      <select class="only-mobyle t-big-select js-big-select">
          <option value="/profile/customer/balance" selected>Счёт</option>
          <option value="/profile/customer/balance-log">История</option>
          <option value="/profile/customer/bonus">Бонусы</option>
      </select>
    <a href="/profile/customer/balance" class="active">Счёт</a>
    <a href="/profile/customer/balance-log">История</a>
    <a href="/profile/customer/bonus">Бонусы</a>
  </div>
  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>
    <div class="t-work-area_blur__box t-balance">
      <h3>На счёте</h3>
      <p class="t-balance__money"><?=BalanceHelper::getMoneyFormatted($user->id)?> Р<span>= <?=BalanceAffordOfServicesHelper::getCountTasks($user->id)?> задачи или <?=BalanceAffordOfServicesHelper::getCountCakes($user->id)?> TU</span></p>
      <p class="t-balance__info">
        Ваши возможности напрямую зависят от баланса вашего счёта. При пополнении счёта, рассчитывайте, что стоимость публикации одной задачи равна <?=\Yii::$app->settings->get('price.task')?> Р. и установки 1 TU в качестве привилегии равна <?=\Yii::$app->settings->get('price.tu')?> Р.
      </p>
    </div>
    <div class="t-work-area_blur__box">
      <form id="pay_balance" action="" method="POST">
        <div class="t-input">
          <span>Способ оплаты</span>
          <input type="radio" name="fd[pay]" id="pay_card_balance" class="radio" checked>
          <label for="pay_card_balance">Оплата банковской картой</label>
          <input type="radio" name="fd[pay]" id="pay_yandex_balance" class="radio">
          <label for="pay_yandex_balance">Яндекс.Деньги</label>
        </div>
        <div class="t-input t-input_30">
          <span>Сумма пополнения</span>
          <input type="text" name="fd[summ]" class="validate js-only-number">
        </div>
        <div class="t-btnBox">
          <button class="t-btn">Перейти к оплате</button>
        </div>
      </form>
    </div>
  </div>
</main>