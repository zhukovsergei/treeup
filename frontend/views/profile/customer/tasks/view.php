<div class="t-background-img"><img src="/img/registration.jpg"></div>
<main>
    <h1 class="white">Задача</h1>
    <div class="t-tabs">
        <select class="only-mobyle t-big-select js-big-select">
            <option selected value="/profile/customer/tasks/<?= $row->id ?>/view">Общее</option>
            <option value="/profile/customer/tasks/<?= $row->id ?>/log">Обновления</option>
            <? if ($row->status == 'finished' || $row->status == 'published'): ?>
                <option value="/profile/customer/tasks/<?= $row->id ?>/participants">Участники</option>
            <? endif; ?>
        </select>
        <a href="/profile/customer/tasks/<?= $row->id ?>/view" class="active">Общее</a>
        <a href="/profile/customer/tasks/<?= $row->id ?>/log">Обновления</a>
        <? if ($row->status == 'finished' || $row->status == 'published'): ?>
            <a href="/profile/customer/tasks/<?= $row->id ?>/participants">Участники</a>
        <? endif; ?>
    </div>
    <div class="t-work-area t-work-area_blur">
        <div class="t-work-area__background"></div>
        <div class="t-task-list">
            <div class="t-task-custumer t-task-custumer_start" data-id="<?= $row->id ?>">
                <div class="t-task-custumer__description">
                    <h2>Описание</h2>
                    <div class="box">
                        <h2><?= $row->name ?></h2>
                        <h3><?= $row->project->name ?? '' ?></h3>
                        <div class="date">Завершится <?= $row->date_end ?></div>
                        <div class="text">
                            <?= $row->text ?>
                        </div>
                        <? if ($creatorFiles): ?>
                            <div class="t-load-file">
                                <h3>Материалы</h3>
                                <ul>
                                    <? foreach ($creatorFiles as $file): ?>
                                        <li>
                                            <div class="t-load-file__item"><a target="_blank"
                                                                              href="<?= $file->getUploadedFileUrl('name') ?>"
                                                                              class="name"><img
                                                            src="/img/file.png"><?= $file->name ?></a></div>
                                        </li>
                                    <? endforeach; ?>
                                </ul>
                            </div>
                        <? endif; ?>
                    </div>
                </div>
                <div class="t-task-custumer__info t-task-custumer__info_long">
                    <h2>Действия</h2>
                    <?= \frontend\widgets\MyTaskViewCustomer::widget(['task' => $row]) ?>
                    <h2>Информация</h2>
                    <ul class="t-task-custumer__info_long__box list">
                        <li>
                            <h3>Тип задачи</h3>
                            <p><?= \common\models\Task::TYPE[$row->type_id] ?? '—' ?></p>
                        </li>
                        <li>
                            <h3>Специализация</h3>
                            <p><?= $row->specialization->name ?? '—' ?></p>
                        </li>
                        <li>
                            <h3>Навыки</h3>
                            <p>
                                <? if ($row->skills): ?>
                                    <?= implode(', ', \yii\helpers\ArrayHelper::getColumn($row->skills, 'name')) ?>
                                <? else: ?>
                                    —
                                <? endif; ?>
                            </p>
                        </li>
                        <li>
                            <h3>Регион</h3>
                            <p><?= $row->town->name ?? '—' ?></p>
                        </li>
                        <li>
                            <h3>Отрасль</h3>
                            <p><?= $row->industry->name ?? '—' ?></p>
                        </li>
                        <li>
                            <h3>Уровень</h3>
                            <p><?= \common\models\Task::COMPLEXITY[$row->complexity] ?? '—' ?></p>
                        </li>
                        <li>
                            <h3>Победителю</h3>
                            <p><?= $row->points_win ?? 0 ?></p>
                        </li>
                        <li>
                            <h3>Участнику</h3>
                            <p><?= $row->points_participant ?? 0 ?></p>
                        </li>
                        <li>
                            <h3>Привилегия</h3>
                            <p><?= \common\models\Task::PRIVILEGE[$row->privilege] ?? '—' ?></p>
                        </li>
                        <?php if ($row->privilege == \common\models\Task::PRIVILEGE_DB['Приз']): ?>
                            <li>
                                <h3>Описание привилегии</h3>
                                <p><?= $row->privilege_description ?></p>
                            </li>
                        <?php elseif ($row->privilege == \common\models\Task::PRIVILEGE_DB['TU']): ?>
                            <li>
                                <h3>Количество TU</h3>
                                <p><?= $row->tu_count ?></p>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</main>