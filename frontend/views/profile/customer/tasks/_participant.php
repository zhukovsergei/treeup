<div class="t-participant__collumn_right">
    <a class="t-participantBack only-mobyle js-participantBack">Назад к списку</a>
    <div class="t-participant__active">
        <div class="t-participant__item t-task-item t-task-item_white js-participant__open">
            <div class="t-task-item__img">
                <img src="<?= \aiur\helpers\user\LogoHelper::getUserLogoUrl($participant) ?>">
            </div>
            <div class="t-task-item__info">
                <h4 class="name">
                    <?= \aiur\helpers\user\UserHelper::getUserName($participant) ?>
                </h4>
                <div class="rate">Рейтинг 5.0</div>
                <ul class="base-info">
                    <li><img src="/img/file.png"> 500+</li>
                    <li class="grey">25.08.18</li>
                </ul>
            </div>
            <div class="clearfix"></div>
            <div class="buttons">
                <? if ($row->isTeam()): ?>
                    <? if (!\aiur\helpers\TaskHelper::isUserPerformer($row->id, $participant->id) && $row->status !== 'finished'): ?>
                        <a href="#performer" class="t-btn js-become-performer" data-id="<?= $participant->id ?>"
                           data-task="<?= $row->id ?>">Выбрать исполнителем</a>
                    <? elseif($row->status !== 'finished'): ?>
                        <a href="#no_win" class="t-btn t-btn_border js-del-performer" data-id="<?= $participant->id ?>"
                           data-task="<?= $row->id ?>">Убрать исполнителя</a>
                    <? endif; ?>
                <? else: ?>
                    <? if (!\aiur\helpers\TaskHelper::isUserWinner($row->id, $participant->id) && $row->status !== 'finished'): ?>
                        <a href="#winner" class="t-btn js-become-winner" data-id="<?= $participant->id ?>"
                           data-task="<?= $row->id ?>">Выбрать победителем</a>
                    <? elseif($row->status !== 'finished'): ?>
                        <a href="#no_win" class="t-btn t-btn_border js-del-winner" data-id="<?= $participant->id ?>"
                           data-task="<?= $row->id ?>">Убрать победителя</a>
                    <? endif; ?>
                <? endif; ?>
                <a href="/src/dialog/index.html" class="t-btn">Напсиать сообщение</a><!--Кнопка должна быть видна только если данный исполнитель выбран победителем в данной задаче.-->
                <a href="/user/<?= $participant->id ?>/business/personal" class="t-btn t-btn_border">Открыть профиль</a>
                <? if (!\aiur\helpers\TaskHelper::isUserPinned($row->id, $participant->id)): ?>
                    <a href="#lock" class="t-btn t-btn_border js-lock">Закрепить</a>
                <? else: ?>
                    <a href="#unlock" class="t-btn t-btn_border js-lock active">Открепить</a>
                <? endif; ?>

            </div>
        </div>
    </div>

    <? if ($indexedFiles): ?>
        <h2>Файлы</h2>
        <div class="t-fileBox js-fileBox">
            <div class="scrollbar-inner">
                <ul>
                    <? foreach ($indexedFiles as $formatted_date => $collapseDate): ?>
                        <li>
                            <p class="date"><?= $formatted_date; ?></p>
                            <? foreach ($collapseDate as $file): ?>
                                <div class="t-load-file__item">
                                    <a href="<?= $file->getUploadedFileUrl('name') ?>" download class="name">
                                        <img src="/img/file.png"><?= $file->name ?></a>
                                </div>
                            <? endforeach; ?>

                        </li>
                    <? endforeach; ?>
                </ul>
            </div>
        </div>
    <? endif; ?>

</div>