<div class="t-background-img"><img src="/img/registration.jpg"></div>
<main>
  <h1 class="white">Задача</h1>
  <div class="t-tabs">
      <select class="only-mobyle t-big-select js-big-select">
          <option value="/profile/customer/tasks/<?=$row->id?>/view">Общее</option>
          <option selected value="/profile/customer/tasks/<?=$row->id?>/log">Обновления</option>
          <option value="/profile/customer/tasks/<?=$row->id?>/participants">Участники</option>
      </select>
    <a href="/profile/customer/tasks/<?=$row->id?>/view">Общее</a>
    <a href="/profile/customer/tasks/<?=$row->id?>/log" class="active">Обновления</a>
    <?if($row->status == 'finished' || $row->status == 'published'):?>
      <a href="/profile/customer/tasks/<?=$row->id?>/participants">Участники</a>
    <?endif;?>
  </div>
  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>
    <div class="t-work-area_blur__box">
      <?if(empty($row->notifies)):?>
      <p>Нет событий</p>
      <?endif;?>
      <?foreach($row->notifies as $notify):?>
        <div class="t-updates">
          <h3><?=$notify->name?></h3>
<!--          <h4>Создание сайта</h4>-->
          <p><?=$notify->date_add?></p>
        </div>
      <?endforeach;?>
    </div>
  </div>
<!--  <div class="t-show-more"><a href="#more" class="t-btn">Показать ещё</a></div>-->
</main>