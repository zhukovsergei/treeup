<div class="t-background-img"><img src="/img/registration.jpg"></div>
<main>
  <h1 class="white">Задача</h1>
  <div class="t-tabs">
    <select class="only-mobyle t-big-select js-big-select">
      <option value="/profile/customer/tasks/<?=$row->id?>/view">Общее</option>
      <option value="/profile/customer/tasks/<?=$row->id?>/log">Обновления</option>
      <option selected value="/profile/customer/tasks/<?=$row->id?>/participants">Участники</option>
    </select>
    <a href="/profile/customer/tasks/<?=$row->id?>/view">Общее</a>
    <a href="/profile/customer/tasks/<?=$row->id?>/log">Обновления</a>
    <a href="/profile/customer/tasks/<?=$row->id?>/participants" class="active">Участники</a>
  </div>
  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>

    <div class="t-participant">
      <?if(!count($row->participants)):?>
        <div class="t-participant__collumn_full">
          <?if($row->status == 'finished'){?>
            <div class="t-defaultMsg">
              <h2>Задача завершена</h2>
              <p>Победителя нет</p>
            </div>
          <?} else{?>
            <?=\frontend\widgets\TaskViewParticipantActions::widget(['task' => $row])?>
            <div class="t-defaultMsg">
              <h2>Сейчас здесь пусто</h2>
              <p>В вашей задаче пока нет участников, позже здесь будет отображаться список участников, загруженные файлы и сообщения</p>
            </div>
          <?}?>
        </div>
      <?else:?>
        <div class="t-participant__collumn_left">

          <?=\frontend\widgets\TaskViewParticipantActions::widget(['task' => $row])?>


          <div class="t-participant__list <?if(!$pinnedUsers):?>hidden<?endif;?>" data-type="lock">
              <h2>Закреплённые</h2>
              <?foreach($pinnedUsers as $pinnedUser):?>
                <div class="t-participant__item t-task-item t-task-item_white js-participant__item" data-id="<?=$pinnedUser->id?>" data-task="<?=$row->id?>">
                  <div class="t-task-item__img">
                    <img src="<?=\aiur\helpers\user\LogoHelper::getUserLogoUrl($pinnedUser)?>">
                  </div>
                  <div class="t-task-item__info">
                    <h4 class="name">
                      <?=\aiur\helpers\user\UserHelper::getUserName($pinnedUser)?>
                    </h4>
                    <ul class="base-info">
                      <li><img src="/img/file.png"> <?=$pinnedUser->getMyCountMyFilesOn($row->id)?></li>
                      <li class="grey"><?=$pinnedUser->getLastDateUploadedFileOn($row->id)?></li>
                    </ul>
                  </div>
                </div>
              <?endforeach;?>
          </div>



          <?if($notPinnedUsers):?>
            <div class="t-participant__list" data-type="unlock">
              <h2>Откликнувшиеся</h2>
              <?foreach($notPinnedUsers as $notPinnedUser):?>
                <div class="t-participant__item t-task-item t-task-item_white js-participant__item" data-id="<?=$notPinnedUser->id?>" data-task="<?=$row->id?>">
                  <div class="t-task-item__img">
                    <img src="<?=\aiur\helpers\user\LogoHelper::getUserLogoUrl($notPinnedUser)?>">
                  </div>
                  <div class="t-task-item__info">
                    <h4 class="name">
                      <?=\aiur\helpers\user\UserHelper::getUserName($notPinnedUser)?>
                    </h4>
                    <ul class="base-info">
                      <li><img src="/img/file.png"> <?=$notPinnedUser->getMyCountMyFilesOn($row->id)?></li>
                      <li class="grey"><?=$notPinnedUser->getLastDateUploadedFileOn($row->id)?></li>
                    </ul>
                  </div>
                </div>
              <?endforeach;?>
            </div>
          <?endif;?>

        </div>
      <?endif;?>
    </div>
</main>