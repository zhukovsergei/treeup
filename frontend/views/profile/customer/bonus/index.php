<div class="t-background-img"><img src="/img/profile/1.jpg"></div>
<main class="t-page_profile">
  <h1 class="white">Баланс</h1>
  <div class="t-tabs">
      <select class="only-mobyle t-big-select js-big-select">
          <option value="/profile/customer/balance" >Счёт</option>
          <option value="/profile/customer/balance-log">История</option>
          <option value="/profile/customer/bonus" selected>Бонусы</option>
      </select>
    <a href="/profile/customer/balance">Счёт</a>
    <a href="/profile/customer/balance-log">История</a>
    <a href="/profile/customer/bonus" class="active">Бонусы</a>
  </div>
  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>
    <div class="t-work-area_blur__box t-balance">
      <h3>На счёте</h3>
      <p class="t-balance__money">2 700 Р<span>= 2 задачи или 2 TU</span></p>
      <p class="t-balance__info">Ваши возможности напрямую зависят от баланса вашего счёта. При пополнении счёта, расчитывайте, что стоимость публикации одной задачи или установки TU в качестве привилегии равна 1 000 Р.</p>
    </div>
    <div class="t-work-area_blur__box">
      <form id="pay_bonusForm" action="" method="POST">
        <div class="t-input">
          <span>Способ оплаты</span>
          <input type="radio" name="fd[pay]" id="pay_card_bonus" class="radio" checked>
          <label for="pay_card_bonus">Оплата банковской картой</label>
          <input type="radio" name="fd[pay]" id="pay_yandex_bonus" class="radio">
          <label for="pay_yandex_bonus">Яндекс.Деньги</label>
        </div>
        <div class="t-input t-input_30">
          <span>Сумма пополнения</span>
          <input type="text" name="fd[summ]" class="validate js-only-number">
        </div>
        <div class="t-btnBox">
          <button class="t-btn">Перейти к оплате</button>
        </div>
      </form>
    </div>
  </div>
</main>