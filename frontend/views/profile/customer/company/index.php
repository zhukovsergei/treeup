<div class="t-background-img"><img src="/img/profile/1.jpg"></div>
<main class="t-page_profile">
  <h1 class="white">Профиль</h1>
  <div class="t-tabs">
      <select class="only-mobyle t-big-select js-big-select">
          <option value="/profile/customer/personal">Личные данные</option>
          <option selected value="/profile/customer/company">О компании</option>
          <option value="/profile/customer/projects">Проекты</option>
          <option value="/profile/customer/reviews">Отзывы</option>
          <option value="/profile/customer/settings">Настройки</option>
      </select>
    <a href="/profile/customer/personal">Личные данные</a>
    <a href="/profile/customer/company" class="active">О компании</a>
    <a href="/profile/customer/projects">Проекты</a>
    <a href="/profile/customer/reviews">Отзывы</a>
    <a href="/profile/customer/settings">Настройки</a>
  </div>
    <div class="t-loadLine">
        <p>Заполненность профиля —<span class="js-percent">84%</span></p>
        <div class="t-loadLine__box">
            <div data-width="84" data-percent="8" class="js-percent_line"></div>
            <div class="js-perspective"></div>
        </div>
    </div>
  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>
    <h2>Данные компании</h2>
    <div class="t-work-area_blur__box">
      <form id="personal_info" action="/profile/customer/company/save-company" method="POST" class="js_ajaxForm" enctype="multipart/form-data">
        <div class="t-avatar">
          <img src="<?=$company->getImageFileUrl('image')??'/img/IconEmptyAvatar.png'?>" data-input="setAvatar">
          <div class="hidden js-hidden_box">
            <p>Логотип</p>
            <label for="setAvatar"><span class="t-btn">Выбрать файл</span>
              <input id="setAvatar" type="file" name="file" data-value="<?=$company->getImageFileUrl('image')??'/img/IconEmptyAvatar.png'?>">
            </label>
          </div>
        </div>
        <div class="t-input-box">
          <div class="t-input t-input_30"><span>Название компании</span>
            <input type="text" name="fd[name]" value="<?=$company->name?>" disabled class="validate">
          </div>
          <div class="t-input t-input_30"><span>Тип компании</span>
            <select data-value="ooo" disabled name="fd[jur_type]" class="js-styler">
              <?foreach(\common\models\Company::JUR_TYPE as $abbr => $value):?>
                <option <?if($company->jur_type === $abbr):?>selected<?endif;?> value="<?=$abbr?>"><?=$value?></option>
              <?endforeach;?>
            </select>
          </div>
          <div class="t-input t-input_30"><span>Стадия бизнеса</span>
            <select data-value="idea" disabled name="fd[business]" class="js-styler">
              <?foreach(\common\models\Company::BUSINESS as $abbr => $value):?>
                <option <?if($company->business === $abbr):?>selected<?endif;?> value="<?=$abbr?>"><?=$value?></option>
              <?endforeach;?>
            </select>
          </div>
          <div class="t-input t-input_30"><span>Отрасль бизнеса</span>
            <div class="t-select">
              <select class="js-selectSearch" disabled name="fd[industry_id]" data-placeholder="Введите отрасль" data-search-placeholder="Введите отрасль" data-search-not-found="Совпадений не найдено.">
                  <option selected value="-1"></option>
                <?foreach($industries as $industry):?>
                  <option <?if($industry->id == $company->industry_id):?>selected<?endif;?> value="<?=$industry->id?>"><?=$industry->name?></option>
                <?endforeach;?>
              </select>
            </div>
          </div>
          <br>
          <div class="t-input"><span>Описание бизнеса</span>
            <textarea name="fd[text]" disabled><?=$company->text?></textarea>
          </div>
        </div>
        <div class="t-btnBox"><a href="#edit" class="t-btn js-edit js-hidden_box">Изменить</a>
          <div class="hidden js-hidden_box">
            <button class="t-btn">Сохранить</button><a href="#clearForm" class="t-btn t-btn_border js-clear-form">Отменить редактирование</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</main>