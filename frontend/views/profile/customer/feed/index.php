<div class="t-background-img"><img src="/img/profile/1.jpg"></div>
<main class="t-page_profile">
  <div class="t-notice-block"><a href="/msg.html">
      <svg width="32" height="26" viewBox="0 0 14 11" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <title>Msg</title>
        <desc>Created using Figma</desc>
        <g id="Canvas_msg_2" transform="translate(-3274 2831)">
          <g id="Group_msg_2">
            <g id="Rectangle_msg_2">
              <use xlink:href="#path_msg_2" transform="translate(3274.5 -2830.5)" fill="#ffffff"></use>
            </g>
          </g>
        </g>
        <defs>
          <path id="path_msg_2" d="M 0 0L 0 -0.5C -0.276142 -0.5 -0.5 -0.276142 -0.5 1.11022e-16L 0 0ZM 13 0L 13.5 0C 13.5 -0.276142 13.2761 -0.5 13 -0.5L 13 0ZM 13 10L 13 10.5C 13.2761 10.5 13.5 10.2761 13.5 10L 13 10ZM 0 10L -0.5 10C -0.5 10.2761 -0.276142 10.5 1.11022e-16 10.5L 0 10ZM 6.5 6L 6.16086 6.3674C 6.35239 6.5442 6.64761 6.5442 6.83914 6.3674L 6.5 6ZM 0 0.5L 13 0.5L 13 -0.5L 0 -0.5L 0 0.5ZM 12.5 0L 12.5 10L 13.5 10L 13.5 0L 12.5 0ZM 13 9.5L 0 9.5L 0 10.5L 13 10.5L 13 9.5ZM 0.5 10L 0.5 0L -0.5 0L -0.5 10L 0.5 10ZM -0.33914 0.367402L 6.16086 6.3674L 6.83914 5.6326L 0.33914 -0.367402L -0.33914 0.367402ZM 6.83914 6.3674L 13.3391 0.367402L 12.6609 -0.367402L 6.16086 5.6326L 6.83914 6.3674Z"></path>
        </defs>
      </svg><i>2</i></a><a href="#notice" class="js-open-notice">
      <svg width="27" height="30" viewBox="0 0 12 13" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <title>Notice</title>
        <desc>Created using Figma</desc>
        <g id="Canvas_notice_2" transform="translate(-3239 2832)">
          <g id="Union_notice_2">
            <use xlink:href="#path_notice_2" transform="translate(3239.5 -2831.5)" fill="#ffffff"></use>
          </g>
        </g>
        <defs>
          <path id="path_notice_2" fill-rule="evenodd" d="M 5.5 0C 3.01465 0 1 2.01465 1 4.5L 1 8.5C 1 8.77612 0.776123 9 0.5 9C 0.223877 9 0 9.22388 0 9.5L 0 10C 0 10.8284 0.671631 11.5 1.5 11.5L 4.5 11.5C 4.5 12.0522 4.94775 12.5 5.5 12.5C 6.05225 12.5 6.5 12.0522 6.5 11.5L 9.5 11.5C 10.3284 11.5 11 10.8284 11 10L 11 9.5C 11 9.22388 10.7761 9 10.5 9C 10.2239 9 10 8.77612 10 8.5L 10 4.5C 10 2.01465 7.98535 0 5.5 0ZM 5.5 10.5L 9.5 10.5C 9.77612 10.5 10 10.2761 10 10L 10 9.91455C 9.41748 9.70874 9 9.15308 9 8.5L 9 4.5C 9 2.56689 7.43311 1 5.5 1C 3.56689 1 2 2.56689 2 4.5L 2 8.5C 2 9.15308 1.58252 9.70874 1 9.91455L 1 10C 1 10.2761 1.22388 10.5 1.5 10.5L 5.5 10.5Z"></path>
        </defs>
      </svg><i>99+</i></a></div>
  <div class="t-stars t-stars_company">
    <div class="t-stars__title">
      <h1><?=$user->company->name??''?></h1>
      <p>Рейтинг <?=\aiur\helpers\user\TypingProfileHelper::getRating($user)?></p>
    </div>
    <div class="t-stars__box">
      <div class="t-personal-logo">
        <div class="t-personal-logo__percent">
          <input type="text" value="60" data-linecap="round" data-readOnly="true" data-thickness="0.07" class="dial">
        </div>
        <div class="t-personal-logo__img"><img src="<?=\aiur\helpers\user\LogoHelper::getUserLogoUrl($user)?>">
          <div>
            <p><span>60%</span>Заполненность профиля</p>
          </div>
        </div>
      </div>
      <div class="t-ray">
        <div class="t-ray__box js-ray_hover">
          <a href="/profile/customer/projects">
            <img src="/img/profile/projects.png"></a><span>Проекты</span>
        </div>
        <div class="t-ray__line js-ray_hover_show">
          <hr>
          <svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" class="wave">
            <title>Wave</title>
            <defs></defs>
            <path id="feel-the-wave_projects" d="" stroke="rgba(255,255,255,.3)"></path>
          </svg>
        </div>
      </div>
      <div class="t-ray">
        <div class="t-ray__box js-ray_hover"><a href="/profile/customer/balance"><img src="/img/profile/balance.png">
            <p>75 600 Р</p></a><span>Баланс</span></div>
        <div class="t-ray__line js-ray_hover_show">
          <hr>
          <svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" class="wave">
            <title>Wave</title>
            <defs></defs>
            <path id="feel-the-wave_balance" d="" stroke="rgba(255,255,255,.3)"></path>
          </svg>
        </div>
      </div>
      <div class="t-ray">
        <div class="t-ray__box js-ray_hover"><a href="/src/profile_customer/bonus.html"><img src="/img/profile/bonus.png">
            <p>250</p></a><span>Бонусы</span></div>
        <div class="t-ray__line js-ray_hover_show">
          <hr>
          <svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" class="wave">
            <title>Wave</title>
            <defs></defs>
            <path id="feel-the-wave_bonus" d="" stroke="rgba(255,255,255,.3)"></path>
          </svg>
        </div>
      </div>
      <div class="t-ray">
        <div class="t-ray__box js-ray_hover">
          <a href="/profile/customer/reviews">
            <img src="/img/profile/reviews.png"></a><span>Отзывы</span>
        </div>
        <div class="t-ray__line js-ray_hover_show">
          <hr>
          <svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" class="wave">
            <title>Wave</title>
            <defs></defs>
            <path id="feel-the-wave_reviews" d="" stroke="rgba(255,255,255,.3)"></path>
          </svg>
        </div>
      </div>
      <div class="t-ray">
        <div class="t-ray__box js-ray_hover">
          <a href="/profile/customer/tasks">
            <img src="/img/profile/tasks.png"></a>
          <span>Задачи</span>
        </div>
        <div class="t-ray__line js-ray_hover_show">
          <hr>
          <svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" class="wave">
            <title>Wave</title>
            <defs></defs>
            <path id="feel-the-wave_tasks" d="" stroke="rgba(255,255,255,.3)"></path>
          </svg>
        </div>
      </div>
      <div class="t-ray">
        <div class="t-ray__box js-ray_hover">
          <a href="/profile/customer/personal">
            <img src="/img/profile/profile.png"></a><span>Профиль</span>
        </div>
        <div class="t-ray__line js-ray_hover_show">
          <hr>
          <svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" class="wave">
            <title>Wave</title>
            <defs></defs>
            <path id="feel-the-wave_profile" d="" stroke="rgba(255,255,255,.3)"></path>
          </svg>
        </div>
      </div>
    </div>
    <div class="t-stars__directions">

      <?=\aiur\helpers\user\TypingProfileHelper::getTypeCompany($user)?>
      <br>
      <?=\aiur\helpers\user\TypingProfileHelper::getBusinessCompany($user)?>
      <br>
      <?=\aiur\helpers\user\TypingProfileHelper::getCompanyKind($user)?>

    </div>
  </div>
  <h2 class="t-title">Новости</h2>
  <div class="t-news-list">
    <?foreach($news as $item):?>
      <a href="" class="t-blur-box">
        <div class="t-blur-box__background"></div>
        <div class="t-blur-box__title"><?=$item->name?></div>
        <div class="t-blur-box__date"><?=$item->date_add?></div>
      </a>
    <?endforeach;?>
    <a href="/news" class="t-btn">Все новости</a>
  </div>
  <h2 class="t-title">Учебные материалы</h2>
  <div class="t-news-list"><a href="" class="t-blur-box">
      <div class="t-blur-box__background"></div>
      <div class="t-blur-box__title">Закон о запрете обхода блокировок через VPN вступил в силу</div></a><a href="" class="t-blur-box">
      <div class="t-blur-box__background"></div>
      <div class="t-blur-box__title">Закон о запрете обхода блокировок через анонимайзеры и VPN вступил в силу</div></a><a href="" class="t-blur-box">
      <div class="t-blur-box__background"></div>
      <div class="t-blur-box__title">Закон о запрете обхода блокировок через VPN вступил в силу</div></a><a href="" class="t-blur-box">
      <div class="t-blur-box__background"></div>
      <div class="t-blur-box__title">Закон о запрете обхода блокировок через анонимайзеры и VPN вступил в силу</div></a><a href="" class="t-blur-box">
      <div class="t-blur-box__background"></div>
      <div class="t-blur-box__title">Закон о запрете обхода блокировок через анонимайзеры и VPN вступил в силу. Закон о запрете обхода блокировок через анонимайзеры и VPN вступил в силу. Закон о запрете обхода блокировок через анонимайзеры и VPN вступил в силу</div></a>
      <a href="/news.html" class="t-btn">Все материалы</a>
  </div>
</main>