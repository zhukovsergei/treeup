<div class="t-background-img"><img src="/img/profile/1.jpg"></div>
<main class="t-page_profile">
  <h1 class="white">Баланс</h1>
  <div class="t-tabs">
      <select class="only-mobyle t-big-select js-big-select">
          <option value="/profile/customer/balance" >Счёт</option>
          <option value="/profile/customer/balance-log" selected>История</option>
          <option value="/profile/customer/bonus" >Бонусы</option>
      </select>
    <a href="/profile/customer/balance">Счёт</a>
    <a href="/profile/customer/balance-log" class="active">История</a>
    <a href="/profile/customer/bonus">Бонусы</a>
  </div>
  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>
    <div class="t-search-filter">
      <div class="t-search-filter__overflow js-hide-filter"></div>
      <div class="t-search-filter__header js-open-filter">
        <h3>Поиск и фильтрация</h3>
        <svg width="16" height="16" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
          <path fill="#BDBDBD" fill-rule="evenodd" d="M 3 1C 3 0.447754 2.55225 0 2 0C 1.44775 0 1 0.447754 1 1L 1 8.26758C 0.402344 8.61353 0 9.25977 0 10C 0 10.7402 0.402344 11.3865 1 11.7324L 1 15C 1 15.5522 1.44775 16 2 16C 2.55225 16 3 15.5522 3 15L 3 11.7324C 3.59766 11.3865 4 10.7402 4 10C 4 9.25977 3.59766 8.61353 3 8.26758L 3 1ZM 9 1C 9 0.447754 8.55225 0 8 0C 7.44775 0 7 0.447754 7 1L 7 3.26758C 6.40234 3.61353 6 4.25977 6 5C 6 5.74023 6.40234 6.38647 7 6.73242L 7 15C 7 15.5522 7.44775 16 8 16C 8.55225 16 9 15.5522 9 15L 9 6.73242C 9.59766 6.38647 10 5.74023 10 5C 10 4.25977 9.59766 3.61353 9 3.26758L 9 1ZM 14 0C 14.5522 0 15 0.447754 15 1L 15 9.26758C 15.5977 9.61353 16 10.2598 16 11C 16 11.7402 15.5977 12.3865 15 12.7324L 15 15C 15 15.5522 14.5522 16 14 16C 13.4478 16 13 15.5522 13 15L 13 12.7324C 12.4023 12.3865 12 11.7402 12 11C 12 10.2598 12.4023 9.61353 13 9.26758L 13 1C 13 0.447754 13.4478 0 14 0Z"></path>
        </svg>
      </div>
      <div class="t-search-filter__body">
        <form id="search-filter" action="" method="POST">
          <div class="t-input-box">
            <div class="t-input t-input_30 t-input_search"><span>Поиск</span>
              <input name="fd[q]">
              <button type="submit">
                <svg width="16" height="16" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <path fill="#E0E0E0" fill-rule="evenodd" d="M 4 7C 4 4.23853 6.23877 2 9 2C 11.7612 2 14 4.23853 14 7C 14 9.76147 11.7612 12 9 12C 6.23877 12 4 9.76147 4 7ZM 9 0C 5.13379 0 2 3.13403 2 7C 2 8.57227 2.51855 10.0237 3.39355 11.1924L 0.292969 14.293C -0.0976562 14.6836 -0.0976562 15.3167 0.292969 15.7073C 0.683594 16.0977 1.31641 16.0977 1.70703 15.7073L 4.80762 12.6064C 5.97607 13.4817 7.42773 14 9 14C 12.8662 14 16 10.866 16 7C 16 3.13403 12.8662 0 9 0Z"></path>
                </svg>
              </button>
            </div>
            <div class="t-input t-input_30"><span>Период</span>
              <select name="fd[period]" class="js-styler validate">
                <option selected value="1">За всё время</option>
                <option value="2">Сегодня</option>
                <option value="3">Вчера</option>
                <option value="4">7 дней</option>
                <option value="4">30 дней</option>
                <option value="4">Квартал</option>
                <option value="4">Полугодие</option>
                <option value="4">Год</option>
              </select>
            </div>
            <div class="t-input t-input_30"><span>Тип операции</span>
              <select name="fd[type]" class="js-styler validate">
                <option selected value="0">Любой</option>
                <option value="1">Пополнение</option>
                <option value="2">Снятие</option>
              </select>
            </div>
            <!--<div class="t-input"><span>Способ оплаты</span>
              <select name="fd[pay_type]" class="js-styler validate">
                <option selected value="0">Любой</option>
                <option value="1">Банковская карта</option>
                <option value="2">Яндекс.Деньги</option>
              </select>
            </div>-->
          </div>
          <div class="t-input-box__buttons t-input-box__buttons_right"><a href="#clear" class="t-btn t-btn_border js-clearFilter">Сбросить</a></div>
        </form>
      </div>
    </div>
    <div class="t-work-area_blur__box t-balance">
      <div class="t-balance__table">
        <table>
          <thead>
          <tr>
            <th><a class="js-table-sort down" href="#sort"><i></i>Дата</a></th>
            <th>Операция</th>
<!--            <th>Способ оплаты</th>-->
            <th>Баланс</th>
<!--            <th>Примечание</th>-->
          </tr>
          </thead>
          <tbody>

          <?foreach($transactions as $transaction):?>
            <tr>
              <td><?=\Yii::$app->formatter->asDatetime((new \DateTime())->setTimestamp($transaction->date),'php:d.m.Y, H:i')?></td>
              <td <?if($transaction->amount > 0):?>class="green"<?else:?>class="red"<?endif;?>><?=$transaction->amount?> Р</td>
<!--              <td>Банковская карта</td>-->
              <td><?=$transaction->getStack()?> Р</td>
<!--              <td>Оплата за задачу</td>-->
            </tr>
          <?endforeach;?>

          </tbody>
        </table>
      </div>
    </div>
  </div>
</main>