<div class="t-background-img"><img src="/img/profile/1.jpg"></div>
<main class="t-page_profile">
  <h1 class="white">Профиль</h1>
  <div class="t-tabs">
      <select class="only-mobyle t-big-select js-big-select">
          <option value="/profile/customer/personal">Личные данные</option>
          <option value="/profile/customer/company">О компании</option>
          <option value="/profile/customer/projects">Проекты</option>
          <option selected value="/profile/customer/reviews">Отзывы</option>
          <option value="/profile/customer/settings">Настройки</option>
      </select>
    <a href="/profile/customer/personal">Личные данные</a>
    <a href="/profile/customer/company">О компании</a>
    <a href="/profile/customer/projects">Проекты</a>
    <a href="/profile/customer/reviews" class="active">Отзывы</a>
    <a href="/profile/customer/settings">Настройки</a>
  </div>
    <div class="t-loadLine">
        <p>Заполненность профиля —<span class="js-percent">84%</span></p>
        <div class="t-loadLine__box">
            <div data-width="84" data-percent="8" class="js-percent_line"></div>
            <div class="js-perspective"></div>
        </div>
    </div>
  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>
      <select class="only-mobyle t-big-select t-big-select_no-border js-big-select">
          <option selected value="/profile/custuner/reviews">Все отзывы</option>
          <option value="/profile/custuner/reviews?reviews=1">Отзывы обо мне</option>
          <option value="/profile/custuner/reviews?reviews=2">Отзывы от меня</option>
      </select>
      <div class="t-tabs_task">
          <ul>
              <li><a href="/profile/custuner/reviews" class="active">Все отзывы</a></li>
              <li><a href="/profile/custuner/reviews?reviews=1">Отзывы обо мне</a></li>
              <li><a href="/profile/custuner/reviews?reviews=2">Отзывы от меня</a></li>
          </ul>
      </div>
    <div class="t-reviews-list">
      <div class="t-review">
        <div class="t-review__personal-info">
          <div class="t-review__avatar"><img src="/img/face.jpg"></div>
          <div class="t-review__name"><span>Я</span>14.02.2016</div>
        </div>
        <div class="t-work-area_blur__box">
          <div class="t-review__type">О задаче</div>
          <div class="t-review__task">Создать сайт булочной</div>
          <div class="t-review__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</div>
        </div>
      </div>
      <div class="t-review">
        <div class="t-review__personal-info">
          <div class="t-review__avatar"><img src="/img/face.jpg"></div>
          <div class="t-review__name"><span>Николай Артемьев</span>14.02.2016</div>
        </div>
        <div class="t-work-area_blur__box">
          <div class="t-review__type">О задаче</div>
          <div class="t-review__task">Создать сайт булочной</div>
          <div class="t-review__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</div>
        </div>
      </div>
    </div>
  </div>
</main>