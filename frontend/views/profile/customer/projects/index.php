<div class="t-background-img"><img src="/img/profile/1.jpg"></div>
<main class="t-page_profile">
  <h1 class="white">Профиль</h1>
  <div class="t-tabs">
      <select class="only-mobyle t-big-select js-big-select">
          <option value="/profile/customer/personal">Личные данные</option>
          <option value="/profile/customer/company">О компании</option>
          <option selected value="/profile/customer/projects">Проекты</option>
          <option value="/profile/customer/reviews">Отзывы</option>
          <option value="/profile/customer/settings">Настройки</option>
      </select>
    <a href="/profile/customer/personal">Личные данные</a>
    <a href="/profile/customer/company">О компании</a>
    <a href="/profile/customer/projects" class="active">Проекты</a>
    <a href="/profile/customer/reviews">Отзывы</a>
    <a href="/profile/customer/settings">Настройки</a>
  </div>
    <div class="t-loadLine">
        <p>Заполненность профиля —<span class="js-percent">84%</span></p>
        <div class="t-loadLine__box">
            <div data-width="84" data-percent="8" class="js-percent_line"></div>
            <div class="js-perspective"></div>
        </div>
    </div>
  <div class="t-work-area t-work-area_blur js-blocked">
    <div class="t-work-area__background"></div>
    <h2>Все проекты</h2>
      <div id="project_list">
          <?$i = 1?>
          <?foreach($projects as $project):?>
            <form class="t-work-area_blur__box js-blocked t-project"  action="/profile/customer/projects/update" method="POST" data-type="project">
              <div class="t-project__static js-hidden_box">
                <h2 class="js-react_bin" data-react="title"><?=$project->name?></h2>
                <div class="t-project__tasks">Задачи — <span class="js-react_bin" data-react="task"><?=$project->getCountAssignedCount()?></span></div>
                <div class="t-project__text js-react_bin" data-react="description">
                  <?=$project->text?>
                </div>
              </div>
              <div class="t-project__edit js-hidden_box hidden">
                <div class="t-input t-input_30">
                  <span>Название проекта</span>
                  <input name="fd[name]" class="validation js-showButton_input js-react" data-react="title" value="<?=$project->name?>">
                </div>
                <div class="t-input">
                  <span>Описание</span>
                  <textarea name="fd[text]" class="validation js-react" data-react="description"><?=$project->text?></textarea>
                </div>
                <div class="t-input">
                  <span>Задачи проекта</span>
                  <div class="t-input t-input_30">
                    <div data-name="fd[tasks]" class="t-custom-select js-custom-select js-react_select <? if(count($freeTasks) == 0): ?>disabled<?endif;?>" data-react="task">
                      <div class="t-custom-select__result"><? if(count($freeTasks) == 0): ?>Нет задач<?else:?>Выбрано <?=$project->getCountAssignedCount()?><?endif;?></div>
                      <div class="t-custom-select__options">
                        <ul class="scrollbar-inner">
                          <?foreach($freeTasks as $task):?>
                            <li>
                              <input <?if($task->project_id == $project->id):?>checked="checked"<?endif;?> id="options_task_<?=$task->id?>_<?=$i?>" type="checkbox" value="<?=$task->id?>" class="checkbox">
                              <label for="options_task_<?=$task->id?>_<?=$i?>"><?=$task->name?></label>
                            </li>
                          <?endforeach;?>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="t-input t-input_30">
                    <a class="t-btn" href="/tasks/add" data-project="<?=$project->id?>">Добавить новую задачу</a>
                  </div>
                </div>
              </div>
              <div class="t-btnBox">
                <a class="t-btn t-btn_border js-edit js-hidden_box" data-blocked="blocked" href="#edit">Изменить</a>
                <a href="#del" class="t-del js-del js-hidden_box" data-type="project">
                  <svg width="10" height="15" viewBox="0 0 10 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <path fill="#CCCCCC" d="M 5.0001 0C 5.55283 0 6.0001 0.447693 6.0001 1L 6.0001 2L 9.0001 2C 9.55283 2 10.0001 2.44769 10.0001 3C 10.0001 3.55231 9.55283 4 9.0001 4L 5.0001 4L 1.0001 4C 0.447362 4 9.62968e-05 3.55231 9.62968e-05 3C 9.62968e-05 2.44769 0.447362 2 1.0001 2L 4.0001 2L 4.0001 1C 4.0001 0.447693 4.44736 0 5.0001 0ZM 0.250096 5.3382C 0.440526 5.12317 0.712987 5 1.0001 5L 9.0001 5C 9.28721 5 9.55967 5.12317 9.7501 5.3382C 9.93955 5.55322 10.0284 5.83942 9.99228 6.12402L 8.99228 14.124C 8.92978 14.6245 8.504 15 8.0001 15L 2.0001 15C 1.49619 15 1.07041 14.6245 1.00791 14.124L 0.0079088 6.12402C -0.028224 5.83942 0.0606432 5.55322 0.250096 5.3382ZM 2.13291 7L 2.88291 13L 7.11728 13L 7.86728 7L 2.13291 7Z"></path>
                  </svg>
                </a>
                <div class="js-hidden_box hidden">
                  <input type="hidden" name="id" value="<?=$project->id?>">
                  <button class="t-btn t-btn_120 js-add_info" data-blocked="blocked" disabled>Сохранить</button>
                  <a href="#cancel" class="t-btn t-btn_border js-clear-form t-btn_120" data-blocked="blocked">Отмена</a>
                </div>
              </div>
            </form>
              <?$i++?>
          <?endforeach;?>
          <div class="t-addBox t-addBox_full"><a href="#add" data-type="project" class="t-btn js-addProject" data-blocked="blocked">Добавить проект</a></div>
      </div>
  </div>
</main>

<div class="t-templates">
    <form class="t-work-area_blur__box js-blocked t-project" action="/profile/customer/projects/create" method="POST" data-type="project">
        <div class="t-project__static js-hidden_box hidden">
            <h2 class="js-react_bin" data-react="title"></h2>
            <div class="t-project__tasks">Задачи — <span>0</span></div>
            <div class="t-project__text js-react_bin" data-react="description"></div>
        </div>
        <div class="t-project__edit js-hidden_box ">
            <div class="t-input t-input_30">
                <span>Название проекта</span>
                <input name="fd[name]" class="validation js-showButton_input js-react" data-react="title" value="">
            </div>
            <div class="t-input">
                <span>Описание</span>
                <textarea name="fd[text]" class="validation js-react" data-react="description"></textarea>
            </div>
            <div class="t-input">
                <span>Задачи проекта</span>
                <div class="t-input t-input_30">
                    <div data-name="fd[tasks]" class="t-custom-select js-custom-select">
                        <div class="t-custom-select__result">Выбрано 0</div>
                        <div class="t-custom-select__options">
                            <ul class="scrollbar-inner">
                              <?foreach($freeTasks as $task):?>
                                <li>

                                  <input id="options_task_<?=$task->id?>_" type="checkbox" value="<?=$task->id?>_" class="checkbox">
                                  <label for="options_task_<?=$task->id?>_"><?=$task->name?></label>

                                </li>
                              <?endforeach;?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="t-input t-input_30 js-hidden_box_2 hidden">
                  <a class="t-btn" href="/tasks/add" data-project="disabled">Добавить новую задачу</a>
                </div>
            </div>
        </div>
        <div class="t-btnBox">
            <a class="t-btn t-btn_border js-edit js-hidden_box hidden" data-blocked="blocked" href="#edit">Изменить</a>
            <a href="#del" class="t-del js-del js-hidden_box hidden" data-type="project">
                <svg width="10" height="15" viewBox="0 0 10 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <path fill="#CCCCCC" d="M 5.0001 0C 5.55283 0 6.0001 0.447693 6.0001 1L 6.0001 2L 9.0001 2C 9.55283 2 10.0001 2.44769 10.0001 3C 10.0001 3.55231 9.55283 4 9.0001 4L 5.0001 4L 1.0001 4C 0.447362 4 9.62968e-05 3.55231 9.62968e-05 3C 9.62968e-05 2.44769 0.447362 2 1.0001 2L 4.0001 2L 4.0001 1C 4.0001 0.447693 4.44736 0 5.0001 0ZM 0.250096 5.3382C 0.440526 5.12317 0.712987 5 1.0001 5L 9.0001 5C 9.28721 5 9.55967 5.12317 9.7501 5.3382C 9.93955 5.55322 10.0284 5.83942 9.99228 6.12402L 8.99228 14.124C 8.92978 14.6245 8.504 15 8.0001 15L 2.0001 15C 1.49619 15 1.07041 14.6245 1.00791 14.124L 0.0079088 6.12402C -0.028224 5.83942 0.0606432 5.55322 0.250096 5.3382ZM 2.13291 7L 2.88291 13L 7.11728 13L 7.86728 7L 2.13291 7Z"></path>
                </svg>
            </a>
            <div class="js-hidden_box">
                <input type="hidden" name="id" value="">
                <button class="t-btn t-btn_120 js-add_info" data-blocked="blocked" disabled>Сохранить</button>
                <a href="#cancel" class="t-btn t-btn_border js-clear-form t-btn_120" data-blocked="blocked" data-del="on">Отмена</a>
            </div>
        </div>
    </form>
</div>