<?php
$this->title = $exception->getMessage();
?>
<?php if ($exception->statusCode == 404):?>
<main class="t-page_text">
    <div class="t-error-page"><img src="/img/no_img.png">
        <h1>404</h1>
        <h2>Страница не найдена</h2>
        <p>Возможно страница была удалена — такое бывает.<br>Можно перейти на главную.</p>
    </div>
</main>
<?php else:;?>
    <main class="t-page_text">
        <div class="t-error-page"><img src="/img/no_img.png">
            <h1><?=$exception->statusCode?></h1>
            <h2><?=$exception->getName()?></h2>
            <p><?=$exception->getMessage()?></p>
        </div>
    </main>
<?php endif;?>
