<?php
$this->title = 'Главная страница';
?>

<main data-theme="yeallow-white" class="t-page_home">
  <section class="t-preview">
    <div class="t-stuck_img"></div>
    <h1>Treeup — это сервис по построению команды с решением задач на основе краудсорсинга</h1>
    <?if(\Yii::$app->user->getIsGuest()):?>
      <div class="t-create-get-task flip-container">
        <div data-type="flip" class="tabs only-mobyle"><a href="#advantage_client" class="active">Для заказчика</a><i></i><a href="#advantage_developer">Для исполнителя</a></div>
        <ul>
          <li>
            <h2>Для заказчика</h2>
            <p>Лучшие исполнители с гарантией выполнения работы</p><a href="#create-task" class="t-btn js-link_auth">Создать задачу</a>
          </li>
          <li>
            <h2>Для исполнителя</h2>
            <p>Получай заказы с гарантией оплаты от лучших заказчиков</p><a href="#get-task" class="t-btn js-link_auth">Предложить решение</a>
          </li>
        </ul>
      </div>
    <?else:?>
      <?if($user->typeProfileIs('customer')):?>
          <div class="t-create-get-task t-create-get-task_buttons">
            <a href="/tasks/add" class="t-btn">Создать задачу</a>
            <a href="/profile/customer/feed" class="t-btn t-btn_border t-btn_white">Личный кабинет</a>
          </div>
        <?else:?>
          <div class="t-create-get-task t-create-get-task_buttons">
            <a href="/tasks" class="t-btn">Приступить к работе</a>
            <a href="/profile/business/feed" class="t-btn t-btn_border t-btn_white">Личный кабинет</a>
          </div>
        <?endif;?>
    <?endif;?>

    <div class="t-new-task">
      <h2>Новые задачи <span><?=\Yii::t('app', '{n,plural, one{# задача} few{# задачи} many{# задач} other{# задач}}', ['n' => $countTasks])?> в <?=\Yii::t('app', '{n,plural, one{# категории} few{# категориях} many{# категориях} other{# категориях}}', ['n' => $countCategories])?> </span></h2>
      <div class="swiper-container">
        <div class="swiper-wrapper">
          <?foreach($lasTasks as $lastTask):?>
            <div class="swiper-slide">
              <a href="/tasks/<?=$lastTask->id?>/view" class="t-task-item">
                <div class="t-task-item__background"></div>
                <div class="t-task-item__img">
                  <span>2</span>
                  <img src="<?=\aiur\helpers\user\LogoHelper::getUserLogoUrl($lastTask->creator)?>">
                </div>
                <div class="t-task-item__info">
                  <div class="price">
                    <?if(!$lastTask->isTeam() && $lastTask->tu_count):?>
                      <img src="/img/bonus1.png"><span><?=$lastTask->tu_count?></span>
                    <?endif;?>
                    <img src="/img/bonus2.png"><span><?=$lastTask->points_win?><?if(!$lastTask->isTeam() && $lastTask->points_participant):?>/<?endif;?><?=$lastTask->points_participant?></span>
                  </div>
                  <div class="level level_<?=$lastTask->complexity?>"><span></span><span></span><span></span></div>
                  <div class="name"><?=\aiur\helpers\user\UserHelper::getUserName($lastTask->creator)?></div>
                  <div class="description"><?=$lastTask->name?></div>
                  <hr>
                  <ul class="base-info">
                    <li><?=$lastTask->date_end?></li>
                    <?if(isset($lastTask->industry)):?>
                      <li><?=$lastTask->industry->name?></li>
                    <?endif;?>
                    <li><?=$lastTask->town->name??''?></li>
                  </ul>
                </div>
              </a>
            </div>
          <?endforeach;?>
        </div>
        <div class="swiper-pagination"></div>
      </div><a href="/tasks" class="t-btn">Посмотреть все</a>
    </div>
  </section>

  <?=\frontend\widgets\Advantages::widget()?>

  <?=\frontend\widgets\HowToWork::widget()?>

  <section class="t-reviews">
    <h2>Отзывы о нас</h2>
    <div class="swiper-container t-reviews-slider">
      <div class="swiper-pagination"></div>
      <div class="swiper-wrapper">
        <?foreach($mainReviews as $review):?>
          <div class="swiper-slide t-reviews-slider__item"><img src="<?=$review->getImageFileUrl('image')?>">
            <div class="prof"><?=$review->name?></div>
            <div class="text"><?=$review->text?></div>
            <?if(filter_var($review->link, FILTER_VALIDATE_URL)):?>
              <a href="<?=$review->link?>" target="_blank"><?=$review->link?></a>
            <?else:?>
              <a href="http://<?=$review->link?>" target="_blank"><?=$review->link?></a>
            <?endif;?>
          </div>
        <?endforeach;?>
      </div>
    </div>
  </section>
</main>