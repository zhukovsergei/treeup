<?php
$this->title = 'Сброс пароля';
?>

<main class="t-page_text">
  <h1>Сброс пароля</h1>
  <div class="t-work-area">
    <form id="create_password" action="<?=\Yii::$app->request->url?>" method="POST" class="js-create_password">
        <div class="t-input"><span>Новый пароль</span>
            <input type="password" name="fd[password]" data-name="password_doubel" class="identical">
        </div>
        <div class="t-input"><span>Повторите новый пароль</span>
            <input type="password" name="fd[password_repeat]" data-name="password_doubel" class="identical">
        </div>
        <button disabled class="t-btn">Сохранить</button>
    </form>
  </div>
</main>