<div class="t-background-img"><img src="/img/registration.jpg"></div>
<main class="t-page_registration">
  <h1 class="white">Регистрация</h1>
  <ul class="t-tabs_registration">
    <li>Роль</li>
    <li class="active">Данные</li>
    <li>Компания</li>
  </ul>

  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>
    <h2>Личные данные</h2>
    <div class="t-work-area_blur__box">
      <form id="personal_info" action="<?=\yii\helpers\Url::to(['/registration/customer/save-data'])?>" method="POST" class="js-profileForm" enctype="multipart/form-data">
        <div class="t-avatar">
          <img src="<?=$form->getImage()?>" data-input="setAvatar">
          <div>
            <p>Аватар профиля</p>
            <label for="setAvatar"><span class="t-btn">Выбрать файл</span>
              <input id="setAvatar" type="file" name="file" data-value="<?=$form->getImage()?>">
            </label>
          </div>
        </div>
        <div class="t-input-box">
          <div class="t-input t-input_30"><span>Имя</span>
            <input type="text" name="fd[name]" value="<?=$form->name?>" class="validate">
          </div>
          <div class="t-input t-input_30"><span>Фамилия</span>
            <input type="text" name="fd[surname]" value="<?=$form->surname?>" class="validate">
          </div>
          <div class="t-input t-input_30"><span>Отчество</span>
            <input type="text" name="fd[patronymic]" value="<?=$form->patronymic?>">
          </div>
          <div class="t-input t-input_30"><span>Дата рождения</span>
            <input type="hidden" name="fd[birthday]" value="<?=$form->birthday?>" class="validate">
            <div class="js-datepicker t-input_date js-disabled" data-max="true"></div>
          </div>
          <div class="t-input t-input_30"><span>Город проживания</span>
            <select name="fd[town_id]" class="js-selectSearch validate">
              <?foreach($towns as $town):?>
                <option <?if($form->town_id === $town->id):?>selected<?endif;?> value="<?=$town->id?>"><?=$town->name?></option>
              <?endforeach;?>
            </select>
          </div>
          <div class="t-input t-input_30"><span>Тип учётной записи</span>
            <select data-value="<?=$form->jur_status?>" name="fd[jur_status]" class="js-styler validate">
              <?foreach(\common\models\Profile::JUR_STATUS as $abbr => $value):?>
                <option <?if($form->jur_status === $abbr):?>selected<?endif;?> value="<?=$abbr?>"><?=$value?></option>
              <?endforeach;?>
            </select>
          </div>
          <div class="t-input t-input_30"><span>Электронная почта</span>
            <input type="email" name="fd[email]" value="<?=$form->email?>" class="validate">
          </div>
          <div class="t-input t-input_30"><span>Номер телефона</span>
            <input type="text" name="fd[phone]" value="<?=$form->phone?>" placeholder="+7" class="validate">
          </div>
        </div>
        <div class="t-btnBox">
          <input type="hidden" name="_csrf" value="<?=\Yii::$app->request->getCsrfToken()?>">
          <button class="t-btn">Сохранить и продолжить</button>
          <a href="/registration/choice/role" class="t-btn t-btn_border">Шаг назад</a>
        </div>
      </form>
    </div>
  </div>
</main>