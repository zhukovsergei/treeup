<div class="t-background-img"><img src="/img/registration.jpg"></div>
<main class="t-page_registration">
  <h1 class="white">Регистрация</h1>
  <ul class="t-tabs_registration">
    <li>Роль</li>
    <li>Данные</li>
    <li class="active">Компания</li>
  </ul>

  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>
    <h2>Данные компании</h2>
    <div class="t-work-area_blur__box">
      <form id="company_info" action="<?=\yii\helpers\Url::to(['/registration/customer/save-company'])?>" method="POST" class="js-profileForm" enctype="multipart/form-data">
        <div class="t-avatar">
          <img src="<?=$form->getImage()?>" data-input="setAvatar">
          <div>
            <p>Логотип</p>
            <label for="setAvatar">
              <span class="t-btn">Выбрать файл</span>
              <input id="setAvatar" type="file" name="file" data-value="<?=$form->getImage()?>">
            </label>
          </div>
        </div>
        <div class="t-input-box">
          <div class="t-input t-input_30"><span>Название компании</span>
            <input type="text" value="<?=$form->name?>" name="fd[name]"  class="validate">
          </div>
          <div class="t-input t-input_30"><span>Тип компании</span>
            <select data-value="ooo" name="fd[jur_type]" class="js-styler">
<!--              <option selected disabled value="" class="hidden_option">Не выбран</option>-->
              <?foreach(\common\models\Company::JUR_TYPE as $abbr => $value):?>
                <option <?if($form->jur_type === $abbr):?>selected<?endif;?> value="<?=$abbr?>"><?=$value?></option>
              <?endforeach;?>
            </select>
          </div>
          <div class="t-input t-input_30"><span>Отрасль бизнеса</span>
            <div class="t-select">
              <select class="js-selectSearch" name="fd[industry_id]" data-placeholder="Введите отрасль" data-search-placeholder="Введите отрасль" data-search-not-found="Совпадений не найдено.">
                <option selected value="-1"></option>
                <?foreach($industries as $industry):?>
                  <option value="<?=$industry->id?>"><?=$industry->name?></option>
                <?endforeach;?>
              </select>
            </div>
          </div>
          <div class="t-input t-input_30"><span>Стадия бизнеса</span>
            <select data-value="idea" name="fd[business]" class="js-styler">
              <!--              <option selected disabled value="" class="hidden_option">Не выбран</option>-->
              <?foreach(\common\models\Company::BUSINESS as $abbr => $value):?>
                <option <?if($form->business === $abbr):?>selected<?endif;?> value="<?=$abbr?>"><?=$value?></option>
              <?endforeach;?>
            </select>
          </div>
          <div class="t-input"><span>Описание бизнеса</span>
            <textarea name="fd[text]"><?=$form->text?></textarea>
          </div>
        </div>
        <div class="t-btnBox">
          <input type="hidden" name="_csrf" value="<?=\Yii::$app->request->getCsrfToken()?>">
          <button class="t-btn">Сохранить и продолжить</button>
          <a href="/registration/customer/data" class="t-btn t-btn_border">Шаг назад</a>
        </div>
      </form>
    </div>
  </div>
</main>