<div class="t-background-img"><img src="/img/registration.jpg"></div>
<main class="t-page_registration">
  <h1 class="white">Регистрация</h1>
  <h2 class="t-page_registration__title">Роль на сайте</h2>
  <p class="t-page_registration__hint">Выбранная роль определяет ваши возможности на сервисе. Она закрепляется за аккаунтом и не подлежит изменению в будущем.</p>
  <div class="t-work-area t-work-area_registration">
    <div class="t-work-area__background"></div>
    <ul class="t-registration__role">
      <li><img src="/img/ManA.png">
        <h3>Заказчик</h3><a href="/registration/customer/data" class="t-btn">Выбрать</a>
      </li>
      <li><img src="/img/ManB.png">
        <h3>Исполнитель</h3><a href="/registration/business/data" class="t-btn">Выбрать</a>
      </li>
    </ul>
  </div>
</main>