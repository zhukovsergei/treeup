<div class="t-background-img"><img src="/img/registration.jpg"></div>
<main class="t-page_registration">
    <h1 class="white">Регистрация</h1>
    <ul class="t-tabs_registration">
        <li>Роль</li>
        <li>Данные</li>
        <li class="active">Резюме</li>
    </ul>

    <div class="t-work-area t-work-area_blur">
        <div class="t-work-area__background"></div>
        <div class="js-accardion">
            <h2>Специализации</h2>
            <div class="js-addcontainer" data-type="specializations">
                <?php if (!empty($user->specs)): ?>
                    <? foreach ($user->specs as $spec): ?>
                        <form class="t-work-area_blur__box t-work-area_blur__box_30 js-blocked"
                              action="/profile/business/cv/specialization/update" method="POST">
                            <div class="t-input">
                                <span class="js-hidden_box js-radioText ">Практикуемая отрасль</span>
                                <span class="js-hidden_box hidden">Отрасль</span>
                                <input type="radio" class="radio" id="practiced_<?= $spec->id ?>" disabled
                                       name="fd[type_industry]" checked value="praktisch">
                                <label for="practiced_<?= $spec->id ?>"
                                       class="js-hidden_box hidden">Практикуемая</label>
                                <input type="radio" class="radio" id="interested_<?= $spec->id ?>" disabled
                                       name="fd[type_industry]" value="interesse">
                                <label for="interested_<?= $spec->id ?>"
                                       class="js-hidden_box hidden">Интересующая</label>
                                <div class="t-select">
                                    <select class="js-selectSearch" disabled name="fd[industry]"
                                            data-placeholder="Введите отрасль"
                                            data-search-placeholder="Введите отрасль"
                                            data-search-not-found="Совпадений не найдено.">
                                        <? foreach ($industries as $ind): ?>
                                            <option <? if ($spec->industry_id == $ind->id): ?>selected<? endif; ?>
                                                    value="<?= $ind->id ?>"><?= $ind->name ?></option>
                                        <? endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="t-input">
                                <span>Специализация</span>
                                <div class="t-select">
                                    <select class="js-selectSearch" disabled name="fd[specialization]"
                                            data-placeholder="Введите специализацию"
                                            data-search-placeholder="Введите специализацию"
                                            data-search-not-found="Совпадений не найдено.">
                                        <? foreach ($specializations as $sp): ?>
                                            <option
                                                <? if ($spec->specialization_id == $sp->id): ?>selected<? endif; ?>
                                                value="<?= $sp->id ?>"><?= $sp->name ?></option>
                                        <? endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="t-input">
                                <span>Опыт</span>
                                <select class="js-styler validate js-showButton_select" disabled name="fd[exp]">
                                    <option value="-1" disabled class="hidden_option">Выберите из списка</option>
                                    <? foreach (\common\models\Spec::EXPERIENCE as $key => $exp): ?>
                                        <option <? if ($key == $spec->exp): ?>selected<? endif; ?>
                                                value="<?= $key ?>"><?= $exp ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                            <div class="t-input">
                                <span>Навыки через запятую</span>
                                <div class="t-tag-list js-tab-list">
                                    <ul class="t-tag-list__box disabled js-disabled">
                                        <? foreach ($spec->skills as $skill): ?>
                                            <li><p data-value="<?= $skill->id ?>"><?= $skill->name ?><span
                                                            class="js-del_tag"></span></p></li>
                                        <? endforeach; ?>
                                        <li>
                                            <input class="js-tags-set" disabled placeholder="Укажите через запятую">
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="t-btnBox">
                                <a class="t-btn js-edit js-hidden_box" data-blocked="blocked"
                                   href="#edit">Изменить</a>
                                <a href="#del" class="t-del js-del js-hidden_box " data-type="specialization">
                                    <svg width="10" height="15" viewBox="0 0 10 15" version="1.1"
                                         xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <path fill="#CCCCCC"
                                              d="M 5.0001 0C 5.55283 0 6.0001 0.447693 6.0001 1L 6.0001 2L 9.0001 2C 9.55283 2 10.0001 2.44769 10.0001 3C 10.0001 3.55231 9.55283 4 9.0001 4L 5.0001 4L 1.0001 4C 0.447362 4 9.62968e-05 3.55231 9.62968e-05 3C 9.62968e-05 2.44769 0.447362 2 1.0001 2L 4.0001 2L 4.0001 1C 4.0001 0.447693 4.44736 0 5.0001 0ZM 0.250096 5.3382C 0.440526 5.12317 0.712987 5 1.0001 5L 9.0001 5C 9.28721 5 9.55967 5.12317 9.7501 5.3382C 9.93955 5.55322 10.0284 5.83942 9.99228 6.12402L 8.99228 14.124C 8.92978 14.6245 8.504 15 8.0001 15L 2.0001 15C 1.49619 15 1.07041 14.6245 1.00791 14.124L 0.0079088 6.12402C -0.028224 5.83942 0.0606432 5.55322 0.250096 5.3382ZM 2.13291 7L 2.88291 13L 7.11728 13L 7.86728 7L 2.13291 7Z"></path>
                                    </svg>
                                </a>
                                <div class="js-hidden_box hidden">
                                    <input type="hidden" name="id" value="<?= $spec->id ?>">
                                    <button class="t-btn t-btn_120 js-add_info" data-blocked="blocked" disabled>
                                        Сохранить
                                    </button>
                                    <a href="#cancel" class="t-btn t-btn_border js-clear-form t-btn_120"
                                       data-blocked="blocked">Отмена</a>
                                </div>
                            </div>
                        </form>
                    <? endforeach; ?>
                <?php endif; ?>
                <div class="t-addBox">
                    <a href="#add" data-type="specializations" data-blocked="blocked"
                       class="t-btn js-addBox">Добавить</a>
                </div>
            </div>
            <h2>Образование</h2>
            <div class="js-addcontainer" data-type="education">
                <?php if (!empty($user->educations)):?>
                    <?foreach($user->educations as $education):?>
                    <form class="t-work-area_blur__box t-work-area_blur__box_30 js-blocked" action="/profile/business/cv/education/update" method="POST">
                        <div class="t-input">
                            <span>Период обучения</span>
                            <div class="t-dateBox">
                                <input type="hidden" name="fd[date_from]" value="<?=$education->date_from?>" class="validate">
                                <div class="js-datepicker t-input_date disabled js-disabled">Выберите дату начала</div>
                            </div>
                            <div class="t-dateBox">
                                <input type="hidden" name="fd[date_to]" value="<?=$education->date_to?>" class="validate">
                                <div class="js-datepicker t-input_date disabled js-disabled">Выберите дату окончания</div>
                            </div>
                        </div>
                        <div class="t-input">
                            <span>Учебное заведение</span>
                            <input name="fd[university]" value="<?=$education->university?>" disabled class="validate js-showButton_input" >
                        </div>
                        <div class="t-input">
                            <span>Город</span>
                            <select name="fd[town_id]" disabled class="js-styler js-showButton_select">
                                <?foreach($towns as $town):?>
                                    <option <?if($education->town_id == $town->id):?>selected<?endif;?> value="<?=$town->id?>"><?=$town->name?></option>
                                <?endforeach;?>
                            </select>
                        </div>
                        <div class="t-input">
                            <span>Специальность</span>
                            <input name="fd[spec]" value="<?=$education->spec?>" disabled class="validate js-showButton_input">
                        </div>
                        <div class="t-btnBox">
                            <a class="t-btn js-edit js-hidden_box " data-blocked="blocked" href="#edit">Изменить</a>
                            <a href="#del" class="t-del js-del js-hidden_box " data-type="education">
                                <svg width="10" height="15" viewBox="0 0 10 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <path fill="#CCCCCC" d="M 5.0001 0C 5.55283 0 6.0001 0.447693 6.0001 1L 6.0001 2L 9.0001 2C 9.55283 2 10.0001 2.44769 10.0001 3C 10.0001 3.55231 9.55283 4 9.0001 4L 5.0001 4L 1.0001 4C 0.447362 4 9.62968e-05 3.55231 9.62968e-05 3C 9.62968e-05 2.44769 0.447362 2 1.0001 2L 4.0001 2L 4.0001 1C 4.0001 0.447693 4.44736 0 5.0001 0ZM 0.250096 5.3382C 0.440526 5.12317 0.712987 5 1.0001 5L 9.0001 5C 9.28721 5 9.55967 5.12317 9.7501 5.3382C 9.93955 5.55322 10.0284 5.83942 9.99228 6.12402L 8.99228 14.124C 8.92978 14.6245 8.504 15 8.0001 15L 2.0001 15C 1.49619 15 1.07041 14.6245 1.00791 14.124L 0.0079088 6.12402C -0.028224 5.83942 0.0606432 5.55322 0.250096 5.3382ZM 2.13291 7L 2.88291 13L 7.11728 13L 7.86728 7L 2.13291 7Z"></path>
                                </svg>
                            </a>
                            <div class="js-hidden_box hidden">
                                <input type="hidden" name="id" value="<?=$education->id?>">
                                <button class="t-btn t-btn_120 js-add_info" disabled data-blocked="blocked">Сохранить</button>
                                <a href="#cancel" class="t-btn t-btn_border js-clear-form t-btn_120" data-blocked="blocked">Отмена</a>
                            </div>
                        </div>
                    </form>
                    <?endforeach;?>
                <?php endif;?>
                <div class="t-addBox"><a href="#add" data-type="education" data-blocked="blocked"
                                         class="t-btn js-addBox">Добавить</a></div>
            </div>
            <h2>Опыт работы</h2>
            <div class="js-addcontainer" data-type="experience">
                <?php if (!empty($user->experiences)):?>
                    <?foreach($user->experiences as $experience):?>
                        <form class="t-work-area_blur__box t-work-area_blur__box_30 js-blocked" action="/profile/business/cv/experience/update" method="POST">
                            <div class="t-input">
                                <span>Период работы</span>
                                <div class="t-dateBox">
                                    <input type="hidden" name="fd[date_from]" class="js-showButton_input"  value="<?=$experience->date_from?>">
                                    <div class="js-datepicker t-input_date disabled js-disabled" data-max="true">Выберите дату начала</div>
                                </div>
                                <div class="t-dateBox <?if($experience->current):?>hidden<?endif;?>">
                                    <input type="hidden" name="fd[date_to]" class="js-showButton_input" value="<?=$experience->date_to?>" placeholder="Выберите дату окончания">
                                    <div class="js-datepicker t-input_date disabled js-disabled" data-max="true">Выберите дату окончания</div>
                                </div>
                                <input type="checkbox" name="fd[current]" disabled class="checkbox js-until-now js-showButton_input t-until-now" value="<?if($experience->current):?>1<?else:?>0<?endif;?>" id="until_now_<?=$experience->id?>" <?if($experience->current):?>checked<?endif;?>>
                                <label for="until_now_<?=$experience->id?>">Работаю в настоящее время</label>
                            </div>
                            <div class="t-input">
                                <span>Компания</span>
                                <input name="fd[company]" disabled class="validate js-showButton_input" value="<?=$experience->company?>">
                            </div>
                            <div class="t-input">
                                <span>Должность</span>
                                <input name="fd[post]" disabled class="validate js-showButton_input" value="<?=$experience->post?>">
                            </div>
                            <div class="t-input">
                                <span>Обязанности</span>
                                <input name="fd[duties]" disabled class="validate js-showButton_input" value="<?=$experience->duties?>">
                            </div>
                            <div class="t-btnBox">
                                <a class="t-btn js-edit js-hidden_box " href="#edit" data-blocked="blocked">Изменить</a>
                                <a href="#del" class="t-del js-del js-hidden_box " data-type="experience">
                                    <svg width="10" height="15" viewBox="0 0 10 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <path fill="#CCCCCC" d="M 5.0001 0C 5.55283 0 6.0001 0.447693 6.0001 1L 6.0001 2L 9.0001 2C 9.55283 2 10.0001 2.44769 10.0001 3C 10.0001 3.55231 9.55283 4 9.0001 4L 5.0001 4L 1.0001 4C 0.447362 4 9.62968e-05 3.55231 9.62968e-05 3C 9.62968e-05 2.44769 0.447362 2 1.0001 2L 4.0001 2L 4.0001 1C 4.0001 0.447693 4.44736 0 5.0001 0ZM 0.250096 5.3382C 0.440526 5.12317 0.712987 5 1.0001 5L 9.0001 5C 9.28721 5 9.55967 5.12317 9.7501 5.3382C 9.93955 5.55322 10.0284 5.83942 9.99228 6.12402L 8.99228 14.124C 8.92978 14.6245 8.504 15 8.0001 15L 2.0001 15C 1.49619 15 1.07041 14.6245 1.00791 14.124L 0.0079088 6.12402C -0.028224 5.83942 0.0606432 5.55322 0.250096 5.3382ZM 2.13291 7L 2.88291 13L 7.11728 13L 7.86728 7L 2.13291 7Z"></path>
                                    </svg>
                                </a>
                                <div class="js-hidden_box hidden">
                                    <input type="hidden" name="id" value="<?=$experience->id?>">
                                    <button class="t-btn t-btn_120 js-add_info" disabled data-blocked="blocked">Сохранить</button>
                                    <a href="#cancel" class="t-btn t-btn_border js-clear-form t-btn_120" data-blocked="blocked">Отмена</a>
                                </div>
                            </div>
                        </form>
                    <?endforeach;?>
                <?php endif;?>
                <div class="t-addBox"><a href="#add" data-type="experience" data-blocked="blocked"
                                         class="t-btn js-addBox">Добавить</a></div>
            </div>
        </div>

        <form id="personal_info" action="<?= \yii\helpers\Url::to(['/registration/business/save-cv']) ?>" method="POST">
            <div class="t-btnBox">
                <input type="hidden" name="_csrf" value="<?= \Yii::$app->request->getCsrfToken() ?>">
                <button class="t-btn">Сохранить и продолжить</button>
                <a href="/registration/business/data" class="t-btn t-btn_border t-btn_white">Шаг назад</a>
                <a href="/profile/business/feed" class="t-btn t-btn_border t-btn_white">Пропустить</a>
            </div>
        </form>
    </div>
</main>

<div class="t-templates">
    <form class="t-work-area_blur__box t-work-area_blur__box_30 js-blocked"
          action="/profile/business/cv/specialization/add" method="POST" data-type="specializations">
        <div class="t-input">
            <span class="js-hidden_box js-radioText hidden">Практикуемая отрасль</span>
            <span class="js-hidden_box">Отрасль</span>
            <input type="radio" class="radio" id="practiced_" name="fd[type_industry]" checked value="praktisch">
            <label for="practiced_" class="js-hidden_box">Практикуемая</label>
            <input type="radio" class="radio" id="interested_" name="fd[type_industry]" value="interesse">
            <label for="interested_" class="js-hidden_box">Интересующая</label>
            <div class="t-select">
                <select class="js-selectSearch js-showButton_select" name="fd[industry]"
                        data-placeholder="Введите отрасль" data-search-placeholder="Введите отрасль"
                        data-search-not-found="Совпадений не найдено.">
                    <option selected value="-1"></option>
                    <? foreach ($industries as $industry): ?>
                        <option value="<?= $industry->id ?>"><?= $industry->name ?></option>
                    <? endforeach; ?>
                </select>
            </div>
        </div>
        <div class="t-input">
            <span>Специализация</span>
            <div class="t-select">
                <select class="js-selectSearch js-showButton_select" name="fd[specialization]"
                        data-placeholder="Введите специализацию" data-search-placeholder="Введите специализацию"
                        data-search-not-found="Совпадений не найдено.">
                    <option selected value="-1"></option>
                    <? foreach ($specializations as $specialization): ?>
                        <option value="<?= $specialization->id ?>"><?= $specialization->name ?></option>
                    <? endforeach; ?>
                </select>
            </div>
        </div>
        <div class="t-input">
            <span>Опыт</span>
            <select class="js-styler validate js-showButton_select" name="fd[complexity]">
                <option value="-1" selected disabled class="hidden_option">Выберите из списка</option>
                <? foreach (\common\models\Task::COMPLEXITY as $val => $label): ?>
                    <option value="<?= $val ?>"><?= $label ?></option>
                <? endforeach; ?>
            </select>
        </div>
        <div class="t-input">
            <span>Навыки через запятую</span>
            <div class="t-tag-list js-tab-list">
                <ul class="t-tag-list__box  js-disabled">
                    <li>
                        <input class="js-tags-set" placeholder="Укажите через запятую">
                    </li>
                </ul>
            </div>
        </div>
        <div class="t-btnBox">
            <a class="t-btn js-edit js-hidden_box hidden" href="#edit" data-blocked="blocked">Изменить</a>
            <a href="#del" class="t-del js-del js-hidden_box hidden" data-type="specialization">
                <svg width="10" height="15" viewBox="0 0 10 15" version="1.1" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink">
                    <path fill="#CCCCCC"
                          d="M 5.0001 0C 5.55283 0 6.0001 0.447693 6.0001 1L 6.0001 2L 9.0001 2C 9.55283 2 10.0001 2.44769 10.0001 3C 10.0001 3.55231 9.55283 4 9.0001 4L 5.0001 4L 1.0001 4C 0.447362 4 9.62968e-05 3.55231 9.62968e-05 3C 9.62968e-05 2.44769 0.447362 2 1.0001 2L 4.0001 2L 4.0001 1C 4.0001 0.447693 4.44736 0 5.0001 0ZM 0.250096 5.3382C 0.440526 5.12317 0.712987 5 1.0001 5L 9.0001 5C 9.28721 5 9.55967 5.12317 9.7501 5.3382C 9.93955 5.55322 10.0284 5.83942 9.99228 6.12402L 8.99228 14.124C 8.92978 14.6245 8.504 15 8.0001 15L 2.0001 15C 1.49619 15 1.07041 14.6245 1.00791 14.124L 0.0079088 6.12402C -0.028224 5.83942 0.0606432 5.55322 0.250096 5.3382ZM 2.13291 7L 2.88291 13L 7.11728 13L 7.86728 7L 2.13291 7Z"></path>
                </svg>
            </a>
            <div class="js-hidden_box">
                <input type="hidden" name="id" value="">
                <button class="t-btn t-btn_120 js-add_info" disabled data-blocked="blocked">Сохранить</button>
                <a href="#cancel" class="t-btn t-btn_border js-clear-form t-btn_120" data-blocked="blocked"
                   data-del="on">Отмена</a>
            </div>
        </div>
    </form>
    <form class="t-work-area_blur__box t-work-area_blur__box_30 js-blocked" action="/profile/business/cv/education/add"
          method="POST" data-type="education">
        <div class="t-input">
            <span>Период обучения</span>
            <div class="t-dateBox">
                <input type="hidden" name="fd[date_from]" value="" class="validate">
                <div class="js-datepicker t-input_date js-disabled">Выберите дату начала</div>
            </div>
            <div class="t-dateBox">
                <input type="hidden" name="fd[date_to]" value="" class="validate">
                <div class="js-datepicker t-input_date js-disabled">Выберите дату окончания</div>
            </div>
        </div>
        <div class="t-input">
            <span>Учебное заведение</span>
            <input name="fd[university]" class="validate js-showButton_input">
        </div>
        <div class="t-input">
            <span>Город</span>
            <select name="fd[town_id]" class="js-styler js-showButton_select">
                <? foreach ($towns as $town): ?>
                    <option value="<?= $town->id ?>"><?= $town->name ?></option>
                <? endforeach; ?>
            </select>
        </div>
        <div class="t-input">
            <span>Специальность</span>
            <input name="fd[spec]" class="validate js-showButton_input">
        </div>
        <div class="t-btnBox">
            <a class="t-btn js-edit js-hidden_box hidden" href="#edit" data-blocked="blocked">Изменить</a>
            <a href="#del" class="t-del js-del js-hidden_box hidden" data-type="education">
                <svg width="10" height="15" viewBox="0 0 10 15" version="1.1" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink">
                    <path fill="#CCCCCC"
                          d="M 5.0001 0C 5.55283 0 6.0001 0.447693 6.0001 1L 6.0001 2L 9.0001 2C 9.55283 2 10.0001 2.44769 10.0001 3C 10.0001 3.55231 9.55283 4 9.0001 4L 5.0001 4L 1.0001 4C 0.447362 4 9.62968e-05 3.55231 9.62968e-05 3C 9.62968e-05 2.44769 0.447362 2 1.0001 2L 4.0001 2L 4.0001 1C 4.0001 0.447693 4.44736 0 5.0001 0ZM 0.250096 5.3382C 0.440526 5.12317 0.712987 5 1.0001 5L 9.0001 5C 9.28721 5 9.55967 5.12317 9.7501 5.3382C 9.93955 5.55322 10.0284 5.83942 9.99228 6.12402L 8.99228 14.124C 8.92978 14.6245 8.504 15 8.0001 15L 2.0001 15C 1.49619 15 1.07041 14.6245 1.00791 14.124L 0.0079088 6.12402C -0.028224 5.83942 0.0606432 5.55322 0.250096 5.3382ZM 2.13291 7L 2.88291 13L 7.11728 13L 7.86728 7L 2.13291 7Z"></path>
                </svg>
            </a>
            <div class="js-hidden_box">
                <input type="hidden" name="id" value="">
                <button class="t-btn t-btn_120 js-add_info" disabled data-blocked="blocked">Сохранить</button>
                <a href="#cancel" class="t-btn t-btn_border js-clear-form t-btn_120" data-blocked="blocked"
                   data-del="on">Отмена</a>
            </div>
        </div>
    </form>
    <form class="t-work-area_blur__box t-work-area_blur__box_30 js-blocked" action="/profile/business/cv/experience/add"
          method="POST" data-type="experience">
        <div class="t-input">
            <span>Период работы</span>
            <div class="t-dateBox">
                <input type="hidden" name="fd[date_from]" class="js-showButton_input" value="">
                <div class="js-datepicker t-input_date js-disabled" data-max="true">Выберите дату начала</div>
            </div>
            <div class="t-dateBox">
                <input type="hidden" name="fd[date_to]" class="js-showButton_input" value="">
                <div class="js-datepicker t-input_date js-disabled" data-max="true">Выберите дату окончания</div>
            </div>
            <input type="checkbox" name="fd[current]" class="checkbox js-until-now t-until-now" id="until_now_"
                   value="0">
            <label for="until_now_">Работаю в настоящее время</label>
        </div>
        <div class="t-input">
            <span>Компания</span>
            <input name="fd[company]" class="validate js-showButton_input">
        </div>
        <div class="t-input">
            <span>Должность</span>
            <input name="fd[post]" class="validate js-showButton_input">
        </div>
        <div class="t-input">
            <span>Обязанности</span>
            <input name="fd[duties]" class="validate js-showButton_input">
        </div>
        <div class="t-btnBox">
            <a class="t-btn js-edit js-hidden_box hidden" href="#edit" data-blocked="blocked">Изменить</a>
            <a href="#del" class="t-del js-del js-hidden_box hidden" data-type="experience">
                <svg width="10" height="15" viewBox="0 0 10 15" version="1.1" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink">
                    <path fill="#CCCCCC"
                          d="M 5.0001 0C 5.55283 0 6.0001 0.447693 6.0001 1L 6.0001 2L 9.0001 2C 9.55283 2 10.0001 2.44769 10.0001 3C 10.0001 3.55231 9.55283 4 9.0001 4L 5.0001 4L 1.0001 4C 0.447362 4 9.62968e-05 3.55231 9.62968e-05 3C 9.62968e-05 2.44769 0.447362 2 1.0001 2L 4.0001 2L 4.0001 1C 4.0001 0.447693 4.44736 0 5.0001 0ZM 0.250096 5.3382C 0.440526 5.12317 0.712987 5 1.0001 5L 9.0001 5C 9.28721 5 9.55967 5.12317 9.7501 5.3382C 9.93955 5.55322 10.0284 5.83942 9.99228 6.12402L 8.99228 14.124C 8.92978 14.6245 8.504 15 8.0001 15L 2.0001 15C 1.49619 15 1.07041 14.6245 1.00791 14.124L 0.0079088 6.12402C -0.028224 5.83942 0.0606432 5.55322 0.250096 5.3382ZM 2.13291 7L 2.88291 13L 7.11728 13L 7.86728 7L 2.13291 7Z"></path>
                </svg>
            </a>
            <div class="js-hidden_box">
                <input type="hidden" name="id" value="">
                <button class="t-btn t-btn_120 js-add_info" disabled data-blocked="blocked">Сохранить</button>
                <a href="#cancel" class="t-btn t-btn_border js-clear-form t-btn_120" data-blocked="blocked"
                   data-del="on">Отмена</a>
            </div>
        </div>
    </form>
</div>