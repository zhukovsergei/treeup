<div class="t-background-img"><img src="/img/registration.jpg"></div>
<main>
    <h1 class="white">Задача</h1>
    <div class="t-tabs">
        <select class="only-mobyle t-big-select js-big-select">
            <option value="/tasks/<?= $row->id ?>/view">Общее</option>
            <option value="/tasks/<?= $row->id ?>/log">Обновления</option>
            <option value="/tasks/<?= $row->id ?>/files" selected>Файлы</option>
        </select>
        <a href="/tasks/<?= $row->id ?>/view">Общее</a>
        <a href="/tasks/<?= $row->id ?>/log">Обновления</a>
        <a href="/tasks/<?= $row->id ?>/files" class="active">Файлы</a>
    </div>
    <div class="t-work-area t-work-area_blur">
        <div class="t-work-area__background"></div>

        <div class="t-participant t-work-area_blur__box">
            <div class="t-participant__collumn_full">
                <? if (count($files)): ?>
                    <div class="t-fileBox t-load-file t-load-file_page">
                        <?php if ($row->status == \common\models\Task::STATUS_DB['published']): ?>
                            <form action="/tasks/files/upload" method="post" data-id="<?= $row->id ?>">
                                <label class="t-btn">Добавить файлы
                                    <input type="file" name="files" multiple class="js-setFile">
                                </label>
                            </form>
                        <?php endif; ?>
                        <ul class="js-file-list">
                            <? foreach ($indexedFiles as $formatted_date => $collapseDate): ?>
                                <li>
                                    <p class="date"><?= $formatted_date; ?></p>
                                    <? foreach ($collapseDate as $file): ?>
                                        <div class="t-load-file__item">
                                            <a href="<?= $file->getUploadedFileUrl('name') ?>" download="" class="name">
                                                <img src="/img/file.png"> <?= $file->name ?></a>

                                        </div>
                                    <? endforeach; ?>
                                </li>
                            <? endforeach; ?>
                        </ul>

                    </div>
                <? else: ?>
                    <div class="t-defaultMsg_file">
                        <h2>Сейчас здесь пусто</h2>
                        <p>Здесь будет отображаться список загруженных вами файлов</p>
                        <form action="/tasks/files/upload" method="post" data-id="<?= $row->id ?>" class="t-load-file">
                            <label class="t-btn">Прикрепить файлы
                                <input type="file" name="files" multiple class="js-setFile">
                            </label>
                        </form>
                    </div>
                <? endif; ?>
            </div>
        </div>

        <!--    <div class="t-work-area_blur__box">-->
        <!--      <div class="t-load-file t-load-file_page">-->
        <!--          <form action="/tasks/files/upload" method="post" data-id="--><? //=$row->id?><!--">-->
        <!--              <label class="t-btn">Добавить файлы-->
        <!--                  <input type="file" name="files" multiple class="js-setFile">-->
        <!--              </label>-->
        <!--          </form>-->
        <!--        <ul class="js-file-list">-->
        <!--          --><? //foreach($files as $file):?>
        <!--            <li data-id="--><? //=$file->id?><!--">-->
        <!--                <div class="t-load-file__item">-->
        <!--                    <a href="-->
        <? //=$file->getUploadedFileUrl('name')?><!--" download class="name"><img src="/img/file.png">-->
        <? //=$file->name?><!--</a>-->
        <!--                    <a class="js-del-file_2" data-id="--><? //=$file->id?><!--">-->
        <!--                        <svg width="10" height="15" viewBox="0 0 10 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path fill="#CCCCCC" d="M 5.0001 0C 5.55283 0 6.0001 0.447693 6.0001 1L 6.0001 2L 9.0001 2C 9.55283 2 10.0001 2.44769 10.0001 3C 10.0001 3.55231 9.55283 4 9.0001 4L 5.0001 4L 1.0001 4C 0.447362 4 9.62968e-05 3.55231 9.62968e-05 3C 9.62968e-05 2.44769 0.447362 2 1.0001 2L 4.0001 2L 4.0001 1C 4.0001 0.447693 4.44736 0 5.0001 0ZM 0.250096 5.3382C 0.440526 5.12317 0.712987 5 1.0001 5L 9.0001 5C 9.28721 5 9.55967 5.12317 9.7501 5.3382C 9.93955 5.55322 10.0284 5.83942 9.99228 6.12402L 8.99228 14.124C 8.92978 14.6245 8.504 15 8.0001 15L 2.0001 15C 1.49619 15 1.07041 14.6245 1.00791 14.124L 0.0079088 6.12402C -0.028224 5.83942 0.0606432 5.55322 0.250096 5.3382ZM 2.13291 7L 2.88291 13L 7.11728 13L 7.86728 7L 2.13291 7Z"></path></svg>-->
        <!--                    </a>-->
        <!--                </div>-->
        <!--            </li>-->
        <!--          --><? //endforeach;?>
        <!--        </ul>-->
        <!--      </div>-->
        <!--    </div>-->
    </div>
</main>