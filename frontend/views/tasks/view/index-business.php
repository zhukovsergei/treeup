<div class="t-background-img"><img src="/img/registration.jpg"></div>
<main>
    <h1 class="white">Задача</h1>
    <div class="t-tabs">
        <select class="only-mobyle t-big-select js-big-select">
            <option value="/tasks/<?= $row->id ?>/view" selected>Общее</option>
            <option value="/tasks/<?= $row->id ?>/log">Обновления</option>
            <option value="/tasks/<?= $row->id ?>/files">Файлы</option>
        </select>
        <a href="/tasks/<?= $row->id ?>/view" class="active">Общее</a>
        <a href="/tasks/<?= $row->id ?>/log">Обновления</a>
        <?php if($row->isParticipant()):?>
        <a href="/tasks/<?= $row->id ?>/files">Файлы</a>
        <?php endif;?>
    </div>
    <div class="t-work-area t-work-area_blur">
        <div class="t-work-area__background"></div>
        <div class="t-task-list">
            <div class="t-task-custumer t-task-custumer_start" data-id="<?= $row->id ?>">
                <div class="t-task-custumer__description">
                    <h2>Описание</h2>
                    <div class="box">
                        <h2><?= $row->name ?></h2>
                        <!--            <h3>Создание интернет-магазина</h3>-->
                        <div class="date">Завершится <?= $row->date_end ?></div>
                        <div class="t-custumer-info">
                            <img src="<?= \aiur\helpers\user\LogoHelper::getUserLogoUrl($row->creator) ?>">
                            <h4><?= \aiur\helpers\user\UserHelper::getUserName($row->creator) ?></h4>
                            <!--              <p>Рейтинг 5.0</p>-->
                        </div>
                        <div class="text">
                            <?= $row->text ?>
                        </div>
                        <? if ($creatorFiles): ?>
                            <div class="t-load-file">
                                <h3>Материалы</h3>
                                <ul>
                                    <? foreach ($creatorFiles as $file): ?>
                                        <li><a target="_blank" href="<?= $file->getUploadedFileUrl('name') ?>"
                                               class="name"><img src="/img/file.png" class="file"><?= $file->name ?></a>
                                        </li>
                                    <? endforeach; ?>
                                </ul>
                            </div>
                        <? endif; ?>
                    </div>
                </div>
                <div class="t-task-custumer__info t-task-custumer__info_long">
                    <h2>Действия</h2>

                    <?= \frontend\widgets\TaskViewAssignButton::widget([
                        'user' => \Yii::$app->getUser()->getIdentity(),
                        'task' => $row
                    ]) ?>

                    <h2>Информация</h2>
                    <ul class="t-task-custumer__info_long__box list">
                        <li>
                            <h3>Тип задачи</h3>
                            <p><?= \common\models\Task::TYPE[$row->type_id] ?? '—' ?></p>
                        </li>
                        <li>
                            <h3>Специализация</h3>
                            <p><?= $row->specialization->name ?? '—' ?></p>
                        </li>
                        <li>
                            <h3>Навыки</h3>
                            <p>
                                <? if ($row->skills): ?>
                                    <?= implode(', ', \yii\helpers\ArrayHelper::getColumn($row->skills, 'name')) ?>
                                <? else: ?>
                                    —
                                <? endif; ?>
                            </p>
                        </li>
                        <li>
                            <h3>Регион</h3>
                            <p><?= $row->town->name ?? '—' ?></p>
                        </li>
                        <li>
                            <h3>Отрасль</h3>
                            <p><?= $row->industry->name ?? '—' ?></p>
                        </li>
                        <li>
                            <h3>Уровень</h3>
                            <p><?= \common\models\Task::COMPLEXITY[$row->complexity] ?? '—' ?></p>
                        </li>
                        <li>
                            <h3>Победителю</h3>
                            <p><?= $row->points_win ?? 0 ?></p>
                        </li>
                        <li>
                            <h3>Участнику</h3>
                            <p><?= $row->points_participant ?? 0 ?></p>
                        </li>
                        <li>
                            <h3>Привилегия</h3>
                            <p><?= \common\models\Task::PRIVILEGE[$row->privilege] ?? '—' ?></p>
                        </li>
                        <?php if ($row->privilege == \common\models\Task::PRIVILEGE_DB['Приз']): ?>
                            <li>
                                <h3>Описание привилегии</h3>
                                <p><?= $row->privilege_description ?></p>
                            </li>
                        <?php elseif ($row->privilege == \common\models\Task::PRIVILEGE_DB['TU']): ?>
                            <li>
                                <h3>Количество TU</h3>
                                <p><?= $row->tu_count ?></p>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
        <h2>Похожие задачи</h2>
        <div class="t-task-list"><a class="t-task-item t-task-item_white">
                <div class="t-task-item__img"><img src="/img/face.jpg"></div>
                <div class="t-task-item__info">
                    <div class="price"><img src="/img/bonus1.png"><span>2</span><img src="/img/bonus2.png"><span>1200/800</span>
                    </div>
                    <div class="level level_1"><span></span><span></span><span></span></div>
                    <div class="name">Роснефтегаз</div>
                    <div class="description">Разработать сайт-портал автомобильной тематики на Битриксе в красивой
                        упаковке, недорого и быстро чтобы сайт-портал автомобильной тематики на Битриксе в красивой
                        упаковке, недорого и быстро чтобы сайт-портал автомобильной тематики на Битриксе в красивой
                        упаковке, недорого и быстро чтобы
                    </div>
                    <hr>
                    <ul class="base-info">
                        <li>2 недели</li>
                        <li>Интернет технологии</li>
                        <li>Санкт-Петербург</li>
                    </ul>
                </div>
            </a><a class="t-task-item t-task-item_white">
                <div class="t-task-item__img"><img src="/img/face.jpg"></div>
                <div class="t-task-item__info">
                    <div class="price"><img src="/img/bonus1.png"><span>2</span><img src="/img/bonus2.png"><span>1200/800</span>
                    </div>
                    <div class="level level_2"><span></span><span></span><span></span></div>
                    <div class="name">Роснефтегаз</div>
                    <div class="description">Разработать сайт-портал автомобильной тематики на Битриксе в красивой
                        упаковке, недорого и быстро чтобы сайт-портал автомобильной тематики на Битриксе в красивой
                        упаковке, недорого и быстро чтобы сайт-портал автомобильной тематики на Битриксе в красивой
                        упаковке, недорого и быстро чтобы
                    </div>
                    <hr>
                    <ul class="base-info">
                        <li>2 недели</li>
                        <li>Интернет технологии</li>
                        <li>Санкт-Петербург</li>
                    </ul>
                </div>
            </a><a class="t-task-item t-task-item_white">
                <div class="t-task-item__img"><img src="/img/face.jpg"></div>
                <div class="t-task-item__info">
                    <div class="price"><img src="/img/bonus1.png"><span>2</span><img src="/img/bonus2.png"><span>1200/800</span>
                    </div>
                    <div class="level level_3"><span></span><span></span><span></span></div>
                    <div class="name">Роснефтегаз</div>
                    <div class="description">Разработать сайт-портал автомобильной тематики на Битриксе в красивой
                        упаковке, недорого и быстро чтобы сайт-портал автомобильной тематики на Битриксе в красивой
                        упаковке, недорого и быстро чтобы сайт-портал автомобильной тематики на Битриксе в красивой
                        упаковке, недорого и быстро чтобы
                    </div>
                    <hr>
                    <ul class="base-info">
                        <li>2 недели</li>
                        <li>Интернет технологии</li>
                        <li>Санкт-Петербург</li>
                    </ul>
                </div>
            </a><a class="t-task-item t-task-item_white">
                <div class="t-task-item__img"><img src="/img/face.jpg"></div>
                <div class="t-task-item__info">
                    <div class="price"><img src="/img/bonus1.png"><span>2</span><img src="/img/bonus2.png"><span>1200/800</span>
                    </div>
                    <div class="level level_1"><span></span><span></span><span></span></div>
                    <div class="name">Роснефтегаз</div>
                    <div class="description">Разработать сайт-портал автомобильной тематики на Битриксе в красивой
                        упаковке, недорого и быстро чтобы сайт-портал автомобильной тематики на Битриксе в красивой
                        упаковке, недорого и быстро чтобы сайт-портал автомобильной тематики на Битриксе в красивой
                        упаковке, недорого и быстро чтобы
                    </div>
                    <hr>
                    <ul class="base-info">
                        <li>2 недели</li>
                        <li>Интернет технологии</li>
                        <li>Санкт-Петербург</li>
                    </ul>
                </div>
            </a><a class="t-task-item t-task-item_white">
                <div class="t-task-item__img"><img src="/img/face.jpg"></div>
                <div class="t-task-item__info">
                    <div class="price"><img src="/img/bonus1.png"><span>2</span><img src="/img/bonus2.png"><span>1200/800</span>
                    </div>
                    <div class="level level_1"><span></span><span></span><span></span></div>
                    <div class="name">Роснефтегаз</div>
                    <div class="description">Разработать сайт-портал автомобильной тематики на Битриксе в красивой
                        упаковке, недорого и быстро чтобы сайт-портал автомобильной тематики на Битриксе в красивой
                        упаковке, недорого и быстро чтобы сайт-портал автомобильной тематики на Битриксе в красивой
                        упаковке, недорого и быстро чтобы
                    </div>
                    <hr>
                    <ul class="base-info">
                        <li>2 недели</li>
                        <li>Интернет технологии</li>
                        <li>Санкт-Петербург</li>
                    </ul>
                </div>
            </a>
        </div>
    </div>
</main>