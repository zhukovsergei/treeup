<?php

use common\models\Task;

?>
<div class="t-background-img"><img src="/img/registration.jpg"></div>
<main>
    <h1 class="white">Создание</h1>
    <div class="t-work-area t-work-area_blur _margin">
        <div class="t-work-area__background"></div>
        <form action="/tasks/create" method="POST" class="t-new-task-form">
            <h2 class="js-title">Новая задача</h2>
            <div class="t-input"><span>Название задачи</span>
                <input name="fd[name]" placeholder="Например — Запустить интернет-магазин по продаже детских игрушек"
                       class="validate">
            </div>
            <div class="t-input-box">
                <div class="t-input t-input_30"><span>Тип задачи</span>
                    <select data-value="1" name="fd[type_id]" class="js-styler validate js-taskType">
                        <option selected value="0" disabled class="hidden_option">Выбрать</option>
                        <?php foreach (Task::TYPE as $db => $label): ?>
                            <option value="<?= $db ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="t-input t-input_30"><span>Проект</span>
                    <select data-value="1" name="fd[project_id]" class="js-styler">
                        <option selected value="0">Без проекта</option>
                        <?php if (!empty($projects)): ?>
                            <?php foreach ($projects as $project): ?>
                                <option value="<?= $project->id ?>"><?= $project->name ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div class="t-input t-input_30"><span>Регионы</span>
                    <select data-value="1" name="fd[town_id]" class="js-styler validate">
                        <option selected value="0">Любой</option>
                        <?php if (!empty($towns)): ?>
                            <?php foreach ($towns as $town): ?>
                                <option value="<?= $town->id ?>"><?= $town->name ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div class="t-input t-input_30"><span>Сфера</span>
                    <select data-value="1" name="fd[industry_id]" class="js-styler validate">
                        <option selected value="0" disabled class="hidden_option">Выбрать</option>
                        <?php if (!empty($industries)): ?>
                            <?php foreach ($industries as $industry): ?>
                                <option value="<?= $industry->id ?>"><?= $industry->name ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div class="t-input t-input_30"><span>Уровень сложности</span>
                    <select data-value="1" name="fd[complexity]" class="js-styler validate js-poits">
                        <option selected value="0" disabled class="hidden_option">Выбрать</option>
                        <?php foreach (Task::COMPLEXITY as $k => $v): ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="t-input t-input_30"><span>Специализация</span>
                    <select data-value="1" name="fd[specialization_id]" class="js-styler validate">
                        <option selected value="0" disabled class="hidden_option">Выбрать</option>
                        <?php if (!empty($specializations)): ?>
                            <?php foreach ($specializations as $specialization): ?>
                                <option value="<?= $specialization->id ?>"><?= $specialization->name ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>
            <div class="t-input"><span>Навыки через запятую</span>
                <div class="t-tag-list js-tab-list">
                    <ul class="t-tag-list__box">
                        <li>
                            <input placeholder="Укажите через запятую" class="js-tags-set full-width">
                        </li>
                    </ul>
                </div>
            </div>
            <div class="t-input"><span>Описание</span>
                <textarea name="fd[text]" class="validate"></textarea>
                <!--        <input name="fd[text]" placeholder="Нажмите для редактирования" class="validate">-->
            </div>
            <div class="t-input"><span>Прикреплённые файлы</span>
                <div class="t-load-file">
                    <ul class="js-file-list" data-max="10">
                        <li class="load">
                            <div class="t-load-file__item">
                            </div>
                            <div class="t-load-file__controls">
                                <label class="js-load t-btn t-btn_border load">Прикрепить файл
                                    <input type="file" name="files" class="success">
                                </label>
                                <span>doc, .xls, .ppt, .odt, .pdf и .odp</span>
                            </div>
                        </li>
                        <li>
                            <div class="t-load-file__controls">
                                <label class="js-load t-btn t-btn_border">Прикрепить файл
                                    <input type="file" name="files">
                                </label>
                                <span>doc, .xls, .ppt, .odt, .pdf и .odp</span>
                            </div>
                        </li>
                    </ul>
                    <span class="t-load-file_help">
            Загружено <span class="js-fileWeight">0</span> MБ из 15 MБ доступных
          </span>
                </div>
            </div>
            <div class="t-input-box">
                <div class="t-input t-input_30"><span>Окончание приёма заявок</span>
                    <input type="hidden" name="fd[date_end]" class="validate" value="<?= date('Y-m-d') ?>">
                    <div class="js-datepicker t-input_date"></div>
                </div>
                <div class="t-input t-input_30"><span>Очки для победителя</span>
                    <select data-value="1" name="fd[points_win]" disabled class="js-styler validate">
                        <option selected value="-1" disabled class="hidden_option">Выберите вознаграждение</option>
                    </select>
                </div>
                <div class="t-input t-input_30"><span>Очки для участника</span>
                    <select data-value="1" name="fd[points_participant]" disabled class="js-styler validate">
                        <option selected value="-1" disabled class="hidden_option">Выберите вознаграждение</option>
                    </select>
                </div>
            </div>
            <div class="t-input-box">
                <div class="t-input t-input_30">
                    <span>Привилегия</span>
                    <select data-value="1" name="fd[privilege]" class="js-styler validate js-privilege">
                        <?php $first = true; ?>
                        <?php foreach (Task::PRIVILEGE as $val => $name): ?>
                            <option <?= $first ? 'selected' : '';
                            $first = false; ?> value="<?= $val ?>"><?= $name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="t-input t-input_60 js-privilege__box hidden" data-type="<?=Task::PRIVILEGE_DB['Приз']?>">
                    <span>Описание привилегии</span>
                    <input name="fd[privilege_description]" placeholder="Например, место в основном штате">
                </div>
                <div class="t-input t-input_30 js-privilege__box hidden" data-type="<?=Task::PRIVILEGE_DB['TU']?>">
                    <span>Количество</span>
                    <select data-value="<?=Task::PRIVILEGE_DB['Нет']?>" name="fd[tu_count]" class="js-styler validate js_price">
                        <? $first = true; ?>
                        <?php foreach (Task::TU_COUNT as $tu): ?>
                            <option <?= $first ? 'selected' : '';
                            $first = false; ?> value="<?= $tu ?>"><?= $tu ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="t-input">
                <input name="fd[authored_visible]" id="only_auth" type="checkbox" class="checkbox">
                <label for="only_auth">Видна только зарегистрированным пользователям</label>
            </div>
            <?= \frontend\widgets\TaskPrice::widget(['user_id' => \Yii::$app->getUser()->getId()]) ?>
            <div class="t-input-box__buttons t-input-box__buttons_left" data-type="new">
                <a data-type="moderation" class="t-btn js-addTask" href="#moderation">Отправить на модерацию</a>
                <!--          <a href="#pay" class="t-btn js-pay">Пополнить счёт</a>--><!--Если не хватает средств-->
                <a data-type="draft" class="t-btn t-btn_border js-addTask" href="#draft">Сохранить как черновик</a>
            </div>
        </form>
    </div>
</main>