<?php

use common\models\Task;

?>
<div class="t-background-img"><img src="/img/registration.jpg"></div>
<main>
    <h1 class="white">Редактирование</h1>
    <div class="t-work-area t-work-area_blur _margin">
        <div class="t-work-area__background"></div>
        <form action="/tasks/update" method="POST" class="t-new-task-form">
            <div class="t-input"><span>Название задачи</span>
                <input name="fd[name]" placeholder="Например — Запустить интернет-магазин по продаже детских игрушек"
                       class="validate" value="<?= $task->name ?>">
            </div>
            <div class="t-input-box">
                <div class="t-input t-input_30"><span>Тип задачи</span>
                    <select data-value="1" name="fd[type_id]" class="js-styler validate js-taskType">
                        <option selected value="0" disabled class="hidden_option">Выбрать</option>
                        <?php foreach (Task::TYPE as $db => $label): ?>
                            <option <?= $task->type_id !== $db ?: 'selected' ?>
                                    value="<?= $db ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="t-input t-input_30"><span>Проект</span>
                    <select data-value="1" name="fd[project_id]" class="js-styler">
                        <option selected value="0">Без проекта</option>
                        <?php if (!empty($projects)): ?>
                            <?php foreach ($projects as $project): ?>
                                <option <?= $task->project_id !== $project->id ?: 'selected' ?>
                                        value="<?= $project->id ?>"><?= $project->name ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div class="t-input t-input_30"><span>Регионы</span>
                    <select data-value="1" name="fd[town_id]" class="js-styler validate">
                        <option selected value="0">Любой</option>
                        <?php if (!empty($towns)): ?>
                            <?php foreach ($towns as $town): ?>
                                <option <?= $task->town_id !== $town->id ?: 'selected' ?>
                                        value="<?= $town->id ?>"><?= $town->name ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div class="t-input t-input_30"><span>Сфера</span>
                    <select data-value="1" name="fd[industry_id]" class="js-styler validate">
                        <option selected value="0" disabled class="hidden_option">Выбрать</option>
                        <?php if (!empty($industries)): ?>
                            <?php foreach ($industries as $industry): ?>
                                <option <?= $task->industry_id !== $industry->id ?: 'selected' ?>
                                        value="<?= $industry->id ?>"><?= $industry->name ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div class="t-input t-input_30"><span>Уровень сложности</span>
                    <select data-value="1" name="fd[complexity]" class="js-styler validate js-poits">
                        <option selected value="0" disabled class="hidden_option">Выбрать</option>
                        <?php foreach (Task::COMPLEXITY as $k => $v): ?>
                            <option <?= $task->complexity !== $k ?: 'selected' ?> value="<?= $k ?>"><?= $v ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="t-input t-input_30"><span>Специализация</span>
                    <select data-value="1" name="fd[specialization_id]" class="js-styler validate">
                        <option selected value="0" disabled class="hidden_option">Выбрать</option>
                        <?php if (!empty($specializations)): ?>
                            <?php foreach ($specializations as $specialization): ?>
                                <option <?= $task->specialization_id !== $specialization->id ?: 'selected' ?>
                                        value="<?= $specialization->id ?>"><?= $specialization->name ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>
            <div class="t-input"><span>Навыки через запятую</span>
                <div class="t-tag-list js-tab-list">
                    <ul class="t-tag-list__box">
                        <?php if (!empty($task->skills)): ?>
                            <?php foreach ($task->skills as $skill): ?>
                                <li><p><?= $skill->name ?><span class="js-del_tag"></span></p></li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <li>
                            <input placeholder="Укажите через запятую" class="js-tags-set full-width">
                        </li>
                    </ul>
                </div>
            </div>
            <div class="t-input"><span>Описание</span>
                <textarea name="fd[text]" class="validate"><?= $task->text ?></textarea>
                <!--        <input name="fd[text]" placeholder="Нажмите для редактирования" class="validate">-->
            </div>
            <div class="t-input"><span>Прикреплённые файлы</span>
                <div class="t-load-file">
                    <ul class="js-file-list" data-max="10">
                        <?php if ($task->files): ?>
                            <?php foreach ($task->files as $file): ?>
                                <li class="load">
                                    <div class="t-load-file__item">
                                        <img src="/img/file.png">
                                        <?= $file->name ?> — <span
                                                class="js-fileWeight_load"><?= $file->getFileSizeMb() ?></span> MB
                                        <a class="js-del-file" data-id = <?=$file->id?>>
                                            <svg width="10" height="15" viewBox="0 0 10 15" version="1.1"
                                                 xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <path fill="#CCCCCC"
                                                      d="M 5.0001 0C 5.55283 0 6.0001 0.447693 6.0001 1L 6.0001 2L 9.0001 2C 9.55283 2 10.0001 2.44769 10.0001 3C 10.0001 3.55231 9.55283 4 9.0001 4L 5.0001 4L 1.0001 4C 0.447362 4 9.62968e-05 3.55231 9.62968e-05 3C 9.62968e-05 2.44769 0.447362 2 1.0001 2L 4.0001 2L 4.0001 1C 4.0001 0.447693 4.44736 0 5.0001 0ZM 0.250096 5.3382C 0.440526 5.12317 0.712987 5 1.0001 5L 9.0001 5C 9.28721 5 9.55967 5.12317 9.7501 5.3382C 9.93955 5.55322 10.0284 5.83942 9.99228 6.12402L 8.99228 14.124C 8.92978 14.6245 8.504 15 8.0001 15L 2.0001 15C 1.49619 15 1.07041 14.6245 1.00791 14.124L 0.0079088 6.12402C -0.028224 5.83942 0.0606432 5.55322 0.250096 5.3382ZM 2.13291 7L 2.88291 13L 7.11728 13L 7.86728 7L 2.13291 7Z"></path>
                                            </svg>
                                        </a>
                                    </div>
                                    <div class="t-load-file__controls">
                                        <label class="js-load t-btn t-btn_border load">Прикрепить файл
                                            <input type="file" name="files" class="success">
                                        </label>
                                        <span>doc, .xls, .ppt, .odt, .pdf и .odp</span>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <li>
                            <div class="t-load-file__controls">
                                <label class="js-load t-btn t-btn_border">Прикрепить файл
                                    <input type="file" name="files">
                                </label>
                                <span>doc, .xls, .ppt, .odt, .pdf и .odp</span>
                            </div>
                        </li>
                    </ul>
                    <span class="t-load-file_help">
            Загружено <span class="js-fileWeight"><?= $task->getFilesSize() ?></span> MБ из 15 MБ доступных
          </span>
                </div>
            </div>
            <div class="t-input-box">
                <div class="t-input t-input_30"><span>Окончание приёма заявок</span>
                    <input type="hidden" name="fd[date_end]" class="validate" value="<?= $task->date_end ?>">
                    <div class="js-datepicker t-input_date"></div>
                </div>
                <div class="t-input t-input_30">
                    <span><?= $task->type_id == Task::TYPE['team'] ? 'Очки для участников' : 'Очки для победителя' ?></span>
                    <select data-value="1" name="fd[points_win]" class="js-styler validate">
                        <?php foreach ($points->getWinnerPoints() as $point): ?>
                            <option <?= $task->points_win !== $point ?: 'selected' ?>
                                    value="<?= $point ?>"><?= $point ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="t-input t-input_30 <?= $task->type_id != Task::TYPE['team'] ?: 'hidden'; ?>"><span>Очки для участника</span>
                    <select data-value="1" name="fd[points_participant]" class="js-styler validate">
                        <?php foreach ($points->getParticipantPoints() as $point): ?>
                            <option <?= $task->points_participant !== $point ?: 'selected' ?>
                                    value="<?= $point ?>"><?= $point ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="t-input-box">
                <div class="t-input t-input_30">
                    <span>Привилегия</span>
                    <select data-value="1" name="fd[privilege]" class="js-styler validate js-privilege">
                        <?php foreach (Task::PRIVILEGE as $val => $name): ?>
                            <option <?= $task->privilege !== $val ?: 'selected' ?>
                                    value="<?= $val ?>"><?= $name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="t-input t-input_60 js-privilege__box <?=$task->privilege === Task::PRIVILEGE_DB['Приз']? :'hidden'?>" data-type="<?=Task::PRIVILEGE_DB['Приз']?>">
                    <span>Описание привилегии</span>
                    <input name="fd[privilege_description]" placeholder="Например, место в основном штате"
                           value="<?= $task->privilege_description ?>">
                </div>
                <div class="t-input t-input_30 js-privilege__box <?=$task->privilege === Task::PRIVILEGE_DB['TU']? :'hidden'?>" data-type="<?=Task::PRIVILEGE_DB['TU']?>">
                    <span>Количество</span>
                    <select data-value="<?=Task::PRIVILEGE_DB['Нет']?>" name="fd[tu_count]" class="js-styler validate js_price">
                        <?php foreach (Task::TU_COUNT as $tu): ?>
                            <option <?= $task->tu_count == $tu ? 'selected' : ''; ?>
                                    value="<?= $tu ?>"><?= $tu ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="t-input">
                <input name="fd[authored_visible]" id="only_auth" type="checkbox" class="checkbox"
                       value="<?= $task->authored_visible ?>">
                <label for="only_auth">Видна только зарегистрированным пользователям</label>
            </div>
            <?php if ($task->status == Task::STATUS_DB['reject']): ?>
                <div class="t-payStatus_reject">
                    <h2>Причины отклонения</h2>
                    <div class="error_box">
                        <span><?= $task->reject_reason ?></span>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($task->is_paid !== 1): ?>
                <?= \frontend\widgets\TaskPrice::widget([
                    'tu_quantity' => $task->tu_count,
                    'user_id' => Yii::$app->getUser()->getId(),
                ]) ?>
            <?php endif; ?>
            <?php if ($task->status == Task::STATUS_DB['draft']):?>
            <div class="t-input-box__buttons t-input-box__buttons_left" data-type="update">
                <input type="hidden" name="id" value="<?=$task->id?>">
                <input type="hidden" name="fd[deleted_files]">
                <a class="t-btn js-addTask" data-type="moderation" href="#moderation">Отправить на модерацию</a>
                <a data-type="draft" class="t-btn t-btn_border js-addTask" href="#draft">Сохранить</a>
                <a href="#del" class="t-del_task js-del_task">
                    <svg width="10" height="15" viewBox="0 0 10 15" version="1.1" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink">
                        <path fill="#CCCCCC" fill-rule="evenodd"
                              d="M 5.0001 0C 5.55283 0 6.0001 0.447693 6.0001 1L 6.0001 2L 9.0001 2C 9.55283 2 10.0001 2.44769 10.0001 3C 10.0001 3.55231 9.55283 4 9.0001 4L 5.0001 4L 1.0001 4C 0.447362 4 9.62968e-05 3.55231 9.62968e-05 3C 9.62968e-05 2.44769 0.447362 2 1.0001 2L 4.0001 2L 4.0001 1C 4.0001 0.447693 4.44736 0 5.0001 0ZM 0.250096 5.3382C 0.440526 5.12317 0.712987 5 1.0001 5L 9.0001 5C 9.28721 5 9.55967 5.12317 9.7501 5.3382C 9.93955 5.55322 10.0284 5.83942 9.99228 6.12402L 8.99228 14.124C 8.92978 14.6245 8.504 15 8.0001 15L 2.0001 15C 1.49619 15 1.07041 14.6245 1.00791 14.124L 0.0079088 6.12402C -0.028224 5.83942 0.0606432 5.55322 0.250096 5.3382ZM 2.13291 7L 2.88291 13L 7.11728 13L 7.86728 7L 2.13291 7Z"></path>
                    </svg>
                </a>
            </div>
            <?php else:?>
            <div class="t-input-box__buttons t-input-box__buttons_left" data-type="reject">
                <input type="hidden" name="id" value="<?=$task->id?>">
                <input type="hidden" name="fd[deleted_files]">
                <a class="t-btn js-addTask" data-type="moderation" href="#moderation">Отправить на модерацию</a>
                <a data-type="reject" class="t-btn t-btn_border js-addTask" href="#reject">Сохранить</a>
            </div>
            <?php endif;?>
        </form>
    </div>
</main>