<div class="t-background-img"><img src="/img/registration.jpg"></div>
<main>
  <h1 class="white">Задача</h1>
  <div class="t-tabs">
      <select class="only-mobyle t-big-select js-big-select">
          <option value="/tasks/<?=$row->id?>/view" >Общее</option>
          <option value="/tasks/<?=$row->id?>/log" selected>Обновления</option>
          <option value="/tasks/<?=$row->id?>/files">Файлы</option>
      </select>
    <a href="/tasks/<?=$row->id?>/view">Общее</a>
    <a href="/tasks/<?=$row->id?>/log" class="active">Обновления</a>
    <a href="/tasks/<?=$row->id?>/files">Файлы</a>
  </div>
  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>
    <div class="t-work-area_blur__box">
      <?if(empty($row->notifies)):?>
        <p>Нет событий</p>
      <?endif;?>
      <?foreach($row->notifies as $notify):?>
        <div class="t-updates">
          <h3><?=$notify->name?></h3>
          <!--          <h4>Создание сайта</h4>-->
          <p><?=$notify->date_add?></p>
        </div>
      <?endforeach;?>
    </div>
  </div>
<!--  <div class="t-show-more"><a href="#more" class="t-btn">Показать ещё</a></div>-->
</main>