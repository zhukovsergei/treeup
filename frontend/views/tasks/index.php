<?php
use aiur\helpers\TaskPointsHelper;

?>
<div class="t-background-img"><img src="/img/registration.jpg"></div>
<main>
  <h1 class="white">Задачи</h1>
  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>
    <div class="t-search-filter">
      <div class="t-search-filter__overflow js-hide-filter"></div>
      <div class="t-search-filter__header js-open-filter">
        <h3>Поиск и фильтрация</h3>
        <svg width="16" height="16" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
          <path fill="#BDBDBD" fill-rule="evenodd" d="M 3 1C 3 0.447754 2.55225 0 2 0C 1.44775 0 1 0.447754 1 1L 1 8.26758C 0.402344 8.61353 0 9.25977 0 10C 0 10.7402 0.402344 11.3865 1 11.7324L 1 15C 1 15.5522 1.44775 16 2 16C 2.55225 16 3 15.5522 3 15L 3 11.7324C 3.59766 11.3865 4 10.7402 4 10C 4 9.25977 3.59766 8.61353 3 8.26758L 3 1ZM 9 1C 9 0.447754 8.55225 0 8 0C 7.44775 0 7 0.447754 7 1L 7 3.26758C 6.40234 3.61353 6 4.25977 6 5C 6 5.74023 6.40234 6.38647 7 6.73242L 7 15C 7 15.5522 7.44775 16 8 16C 8.55225 16 9 15.5522 9 15L 9 6.73242C 9.59766 6.38647 10 5.74023 10 5C 10 4.25977 9.59766 3.61353 9 3.26758L 9 1ZM 14 0C 14.5522 0 15 0.447754 15 1L 15 9.26758C 15.5977 9.61353 16 10.2598 16 11C 16 11.7402 15.5977 12.3865 15 12.7324L 15 15C 15 15.5522 14.5522 16 14 16C 13.4478 16 13 15.5522 13 15L 13 12.7324C 12.4023 12.3865 12 11.7402 12 11C 12 10.2598 12.4023 9.61353 13 9.26758L 13 1C 13 0.447754 13.4478 0 14 0Z"></path>
        </svg>
      </div>
      <div class="t-search-filter__body">
        <form id="search-filter" action="/tasks/filter" method="POST">
          <div class="t-input-box">
            <div class="t-input t-input_30 t-input_search"><span>Поиск</span>
              <input name="fd[q]">
              <button type="submit">
                <svg width="16" height="16" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <path fill="#E0E0E0" fill-rule="evenodd" d="M 4 7C 4 4.23853 6.23877 2 9 2C 11.7612 2 14 4.23853 14 7C 14 9.76147 11.7612 12 9 12C 6.23877 12 4 9.76147 4 7ZM 9 0C 5.13379 0 2 3.13403 2 7C 2 8.57227 2.51855 10.0237 3.39355 11.1924L 0.292969 14.293C -0.0976562 14.6836 -0.0976562 15.3167 0.292969 15.7073C 0.683594 16.0977 1.31641 16.0977 1.70703 15.7073L 4.80762 12.6064C 5.97607 13.4817 7.42773 14 9 14C 12.8662 14 16 10.866 16 7C 16 3.13403 12.8662 0 9 0Z"></path>
                </svg>
              </button>
            </div>
            <div class="t-input t-input_30"><span>Тип задачи</span>
              <select data-value="1" name="fd[type_id]" class="js-styler validate">
                <option selected value="0">Любой</option>
                <?foreach( \common\models\Task::TYPE as $val => $label):?>
                  <option value="<?=$val?>"><?=$label?></option>
                <?endforeach;?>
              </select>
            </div>
            <div class="t-input t-input_30"><span>Город</span>
              <select data-value="1" name="fd[town_id]" class="js-styler validate">
                <option selected value="0">Любой</option>
                <?foreach($towns as $town):?>
                  <option value="<?=$town->id?>"><?=$town->name?></option>
                <?endforeach;?>
              </select>
            </div>
          </div>
          <div class="t-more js-more-options">
            <div class="t-input-box">
              <div class="t-input t-input_30"><span>Сложность</span>
                <select data-value="1" name="fd[complexity]" class="js-styler validate">
                  <option selected value="0">Любая</option>
                  <?foreach(\common\models\Task::COMPLEXITY as $val => $label):?>
                    <option value="<?=$val?>"><?=$label?></option>
                  <?endforeach;?>
                </select>
              </div>
              <div class="t-input t-input_30"><span>Очки за участие</span>
                <div class="t-input__slider">
                  <ul>
                    <li>
                      <input value="<?=TaskPointsHelper::getMinPointsParticipants()?>" name="fd[points_participant_start]" class="js-min js-only-number">
                    </li>
                    <li>
                      <input value="<?=TaskPointsHelper::getMaxPointsParticipants()?>" name="fd[points_participant_end]" class="js-max js-only-number">
                    </li>
                  </ul>
                  <div data-min="<?=TaskPointsHelper::getMinPointsParticipants()?>" data-max="<?=TaskPointsHelper::getMaxPointsParticipants()?>" class="js-slider"></div>
                </div>
              </div>
              <div class="t-input t-input_30"><span>Очки за победу</span>
                <div class="t-input__slider">
                  <ul>
                    <li>
                      <input value="<?=TaskPointsHelper::getMinPointsWinners()?>" name="fd[points_win_start]" class="js-min js-only-number">
                    </li>
                    <li>
                      <input value="<?=TaskPointsHelper::getMaxPointsWinners()?>" name="fd[points_win_end]" class="js-max js-only-number">
                    </li>
                  </ul>
                  <div data-min="<?=TaskPointsHelper::getMinPointsWinners()?>" data-max="<?=TaskPointsHelper::getMaxPointsWinners()?>" class="js-slider"></div>
                </div>
              </div>
              <div class="t-input t-input_30"><span>Окончание приёма заявок</span>
                  <input type="hidden" name="fd[date_end]" class="validate">
                  <div class="js-datepicker t-input_date">Любая дата</div>
              </div>
              <div class="t-input t-input_30"><span>Специализации</span>
                <div data-name="fd[specializations][]" class="t-custom-select js-custom-select">
                  <div class="t-custom-select__result">Любые</div>
                  <div class="t-custom-select__options">
                    <ul class="scrollbar-inner">
                      <li>
                        <input id="all_options_spec" type="checkbox" value="0" checked data-type="all" class="checkbox">
                        <label for="all_options_spec">Все Специализации</label>
                      </li>
                      <?foreach($specializations as $specialization):?>
                        <li>
                          <input id="options_spec_<?=$specialization->id?>" type="checkbox" value="<?=$specialization->id?>" class="checkbox">
                          <label for="options_spec_<?=$specialization->id?>"><?=$specialization->name?></label>
                        </li>
                      <?endforeach;?>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="t-input t-input_30"><span>Отрасли</span>
                <div data-name="fd[industries][]" class="t-custom-select js-custom-select">
                  <div class="t-custom-select__result">Любые</div>
                  <div class="t-custom-select__options">
                    <ul class="scrollbar-inner">
                      <li>
                        <input id="all_options_ind" type="checkbox" value="0" checked data-type="all" class="checkbox">
                        <label for="all_options_ind">Все отрасли</label>
                      </li>
                      <?foreach($industries as $industry):?>
                        <li>
                          <input id="options_ind_<?=$industry->id?>" type="checkbox" value="<?=$industry->id?>" class="checkbox">
                          <label for="options_ind_<?=$industry->id?>"><?=$industry->name?></label>
                        </li>
                      <?endforeach;?>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="t-input-box__buttons">
            <div class="t-input t-input_30">
              <input id="more_options" type="checkbox" class="checkbox">
              <label for="more_options">Расширенный фильтр</label>
            </div><a href="#clear" class="t-btn t-btn_border js-clearFilter">Сбросить</a>
          </div>
        </form>
      </div>
    </div>
    <div class="t-task-list">
      <?foreach($tasks as $task):?>
          <a href="/tasks/<?=$task->id?>/view" class="t-task-item t-task-item_white" data-id="<?=$task->id?>">
              <div class="t-task-item__img">
                  <span>2</span>
                  <img src="<?=\aiur\helpers\user\LogoHelper::getUserLogoUrl($task->creator)?>">
              </div>
              <div class="t-task-item__info">
                  <div class="price">
                      <?if(!$task->isTeam() && $task->tu_count):?>
                          <img src="/img/bonus1.png"><span><?=$task->tu_count?></span>
                      <?endif;?>
                      <img src="/img/bonus2.png"><span><?=$task->points_win?><?if(!$task->isTeam() && $task->points_participant>0):?>/<?=$task->points_participant?><?endif;?></span>
                  </div>
                  <div class="level level_<?=$task->complexity?>"><span></span><span></span><span></span></div>
                  <div class="name"><?=\aiur\helpers\user\UserHelper::getUserName($task->creator)?></div>
                  <div class="description"><?=$task->name?></div>
                  <hr>
                  <ul class="base-info">
                      <li><?=$task->date_end?></li>
                    <?if(isset($task->industry)):?>
                      <li><?=$task->industry->name?></li>
                    <?endif;?>
                      <li><?=$task->town->name??''?></li>
                  </ul>
              </div>
          </a>
      <?endforeach;?>
    </div>
  </div>
<!--  <div class="t-show-more"><a href="#more" class="t-btn">Показать ещё</a></div>-->
</main>