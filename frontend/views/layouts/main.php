<?php

use frontend\components\ulogin\ULogin;
use yii\helpers\Html;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <?= Html::csrfMetaTags() ?>
  <?if( ! empty(\Yii::$app->seo->block('title')) ):?>
    <title><?= Html::encode(\Yii::$app->seo->block('title')) ?></title>
  <?else:?>
    <title><?= Html::encode($this->title) ?></title>
  <?endif;?>
  <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?= frontend\widgets\Header::widget()?>

<?=$content?>

<?= frontend\widgets\Footer::widget()?>

<div id="auth_reg" class="t-dialog-form">
  <div class="flip-container">
    <div class="flipper">
      <form action="/ajax/login" method="POST" class="front">
        <div class="t-dialog-close"><span></span><span></span></div>
        <h2>Вход</h2>
        <p>Войти через социальную сеть</p>
        <div class="t-social-auth">
            <div id="uLogin_13" data-ulogin="display=small;fields=first_name,last_name,email,photo,photo_big;mobilebuttons=0;sort=default;theme=flat;lang=ru;providers=vkontakte,odnoklassniki,facebook,google;redirect_uri=<?=urlencode(\Yii::$app->getUrlManager()->createAbsoluteUrl('/auth/ulogin'))?>"> </div>
<!--          --><?//=ULogin::widget();?>
        </div>
        <hr>
        <p>Или войти при помощи почты и пароля</p>
        <div class="t-input"><span>Логин</span>
          <input type="text" name="login" placeholder="электронная почта или телефон" class="validate">
        </div>
        <div class="t-input"><span>Пароль</span>
          <input type="password" name="password" class="validate"><a href="#new-password" class="t-link_new-password">Восстановить пароль</a>
        </div>
        <div class="t-buttons"><button class="t-btn">Войти</button><a href="#reg">Зарегистрироваться</a></div>
      </form>
      <form action="/ajax/register" method="POST" class="back">
        <div class="t-dialog-close"><span></span><span></span></div>
        <h2>Регистрация</h2>
        <p>Через социальную сеть</p>
        <div class="t-social-auth">
          <?=ULogin::widget();?>
        </div>
        <hr>
        <p>Или при помощи почты</p>
        <div class="t-input"><span>Логин</span>
          <input type="text" name="login" placeholder="электронная почта" class="validate">
        </div>
        <div class="t-input"><span>Пароль</span>
          <input type="password" name="password" placeholder="Не менее 6 символов" class="validate">
        </div>
        <div class="t-buttons"><button class="t-btn">Зарегистрироваться</button><a href="#auth">Войти</a></div>
      </form>
    </div>
  </div>
</div>
<div id="new_password" class="t-dialog-form">
    <form action="/site/lost-password" method="POST" class="front">
        <div class="t-dialog-close"><span></span><span></span></div>
        <h2>Сброс пароля</h2>
        <div class="t-input"><span>Электронная почта</span>
            <input type="text" name="login" class="validate"><span class="hint">укажите адрес, который вы указали при регистрации — мы вышлем на него ссылку для сброса пароля</span>
        </div>
        <div class="t-buttons"><button class="t-btn">Выслать ссылку для восстановления</button></div>
    </form>
</div>
<div id="addAccount" class="t-dialog-form">
    <form id="pay" action="" method="POST">
        <div class="t-dialog-close"><span></span><span></span></div>
        <h2 class="black">Нет подписки</h2>
        <p>Для продолжения необходима действующая подписка.</p>
        <div class="t-input">
            <span>Способ оплаты</span>
            <input type="radio" name="fd[pay]" id="pay_bonus" class="radio" checked>
            <label for="pay_bonus">Очки <span>— доступно 20 000</span></label>
            <input type="radio" name="fd[pay]" id="pay_card" class="radio">
            <label for="pay_card">Оплата банковской картой</label>
            <input type="radio" name="fd[pay]" id="pay_yandex" class="radio">
            <label for="pay_yandex">Яндекс.Деньги</label>
        </div>
        <div class="t-input">
            <span>Продолжительность</span>
            <select class="js-styler" name="fd[duration]">
                <option value="1" selected>1 месяц</option>
                <option value="2" >2 месяца</option>
                <option value="3" >3 месяца</option>
                <option value="4" >4 месяца</option>
                <option value="5" >5 месяцев</option>
                <option value="6" >6 месяцев</option>
                <option value="7" >7 месяцев</option>
                <option value="8" >8 месяцев</option>
                <option value="9" >9 месяцев</option>
                <option value="10" >10 месяцев</option>
                <option value="11" >11 месяцев</option>
                <option value="12" >12 месяцев</option>
            </select>
        </div>
        <div class="t-input">
            <span>Стоимость</span>
            <p>1 000 P</p>
        </div>
        <div class="t-btnBox">
            <button class="t-btn">Оплатить</button>
        </div>
    </form>
</div>
<div id="addAccount_customer" class="t-dialog-form">
    <form id="pay_customer" action="" method="POST">
        <div class="t-dialog-close"><span></span><span></span></div>
        <h2 class="black">Нет средств</h2>
        <div class="t-input">
            <span>Способ оплаты</span>
            <input type="radio" name="fd[pay_customer]" id="pay_card_customer" class="radio" checked>
            <label for="pay_card_customer">Оплата банковской картой</label>
            <input type="radio" name="fd[pay_customer]" id="pay_yandex_customer" class="radio">
            <label for="pay_yandex_customer">Яндекс.Деньги</label>
        </div>
        <div class="t-input">
            <span>Сумма пополнения</span>
            <input type="text" name="fd[summ]" class="validate js-only-number">
        </div>
        <div class="t-btnBox">
            <button class="t-btn">Перейти к оплате</button>
        </div>
    </form>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
