<div class="t-background-img"><img src="/img/profile/1.jpg"></div>
<main class="t-page_profile">
  <h1 class="white">Профиль исполнителя</h1>
  <div class="t-tabs">
    <a href="/user/<?=$user->id?>/business/personal" class="active">Данные</a>
    <a href="/user/<?=$user->id?>/business/tasks">Задачи</a>
    <a href="/user/<?=$user->id?>/business/cv">Резюме</a>
    <a href="/user/<?=$user->id?>/business/portfolio">Портфолио</a>
    <a href="/user/<?=$user->id?>/business/reviews">Отзывы</a>
  </div>
  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>
    <div class="t-work-area_blur__box t-personal-info">
      <div class="t-personal-info__img">
        <img src="<?=\aiur\helpers\user\LogoHelper::getUserLogoUrl($user)?>">
      </div>
      <div class="t-personal-info__header">
        <div class="t-personal-info__name"><?=\aiur\helpers\user\UserHelper::getUserName($user)?></div>
        <div class="t-personal-info__rate">Рейтинг <?=\aiur\helpers\user\TypingProfileHelper::getRating($user)?></div>
        <?if(isset($user->profile->town->name)):?>
          <p><?=$user->profile->town->name?></p>
        <?endif;?>
      </div>
      <a class="t-btn t-personal-info__openDialog" href="/src/dialog/index.html">Написать</a>
      <hr>
      <div class="t-personal-info__text">

      </div>
    </div>
    <div class="t-work-area_blur__box t-personal-info_social">
      <?if($user->profile->email):?>
        <h3>Электронная почта</h3>
        <p><?=$user->profile->email?></p>
      <?endif;?>

      <?if($user->profile->phone):?>
        <h3>Номер телефона</h3>
        <p><?=$user->profile->phone?></p>
      <?endif;?>

      <?if($user->profile->skype):?>
        <h3>Skype</h3>
        <p><?=$user->profile->skype?></p>
      <?endif;?>

      <h3>Социальные сети</h3>
      <div class="t-social-auth">
        <?if($user->profile->skype):?>
          <h3>Skype</h3>
          <p><?=$user->profile->skype?></p>
        <?endif;?>
        <?if($user->profile->fb):?>
          <a href="<?=$user->profile->fb?>" class="t-fb">
            <svg width="11" height="20" viewBox="0 0 11 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <title>facebook</title>
              <desc>Created using Figma</desc>
              <g id="Canvas_facebook_reg" transform="translate(-2074 -315)">
                <g id="facebook_reg">
                  <g id="facebook_reg_2">
                    <use xlink:href="#path_facebook_reg" transform="translate(2074 315)" fill="#4F4F4F"></use>
                  </g>
                </g>
              </g>
              <defs>
                <path id="path_facebook_reg" fill-rule="evenodd" d="M 7.448 3.807L 10.025 3.807L 10.025 -3.66211e-07L 6.996 -3.66211e-07L 6.996 0.0130001C 3.327 0.142 2.573 2.206 2.507 4.374L 2.5 4.374L 2.5 6.274L -1.98364e-07 6.274L -1.98364e-07 10.003L 2.5 10.003L 2.5 19.997L 6.268 19.997L 6.268 10.003L 9.353 10.003L 9.947 6.274L 6.268 6.274L 6.268 5.126C 6.2555 4.96119 6.27643 4.79555 6.32954 4.63902C 6.38264 4.4825 6.46682 4.33832 6.57702 4.21513C 6.68723 4.09195 6.82118 3.9923 6.97085 3.92216C 7.12052 3.85203 7.28282 3.81286 7.448 3.807Z"></path>
              </defs>
            </svg></a>
        <?endif;?>
        <?if($user->profile->google):?>
          <a href="<?=$user->profile->google?>" class="t-gp">
            <svg width="29" height="19" viewBox="0 0 29 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <title>gPlus</title>
              <desc>Created using Figma</desc>
              <g id="Canvas_gp_reg" transform="translate(-2103 -316)">
                <g id="gPlus_reg">
                  <g id="gPlus_reg_2">
                    <use xlink:href="#path_gPlus_reg" transform="translate(2103 316)" fill="#4F4F4F"></use>
                  </g>
                </g>
              </g>
              <defs>
                <path id="path_gPlus_reg" fill-rule="evenodd" d="M 9.00001 18.0013C 7.81811 18.0013 6.64779 17.7685 5.55586 17.3162C 4.46393 16.8639 3.47176 16.201 2.63603 15.3652C 1.8003 14.5295 1.13736 13.5374 0.68507 12.4454C 0.232777 11.3535 1.11204e-05 10.1832 1.11204e-05 9.00128C 1.11204e-05 7.81938 0.232777 6.64906 0.68507 5.55713C 1.13736 4.46519 1.8003 3.47304 2.63603 2.63731C 3.47176 1.80159 4.46393 1.13865 5.55586 0.68636C 6.64779 0.234068 7.81811 0.00127595 9.00001 0.00127597C 10.1197 -0.018317 11.2319 0.187912 12.2702 0.607649C 13.3084 1.02739 14.2515 1.65203 15.043 2.44428L 12.6 4.75828C 12.1254 4.2932 11.5622 3.92835 10.9437 3.68543C 10.3252 3.44252 9.66423 3.3265 9.00001 3.34428C 7.5181 3.37214 6.10628 3.98039 5.06813 5.03825C 4.02997 6.09611 3.44838 7.5191 3.44838 9.00128C 3.44838 10.4834 4.02997 11.9064 5.06813 12.9643C 6.10628 14.0222 7.5181 14.6304 9.00001 14.6583C 10.177 14.7362 11.3429 14.39 12.2867 13.6825C 13.2305 12.975 13.8897 11.9529 14.145 10.8013L 9.003 10.8013L 9.003 7.71328L 17.488 7.71328C 17.6004 8.21943 17.6438 8.73848 17.617 9.25628C 17.614 14.4013 14.145 18.0013 9.00001 18.0013ZM 28.285 10.2873L 25.714 10.2873L 25.714 12.8583L 23.145 12.8583L 23.145 10.2843L 20.574 10.2843L 20.574 7.71328L 23.145 7.71328L 23.145 5.14228L 25.717 5.14228L 25.717 7.71328L 28.288 7.71328L 28.288 10.2843L 28.285 10.2873Z"></path>
              </defs>
            </svg></a>
        <?endif;?>
        <?if($user->profile->vk):?>
          <a href="<?=$user->profile->vk?>" class="t-vk">
            <svg width="23" height="13" viewBox="0 0 23 13" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <title>vk</title>
              <desc>Created using Figma</desc>
              <g id="Canvas_vk_reg" transform="translate(-2144 -319)">
                <g id="vk_reg">
                  <g id="vk_reg_2">
                    <use xlink:href="#path_vk_reg" transform="translate(2144 319)" fill="#4F4F4F"></use>
                  </g>
                </g>
              </g>
              <defs>
                <path id="path_vk_reg" fill-rule="evenodd" d="M 10.9998 12.8312L 12.3438 12.8312C 12.5715 12.8046 12.7859 12.7098 12.9588 12.5592C 13.0823 12.3872 13.1471 12.1799 13.1438 11.9682C 13.1438 11.9682 13.1178 10.1452 13.9548 9.8792C 14.7918 9.6132 15.8398 11.6222 16.9638 12.3942C 17.385 12.731 17.9228 12.8868 18.4588 12.8272L 21.4588 12.8272C 21.4588 12.8272 23.0298 12.7082 22.2848 11.4722C 21.6938 10.4895 20.9385 9.61544 20.0518 8.8882C 18.1678 7.1402 18.4208 7.4232 20.6898 4.3992C 22.0708 2.5582 22.6238 1.4332 22.4508 0.952202C 22.2996 0.800125 22.1143 0.686315 21.9103 0.620225C 21.7063 0.554135 21.4894 0.537671 21.2778 0.572202L 17.8898 0.572202C 17.7389 0.560258 17.5876 0.590373 17.4528 0.659203C 17.3202 0.760977 17.2168 0.895819 17.1528 1.0502C 16.805 1.96893 16.3871 2.85956 15.9028 3.7142C 14.3968 6.2712 13.7948 6.4062 13.5488 6.2472C 12.9758 5.8772 13.1198 4.7612 13.1198 3.9672C 13.1198 1.4892 13.4958 0.456202 12.3878 0.188202C 11.8715 0.0634163 11.3405 0.0105835 10.8098 0.0312022C 9.86469 -0.0564604 8.91157 0.0410254 8.00379 0.318202C 7.61879 0.506202 7.32179 0.926202 7.50379 0.950202C 7.88862 0.983477 8.24627 1.1623 8.50379 1.4502C 8.73798 1.92545 8.85296 2.45057 8.83879 2.9802C 8.83879 2.9802 9.03879 5.8982 8.37379 6.2592C 7.91779 6.5072 7.29279 6.0002 5.94979 3.6802C 5.49077 2.86775 5.08653 2.02554 4.73979 1.1592C 4.67785 1.01356 4.58223 0.884691 4.46079 0.783202C 4.3038 0.678774 4.12723 0.607329 3.94179 0.573202L 0.727791 0.573202C 0.489441 0.574067 0.257817 0.652327 0.0677877 0.796203C 0.0254584 0.885253 0.0024201 0.98224 0.0001798 1.08081C -0.0020605 1.17939 0.0165511 1.27732 0.054791 1.3682C 0.054791 1.3682 2.57079 7.2992 5.41979 10.2682C 6.12772 11.0512 6.98739 11.6821 7.9466 12.1227C 8.90582 12.5633 9.94457 12.8043 10.9998 12.8312Z"></path>
              </defs>
            </svg>
          </a>
        <?endif;?>
      </div>
    </div>
<!--    <h2>Хронология</h2>-->
<!--    <div class="t-work-area_blur__box t-chronology-list">-->
<!--      <div class="t-chronology">-->
<!--        <div class="t-chronology__name">Выбран победитель в задаче</div>-->
<!--        <div class="t-chronology__task">Создание сайта</div>-->
<!--        <div class="t-chronology__date">23 апреля 2017, 17:30</div>-->
<!--      </div>-->
<!--      <div class="t-chronology">-->
<!--        <div class="t-chronology__name">Выбран победитель в задаче</div>-->
<!--        <div class="t-chronology__task">Создание сайта</div>-->
<!--        <div class="t-chronology__date">23 апреля 2017, 17:30</div>-->
<!--      </div>-->
<!--      <div class="t-chronology">-->
<!--        <div class="t-chronology__name">Выбран победитель в задаче</div>-->
<!--        <div class="t-chronology__task">Создание сайта</div>-->
<!--        <div class="t-chronology__date">23 апреля 2017, 17:30</div>-->
<!--      </div>-->
<!--      <div class="t-chronology">-->
<!--        <div class="t-chronology__name">Регистрация на сайте</div>-->
<!--        <div class="t-chronology__date">23 апреля 2017, 17:30</div>-->
<!--      </div>-->
<!--    </div>-->
  </div>
</main>