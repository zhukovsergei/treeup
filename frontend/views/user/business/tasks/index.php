<div class="t-background-img"><img src="/img/profile/1.jpg"></div>
<main class="t-page_profile">
  <h1 class="white">Профиль исполнителя</h1>
  <div class="t-tabs">
    <a href="/user/<?=$user->id?>/business/personal">Данные</a>
    <a href="/user/<?=$user->id?>/business/tasks" class="active">Задачи</a>
    <a href="/user/<?=$user->id?>/business/cv">Резюме</a>
    <a href="/user/<?=$user->id?>/business/portfolio">Портфолио</a>
    <a href="/user/<?=$user->id?>/business/reviews">Отзывы</a>
  </div>
  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>
    <?foreach($tasks as $task):?>
      <a href="/tasks/<?=$task->id?>/view" class="t-task-item t-task-item_white">
        <div class="t-task-item__img">
          <img src="<?=\aiur\helpers\user\LogoHelper::getUserLogoUrl($task->creator)?>">
        </div>
        <div class="t-task-item__info">
          <div class="price">
            <?if(!$task->isTeam() && 0 !==\aiur\helpers\user\TypingProfileHelper::getRating($task->creator)):?>
              <img src="/img/bonus1.png"><span><?=\aiur\helpers\user\TypingProfileHelper::getRating($task->creator)?></span>
            <?endif;?>
          </div>
          <div class="level level_<?=$task->complexity?>"><span></span><span></span><span></span></div>
          <div class="name"><?=\aiur\helpers\user\UserHelper::getUserName($task->creator)?></div>
          <div class="description"><?=$task->name?></div>
          <hr>
          <ul class="base-info">
            <li><?=$task->date_end?></li>
            <?if(isset($task->industry)):?>
              <li><?=$task->industry->name?></li>
            <?endif;?>
            <li><?=$task->town->name??''?></li>
          </ul>
        </div></a>
    <?endforeach;?>
  </div>
</main>