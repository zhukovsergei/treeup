<div class="t-background-img"><img src="/img/profile/1.jpg"></div>
<main class="t-page_profile">
  <h1 class="white">Профиль исполнителя</h1>
  <div class="t-tabs">
    <a href="/user/<?=$user->id?>/business/personal">Данные</a>
    <a href="/user/<?=$user->id?>/business/tasks">Задачи</a>
    <a href="/user/<?=$user->id?>/business/cv">Резюме</a>
    <a href="/user/<?=$user->id?>/business/portfolio">Портфолио</a>
    <a href="/user/<?=$user->id?>/business/reviews" class="active">Отзывы</a>
  </div>
  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>
    <h2>Отзывы</h2>
    <div class="t-reviews-list">
      <div class="t-review">
        <div class="t-review__personal-info">
          <div class="t-review__avatar"><img src="/img/face.jpg"></div>
          <div class="t-review__name"><span>Николай Артемьев</span>14.02.2016</div>
        </div>
        <div class="t-work-area_blur__box">
          <div class="t-review__type">О задаче</div>
          <div class="t-review__task">Создать сайт булочной</div>
          <div class="t-review__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</div>
        </div>
      </div>
      <div class="t-review">
        <div class="t-review__personal-info">
          <div class="t-review__avatar"><img src="/img/face.jpg"></div>
          <div class="t-review__name"><span>Николай Артемьев</span>14.02.2016</div>
        </div>
        <div class="t-work-area_blur__box">
          <div class="t-review__type">О задаче</div>
          <div class="t-review__task">Создать сайт булочной</div>
          <div class="t-review__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</div>
        </div>
      </div>
    </div>
  </div>
</main>