<div class="t-background-img"><img src="/img/profile/1.jpg"></div>
<main class="t-page_profile">
  <h1 class="white">Профиль исполнителя</h1>
  <div class="t-tabs">
    <a href="/user/<?=$user->id?>/business/personal">Данные</a>
    <a href="/user/<?=$user->id?>/business/tasks">Задачи</a>
    <a href="/user/<?=$user->id?>/business/cv">Резюме</a>
    <a href="/user/<?=$user->id?>/business/portfolio" class="active">Портфолио</a>
    <a href="/user/<?=$user->id?>/business/reviews">Отзывы</a>
  </div>
  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>
    <div class="t-portfolio-box t-portfolio-box_public t-work-area_blur__box">
      <?if( ! empty($user->portfolio) ):?>
        <h2><?=$user->portfolio->name?></h2>
        <div class="t-portfolio-box__text">
          <?=$user->portfolio->text?>
        </div>

        <?foreach($user->portfolio->blocks as $block):?>
          <?=\frontend\widgets\PortfolioPublicBlock::widget(['block' => $block])?>
        <?endforeach;?>

      <?else:?>
      Портфолио нет
      <?endif;?>

      <div class="t-btnBox"><a href="#more" class="t-btn t-btn_border js-moreProject">Подробнее</a></div>
    </div>
  </div>
</main>