<div class="t-background-img"><img src="/img/profile/1.jpg"></div>
<main class="t-page_profile">
  <h1 class="white">Профиль исполнителя</h1>
  <div class="t-tabs">
    <a href="/user/<?=$user->id?>/business/personal">Данные</a>
    <a href="/user/<?=$user->id?>/business/tasks">Задачи</a>
    <a href="/user/<?=$user->id?>/business/cv" class="active">Резюме</a>
    <a href="/user/<?=$user->id?>/business/portfolio">Портфолио</a>
    <a href="/user/<?=$user->id?>/business/reviews">Отзывы</a>
  </div>
  <div class="t-work-area t-work-area_blur">
    <div class="t-work-area__background"></div>
    <div class="t-rezume-list">
      <div class="t-rezume__box">
        <h2>Специализации</h2>
        <div class="t-flexBox">
            <?foreach($user->specs as $spec):?>
                <div class="t-work-area_blur__box t-work-area_blur__box_30">
                    <div class="t-input"><span>Практикуемая отрасль</span>
                        <div><?=\common\models\Spec::LABEL_TYPE[$spec->type_industry]??''?></div>
                    </div>

                    <?if($spec->specialization):?>
                        <div class="t-input"><span>Специализация</span>
                            <div><?=$spec->specialization->name?></div>
                        </div>
                    <?endif;?>

                    <div class="t-input"><span>Опыт</span>
                        <div><?=\common\models\Spec::EXPERIENCE[$spec->exp]??''?></div>
                    </div>
                    <div class="t-input"><span>Навыки</span>
                        <div><?foreach($spec->skills as $skill):?>
                                <?=$skill->name?>
                            <?endforeach;?></div>
                    </div>
                </div>
            <?endforeach;?>
        </div>
      </div>
      <div class="t-rezume__box">
        <h2>Образование</h2>
        <div class="t-flexBox">
            <?foreach($user->educations as $education):?>
              <div class="t-work-area_blur__box t-work-area_blur__box_30">
                <div class="t-input"><span>Период обучения</span>
                  <div><?=$education->date_from?> — <?=$education->date_to?></div>
                </div>
                <div class="t-input"><span>Учебное заведение</span>
                  <div><?=$education->university?></div>
                </div>
                <div class="t-input"><span>Специальность</span>
                  <div><?=$education->spec?></div>
                </div>
              </div>
            <?endforeach;?>
        </div>
      </div>
      <div class="t-rezume__box">
        <h2>Опыт работы</h2>
        <div class="t-flexBox">
            <?foreach($user->experiences as $experience):?>
              <div class="t-work-area_blur__box t-work-area_blur__box_30">
                <div class="t-input"><span>Период работы</span>
                  <?if($experience->current):?>
                    <div><?=$experience->date_from?> — по настоящее время</div>
                  <?else:?>
                    <div><?=$experience->date_from?> — <?=$experience->date_to?></div>
                  <?endif;?>
                </div>
                <div class="t-input"><span>Компания</span>
                  <div><?=$experience->company?></div>
                </div>
                <div class="t-input"><span>Должность</span>
                  <div><?=$experience->post?></div>
                </div>
                <div class="t-input"><span>Обязанности</span>
                  <div><?=$experience->duties?></div>
                </div>
              </div>
            <?endforeach;?>
        </div>
      </div>
    </div>
  </div>
</main>