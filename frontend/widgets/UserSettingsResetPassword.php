<?php
namespace frontend\widgets;

use common\models\User;
use yii\base\Widget;

class UserSettingsResetPassword extends Widget
{

  public function run()
  {
    return $this->render('user-settings-reset-password');
  }
}