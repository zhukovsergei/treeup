<?php
namespace frontend\widgets;

use aiur\helpers\TaskHelper;
use common\models\TaskParticipant;
use yii\base\Widget;

class TaskListBusinessBlocks extends Widget
{
  public $task;

  public function run()
  {
    $user_id = \Yii::$app->getUser()->getId();

    if($this->task->status === 'finished')
    {

      if( TaskHelper::isWon($this->task->id, $user_id) )
      {
        return $this->render('task-list-business-blocks/winner-finished', [
          'task' => $this->task,
          'user_id' => $user_id,
        ]);
      }
      else
      {
        return $this->render('task-list-business-blocks/participant-finished', [
          'task' => $this->task,
          'user_id' => $user_id,
        ]);
      }
    }
    return $this->render('task-list-business-blocks/participant', [
      'task' => $this->task,
      'user_id' => $user_id,
    ]);
  }
}