<?php
namespace frontend\widgets;

use yii\base\Widget;

class HowToWork extends Widget
{
  public function init()
  {
    parent::init();

  }
  public function run()
  {
    $user = \Yii::$app->getUser()->getIdentity();

    if($user && $user->typeProfileIs('business'))
    {
      return $this->render('how-to-work/business');
    }

    return $this->render('how-to-work/guest');
  }
}