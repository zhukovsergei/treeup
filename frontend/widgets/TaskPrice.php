<?php
namespace frontend\widgets;

use aiur\helpers\TaskPricePostHelper;
use aiur\helpers\user\BalanceHelper;
use yii\base\Widget;

class TaskPrice extends Widget
{
  public $tu_quantity;
  public $user_id;

  public function run()
  {
    $total =  TaskPricePostHelper::calculate($this->tu_quantity);

    if( TaskPricePostHelper::isEnough($this->user_id, $this->tu_quantity) )
    {
      return $this->render( 'tasks-price/green', [
        'total' => $total
      ] );
    }

    return $this->render( 'tasks-price/red', [
      'total' => $total
    ]);
  }
}