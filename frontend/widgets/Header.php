<?php
namespace frontend\widgets;

use common\models\User;
use yii\base\Widget;

class Header extends Widget
{

  public function run()
  {
    if( ! \Yii::$app->user->getIsGuest() )
    {
      $user = \Yii::$app->getUser()->getIdentity();
      $profile_type = $user->profile_type;

      if( in_array($profile_type, User::PROFILE) )
      {
        return $this->render('header/'.$profile_type);
      }
    }

    return $this->render('header/guest');
  }
}