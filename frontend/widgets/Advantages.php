<?php
namespace frontend\widgets;

use yii\base\Widget;

class Advantages extends Widget
{
  public function init()
  {
    parent::init();

  }
  public function run()
  {
    $user = \Yii::$app->getUser()->getIdentity();

    if($user && $user->typeProfileIs('business'))
    {
      return $this->render('advantages/business');
    }

    return $this->render('advantages/guest');
  }
}