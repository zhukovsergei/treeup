<section class="t-advantage">
  <h2>Преимущества сервиса</h2>
  <div data-type="flip" class="tabs">
    <a href="#advantage_client" class="active">для заказчика</a><i></i><a href="#advantage_developer">для исполнителя</a>
  </div>
  <div class="swiper-container t-advantage-list">
    <div class="swiper-wrapper">
      <div class="swiper-slide t-advantage-item flip-container ">
        <div class="flipper">
          <div class="front">
            <div class="t-advantage-item__img"><img src="/img/home_page/advantage_1.png"></div>
            <p>Решения ваших задач</p>
          </div>
          <div class="back">
            <div class="t-advantage-item__img"><img src="/img/home_page/advantage_4.png"></div>
            <p>Получение реального опыта и привилегий</p>
          </div>
        </div>
      </div>
      <div class="swiper-slide t-advantage-item flip-container ">
        <div class="flipper">
          <div class="front">
            <div class="t-advantage-item__img"><img src="/img/home_page/advantage_2.png"></div>
            <p>Построение эффективной команды</p>
          </div>
          <div class="back">
            <div class="t-advantage-item__img"><img src="/img/home_page/advantage_5.png"></div>
            <p>Выбор сотен команд к присоединению</p>
          </div>
        </div>
      </div>
      <div class="swiper-slide t-advantage-item flip-container ">
        <div class="flipper">
          <div class="front">
            <div class="t-advantage-item__img"><img src="/img/home_page/advantage_4.png"></div>
            <p>Широкий охват аудитории</p>
          </div>
          <div class="back">
            <div class="t-advantage-item__img"><img src="/img/home_page/advantage_6.png"></div>
            <p>Система личностного роста</p>
          </div>
        </div>
      </div>
    </div>
    <div class="swiper-pagination"></div>
  </div>
</section>