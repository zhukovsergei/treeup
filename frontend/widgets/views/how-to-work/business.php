<section class="t-how-works">
  <h2>Как это работает</h2>
  <div data-type="flip" class="tabs">
    <a href="#advantage_client">для заказчика</a><i class="scale"></i><a href="#advantage_developer" class="active">для исполнителя</a>
  </div>
  <ul class="t-how-works__block">
    <li>
      <div class="t-how-works__item flip-container open">
        <div class="flipper">
          <div class="front">
            <h4>Решение задач</h4>
          </div>
          <div class="back">
            <h4>Решение задач</h4>
          </div>
        </div>
      </div>
      <div class="t-how-works__item flip-container open">
        <div class="flipper">
          <div class="front">
            <img src="/img/home_page/how-works_1.png">
            <p>Разместите<br>задачу</p>
            <div class="t-how-works__arrow"></div>
          </div>
          <div class="back">
            <img src="/img/home_page/how-works_7.png">
            <p>Выбирайте<br>задачу</p>
            <div class="t-how-works__arrow"></div>
          </div>
        </div>
      </div>
      <div class="t-how-works__item flip-container open">
        <div class="flipper">
          <div class="front">
            <img src="/img/home_page/how-works_3.png">
            <p>Проверяйте<br>решения</p>
            <div class="t-how-works__arrow"></div>
          </div>
          <div class="back">
            <img src="/img/home_page/how-works_9.png">
            <p>Предложите<br>решение</p>
            <div class="t-how-works__arrow"></div>
          </div>
        </div>
      </div>
      <div class="t-how-works__item flip-container open">
        <div class="flipper">
          <div class="front">
            <img src="/img/home_page/how-works_5.png">
            <p>Выбирайте<br>лучшие работы</p>
          </div>
          <div class="back">
            <img src="/img/home_page/how-works_11.png">
            <p>Получайте<br>привилегии</p>
          </div>
        </div>
      </div>
    </li>
    <li>
      <div class="t-how-works__item flip-container open">
        <div class="flipper">
          <div class="front">
            <h4>Построение команды</h4>
          </div>
          <div class="back">
            <h4>Присоединение к команде</h4>
          </div>
        </div>
      </div>
      <div class="t-how-works__item flip-container open">
        <div class="flipper">
          <div class="front">
            <img src="/img/home_page/how-works_2.png">
            <p>Разместите<br>проект</p>
            <div class="t-how-works__arrow"></div>
          </div>
          <div class="back">
            <img src="/img/home_page/how-works_8.png">
            <p>Выбирайте<br>проекты</p>
            <div class="t-how-works__arrow"></div>
          </div>
        </div>
      </div>
      <div class="t-how-works__item flip-container open">
        <div class="flipper">
          <div class="front">
            <img src="/img/home_page/how-works_4.png">
            <p>Проверяйте<br>отклики</p>
            <div class="t-how-works__arrow"></div>
          </div>
          <div class="back">
            <img src="/img/home_page/how-works_10.png">
            <p>Оставляйте<br>отклики</p>
            <div class="t-how-works__arrow"></div>
          </div>
        </div>
      </div>
      <div class="t-how-works__item flip-container open">
        <div class="flipper">
          <div class="front">
            <img src="/img/home_page/how-works_6.png">
            <p>Выбирайте<br>кандидатов</p>
          </div>
          <div class="back">
            <img src="/img/home_page/how-works_12.png">
            <p>Получайте<br>обратную связь</p>
          </div>
        </div>
      </div>
    </li>
  </ul>
  <a href="/about-us" class="t-btn">Подробное описание сервиса</a>
</section>