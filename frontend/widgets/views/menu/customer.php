<div class="t-personal-info_header">
  <div class="t-personal-info_header__img">
    <img src="<?=\aiur\helpers\user\LogoHelper::getUserLogoUrl($user)?>">
  </div>
    <div class="t-personal-info_header__rightbox">
        <p class="t-personal-info_header__name"><?=\aiur\helpers\user\TypingProfileHelper::getProfileName($user)?></p>
        <p class="t-personal-info_header__status">Заказчик</p>
        <div class="t-personal-info_header__links">
            <a href="/tasks/add" class="t-btn only-mobyle">Создать задачу</a>
            <a href="/profile/customer/feed">Личный кабинет</a>
            <a href="/profile/customer/tasks">Мои задачи</a>
            <a href="/profile/customer/projects">Проекты</a>
            <a href="/profile/customer/reviews">Отзывы</a>
            <a href="/profile/customer/personal">Профиль</a>
            <a href="/profile/customer/bonus">Бонусы</a>
            <a href="/profile/customer/balance">Баланс</a>
            <a href="/auth/logout">Выход</a>
        </div>
    </div>

</div>