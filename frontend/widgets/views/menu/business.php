<div class="t-personal-info_header">
  <div class="t-personal-info_header__img">
    <img src="<?=\aiur\helpers\user\LogoHelper::getUserLogoUrl($user)?>">
  </div>
  <div class="t-personal-info_header__rightbox">
    <p class="t-personal-info_header__name"><?=\aiur\helpers\user\TypingProfileHelper::getProfileName($user)?></p>
    <p class="t-personal-info_header__status">Исполнитель</p>
    <div class="t-personal-info_header__links">
      <a href="/tasks" class="t-btn only-mobyle">
        Все задачи
      </a>
      <a href="/profile/business/feed">Личный кабинет</a>
      <a href="/profile/business/tasks">Мои задачи</a>
      <a href="/profile/business/reviews">Отзывы</a>
      <a href="/profile/business/personal">Профиль</a>
      <a href="/profile/business/bonus">Бонусы</a>
      <a href="/profile/business/subscription">Баланс</a>
      <a href="/auth/logout">Выход</a>
    </div>
  </div>
</div>