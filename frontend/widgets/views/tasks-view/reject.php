<?php

use yii\helpers\Url;

?>
<div class="t-task-custumer__info_long__box">
    <div class="title">Отклонена</div>
    <p>Задача не прошла проверку. Исправьте данные.</p>
    <ul class="t-task-custumer__buttons t-task-custumer__buttons_static">
        <li>
          <a href="<?=Url::to(['/tasks/edit','id' => $row->id])?>" class="t-btn t-btn_border js-edit_task" data-type="reject">Редактировать</a>
        </li>
    </ul>
</div>