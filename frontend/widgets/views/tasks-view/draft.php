<?php

use aiur\helpers\TaskPricePostHelper;
use yii\helpers\Url;

?>

<div class="t-task-custumer__info_long__box">
  <div class="title">Черновик</div>
  <p>Чтобы задача стала активной, отправьте ее на модерацию</p>
  <ul class="t-task-custumer__buttons t-task-custumer__buttons_static">
    <li>
      <a href="<?=Url::to(['/tasks/edit','id' => $row->id])?>" class="t-btn js-edit_task" data-type="mod">Редактировать</a>
    </li>
  </ul>
</div>