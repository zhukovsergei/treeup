<div class="t-portfolio-box__block" data-type="audio">
  <input type="hidden" name="fd[block_id]" value="<?=$row->id?>">
  <div class="t-input">
    <span class="hidden js-hidden_box">Название блока</span>
    <input name="fd[block_name]" value="<?=$row->name?>" disabled class="validate t-portfolio-box__name_block">
  </div>
  <div class="t-input">
    <span class="hidden js-hidden_box">Описание блока</span>
    <input name="fd[block_text]" value="<?=$row->text?>" disabled class="validate">
  </div>
  <div class="t-input_file-list">
    <div class="t-load-file t-load-file_audio">
      <ul class="js-file-list">
        <?foreach($row->audios as $audio):?>
          <li class="load">
            <div class="t-load-file__item">
              <div class="t-input_video__static-box js-hidden_box">
                <a href="<?=$audio->getUploadedFileUrl('filename')?>" target="_blank" class="audio">
                  <svg width="22" height="24" viewBox="0 0 22 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <path fill="#BDBDBD" fill-rule="evenodd" d="M 14.2191 0C 14.5703 0 14.8732 0.125029 15.1294 0.375086C 15.3842 0.625057 15.5122 0.921301 15.5122 1.26338L 15.5122 22.7371C 15.5122 23.079 15.3842 23.3752 15.128 23.6253C 14.8732 23.8748 14.5703 24 14.2191 24C 13.8693 24 13.5663 23.8748 13.3102 23.6253L 6.58416 17.0528L 1.29315 17.0528C 0.943349 17.0528 0.640376 16.9276 0.384225 16.6775C 0.128075 16.4276 0 16.1315 0 15.7895L 0 8.21062C 0 7.86854 0.128075 7.57264 0.384225 7.32232C 0.638998 7.07244 0.943349 6.94741 1.29315 6.94741L 6.58416 6.94741L 13.3102 0.375086C 13.5663 0.125029 13.8693 0 14.2191 0ZM 19.8117 7.25606C 19.2182 6.75052 18.3285 6.82186 17.8231 7.41547C 17.3191 8.00908 17.3907 8.90013 17.9828 9.40568C 18.7169 10.0311 19.1796 10.9601 19.1796 12C 19.1796 13.0399 18.7169 13.9688 17.9828 14.5943C 17.3907 15.0999 17.3191 15.9909 17.8231 16.5844C 18.3285 17.1781 19.2182 17.2495 19.8117 16.7439C 21.1489 15.604 22 13.9006 22 12C 22 10.0994 21.1489 8.39597 19.8117 7.25606Z"/>
                  </svg>
                </a>
                <div class="audio-name"><?=$audio->filename?></div>
              </div>
              <div class="t-input_video__edit-box hidden js-hidden_box">
                <img src="/img/audio.png">
                <div class="audio-name"><?=$audio->filename?></div>
                <a class="js-del-audio" data-id="<?=$audio->id?>">
                  <svg width="10" height="15" viewBox="0 0 10 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path fill="#CCCCCC" d="M 5.0001 0C 5.55283 0 6.0001 0.447693 6.0001 1L 6.0001 2L 9.0001 2C 9.55283 2 10.0001 2.44769 10.0001 3C 10.0001 3.55231 9.55283 4 9.0001 4L 5.0001 4L 1.0001 4C 0.447362 4 9.62968e-05 3.55231 9.62968e-05 3C 9.62968e-05 2.44769 0.447362 2 1.0001 2L 4.0001 2L 4.0001 1C 4.0001 0.447693 4.44736 0 5.0001 0ZM 0.250096 5.3382C 0.440526 5.12317 0.712987 5 1.0001 5L 9.0001 5C 9.28721 5 9.55967 5.12317 9.7501 5.3382C 9.93955 5.55322 10.0284 5.83942 9.99228 6.12402L 8.99228 14.124C 8.92978 14.6245 8.504 15 8.0001 15L 2.0001 15C 1.49619 15 1.07041 14.6245 1.00791 14.124L 0.0079088 6.12402C -0.028224 5.83942 0.0606432 5.55322 0.250096 5.3382ZM 2.13291 7L 2.88291 13L 7.11728 13L 7.86728 7L 2.13291 7Z"></path>
                  </svg>
                </a>
              </div>
            </div>
            <div class="t-load-file__controls">
              <label class="js-load t-btn t-btn_border">Добавить аудио-файл
                <input name="audio" value="<?=$audio->id?>">
              </label>
            </div>
          </li>
        <?endforeach;?>
        <li>
          <div class="t-load-file__controls hidden js-hidden_box">
            <label class="js-load_audio t-btn_img">Добавить аудио-файл
              <input type="file" name="audio">
            </label>
          </div>
        </li>
      </ul>
    </div>
  </div>
  <a href="#del" class="t-btn t-btn_border js-delBlock hidden js-hidden_box">Удалить блок</a>
</div>