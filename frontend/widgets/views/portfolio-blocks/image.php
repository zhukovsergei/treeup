<div class="t-portfolio-box__block" data-type="image">
  <input type="hidden" name="fd[block_id]" value="<?=$row->id?>">
  <div class="t-input">
    <span class="hidden js-hidden_box">Название блока</span>
    <input name="fd[block_name]" value="<?=$row->name?>" disabled class="validate t-portfolio-box__name_block">
  </div>
  <div class="t-input">
    <span class="hidden js-hidden_box">Описание блока</span>
    <input name="fd[block_text]" value="<?=$row->text?>" disabled class="validate">
  </div>
  <div class="t-input_file-list js-file-list_img" >
    <?foreach($row->images as $image):?>
      <div class="t-input_file">
        <label class="hidden js-hidden_box t-fileLoad t-fileLoad_del js-delFile">
          <input name="img" class="t-addFile" value="<?=$image->id?>"><!--value это id картинки-->
        </label>
        <div class="t-input_file__preview">
          <a data-fancybox="img" href="<?=$image->getImageFileUrl('filename')?>">
            <img src="<?=$image->getThumbFileUrl('filename', 'thumb')?>">
          </a>
        </div>
      </div>
    <?endforeach;?>

    <div class="t-input_file">
      <label class="hidden js-hidden_box t-fileLoad t-fileLoad_add js-load_img">
        <input type="file" name="img" class="t-addFile">
      </label>
    </div>
  </div>
  <a href="#del" class="t-btn t-btn_border js-delBlock hidden js-hidden_box">Удалить блок</a>
</div>