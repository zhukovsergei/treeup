<div class="t-portfolio-box__block" data-type="video">
  <input type="hidden" name="fd[block_id]" value="<?=$row->id?>">
  <div class="t-input">
    <span class="hidden js-hidden_box">Название блока</span>
    <input name="fd[block_name]" value="<?=$row->name?>" disabled class="validate t-portfolio-box__name_block">
  </div>
  <div class="t-input">
    <span class="hidden js-hidden_box">Описание блока</span>
    <input name="fd[block_text]" value="<?=$row->text?>" disabled class="validate">
  </div>
  <div class="t-input_file-list">
    <?foreach($row->videos as $video):?>
      <div class="t-input_video">
        <div class="t-input_video__static-box js-hidden_box">
          <a class="various fancybox.iframe" href="<?=$video->url?>">
            <svg width="24" height="24" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <path fill="#BDBDBD" fill-rule="evenodd" d="M 6.00095 1.60982C 11.7389 -1.70394 19.0771 0.26236 22.3902 6.00092C 25.7043 11.7391 23.737 19.0766 17.9985 22.3905C 12.2604 25.7039 4.92246 23.7373 1.60984 17.9988C -1.70398 12.2604 0.262403 4.92252 6.00095 1.60982ZM 16.3191 11.5123L 10.0367 7.84817C 9.46416 7.51392 9.00238 7.78114 9.00552 8.44338L 9.03775 15.7163C 9.04059 16.3784 9.50804 16.6479 10.0822 16.3163L 16.3167 12.7168C 16.8903 12.3857 16.8915 11.8466 16.3191 11.5123Z"/>
            </svg>
          </a>
        </div>
        <div class="t-input_video__edit-box hidden js-hidden_box">
          <svg width="24" height="24" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <path fill="#BDBDBD" fill-rule="evenodd" d="M 6.00095 1.60982C 11.7389 -1.70394 19.0771 0.26236 22.3902 6.00092C 25.7043 11.7391 23.737 19.0766 17.9985 22.3905C 12.2604 25.7039 4.92246 23.7373 1.60984 17.9988C -1.70398 12.2604 0.262403 4.92252 6.00095 1.60982ZM 16.3191 11.5123L 10.0367 7.84817C 9.46416 7.51392 9.00238 7.78114 9.00552 8.44338L 9.03775 15.7163C 9.04059 16.3784 9.50804 16.6479 10.0822 16.3163L 16.3167 12.7168C 16.8903 12.3857 16.8915 11.8466 16.3191 11.5123Z"/>
          </svg>
          <input value="https://www.youtube.com/watch?v=h4bex7hkQaU" class="t-disabled-always" placeholder="Вставьте ссылку на YouTube-ролик">
          <a href="#del" class="t-del js-del-video" data-id="2">
            <svg width="10" height="15" viewBox="0 0 10 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <path fill="#CCCCCC" d="M 5.0001 0C 5.55283 0 6.0001 0.447693 6.0001 1L 6.0001 2L 9.0001 2C 9.55283 2 10.0001 2.44769 10.0001 3C 10.0001 3.55231 9.55283 4 9.0001 4L 5.0001 4L 1.0001 4C 0.447362 4 9.62968e-05 3.55231 9.62968e-05 3C 9.62968e-05 2.44769 0.447362 2 1.0001 2L 4.0001 2L 4.0001 1C 4.0001 0.447693 4.44736 0 5.0001 0ZM 0.250096 5.3382C 0.440526 5.12317 0.712987 5 1.0001 5L 9.0001 5C 9.28721 5 9.55967 5.12317 9.7501 5.3382C 9.93955 5.55322 10.0284 5.83942 9.99228 6.12402L 8.99228 14.124C 8.92978 14.6245 8.504 15 8.0001 15L 2.0001 15C 1.49619 15 1.07041 14.6245 1.00791 14.124L 0.0079088 6.12402C -0.028224 5.83942 0.0606432 5.55322 0.250096 5.3382ZM 2.13291 7L 2.88291 13L 7.11728 13L 7.86728 7L 2.13291 7Z"></path>
            </svg>
          </a>
        </div>
      </div>
    <?endforeach;?>
    <div class="t-input hidden js-hidden_box">
      <a class="t-btn_img js-add_video" href="#addLink">Добавить ссылку на видео</a>
    </div>
  </div>
  <a href="#del" class="t-btn t-btn_border js-delBlock hidden js-hidden_box">Удалить блок</a>
</div>