<h2>Безопасность</h2>
<div class="t-work-area_blur__box">
  <form id="password_form" action="/profile/<?=Yii::$app->getUser()->getIdentity()->profile_type?>/settings/make-password" method="POST">
    <div class="js-hidden_box">
      <h3>Пароль</h3>
      <p>Ваша учётная запись была зарегистрирована при помощи профиля социальной сети.<br>Если вам также необходима возможностть входа посредством электронной почты и пароля, то вы можете создать пароль здесь.</p>
    </div>
    <div class="js-hidden_box hidden">
      <div class="t-input t-input_30"><span>Пароль</span>
        <input type="password" name="fd[password]" data-name="password_doubel" class="identical">
      </div>
      <div class="t-input t-input_30"><span>Повторите пароль</span>
        <input type="password" name="fd[password_repeat]" data-name="password_doubel" class="identical">
      </div>
      <input type="hidden" name="_csrf" value="<?=\Yii::$app->request->getCsrfToken()?>">
    </div>
    <div class="t-btnBox"><a href="#create-password" class="t-btn js-edit js-hidden_box">Создать пароль</a>
      <div class="hidden js-hidden_box">
        <button class="t-btn js-password_create">Сохранить</button>
        <a href="#clearForm" class="t-btn t-btn_border js-clear-form">Отменить редактирование</a>
      </div>
    </div>
  </form>
</div>