<footer>
  <p>© TreeUp 2017</p>
  <div class="t-footer__menu">
      <a href="/text_page.html">Новости</a>
      <a href="/tasks">Задачи</a>
      <a href="/about-us">О сервисе</a>
      <a href="/team">Команда</a>
      <a href="/about-company">О компании</a>
      <a href="/help">Помощь</a>
      <a href="/contacts">Контакты</a>
  </div>
  <a href="http://tdsgn.ru" target="_blank">Сделано в t</a>
</footer>