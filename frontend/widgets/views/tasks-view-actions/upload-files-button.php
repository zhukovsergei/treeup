<div class="t-task-custumer__info_long__box">
    <p class="black">Вы можете прикрепить файлы с результатами вашей работы</p>
    <a class="t-btn" href="/tasks/<?=$task->id?>/files">Перейти к загрузке</a><!--Если эта кнопка одна то добавлять класс t-margin-->
    <a class="t-btn t-btn_border t-margin" href="/src/dialog/index.html">Написать заказчику</a><!--Кнопка должна быть видна только если данный исполнитель выбран победителем в данной задаче-->
    <h3>Вы откликнулись</h3>
    <p class="black"><?=$row->date_add?></p>
</div>