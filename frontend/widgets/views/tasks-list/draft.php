<?php

use aiur\helpers\TaskPricePostHelper;
use yii\helpers\Url;

?>

<div class="t-task-custumer__info">
  <div class="title">Черновик</div>
  <p>Чтобы задача стала активной, отправьте ее на модерацию</p>
  <ul class="t-task-custumer__buttons">
    <li>
      <a href="<?=Url::to(['/tasks/edit','id' => $task->id])?>" class="t-btn t-btn_border js-edit_task" data-type="mod">Редактировать</a>
    </li>
  </ul>
</div>