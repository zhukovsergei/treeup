<div class="t-portfolio-box__block">
  <h3><?=$row->name?></h3>
  <div class="t-portfolio-box__block__text"><?=$row->text?></div>
  <div class="t-input_file-list">
    <?foreach($row->videos as $video):?>
      <div class="t-input_video">
        <div class="t-input_video__static-box">
          <a class="various fancybox.iframe" href="<?=$video->url?>">
            <svg width="24" height="24" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <path fill="#BDBDBD" fill-rule="evenodd" d="M 6.00095 1.60982C 11.7389 -1.70394 19.0771 0.26236 22.3902 6.00092C 25.7043 11.7391 23.737 19.0766 17.9985 22.3905C 12.2604 25.7039 4.92246 23.7373 1.60984 17.9988C -1.70398 12.2604 0.262403 4.92252 6.00095 1.60982ZM 16.3191 11.5123L 10.0367 7.84817C 9.46416 7.51392 9.00238 7.78114 9.00552 8.44338L 9.03775 15.7163C 9.04059 16.3784 9.50804 16.6479 10.0822 16.3163L 16.3167 12.7168C 16.8903 12.3857 16.8915 11.8466 16.3191 11.5123Z"></path>
            </svg>
          </a>
        </div>
      </div>
    <?endforeach;?>

  </div>
</div>