<div class="t-portfolio-box__block">
  <h3><?=$row->name?></h3>
  <div class="t-portfolio-box__block__text"><?=$row->text?></div>
  <div class="t-input_file-list">
    <div class="t-load-file t-load-file_audio">
      <ul class="js-file-list">
        <li class="load">
          <?foreach($row->audios as $audio):?>
            <div class="t-load-file__item">
              <div class="t-input_video__static-box js-hidden_box">
                <a href="<?=$audio->getUploadedFileUrl('filename')?>" target="_blank" class="audio">
                  <svg width="22" height="24" viewBox="0 0 22 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <path fill="#BDBDBD" fill-rule="evenodd" d="M 14.2191 0C 14.5703 0 14.8732 0.125029 15.1294 0.375086C 15.3842 0.625057 15.5122 0.921301 15.5122 1.26338L 15.5122 22.7371C 15.5122 23.079 15.3842 23.3752 15.128 23.6253C 14.8732 23.8748 14.5703 24 14.2191 24C 13.8693 24 13.5663 23.8748 13.3102 23.6253L 6.58416 17.0528L 1.29315 17.0528C 0.943349 17.0528 0.640376 16.9276 0.384225 16.6775C 0.128075 16.4276 0 16.1315 0 15.7895L 0 8.21062C 0 7.86854 0.128075 7.57264 0.384225 7.32232C 0.638998 7.07244 0.943349 6.94741 1.29315 6.94741L 6.58416 6.94741L 13.3102 0.375086C 13.5663 0.125029 13.8693 0 14.2191 0ZM 19.8117 7.25606C 19.2182 6.75052 18.3285 6.82186 17.8231 7.41547C 17.3191 8.00908 17.3907 8.90013 17.9828 9.40568C 18.7169 10.0311 19.1796 10.9601 19.1796 12C 19.1796 13.0399 18.7169 13.9688 17.9828 14.5943C 17.3907 15.0999 17.3191 15.9909 17.8231 16.5844C 18.3285 17.1781 19.2182 17.2495 19.8117 16.7439C 21.1489 15.604 22 13.9006 22 12C 22 10.0994 21.1489 8.39597 19.8117 7.25606Z"></path>
                  </svg>
                </a>
                <div class="audio-name"><?=$audio->filename?></div>
              </div>
            </div>
          <?endforeach;?>
        </li>
      </ul>
    </div>
  </div>
</div>