<div class="t-portfolio-box__block">
  <h3><?=$row->name?></h3>
  <div class="t-portfolio-box__block__text"><?=$row->text?></div>
  <div class="t-input_file-list">
    <?foreach($row->images as $image):?>
      <div class="t-input_file">
        <div class="t-input_file__preview">
          <a data-fancybox="img" href="<?=$image->getImageFileUrl('filename')?>">
            <img src="<?=$image->getThumbFileUrl('filename', 'thumb')?>">
          </a>
        </div>
      </div>
    <?endforeach;?>
  </div>
</div>