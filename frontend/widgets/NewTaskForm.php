<?php
namespace frontend\widgets;

use common\models\Industry;
use common\models\Project;
use common\models\Specialization;
use common\models\Town;
use yii\base\Widget;

class NewTaskForm extends Widget
{

  public function run()
  {
    $projects = Project::find()->where(['uid' => \Yii::$app->getUser()->getId()])->all();
    $towns = Town::find()->all();
    $industries = Industry::find()->all();
    $specializations = Specialization::find()->all();

    return $this->render('new-task-form', [
      'projects' => $projects,
      'towns' => $towns,
      'industries' => $industries,
      'specializations' => $specializations,
      'specializations' => $specializations,
    ]);
  }
}