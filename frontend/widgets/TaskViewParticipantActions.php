<?php
namespace frontend\widgets;

use common\models\TaskParticipant;
use yii\base\Widget;

class TaskViewParticipantActions extends Widget
{
  public $task;

  public function run()
  {
    if( $this->task->isTeam() )
    {
      $countParticipants = TaskParticipant::find()->where(['task_id' => $this->task->id, 'performer' => 1])->count();

      if( $countParticipants > 0 )
      {
        return $this->render('tasks-view-participant-actions/team-finish-button', [
          'row' => $this->task,
          'countParticipants' => $countParticipants,
        ]);
      }
      else
      {
        return $this->render('tasks-view-participant-actions/team-finish-button-without-winner', [
          'row' => $this->task,
        ]);
      }
    }
    else
    {
      $winner = TaskParticipant::find()->where(['task_id' => $this->task->id, 'winner' => 1])->one();

      if( $winner )
      {
        return $this->render('tasks-view-participant-actions/finish-button', [
          'row' => $this->task,
          'winner' => $winner->user,
        ]);
      }
      else
      {
        return $this->render('tasks-view-participant-actions/finish-button-without-winner', [
          'row' => $this->task,
        ]);
      }

    }
  }
}