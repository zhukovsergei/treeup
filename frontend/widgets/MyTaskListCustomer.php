<?php

namespace frontend\widgets;

use common\models\Task;
use yii\base\Widget;

class MyTaskListCustomer extends Widget
{
    public $task;

    public function run()
    {
        if (in_array($this->task->status, array_keys(Task::STATUS))) {
            $user = \Yii::$app->getUser()->getIdentity();
            return $this->render('tasks-list/' . $this->task->status,[
                'user' => $user,
                'task' => $this->task
                ]);
        }

        return null;
//    return new \Exception('not found template for status');
    }
}