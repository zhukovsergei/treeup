<?php

namespace frontend\widgets;

use common\models\TaskParticipant;
use yii\base\Widget;

class MyTaskViewCustomer extends Widget
{
    public $task;

    public function run()
    {
        $quantityRequest = TaskParticipant::find()->where(['task_id' => $this->task->id])->count();
        $user = \Yii::$app->getUser()->getIdentity();
        return $this->render('tasks-view/' . $this->task->status, [
            'row' => $this->task,
            'quantityRequest' => $quantityRequest,
            'user' => $user
        ]);

//    return new \Exception('not found template for status');
    }
}