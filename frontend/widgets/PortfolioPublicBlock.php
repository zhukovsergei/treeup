<?php
namespace frontend\widgets;

use common\models\Task;
use yii\base\Widget;

class PortfolioPublicBlock extends Widget
{
  public $block;

  public function run()
  {
//    if(in_array($this->portfolio->status, array_keys(Task::STATUS)))
//    {
      return $this->render('portfolio-public-blocks/'.$this->block->type, [
        'row' => $this->block,
      ]);
//    }

//    return null;
//    return new \Exception('not found template for status');
  }
}