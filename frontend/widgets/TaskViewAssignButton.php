<?php

namespace frontend\widgets;

use common\models\Industry;
use common\models\Project;
use common\models\Specialization;
use common\models\Task;
use common\models\TaskParticipant;
use common\models\Town;
use yii\base\Widget;

class TaskViewAssignButton extends Widget
{
    public $user;
    public $task;

    public function run()
    {
        if ($this->user && $this->task) {
            $row = TaskParticipant::find()->where(['task_id' => $this->task->id, 'participant_id' => $this->user->id])->one();
            if ($row && $this->task->status == Task::STATUS_DB['published']) {
                return $this->render('tasks-view-actions/upload-files-button', [
                    'row' => $row,
                    'task' => $this->task,
                    'user_id' => \Yii::$app->getUser()->getId(),

                ]);
            }
            if ($row && $this->task->status == Task::STATUS_DB['finished']) {
                return $this->render('tasks-view-actions/finished', [
                    'row' => $row,
                    'task' => $this->task,
                    'user_id' => \Yii::$app->getUser()->getId(),

                ]);
            }
        }

        return $this->render('tasks-view-actions/assign-button', [
        ]);
    }
}