<?php
namespace frontend\controllers;

use aiur\forms\LostPasswordForm;
use aiur\forms\ResetPasswordForm;
use aiur\services\LostPasswordService;
use common\models\Industry;
use common\models\MainReview;
use common\models\Task;
use frontend\components\FrontendController;
use yii\helpers\ArrayHelper;

class SiteController extends FrontendController
{
    public $enableCsrfValidation = false;

    private $lostPasswordService;

    public function __construct( $id, $module, LostPasswordService $lostPasswordService, array $config = [] )
    {
        parent::__construct( $id, $module, $config );

        $this->lostPasswordService = $lostPasswordService;
    }


  public function actionIndex()
  {
    $mainReviews = MainReview::find()->all();
    $countTasks = Task::find()->where(['status' => 'published'])->count();
    $lasTasks = Task::find()->where(['status' => 'published'])->orderBy('date_add DESC')->limit(5)->all();
    $countCategories = Industry::find()->where(['id' => ArrayHelper::getColumn($lasTasks, 'industry_id')])->count();

    return $this->render('index',[
      'user' => \Yii::$app->getUser()->getIdentity(),
      'countTasks' => $countTasks,
      'countCategories' => $countCategories,
      'lasTasks' => $lasTasks,
      'mainReviews' => $mainReviews,
    ]);
  }

    public function actionError()
    {
        $exception = \Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            return $this->render('error', ['exception' => $exception]);
        }
        return false;
    }

    public function actionLostPassword()
    {
        $form = new LostPasswordForm();
        if($form->load(\Yii::$app->request->post(), 'fd') && $form->validate())
        {
            $this->lostPasswordService->request($form);
            return true;
        }

        return false;
    }

    public function actionResetPassword($token)
    {
        $this->lostPasswordService->validate($token);

        $form = new ResetPasswordForm();

        if($form->load(\Yii::$app->request->post(), 'fd') && $form->validate())
        {
            $this->lostPasswordService->reset($token, $form);

            return $this->goHome();
        }

        return $this->render('reset-password');
    }
}
