<?php
namespace frontend\controllers;

use common\models\Callback;
use common\models\Page;
use frontend\components\FrontendController;

class TeamController extends FrontendController
{
  public function actionIndex()
  {
    $row = Page::find()->where(['extra' => 'team'])->one();

    return $this->render('index', [
      'row' => $row,
    ]);
  }
}
