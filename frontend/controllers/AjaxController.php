<?php
namespace frontend\controllers;

use aiur\forms\LoginForm;
use aiur\forms\QuickSignUpForm;
use aiur\helpers\user\ProfileUrlHelper;
use aiur\services\SignUpService;
use aiur\services\UserService;
use frontend\components\FrontendController;
use yii\web\Response;

class AjaxController extends FrontendController
{

  private $signUpService;

  public function __construct( $id, $module, SignUpService $signUpService, array $config = [] )
  {
    parent::__construct( $id, $module, $config );

    $this->signUpService = $signUpService;
  }

  public function init()
  {
    parent::init();

    \Yii::$app->response->format =Response::FORMAT_JSON;
  }

  public function actionLogin()
  {
    $form = new LoginForm();
    if($form->load(\Yii::$app->request->post(), 'fd') && $form->validate())
    {
      \Yii::$app->user->login($form->getAuthoredUser(), 3600*24*7);
      return ProfileUrlHelper::make(\Yii::$app->user->getIdentity());
    }

    return $form->getErrors();
  }

  public function actionRegister()
  {
    $form = new QuickSignUpForm();
    if($form->load(\Yii::$app->request->post(), 'fd') && $form->validate())
    {
      $this->signUpService->request($form);

      return true;
    }

    return $form->getErrors();
  }

  public function actionLogout()
  {
    \Yii::$app->user->logout();
    return true;
  }
}
