<?php
namespace frontend\controllers;

use common\models\Callback;
use common\models\Page;
use frontend\components\FrontendController;

class AboutUsController extends FrontendController
{
  public function actionIndex()
  {
    $row = Page::find()->where(['extra' => 'about_service'])->one();

    return $this->render('index', [
      'row' => $row,
    ]);
  }
}
