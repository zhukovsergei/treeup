<?php
namespace frontend\controllers;

use aiur\services\SignUpService;
use common\models\User;
use frontend\components\FrontendController;

class AuthController extends FrontendController
{
  public $enableCsrfValidation = false;

  private $signUpService;

  public function __construct( $id, $module, SignUpService $signUpService, array $config = [] )
  {
    parent::__construct( $id, $module, $config );

    $this->signUpService = $signUpService;
  }

  public function actionUlogin()
  {
    $token = \Yii::$app->getRequest()->post('token');

    $result = json_decode(file_get_contents('http://ulogin.ru/token.php?token=' . $token .'&host=' . \Yii::$app->getRequest()->getServerName()), true);

    if(empty($result['email']))
    {
      throw new \Exception('Empty email');
    }

    if($user = User::findByEmail($result['email']))
    {
      \Yii::$app->getUser()->login($user, 3600*24*7);
      $type = \Yii::$app->getUser()->getIdentity()->profile_type;
        return $this->redirect('/profile/'.$type.'/feed');
    }
    else
    {
      $user = $this->signUpService->prepareFromSocial($result);
      \Yii::$app->getUser()->login($user, 3600*24*7);

      return $this->redirect('/registration/choice/role');
    }
  }

    public function actionLogout()
    {
        \Yii::$app->user->logout();
        return $this->redirect('/');
    }
}
