<?php
namespace frontend\controllers;

use aiur\services\SignUpService;
use frontend\components\FrontendController;

class SignupController extends FrontendController
{
  public $signUpService;

  public function __construct($id, $module, SignupService $signUpService, $config = [])
  {
    parent::__construct($id, $module, $config);
    $this->signUpService = $signUpService;
  }

  public function actionConfirm($token)
  {
    try {
      $this->signUpService->confirm($token);
      return $this->redirect('/registration/choice/role');
    } catch (\DomainException $e) {
//      Yii::$app->errorHandler->logException($e);
//      Yii::$app->session->setFlash('error', $e->getMessage());
    }
    return $this->goHome();
  }


}
