<?php
/**
 * Created by PhpStorm.
 * User: Adobe
 * Date: 12.12.2017
 * Time: 11:58
 */

namespace frontend\controllers\profile;

trait ProfileBehaviorsTrait
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => \yii\filters\AccessControl::class,
        'rules' => [
          [
            'allow' => true,
            'matchCallback' => function ($rule, $action) {

              $user = \Yii::$app->user->getIdentity();

              if( empty($user) )
              {
                return false;
              }

              if($user->hasAnyRole() && $user->isMatchRoleAndCurrentPage())
              {
                return true;
              }

              return false;
            }
          ],
        ],

        'denyCallback' => function ($rule, $action)
        {
          $user =  \Yii::$app->user->getIdentity();

          if( $user && !$user->hasAnyRole() ){
            return \Yii::$app->response->redirect('/registration/choice/role');
          }
          return \Yii::$app->response->redirect('/');
        }
      ],
    ];
  }

}