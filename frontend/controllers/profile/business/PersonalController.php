<?php
namespace frontend\controllers\profile\business;

use aiur\forms\profile\business\PersonalDataForm;
use aiur\forms\profile\business\SocialForm;
use aiur\services\CompanyService;
use aiur\services\ProfileService;
use common\models\Town;
use frontend\components\FrontendController;
use frontend\controllers\profile\ProfileBehaviorsTrait;
use yii\web\Response;

class PersonalController extends FrontendController
{
  use ProfileBehaviorsTrait;

//  protected $userService;
  protected $profileService;
  protected $companyService;
  protected $me;

  public function __construct( string $id, $module,
//                               UserService $userService,
                               ProfileService $profileService,
                               CompanyService $companyService,
                               array $config = [] )
  {
    parent::__construct( $id, $module, $config );

//    $this->userService = $userService;
    $this->profileService = $profileService;
    $this->companyService = $companyService;
    $this->me = \Yii::$app->getUser()->getIdentity();
  }

  public function actionIndex()
  {
    $user = \Yii::$app->getUser()->getIdentity();
    $towns = Town::find()->all();

    return $this->render('index', [
      'user' => $user,
      'towns' => $towns,
    ]);
  }

  public function actionSavePersonal()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;

    $form = new PersonalDataForm();

    if( $form->load(\Yii::$app->request->post(), 'fd') && $form->validate() )
    {
      $this->profileService->saveOrNew(\Yii::$app->getUser()->getId(), $form);
      return $form;
    }

    return $form->getErrors();
  }

  public function actionSaveSocial()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;

    $form = new SocialForm();

    if( $form->load(\Yii::$app->request->post(), 'fd') && $form->validate() )
    {
      $this->profileService->updateSocial(\Yii::$app->getUser()->getId(), $form);
      return $form;
    }

    return $form->getErrors();
  }

}
