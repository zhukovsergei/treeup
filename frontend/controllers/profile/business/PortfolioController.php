<?php
namespace frontend\controllers\profile\business;

use aiur\forms\portfolio\PortfolioCreateForm;
use aiur\forms\portfolio\PortfolioUpdateForm;
use aiur\services\portfolio\PortfolioService;
use common\models\Portfolio;
use frontend\components\FrontendController;
use frontend\controllers\profile\ProfileBehaviorsTrait;
use yii\web\Response;

class PortfolioController extends FrontendController
{
  use ProfileBehaviorsTrait;

  protected $portfolioService;
  protected $me;

  public function __construct( string $id, $module,
                               PortfolioService $portfolioService,
                               array $config = [] )
  {
    parent::__construct( $id, $module, $config );

    $this->portfolioService = $portfolioService;
    $this->me = \Yii::$app->getUser()->getIdentity();
  }

  public function actionIndex()
  {
    $rows = Portfolio::find()->where(['uid' => \Yii::$app->getUser()->getId()])->all();

    return $this->render('index', [
      'rows' => $rows,
    ]);
  }

  public function actionCreate()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;

    $form = new PortfolioCreateForm();
    if($form->load(\Yii::$app->request->post(), 'fd') && $form->validate())
    {
      $row = $this->portfolioService->createPortfolio(\Yii::$app->getUser()->getId(), $form);

      return $row;
    }

    return $form->getErrors();
  }

  public function actionUpdate()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $id = \Yii::$app->request->post('id');

    $form = new PortfolioUpdateForm($id, \Yii::$app->user->getId());

    if($form->load(\Yii::$app->request->post(), 'fd') && $form->validate())
    {
      $portfolio = $this->portfolioService->updatePortfolio($form);
      return $portfolio;
    }
    return $form->getErrors();
  }

  public function actionRemove()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $id = \Yii::$app->request->post('id');
    $this->portfolioService->removePortfolio($id);
    return true;
  }
}
