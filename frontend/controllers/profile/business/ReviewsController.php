<?php
namespace frontend\controllers\profile\business;

use common\models\Review;
use frontend\components\FrontendController;
use frontend\controllers\profile\ProfileBehaviorsTrait;

class ReviewsController extends FrontendController
{
  use ProfileBehaviorsTrait;

  public function actionIndex()
  {
    return $this->render('index', [
      'reviews' => Review::find()->all()
    ]);
  }
}
