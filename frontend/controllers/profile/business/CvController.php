<?php
namespace frontend\controllers\profile\business;

use aiur\forms\profile\business\EducationForm;
use aiur\services\profile\CvService;
use common\models\Industry;
use common\models\Specialization;
use common\models\Town;
use frontend\components\FrontendController;
use frontend\controllers\profile\ProfileBehaviorsTrait;
use yii\web\Response;

class CvController extends FrontendController
{
  use ProfileBehaviorsTrait;

  protected $cvService;
  protected $me;

  public function __construct( string $id, $module,
                               CvService $cvService,
                               array $config = [] )
  {
    parent::__construct( $id, $module, $config );

    $this->cvService = $cvService;
    $this->me = \Yii::$app->getUser()->getIdentity();
  }

  public function actionIndex()
  {
    return $this->render('index', [
      'user' => $this->me,
      'towns' => Town::find()->all(),
      'industries' => Industry::find()->all(),
      'specializations' => Specialization::find()->all(),
    ]);
  }

}
