<?php
namespace frontend\controllers\profile\business;

use aiur\forms\portfolio\PortfolioCreateForm;
use aiur\forms\portfolio\PortfolioUpdateForm;
use aiur\services\portfolio\PortfolioService;
use common\models\Portfolio;
use frontend\components\FrontendController;
use frontend\controllers\profile\ProfileBehaviorsTrait;
use yii\web\Response;

class BonusController extends FrontendController
{
  use ProfileBehaviorsTrait;

//  protected $portfolioService;
  protected $me;

  public function __construct( string $id, $module,
//                               PortfolioService $portfolioService,
                               array $config = [] )
  {
    parent::__construct( $id, $module, $config );

//    $this->portfolioService = $portfolioService;
    $this->me = \Yii::$app->getUser()->getIdentity();
  }

  public function actionIndex()
  {
    return $this->render('index', [
      'user' => $this->me
    ]);
  }

}
