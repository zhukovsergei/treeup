<?php
namespace frontend\controllers\profile\business;

use aiur\filters\TaskBusinessReadRepository;
use aiur\filters\TaskFilterCriteria;
use common\models\Industry;
use common\models\Specialization;
use common\models\Task;
use common\models\TaskParticipant;
use common\models\Town;
use frontend\components\FrontendController;
use frontend\controllers\profile\ProfileBehaviorsTrait;
use yii\web\Response;

class TasksController extends FrontendController
{
  use ProfileBehaviorsTrait;

  public function actionIndex()
  {
    $task_ids = TaskParticipant::find()->where(['participant_id' => \Yii::$app->getUser()->getId()])->column();

    $tasks = Task::find()->where(['id' => $task_ids])->orderBy('id DESC')->all();

    $specializations = Specialization::find()->all();
    $industries = Industry::find()->all();

    return $this->render('index', [
      'tasks' => $tasks,
      'towns' => Town::find()->all(),
      'specializations' => $specializations,
      'industries' => $industries,
      'me' => \Yii::$app->getUser()->getIdentity(),
    ]);
  }

  public function actionFilter()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;

    $fd = \Yii::$app->request->post('fd');

    $criteria = new TaskFilterCriteria($fd);

    $search = new TaskBusinessReadRepository();

    return $search->search($criteria);
  }

}
