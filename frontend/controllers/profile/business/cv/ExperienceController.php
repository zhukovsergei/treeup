<?php
namespace frontend\controllers\profile\business\cv;

use aiur\forms\profile\business\EducationForm;
use aiur\forms\profile\business\ExperienceForm;
use aiur\services\profile\CvService;
use aiur\services\profile\ExpService;
use common\models\Town;
use frontend\components\FrontendController;
use frontend\controllers\profile\ProfileBehaviorsTrait;
use yii\web\Response;

class ExperienceController extends FrontendController
{
  protected $expService;
  protected $me;

  public function __construct( string $id, $module,
                               ExpService $expService,
                               array $config = [] )
  {
    parent::__construct( $id, $module, $config );

    $this->expService = $expService;
    $this->me = \Yii::$app->getUser()->getIdentity();
  }

  public function actionAdd()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;

    $form = new ExperienceForm();

    if($form->load(\Yii::$app->request->post(), 'fd') && $form->validate())
    {
      $row = $this->expService->createExperience(\Yii::$app->user->getId(), $form);
      return $row;
    }
    return $form->getErrors();
  }

  public function actionUpdate()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $id = \Yii::$app->request->post('id');

    $form = new ExperienceForm($id, \Yii::$app->user->getId());

    if($form->load(\Yii::$app->request->post(), 'fd') && $form->validate())
    {
      $this->expService->updateExperience($form);
      return true;
    }
    return $form->getErrors();
  }

  public function actionRemove()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $id = \Yii::$app->request->post('id');
    $this->expService->removeExperience($id);
    return true;
  }

}
