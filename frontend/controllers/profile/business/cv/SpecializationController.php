<?php
namespace frontend\controllers\profile\business\cv;

use aiur\forms\profile\business\SpecEditForm;
use aiur\services\profile\SpecService;
use frontend\components\FrontendController;
use yii\web\Response;

class SpecializationController extends FrontendController
{
  protected $specService;
  protected $me;
  public $enableCsrfValidation = false;

  public function __construct( string $id, $module,
                               SpecService $specService,
                               array $config = [] )
  {
    parent::__construct( $id, $module, $config );

    $this->specService = $specService;
    $this->me = \Yii::$app->getUser()->getIdentity();
  }

  public function actionAdd()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;

    $form = new SpecEditForm();

    if($form->load(\Yii::$app->request->post(), 'fd') && $form->validate())
    {
      $row = $this->specService->createSpec(\Yii::$app->getUser()->getId(), $form);
      return $row;
    }
    return $form->getErrors();
  }

  public function actionUpdate()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $id = \Yii::$app->request->post('id');

    $form = new SpecEditForm($id, \Yii::$app->user->getId());

    if($form->load(\Yii::$app->request->post(), 'fd') && $form->validate())
    {
      $row = $this->specService->updateSpec($form);
      return $row;
    }
    return $form->getErrors();
  }

  public function actionRemove()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $id = \Yii::$app->request->post('id');
    $this->specService->removeSpec($id);
    return true;
  }

}
