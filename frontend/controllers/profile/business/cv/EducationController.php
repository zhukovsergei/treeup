<?php
namespace frontend\controllers\profile\business\cv;

use aiur\forms\profile\business\EducationForm;
use aiur\services\profile\CvService;
use aiur\services\profile\EducationService;
use common\models\Town;
use frontend\components\FrontendController;
use frontend\controllers\profile\ProfileBehaviorsTrait;
use yii\web\Response;

class EducationController extends FrontendController
{
  protected $educationService;
  protected $me;

  public function __construct( string $id, $module,
                               EducationService $educationService,
                               array $config = [] )
  {
    parent::__construct( $id, $module, $config );

    $this->educationService = $educationService;
    $this->me = \Yii::$app->getUser()->getIdentity();
  }

  public function actionAdd()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;

    $form = new EducationForm();

    if($form->load(\Yii::$app->request->post(), 'fd') && $form->validate())
    {
      $row = $this->educationService->createEducation(\Yii::$app->user->getId(), $form);
      return $row;
    }
    return $form->getErrors();
  }

  public function actionUpdate()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $id = \Yii::$app->request->post('id');

    $form = new EducationForm($id, \Yii::$app->user->getId());

    if($form->load(\Yii::$app->request->post(), 'fd') && $form->validate())
    {
      $this->educationService->updateEducation($form);
      return true;
    }
    return $form->getErrors();
  }

  public function actionRemove()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $id = \Yii::$app->request->post('id');
    $this->educationService->removeEducation($id);
    return true;
  }
}
