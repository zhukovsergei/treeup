<?php
namespace frontend\controllers\profile\customer;

use aiur\forms\profile\customer\ProjectForm;
use aiur\services\profile\ProjectService;
use common\models\Task;
use frontend\components\FrontendController;
use frontend\controllers\profile\ProfileBehaviorsTrait;
use yii\web\Response;

class ProjectsController extends FrontendController
{
  use ProfileBehaviorsTrait;

  protected $projectService;
  protected $me;

  public function __construct( string $id, $module,
                               ProjectService $projectService,
                               array $config = [] )
  {
    parent::__construct( $id, $module, $config );

    $this->projectService = $projectService;
    $this->me = \Yii::$app->getUser()->getIdentity();
  }

  public function actionIndex()
  {
    $projects = $this->me->getProjects()->all();
    $freeTasks = Task::find()->where(['uid' => \Yii::$app->getUser()->getId()])->all();

    return $this->render('index', [
      'projects' => $projects,
      'freeTasks' => $freeTasks,
    ]);
  }

  public function actionCreate()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;

    $form = new ProjectForm();

    if($form->load(\Yii::$app->request->post(), 'fd') && $form->validate())
    {
      $row = $this->projectService->createProject(\Yii::$app->user->getId(), $form);
      return $row;
    }
    return $form->getErrors();
  }

  public function actionUpdate()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $id = \Yii::$app->request->post('id');

    $form = new ProjectForm($id, \Yii::$app->user->getId());

    if($form->load(\Yii::$app->request->post(), 'fd') && $form->validate())
    {
      $row = $this->projectService->updateProject($form);
      return $row;
    }
    return $form->getErrors();
  }

  public function actionRemove()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $id = \Yii::$app->request->post('id');
    $this->projectService->removeProject($id);
    return true;
  }

}
