<?php
namespace frontend\controllers\profile\customer;

use aiur\forms\profile\customer\CompanyForm;
use aiur\services\CompanyService;
use common\models\Industry;
use frontend\components\FrontendController;
use frontend\controllers\profile\ProfileBehaviorsTrait;
use yii\web\Response;
use yii\web\UploadedFile;

class CompanyController extends FrontendController
{
  use ProfileBehaviorsTrait;

  protected $companyService;
  protected $me;

  public function __construct( string $id, $module,
                               CompanyService $companyService,
                               array $config = [] )
  {
    parent::__construct( $id, $module, $config );

    $this->companyService = $companyService;
    $this->me = \Yii::$app->getUser()->getIdentity();
  }
  public function actionIndex()
  {
    return $this->render('index', [
      'company' => \Yii::$app->user->getIdentity()->company,
      'industries' => Industry::find()->all(),
    ]);
  }

  public function actionSaveCompany()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;

    $form = new CompanyForm();
    $form->image = UploadedFile::getInstanceByName('file');

    if( $form->load(\Yii::$app->request->post(), 'fd') && $form->validate() )
    {
      $this->companyService->saveOrNew(\Yii::$app->getUser()->getId(), $form);
      return $form;
    }

    return $form->getErrors();
  }

}
