<?php
namespace frontend\controllers\profile\customer;

use aiur\filters\TaskCustomerReadRepository;
use aiur\filters\TaskFilterCriteria;
use common\models\Project;
use common\models\Task;
use common\models\TaskFile;
use common\models\TaskParticipant;
use common\models\TaskUserPin;
use common\models\User;
use frontend\components\FrontendController;
use frontend\controllers\profile\ProfileBehaviorsTrait;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\Response;

class TasksController extends FrontendController
{
  use ProfileBehaviorsTrait;

  public function actionIndex()
  {
    $tasks = Task::find()->where(['uid' => \Yii::$app->getUser()->getId()])->orderBy('id DESC')->all();
    $projects = Project::find()->where(['uid' => \Yii::$app->user->getId()])->all();

    return $this->render('index', [
      'tasks' => $tasks,
      'projects' => $projects,
    ]);
  }

  public function actionView($id)
  {
    $row = Task::find()->where(['id' => $id])->one();
    $creatorFiles = TaskFile::find()->where(['task_id' => $id, 'uid' => $row->uid])->all();

    return $this->render('view', [
      'row' => $row,
      'creatorFiles' => $creatorFiles,
    ]);
  }

  public function actionLog($id)
  {
    $row = Task::find()->where(['id' => $id])->one();

    return $this->render('log', [
      'row' => $row,
    ]);
  }

  public function actionParticipants($id)
  {
    $row = Task::find()->where(['id' => $id])->one();

    $pinnedUsersIDS = TaskUserPin::find()->select('uid')->where(['task_id' => $id])->column();
    $pinnedUsers = User::find()->where(['id' => $pinnedUsersIDS])->all();
    $notPinnedIDS = TaskParticipant::find()->select('participant_id')->where(['AND', ['NOT', ['participant_id' => $pinnedUsersIDS]], ['task_id' => $id]])->column();
    $notPinnedUsers = User::find()->where(['id' => $notPinnedIDS])->all();

    return $this->render('participants', [
      'row' => $row,
      'pinnedUsers' => $pinnedUsers,
      'notPinnedUsers' => $notPinnedUsers,
    ]);
  }

  public function actionPinParticipant()
  {
    $uid = \Yii::$app->request->post('uid');
    $task_id = \Yii::$app->request->post('task_id');

    $m = new TaskUserPin();
    $m->uid = $uid;
    $m->task_id = $task_id;
    return $m->save();
  }

  public function actionUnpinParticipant()
  {
    $uid = \Yii::$app->request->post('uid');
    $task_id = \Yii::$app->request->post('task_id');

    return TaskUserPin::deleteAll(['uid' => $uid, 'task_id' => $task_id]);
  }

  public function actionParticipantsDetails()
  {
    $id = \Yii::$app->request->post('id');
    $participant_id = \Yii::$app->request->post('participant_id');

    if(!TaskParticipant::find()->where(['task_id' => $id, 'participant_id' => $participant_id])->exists())
    {
      throw new \Exception('Not found pair');
    }

    $row = Task::find()->where(['id' => $id])->one();
    $participant = User::find()->where(['id' => $participant_id])->one();

    $files = TaskFile::find()
      ->select( new Expression("*, DATE_FORMAT(date_add,'%Y-%m-%d') AS date_add_formatted"))
      ->where(['task_id' => $id, 'uid' => $participant_id])
      ->orderBy('date_add DESC')
      ->all();
    $indexedFiles = ArrayHelper::index($files, null, 'date_add_formatted');

    return $this->renderPartial('_participant', [
      'row' => $row,
      'participant' => $participant,
      'indexedFiles' => $indexedFiles,
    ]);
  }

  public function actionFilter()
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;

    $fd = \Yii::$app->request->post('fd');

    $criteria = new TaskFilterCriteria($fd);

    $search = new TaskCustomerReadRepository();

    return $search->search($criteria);
  }
}
