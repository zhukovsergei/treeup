<?php
namespace frontend\controllers\profile\customer;

use frontend\components\FrontendController;
use frontend\controllers\profile\ProfileBehaviorsTrait;

class ReviewsController extends FrontendController
{
  use ProfileBehaviorsTrait;

  public function actionIndex()
  {
    return $this->render('index', [
    ]);
  }

}
