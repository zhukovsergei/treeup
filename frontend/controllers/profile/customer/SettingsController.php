<?php

namespace frontend\controllers\profile\customer;

use aiur\forms\profile\NotifySettingsForm;
use aiur\forms\ResetPasswordSettingsForm;
use aiur\helpers\user\UserSettingsHelper;
use aiur\services\LostPasswordService;
use common\models\SettingsNotify;
use frontend\components\FrontendController;
use frontend\controllers\profile\ProfileBehaviorsTrait;
use yii\base\ErrorException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class SettingsController extends FrontendController
{
    use ProfileBehaviorsTrait;

    public $enableCsrfValidation = false;

    private $lostPasswordService;
    private $me;

    public function __construct($id, $module, LostPasswordService $lostPasswordService, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->lostPasswordService = $lostPasswordService;
        $this->me = \Yii::$app->getUser()->getIdentity();
    }

    public function actionIndex()
    {
        $rowSettings = SettingsNotify::find()->where(['uid' => \Yii::$app->getUser()->getId()])->one();

        $settingsForm = new NotifySettingsForm($rowSettings);

        return $this->render('index', [
            'settings' => $settingsForm,
            'user' => $this->me,
        ]);
    }

    public function actionUpdate()
    {
        $fd = \Yii::$app->request->post('fd');
        return UserSettingsHelper::saveOrUpdate(\Yii::$app->getUser()->getId(), $fd);
    }

    public function actionMakePassword()
    {
        if (!\Yii::$app->request->isAjax || !\Yii::$app->request->isPost){
            throw new NotFoundHttpException();
        }
        $form = new ResetPasswordSettingsForm();
        if ($form->load(\Yii::$app->request->post(), 'fd') && $form->validate()) {
            $this->lostPasswordService->save($this->me->id, $form);
            return $this->sendOk();
        }

       return $this->sendError($form->getErrors());
    }
}
