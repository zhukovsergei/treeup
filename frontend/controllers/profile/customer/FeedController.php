<?php
namespace frontend\controllers\profile\customer;

use common\models\News;
use frontend\components\FrontendController;
use frontend\controllers\profile\ProfileBehaviorsTrait;

class FeedController extends FrontendController
{
  use ProfileBehaviorsTrait;

  public function actionIndex()
  {
    $news = News::find()->orderBy('id DESC')->all();
    $user = \Yii::$app->getUser()->getIdentity();

    return $this->render('index', [
      'news' => $news,
      'user' => $user,
    ]);
  }
}
