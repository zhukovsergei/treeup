<?php
namespace frontend\controllers\profile\customer;

use frontend\components\FrontendController;
use frontend\controllers\profile\ProfileBehaviorsTrait;

class BalanceLogController extends FrontendController
{
  use ProfileBehaviorsTrait;

//  protected $portfolioService;
  protected $me;

  public function __construct( string $id, $module,
//                               PortfolioService $portfolioService,
                               array $config = [] )
  {
    parent::__construct( $id, $module, $config );

//    $this->portfolioService = $portfolioService;
    $this->me = \Yii::$app->getUser()->getIdentity();
  }

  public function actionIndex()
  {
    return $this->render('index', [
      'user' => $this->me,
      'transactions' => $this->me->getMoneyBalanceHistory(),
    ]);
  }

}
