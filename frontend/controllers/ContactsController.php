<?php
namespace frontend\controllers;

use common\models\Callback;
use common\models\Page;
use frontend\components\FrontendController;

class ContactsController extends FrontendController
{
  public function actionIndex()
  {
    $row = Page::find()->where(['extra' => 'contacts'])->one();

    return $this->render('index', [
      'row' => $row,
    ]);
  }
}
