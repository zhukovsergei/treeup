<?php

namespace frontend\controllers\registration;

use aiur\forms\registration\business\DataForm;
use aiur\helpers\user\TypingProfileHelper;
use aiur\services\ProfileService;
use aiur\services\UserService;
use common\models\Industry;
use common\models\Specialization;
use common\models\Town;
use frontend\components\FrontendController;
use yii\web\UploadedFile;

class BusinessController extends FrontendController
{
    use RegistrationBehaviorsTrait;

    protected $userService;
    protected $profileService;
    protected $me;

    public function __construct(string $id, $module,
                                UserService $userService,
                                ProfileService $profileService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->userService = $userService;
        $this->profileService = $profileService;
        $this->me = \Yii::$app->getUser()->getIdentity();
    }

    public function actionData()
    {
        $this->userService->setBusinessProfileFor($this->me->id);

        $profile = \Yii::$app->user->getIdentity()->profile;

        $form = new DataForm($profile);

        $towns = Town::find()->all();

        return $this->render('data', [
            'form' => $form,
            'towns' => $towns,
        ]);
    }

    public function actionSaveData()
    {
        $form = new DataForm();
        $form->attributes = \Yii::$app->request->post('fd');
        $form->image = UploadedFile::getInstanceByName('file');
        if ($form->validate()) {
            $this->profileService->saveOrNew(\Yii::$app->getUser()->getId(), $form);

            return $this->redirect('/registration/business/cv');
        }
        return $this->getBack();
    }

    public function actionCv()
    {
        $industries = Industry::find()->all();
        $specializations = Specialization::find()->all();

        return $this->render('cv', [
            'user' => \Yii::$app->getUser()->getIdentity()?? null,
            'towns' => Town::find()->all(),
            'industries' => $industries,
            'specializations' => $specializations,
        ]);
    }

    public function actionSaveCv()
    {
        TypingProfileHelper::setFinishedSignUp(\Yii::$app->getUser()->getId());

        return $this->redirect('/profile/business/feed');
    }

}
