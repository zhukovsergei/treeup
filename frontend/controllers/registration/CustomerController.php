<?php

namespace frontend\controllers\registration;

use aiur\forms\registration\customer\CompanyForm;
use aiur\forms\registration\customer\DataForm;
use aiur\helpers\user\TypingProfileHelper;
use aiur\services\CompanyService;
use aiur\services\ProfileService;
use aiur\services\UserService;
use common\models\Industry;
use common\models\Town;
use frontend\components\FrontendController;
use yii\web\UploadedFile;

class CustomerController extends FrontendController
{
    use RegistrationBehaviorsTrait;

    protected $userService;
    protected $profileService;
    protected $companyService;
    protected $me;

    public function __construct(string $id, $module,
                                UserService $userService,
                                ProfileService $profileService,
                                CompanyService $companyService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->userService = $userService;
        $this->profileService = $profileService;
        $this->companyService = $companyService;
        $this->me = \Yii::$app->getUser()->getIdentity();
    }

    public function actionData()
    {
        $this->userService->setCustomerProfileFor($this->me->id);

        $profile = \Yii::$app->user->getIdentity()->profile;

        $form = new DataForm($profile);

        $towns = Town::find()->all();


        return $this->render('data', [
            'form' => $form,
            'towns' => $towns,
        ]);
    }

    public function actionSaveData()
    {
        $form = new DataForm();
        $form->attributes = \Yii::$app->request->post('fd');
        $form->image = UploadedFile::getInstanceByName('file');
        if ($form->validate()) {
            $this->profileService->saveOrNew(\Yii::$app->getUser()->getId(), $form);

            return $this->redirect('/registration/customer/company');
        }

        return $this->getBack();
    }

    public function actionCompany()
    {
        $company = \Yii::$app->user->getIdentity()->company;
        $form = new CompanyForm($company);
        return $this->render('company', [
            'form' => $form,
            'industries' => Industry::find()->all(),
        ]);
    }

    public function actionSaveCompany()
    {
        $form = new CompanyForm();
        $form->attributes = \Yii::$app->request->post('fd');
        $form->image = UploadedFile::getInstanceByName('file');

        if ($form->validate()) {
            $this->companyService->saveOrNew(\Yii::$app->getUser()->getId(), $form);
            TypingProfileHelper::setFinishedSignUp(\Yii::$app->getUser()->getId());
            return $this->redirect('/profile/customer/feed');
        }
        return $this->getBack();
    }
}
