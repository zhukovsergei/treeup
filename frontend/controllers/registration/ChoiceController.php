<?php
namespace frontend\controllers\registration;

use common\models\Callback;
use frontend\components\FrontendController;

class ChoiceController extends FrontendController
{
  use RegistrationBehaviorsTrait;

  public function actionRole()
  {
    return $this->render('role');
  }

}
