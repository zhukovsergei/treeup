<?php

namespace frontend\controllers\registration;

trait RegistrationBehaviorsTrait
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => \yii\filters\AccessControl::class,
        'rules' => [
          [
            'allow' => true,
            'matchCallback' => function ($rule, $action) {
              $user =  \Yii::$app->getUser()->getIdentity();

              if( empty($user) )
              {
                return false;
              }

              if($user->finished_signup)
              {
                return false;
              }

              return true;
            }
          ],
        ],

        'denyCallback' => function ($rule, $action)
        {
          return \Yii::$app->response->redirect('/');
        }
      ],
    ];
  }

}