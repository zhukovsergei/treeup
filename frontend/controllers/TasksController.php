<?php

namespace frontend\controllers;

use aiur\filters\TaskFilterCriteria;
use aiur\filters\TaskPublicReadRepository;
use aiur\forms\tasks\TaskCreateForm;
use aiur\forms\tasks\TaskEditForm;
use aiur\providers\TaskProvider;
use aiur\services\TaskService;
use common\models\Industry;
use common\models\Specialization;
use common\models\Task;
use common\models\Project;
use common\models\Town;
use frontend\components\FrontendController;
use frontend\widgets\TaskPrice;
use yii\base\ErrorException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class TasksController extends FrontendController
{
    public $taskService;
    public $taskProvider;
    public $me;

    public function __construct($id, $module,
                                TaskService $taskService,
                                TaskProvider $taskProvider,
                                $config = [])
    {
        $this->taskService = $taskService;
        $this->taskProvider = $taskProvider;
        $this->me = \Yii::$app->getUser()->getIdentity();

        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {

        return $this->render('index', [
            'tasks' => $this->taskProvider->getAll(),
            'towns' => Town::find()->all(),
            'specializations' => Specialization::find()->all(),
            'industries' => Industry::find()->all(),
        ]);
    }

    public function actionCreate()
    {
        if (!\Yii::$app->request->isAjax || !\Yii::$app->request->isPost) {
            throw new ErrorException('Incorrect request.');
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $form = new TaskCreateForm();
        $form->attributes = \Yii::$app->request->post('fd');

        $form->files = UploadedFile::getInstancesByName('files');
        if ($form->validate()) {
            $row = $this->taskService->createTask($this->me->id, $form);
            return $this->sendOk($row->attributes);
        }

        return $this->sendError($form->getErrors());
    }

    public function actionAdd()
    {
        if (\Yii::$app->getUser()->getIdentity()->profile_type != 'customer'){
            throw new NotFoundHttpException();
        }
        $projects = Project::find()->where(['uid' => \Yii::$app->getUser()->getId()])->orderBy('name ASC')->all();
        $towns = Town::find()->orderBy('name ASC')->all();
        $industries = Industry::find()->orderBy('name ASC')->all();
        $specializations = Specialization::find()->orderBy('name ASC')->all();

        return $this->render('add', [
            'projects' => $projects,
            'towns' => $towns,
            'industries' => $industries,
            'specializations' => $specializations,
        ]);
    }

    public function actionEdit($id)
    {
        $task = $this->taskProvider->getTask($id);
        $user = \Yii::$app->getUser()->getIdentity();
        $points = $this->taskProvider->getPoints($task->complexity);
        if ($user->profile_type != 'customer' || array_search($task->status,[Task::STATUS_DB['draft'],Task::STATUS_DB['reject']]) === false || $task->uid != $user->id){
            throw new NotFoundHttpException();
        }
        $projects = Project::find()->where(['uid' => \Yii::$app->getUser()->getId()])->orderBy('name ASC')->all();
        $towns = Town::find()->orderBy('name ASC')->all();
        $industries = Industry::find()->orderBy('name ASC')->all();
        $specializations = Specialization::find()->orderBy('name ASC')->all();

        return $this->render('edit', [
            'projects' => $projects,
            'towns' => $towns,
            'industries' => $industries,
            'specializations' => $specializations,
            'task' => $task,
            'points' => $points
        ]);
    }

    public function actionUpdate()
    {
        if (!\Yii::$app->request->isAjax || !\Yii::$app->request->isPost) {
            throw new ErrorException('Incorrect request.');
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $id = \Yii::$app->request->post('id');

        $form = new TaskEditForm($id, $this->me->id);

        $form->attributes = \Yii::$app->request->post('fd');
        $form->files = UploadedFile::getInstancesByName('files');

        if ($form->validate()) {
            $row = $this->taskService->updateTask($form);
            return $this->sendOk();
        }
        return $this->sendError($form->getErrors());
    }

    public function actionRemove()
    {
        if (!\Yii::$app->request->isAjax || !\Yii::$app->request->isPost) {
            throw new ErrorException('Incorrect request.');
        }
        $id = \Yii::$app->request->post('id');
        $status = $this->taskService->removeTask($id);
        if ($status == false){
            return $this->sendError();
        }
        return $this->sendOk();
    }

    //Отказались от прямого действия. Данное действие выполняется через создание или обновление.
    /*  public function actionSetModeration()
      {
          $id = \Yii::$app->request->post('id');
          return $this->taskService->setModeration($id);
      }*/

    public function actionGet()
    {
        if (!\Yii::$app->request->isAjax || !\Yii::$app->request->isPost) {
            throw new ErrorException('Incorrect request.');
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $id = \Yii::$app->request->post('id', 70);
        $row = Task::find()->where(['id' => $id])->one();
        if ($row->uid != \Yii::$app->getUser()->getId()) {
            return $this->sendError('Отказано в доступе.');
        }
        return $this->sendOk($row);
    }

    public function actionFilter()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $fd = \Yii::$app->request->post('fd');

        $criteria = new TaskFilterCriteria($fd);

        $search = new TaskPublicReadRepository();

        return $search->search($criteria);
    }

    public function actionCalculatePrice()
    {
        $tu_quantity = \Yii::$app->request->post('tu_quantity');

        return TaskPrice::widget([
            'tu_quantity' => $tu_quantity,
            'user_id' => $this->me->id,
        ]);
    }
}
