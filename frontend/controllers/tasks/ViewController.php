<?php
namespace frontend\controllers\tasks;

use aiur\services\ParticipantService;
use common\models\Task;
use common\models\TaskFile;
use frontend\components\FrontendController;
use yii\web\ForbiddenHttpException;

class ViewController extends FrontendController
{
//  use ProfileBehaviorsTrait;

  private $participantService;

  public function __construct( string $id, $module,
                               ParticipantService $participantService,
                               array $config = [] )
  {
    parent::__construct( $id, $module, $config );

    $this->participantService = $participantService;

  }
  public function actionIndex($id)
  {
    $tpl = 'index-guest';

    $user = \Yii::$app->getUser()->getIdentity();

    if($user && $user->profile_type === 'business')
    {
      $tpl = 'index-business';
    }

    $row = Task::find()->where(['id' => $id])->one();
    $creatorFiles = TaskFile::find()->where(['task_id' => $id, 'uid' => $row->uid])->all();
    return $this->render($tpl, [
      'row' => $row,
      'creatorFiles' => $creatorFiles,
    ]);
  }

  public function actionAssignMe()
  {
    $task_id = \Yii::$app->request->post('task_id');
    $user_id = \Yii::$app->getUser()->getId();

    return $this->participantService->assignUserToTask($user_id, $task_id);
  }

  public function actionBecomePerformer()
  {
    $task_id = \Yii::$app->request->post('task_id');
    $participant_id = \Yii::$app->request->post('participant_id');

    return $this->participantService->becomePerformer($task_id, $participant_id);
  }

  public function actionDiscardPerformer()
  {
    $task_id = \Yii::$app->request->post('task_id');
      $task = Task::findOne($task_id);
      if (empty($task) || $task == Task::STATUS_DB['finished']){
          throw new ForbiddenHttpException('Task is finished.');
      }
    $participant_id = \Yii::$app->request->post('participant_id');

    return $this->participantService->discardPerformer($task_id, $participant_id);
  }

  public function actionBecomeWinner()
  {
    $task_id = \Yii::$app->request->post('task_id');
    $participant_id = \Yii::$app->request->post('participant_id');

    return $this->participantService->becomeWinner($task_id, $participant_id);
  }

  public function actionDiscardWinner()
  {
    $task_id = \Yii::$app->request->post('task_id');
      $task = Task::findOne($task_id);
      if (empty($task) || $task == Task::STATUS_DB['finished']){
          throw new ForbiddenHttpException('Task is finished');
      }
    $participant_id = \Yii::$app->request->post('participant_id');

    return $this->participantService->discardWinner($task_id, $participant_id);
  }

  public function actionCloseTask()
  {
    $task_id = \Yii::$app->request->post('task_id');
    $this->participantService->closeTask($task_id);
    return true;
  }

  public function actionDraftTask()
  {
    $task_id = \Yii::$app->request->post('task_id');
    $this->participantService->draftTask($task_id);
    return true;
  }

  public function actionModerationTask()
  {
    $task_id = \Yii::$app->request->post('task_id');
    $this->participantService->moderationTask($task_id);
    return true;
  }
}
