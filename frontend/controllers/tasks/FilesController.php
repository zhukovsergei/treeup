<?php

namespace frontend\controllers\tasks;

use aiur\helpers\TaskFileUploaderHelper;
use common\models\Task;
use common\models\TaskFile;
use common\models\TaskParticipant;
use frontend\components\FrontendController;
use yii\base\ErrorException;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;

class FilesController extends FrontendController
{
    public function actionIndex($id)
    {
        $row = Task::find()->where(['id' => $id])->one();
        if (empty(TaskParticipant::findOne(['task_id' => $row->id,'participant_id' => \Yii::$app->getUser()->getId()]))){
            throw new ForbiddenHttpException();
        }
        $files = TaskFile::find()
            ->select(new Expression("*, DATE_FORMAT(date_add,'%Y-%m-%d') AS date_add_formatted"))
            ->where(['uid' => \Yii::$app->getUser()->getId(), 'task_id' => $id])
            ->orderBy('date_add DESC')
            ->all();

        $indexedFiles = ArrayHelper::index($files, null, 'date_add_formatted');

        return $this->render('index', [
            'row' => $row,
            'files' => $files,
            'indexedFiles' => $indexedFiles,
        ]);
    }

    public function actionUpload()
    {
        $task_id = \Yii::$app->request->post('task_id');
        if (empty(TaskParticipant::findOne(['task_id' => $task_id,'participant_id' => \Yii::$app->getUser()->getId()]))){
            throw new ForbiddenHttpException();
        }
        $files = UploadedFile::getInstancesByName('files');
        $task_id = \Yii::$app->request->post('task_id');
        $row = Task::find()->where(['id' => $task_id])->one();
        if ($row->status == Task::STATUS_DB['finished']) {
            return false;
        }

        TaskFileUploaderHelper::handle($files, $task_id, \Yii::$app->getUser()->getId());
        return true;
    }

    public function actionRemove()
    {
        $file_id = \Yii::$app->request->post('file_id');
        $file = TaskFile::findOne(['id' => $file_id]);
        if (empty($file)){
            throw new ErrorException('Incorrect file');
        }
        if (!$file->task->isParticipant()){
            throw new ForbiddenHttpException();
        }
        return TaskFile::deleteAll(['id' => $file_id, 'uid' => \Yii::$app->getUser()->getId()]);
    }

}
