<?php
namespace frontend\controllers\tasks;

use common\models\Task;
use frontend\components\FrontendController;

class LogController extends FrontendController
{
  public function actionIndex($id)
  {
    $row = Task::find()->where(['id' => $id])->one();

    return $this->render('index', [
      'row' => $row,
    ]);
  }
}
