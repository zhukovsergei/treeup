<?php
namespace frontend\controllers\user\business;

use aiur\forms\portfolio\PortfolioCreateForm;
use aiur\forms\portfolio\PortfolioUpdateForm;
use aiur\repositories\UserRepository;
use aiur\services\portfolio\PortfolioService;
use common\models\Portfolio;
use frontend\components\FrontendController;
use frontend\controllers\profile\ProfileBehaviorsTrait;
use yii\web\Response;

class PortfolioController extends FrontendController
{
  protected $userRepository;
  protected $me;

  public function __construct( string $id, $module,
//                               UserService $userService,
                               UserRepository $userRepository,
                               array $config = [] )
  {
    parent::__construct( $id, $module, $config );

//    $this->userService = $userService;
    $this->userRepository = $userRepository;
    $this->me = \Yii::$app->getUser()->getIdentity();
  }

  public function actionIndex($id)
  {
//    $rows = Portfolio::find()->where(['uid' => \Yii::$app->getUser()->getId()])->all();

    return $this->render('index', [
//      'rows' => $rows,
      'user' => $this->userRepository->get($id),
    ]);
  }

}
