<?php
namespace frontend\controllers\user\business;

use aiur\repositories\UserRepository;
use common\models\Task;
use common\models\TaskParticipant;
use frontend\components\FrontendController;

class TasksController extends FrontendController
{
  protected $userRepository;
  protected $me;

  public function __construct( string $id, $module,
//                               UserService $userService,
                               UserRepository $userRepository,
                               array $config = [] )
  {
    parent::__construct( $id, $module, $config );

//    $this->userService = $userService;
    $this->userRepository = $userRepository;
    $this->me = \Yii::$app->getUser()->getIdentity();
  }

  public function actionIndex($id)
  {
    $task_ids = TaskParticipant::find()->where(['participant_id' => $id])->column();

    $tasks = Task::find()->where(['id' => $task_ids])->orderBy('id DESC')->all();

//    $specializations = Specialization::find()->all();
//    $industries = Industry::find()->all();

    return $this->render('index', [
      'user' => $this->userRepository->get($id),
      'tasks' => $tasks,
//      'specializations' => $specializations,
//      'industries' => $industries,
    ]);
  }
}
