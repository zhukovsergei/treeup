<?php
namespace frontend\controllers\user\business;

use aiur\repositories\UserRepository;
use common\models\Review;
use frontend\components\FrontendController;

class ReviewsController extends FrontendController
{
  protected $userRepository;
  protected $me;

  public function __construct( string $id, $module,
//                               UserService $userService,
                               UserRepository $userRepository,
                               array $config = [] )
  {
    parent::__construct( $id, $module, $config );

//    $this->userService = $userService;
    $this->userRepository = $userRepository;
    $this->me = \Yii::$app->getUser()->getIdentity();
  }

  public function actionIndex($id)
  {
    return $this->render('index', [
      'user' => $this->userRepository->get($id),
      'reviews' => Review::find()->all()
    ]);
  }
}
