<?php
namespace frontend\controllers\user\business;

use aiur\forms\profile\business\PersonalDataForm;
use aiur\forms\profile\business\SocialForm;
use aiur\repositories\UserRepository;
use aiur\services\CompanyService;
use aiur\services\ProfileService;
use common\models\Town;
use frontend\components\FrontendController;
use frontend\controllers\profile\ProfileBehaviorsTrait;
use yii\web\Response;

class CvController extends FrontendController
{
  protected $userRepository;
  protected $me;

  public function __construct( string $id, $module,
//                               UserService $userService,
                               UserRepository $userRepository,
                               array $config = [] )
  {
    parent::__construct( $id, $module, $config );

//    $this->userService = $userService;
    $this->userRepository = $userRepository;
    $this->me = \Yii::$app->getUser()->getIdentity();
  }

  public function actionIndex($id)
  {
//    $towns = Town::find()->all();

    return $this->render('index', [
      'user' => $this->userRepository->get($id),
//      'towns' => $towns,
    ]);
  }

}
