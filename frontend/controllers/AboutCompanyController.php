<?php
namespace frontend\controllers;

use common\models\Callback;
use common\models\Page;
use frontend\components\FrontendController;

class AboutCompanyController extends FrontendController
{
  public function actionIndex()
  {
    $row = Page::find()->where(['extra' => 'about_company'])->one();

    return $this->render('index', [
      'row' => $row,
    ]);
  }
}
