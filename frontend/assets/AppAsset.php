<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';

    public $baseUrl = '@web';

    public $css = [
        '/css/vendor/swiper.min.css',
        '/css/vendor/jquery.fancybox.min.css',
        '/css/main.min.css',
    ];

    public $js = [
      'js/vendor.min.js',
      'js/main.min.js',
    ];

    public $depends = [
      'yii\web\JqueryAsset',
//      'frontend\assets\JqueryUI',
//      'yii\bootstrap\BootstrapAsset',
//      'frontend\assets\Arcticmodal',
//      'frontend\assets\Bxslider',
    ];
}
