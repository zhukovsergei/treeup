<?php
/**
 * Widget.php
 * @author Revin Roman http://phptime.ru
 */

namespace frontend\components\ulogin;

use yii\helpers\Html;
use yii\web\View;

/**
 * Class Widget
 * @package rmrevin\yii\ulogin
 */
class Widget extends \yii\base\Widget
{
    public $display = ULogin::D_PANEL;

    public $fields = [ULogin::F_FIRST_NAME, ULogin::F_LAST_NAME, ULogin::F_EMAIL, ULogin::F_PHOTO, ULogin::F_PHOTO_BIG];

    public $optional = [];

    public $providers = [ULogin::P_FACEBOOK, ULogin::P_VKONTAKTE, ULogin::P_ODNOKLASSNIKI, ULogin::P_GOOGLE];

    public $hidden = [];

    public $lang = ULogin::L_RU;

    public $verifyEmail = false;

    public $sortProviders = ULogin::S_RELEVANT;

    public $mobileButtons = true;

    public $redirect_uri = ['/auth/ulogin'];

    public $execute_custom_init = false;

    public function init()
    {
        parent::init();

        if (empty($this->redirect_uri)) {
            throw new Exception(\Yii::t('app', 'You must specify the "{param}".', ['{param}' => 'redirect_uri']));
        }

        \Yii::$app
            ->getView()
            ->registerJsFile(
                '//ulogin.ru/js/ulogin.js',
                ['position' => View::POS_HEAD]
            );
    }

    /**
     * Executes the widget.
     */
    public function run()
    {
        $widget_id = $this->getId();

        $widgetParams = [
            'display' => $this->display,
            'fields' => implode(',', $this->fields),
            'optional' => implode(',', $this->optional),
            'providers' => implode(',', $this->providers),
            'hidden' => implode(',', $this->hidden),
            'redirect_uri' => urlencode(\Yii::$app->getUrlManager()->createAbsoluteUrl($this->redirect_uri))
        ];

        // lang param by default is not set
        // language is determined by user's browser locale
        if ($this->lang && $this->lang !== ULogin::L_AUTO) {
            $widgetParams['lang'] = $this->lang;
        }

        // relevant providers sorting is enabled by default
        if ($this->sortProviders && $this->sortProviders !== ULogin::S_RELEVANT) {
            $widgetParams['sort'] = $this->sortProviders;
        }

        // verification is disabled by default
        if ($this->verifyEmail) {
            $widgetParams['verify'] = $this->verifyEmail;
        }

        // mobile buttons display is enabled by default
        if (!$this->mobileButtons) {
            $widgetParams['mobilebuttons'] = $this->mobileButtons;
        }

        $action = str_replace(['&', '%2C'], [';', ','], http_build_query($widgetParams));
        $arr = implode(';',array_map(function($k, $v) { return "$k = $v"; }, array_keys($widgetParams), $widgetParams));

        echo Html::tag('div', '', [
            'id' => $widget_id,
            'data-ulogin' => $action,
        ]);

        if ($this->execute_custom_init === true) {
            \Yii::$app
                ->getView()
                ->registerJs("uLogin.customInit('$widget_id');");
        }
    }
}
