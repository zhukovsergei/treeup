<?php
/**
 * Exception.php
 * @author Revin Roman http://phptime.ru
 */

namespace frontend\components\ulogin;

/**
 * Class Exception
 * @package rmrevin\yii\ulogin
 */
class Exception extends \yii\base\Exception
{
}