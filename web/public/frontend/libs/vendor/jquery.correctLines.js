(function ($) {
    $.fn.correctLines = function (options) {
        var options = jQuery.extend({
            maxHeight: 48, //Видимое кол-во строк
            fullText: false, //Добавить ссылку для отображения скрытого текста
            moreText: 'Подробнее', //Текст ссылки до показа скрытого содержания
            lessText: 'Свернуть', //Текст ссылки после показа скрытого содержания
            classNewLink: 'correctLines_more_link', // Класс для ссылки
            position: 'next'
        }, options);
        return this.each(function (index, self) {
            var height = $(self).height();

            if( height > options.maxHeight ){
                $(self).css('height', options.maxHeight).attr('data-height', height);

                if( options.position === 'next' ){
                    $(self).after($("<a/>", {
                        "text": options.moreText,
                        "class": options.classNewLink
                    }));
                } else{
                    $(self).parent().append($("<a/>", {
                        "text": options.moreText,
                        "class": options.classNewLink
                    }));
                }

                $('body').on('click', '.'+options.classNewLink, function () {
                    var $this = $(this);
                    if( $this.text() === options.lessText ){
                        $this.fadeOut(200);
                        if( options.position === 'next' ){
                            $this.prev().animate({
                                'height': options.maxHeight
                            }, 200);
                        } else{
                            $this.parent().find('.js-correctLine').animate({
                                'height': options.maxHeight
                            }, 200);
                        }
                        setTimeout(function () {
                            $this.text(options.moreText).removeClass('open').fadeIn(200);
                        }, 200);
                    } else{
                        $this.fadeOut(200);
                        if( options.position === 'next' ) {
                            $this.prev().animate({
                                'height': $(self).data('height')
                            }, 200);
                        } else{
                            $this.parent().find('.js-correctLine').animate({
                                'height': $(self).data('height')
                            }, 200);
                        }
                        setTimeout(function () {
                            $this.text(options.lessText).addClass('open').fadeIn(200);
                        }, 200);
                    }
                });
            }
        });
    }
})(jQuery);
