(function() {
    $.fn.reviewsSlider = function (set_options) {
        var options = $.extend({
            visible: 7,
            active: 0
        }, set_options);

        return this.each(function() {
            var $container = $(this);
            var $wrapper = $container.find('.swiper-wrapper');
            var $pagination = $container.find('.swiper-pagination');
            var $items = $container.find('.swiper-slide');
            var paginate_count = 0;

            if($items.length > 1){

                initialization();

                $('body').on('click', '.swiper-pagination span', function(e){
                    e.preventDefault();

                    if($(this).hasClass('active'))
                        return false;

                    var index = $(this).data('index');

                    goSlide(index, $(this));
                });

            } else{
                $items.find('img').show();
            }

            function initialization(){
                var paginate_count_clone = paginate_count;

                $pagination.html('<div class="swiper-pagination-wrapper"></div>');
                $pagination = $pagination.find('.swiper-pagination-wrapper');

                var clone_pag = '';
                $items.each(function (i) {
                    $(this).attr('data-index', i);
                    if( i === options.active )
                        $(this).addClass('active');

                    $pagination.append('<span data-index="' + i + '"><img src="' + $(this).find('img').attr('src') + '"></span>');
                    clone_pag += '<span class="clone" data-index="' + i + '"><img src="' + $(this).find('img').attr('src') + '"></span>';
                });

                $pagination.find('span:nth-child('+ (options.active+1) +')').addClass('active');

                do{
                    $pagination.prepend(clone_pag);
                    $pagination.append(clone_pag);

                    paginate_count_clone = $pagination.find('span').length;
                } while(paginate_count_clone < (options.visible * 2));

                var slide_position = $wrapper.find('.swiper-slide.active').position().left;
                $wrapper.css('transform', 'translate(-'+ slide_position +'px, 0)');

                paginateOpacity(function () {
                    var $pagination_position = $pagination.width()/2 - $pagination.find('.active').position().left - $pagination.find('.active').outerWidth()/2;
                    $pagination.css('transform', 'translate('+ $pagination_position +'px, 0)');
                });
            }

            function paginateOpacity(callback){
                $pagination.find('.active').css({
                    'opacity': '1',
                    'width': '144px',
                    'height': '144px',
                    'pointer-events': 'all'
                });

                var opacity_step = 2,
                    opacity = 5,
                    width = 96,
                    width_step = 20,
                    pagination_active = $pagination.find('.active');

                while (true){
                    if(!pagination_active.next().length)
                        break;

                    pagination_active = pagination_active.next();

                    pagination_active.css({
                        'opacity': '0.' + opacity,
                        'width': width + 'px',
                        'height': width + 'px',
                        'pointer-events': 'all'
                    });

                    if(opacity === 0)
                        pagination_active.css({
                            'pointer-events': 'none'
                        });

                    width -= width_step;
                    opacity -= opacity_step;

                    if(opacity < 0)
                        opacity = 0;
                    if(width < 0)
                        width = 0;
                }
                opacity_step = 2;
                opacity = 5;
                width = 96;
                width_step = 20;
                pagination_active = $pagination.find('.active');
                while (true){
                    if(!pagination_active.prev().length)
                        break;

                    pagination_active = pagination_active.prev();

                    pagination_active.css({
                        'opacity': '0.' + opacity,
                        'width': width + 'px',
                        'height': width + 'px',
                        'pointer-events': 'all'
                    });

                    if(opacity === 0)
                        pagination_active.css({
                            'pointer-events': 'none'
                        });

                    width -= width_step;
                    opacity -= opacity_step;

                    if(opacity < 0)
                        opacity = 0;
                    if(width < 0)
                        width = 0;
                }

                if (callback) {
                    callback();
                }
            }

            function goSlide(index, $paginate){
                $pagination.find('.active').removeClass('active');
                $paginate.addClass('active');

                paginateOpacity(function () {
                    var $pagination_position = $pagination.width()/2 - $paginate.position().left - $paginate.outerWidth()/2;
                    $pagination.css( {
                        transition: "transform 0.2s",
                        transform:  'translate('+ $pagination_position +'px, 0)'
                    } );

                    setTimeout( function() {
                        $pagination.css( { transition: "none" } );

                        if($paginate.hasClass('clone')){
                            $paginate.removeClass('active');
                            $pagination.find('span[data-index="'+ index +'"]:not(.clone)').addClass('active');

                            paginateOpacity(function () {
                                var $pagination_position = $pagination.width()/2 - $pagination.find('.active').position().left - $pagination.find('.active').outerWidth()/2;
                                $pagination.css('transform', 'translate('+ $pagination_position +'px, 0)');
                            });
                        }
                    }, 200 );


                    $wrapper.animate({
                        'opacity': '0'
                    }, 500, function () {
                        $wrapper.find('.swiper-slide.active').removeClass('active');
                        $wrapper.find('.swiper-slide[data-index="'+ index +'"]').addClass('active');

                        var slide_position = $wrapper.find('.swiper-slide.active').position().left;
                        $wrapper.css({
                            'transform': 'translate(-'+ slide_position +'px, 0)'
                        });

                        setTimeout(function () {
                            $wrapper.animate({
                                'opacity': '1'
                            }, 500);
                        }, 100);
                    })
                });
            }
        });
    }
})();