var $main = $('main'),
    $header = $('header'),
    header_theme = $main.data('theme');
$header.attr('data-theme', header_theme);

if($main.is('.t-page_profile') && (!device.mobile() && $(window).width() >= 769)){
    $header.find('.js-open-notice').hide();
    $header.find('.js-open-msg').hide();
}
var _autosize;
$(document).ready(function () {
    var mql = window.matchMedia("(orientation: portrait)");
    // if(mql.matches) {
    //     window.location.reload();
    // } else {
    //     window.location.reload();
    // }
    mql.addListener(function(m) {
        if(!$('body').hasClass('contTrl')) {
            if(m.matches) {
                window.location.reload();
            }
            else {
                window.location.reload();
            }
        }
    });

    if($('div').is('.js-fileBox')){
        var h = $(window).innerHeight() - $('.js-fileBox').offset().top - $('footer').innerHeight() - 151;
        $('.js-fileBox').css('height', h);
    }
    if($('div').is('.t-participant__list')){
        if(!device.mobile()  || $(window).width() >= 769){
            $('.js-participant__item:first').addClass('active');
        }
        participantLoad();
    }

    $('.js-only-number').bind("change keyup input click", function() {
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
    });
    _autosize = autosize(document.querySelectorAll('textarea'));
    $('input[name="fd[phone]"]').inputmask({"mask": "+7 (999) 999-9999"});
    $('.js-accardion').accordion({
        heightStyle: "content"
    });
    $('.js-autocomplete').each(function (i, item) {
        var availableTags = $(item).data('source').split(',');
        $(item).autocomplete({
            source: availableTags,
            appendTo: $(item).parent()
        });
    });

    $('.js-autocomplete_multi').each(function (i, item) {
        var availableTags = $(item).data('source').split(',');
        $(item).on( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                $( this ).autocomplete( "instance" ).menu.active ) {
                event.preventDefault();
            }
        }).autocomplete({
            minLength: 0,
            appendTo: $(item).parent(),
            source: function( request, response ) {
                // delegate back to autocomplete, but extract the last term
                response( $.ui.autocomplete.filter(
                    availableTags, extractLast( request.term ) ) );
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            select: function( event, ui ) {
                var terms = split_m( this.value );
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push( ui.item.value );
                // add placeholder to get the comma-and-space at the end
                terms.push( "" );
                this.value = terms.join( ", " );
                return false;
            }
        });
    });
    $('input, select').focus(function(){
        $(this).parent().removeClass('error').find('.t-error-msg').remove();
    });
    $('.scrollbar-inner').scrollbar({
        ignoreMobile: true
    });
    $("[data-fancybox]").fancybox();
    $(".js-correctLine").correctLines({fullText: true, maxHeight: 20, afterLength: '...', position: 'last' });
    //flip
    $('.t-advantage').find('.flip-container').each(function () {
        if( $(this).find('.back').outerHeight() > $(this).find('.front').outerHeight() ){
            $(this).css('height', $(this).find('.back').outerHeight())
        } else {
            $(this).css('height', $(this).find('.front').outerHeight())
        }
    });
    $('.tabs a').on('click', function (e) {
        e.preventDefault();

        if( $(this).hasClass('active') )
            return false;

        $('.tabs a.active').removeClass('active');
        $(this).addClass('active');
        $(this).parent().find('i').toggleClass('scale');

        if( $(this).closest('.tabs').data('type') === 'flip' ){
            $(this).closest('section').find('.flip-container').toggleClass('open');
        }
    });

    if($('div').is('.t-personal-logo__percent')){
        $(".dial").knob({
            "bgColor": "transparent",
            "fgColor": "#FFD91A",
            "width": "188",
            "height": "188"
        });

        $('.t-ray__line').each(function () {
            $(this).find('path').wavify({
                height: 20,
                bones: 10,
                amplitude: 20,
                color: '#fff',
                speed: .5
            });
        });

        $('.js-ray_hover').on('mouseover', function () {
            $(this).parent().find('.js-ray_hover_show').addClass('show');
        }).on('mouseout', function () {
            $(this).parent().find('.js-ray_hover_show').removeClass('show');
        });
    }

    //public
    $('.js-show_tasks').on('click', function (e) {
        e.preventDefault();

        var $this = $(this),
            task_id = $(this).data('tasks');

        if($this.hasClass('open')){
            $this.removeClass('open').removeClass('t-btn_border').html('Показать задачи');
            $('.js-task[data-tasks="'+ task_id +'"]').removeClass('show');
        } else{
            $this.addClass('open').addClass('t-btn_border').html('Скрыть задачи');
            $('.js-task[data-tasks="'+ task_id +'"]').addClass('show');
        }
    });

    //form
    $('.js_ajaxForm').submit(function (e) {
        e.preventDefault();

        var $form = $(this),
            csrfToken = $('meta[name="csrf-token"]').attr('content'),
            error = validate($form);

        if (error) {
            var type = false;
            var data = {};
            if( $form.find('input[type=file]').length > 0 ){
                type = true;

                data = new FormData();

                if($form.find('.t-load-file').length){
                    var files = [];
                    $form.find('input[type=file]').each(function(i, item) {
                        if($(item)[0].files.length)
                            files.push($(item)[0].files);
                    });
                    data.append('files', files);
                } else{
                    $.each($form.find('input[type=file]')[0].files, function(i, file) {
                        data.append($('input[type=file]').attr('name'), file);
                    });
                }
                data.append('_csrf', csrfToken);

                $form.find('input:not([type=file])').each(function (i, item){
                    if($(item).attr('name') === 'fd[authored_visible]'){
                        if($(item).prop('checked')){
                            data.append($(item).attr('name'), 1);
                        } else{
                            data.append($(item).attr('name'), 0);
                        }
                    } else{
                        if($(item).attr('name') === 'file'){
                            console.log('oldFile')
                        } else{
                            if($(item).attr('name'))
                                data.append($(item).attr('name'), $(item).val());
                        }
                    }
                });
                var $tags = $form.find('.t-tag-list__box');
                if($tags.length > 0){
                    var skills = [];

                    $tags.find('p').each(function (i, item) {
                        var tags = {},
                            id = $(item).data('value') || '';

                        if(id !== '')
                            tags['id'] = id;
                        tags['name'] = $(item).text();

                        skills.push(tags)
                    });

                    data.append('fd[skills]', skills);
                }
                $form.find('select').each(function (i, item){
                    data.append($(item).attr('name'), $(item).val());
                });
                $form.find('textarea').each(function (i, item){
                    data.append($(item).attr('name'), $(item).val());
                });

                if($form.hasClass('t-new-task-form')){
                    //console.log($form);
                }
            } else{
                data['_csrf'] = csrfToken;

                $form.find('input:not([type=file])').each(function (i, item){
                    data[$(item).attr('name')] = $(item).val();
                });
                $form.find('select').each(function (i, item){
                    data[$(item).attr('name')] = $(item).val();
                });
                $form.find('textarea').each(function (i, item){
                    data[$(item).attr('name')] = $(item).val();
                });

                if($form.hasClass('t-new-task-form')){
                    //console.log($form);
                }
            }

            if(type){
                $.ajax({
                    url: $form.attr('action'),
                    data: data,
                    type: $form.attr('method'),
                    contentType: false,
                    processData: false,
                    succes: function (data) {
                        if($form.hasClass('t-new-task-form')){
                            new_task.dialog( "close" );
                        }
                    },
                    complete: function (data) {
                        if($form.hasClass('t-new-task-form')){
                            new_task.dialog( "close" );
                        } else{
                            $.each(data.responseJSON, function(key, value){
                                if(key === 'image'){
                                    $form.find('.t-avatar').find('[type="file"]').attr('data-value', $form.find('.t-avatar').find('img').attr('src'));
                                } else{
                                    var item = $form.find('[name="fd['+key+']"]');
                                    if(item.length)
                                        switch (item[0].tagName){
                                            case 'SELECT':
                                                item.find('option[selected]').removeAttr('selected');
                                                item.find('option[value="'+value+'"]').attr('selected', 'select');
                                                break;
                                            case 'INPUT':
                                                item.attr('value', value);
                                                break;
                                            case 'TEXTAREA':
                                                item.html(value);
                                                break;
                                            default:
                                                console.log('такой тип не обработан');
                                                break;
                                        }
                                }
                            });
                            $form.find('.js-clear-form').click();
                        }

                    },
                    error: function (error) {
                        console.log('Error: ', error)
                    }
                });
            } else{
                $.ajax({
                    url: $form.attr('action'),
                    data: data,
                    type: $form.attr('method'),
                    dataType: 'json',
                    succes: function (data) {
                        $form.find('.js-clear-form').click();
                    },
                    complete: function (data) {
                        $.each(data.responseJSON, function(key, value){
                            $form.find('[name="fd['+key+']"]').attr('value', value);
                        });
                        $form.find('.js-clear-form').click();
                    },
                    error: function (error) {
                        console.log('Error: ', error)
                    }
                });
            }
        }
    });

    //filter
    $( ".js-slider" ).each(function (i, item) {
        $( item ).slider({
            range: true,
            min: $(item).data('min'),
            max: $(item).data('max'),
            values: [ $(item).data('min'), $(item).data('max') ],
            slide: function( event, ui ) {
                var slider = $(item).closest('.t-input__slider');
                slider.find('.js-min').val(ui.values[ 0 ]);
                slider.find('.js-max').val(ui.values[ 1 ]);
            },
            change: function () {
                filter($(this).closest('form'));
            }
        });
    });

    $('.js-open-filter').on('click', function (e) {
        e.preventDefault();

        $(this).parent().addClass('open');
    });
    $('.js-hide-filter').on('click', function(e) {
        e.preventDefault();

        $('.js-open-filter').parent().removeClass('open');
    });
    $('#more_options').change(function () {
        if( $(this).prop('checked') ){
            $('.js-more-options').slideDown(200);
        } else{
            $('.js-more-options').slideUp(200);
        }
    });

    $('body').on('click', '.t-custom-select__result', function (e) {
        e.preventDefault();

        if( $(this).parent().hasClass('open') )
            $(this).parent().removeClass('open');
        else{
            $('.js-custom-select.open').removeClass('open');
            $(this).parent().addClass('open');
            $('.js-custom-select ul').scrollbar({
                ignoreMobile: true
            });
        }
    });

    $('.js-participantsFilter').on('click', function (e) {
        e.preventDefault();

        var type = $(this).attr('href'),
            $list = $('.t-participant');

        $(this).closest('ul').find('.active').removeClass('active');
        $(this).addClass('active');

        if(type === '#all'){
            $list.find('.t-work-area_blur__box_30').show();
        } else{
            $list.find('.t-work-area_blur__box_30').hide();
            $list.find('.t-work-area_blur__box_30[data-performer="1"]').show();
            $list.find('.t-work-area_blur__box_30[data-winner="1"]').show();
        }
    });

    //task

    $('.js-poits').change(function () {
        var $form = $(this).closest('form'),
            id = $(this).val(),
            json = JSON.parse($('.js-points').html());

        var obj;
        $.each(json, function (i, item) {
            if(item.id === id)
                obj = item;
        });

        $form.find('[name="fd[points_participant]"]').prop('disabled', false).html('<option selected value="-1" disabled class="hidden_option">Выберите вознаграждение</option>');
        $.each(obj.participant.split(','), function (i, item) {
            $form.find('[name="fd[points_participant]"]').append('<option value="' + item + '">' + item + '</option>');
        });
        $form.find('[name="fd[points_win]"]').prop('disabled', false).html('<option selected value="-1" disabled class="hidden_option">Выберите вознаграждение</option>');
        $.each(obj.winner.split(','), function (i, item) {
            $form.find('[name="fd[points_win]"]').append('<option value="' + item + '">' + item + '</option>');
        });

        $('.js-styler').trigger('refresh');
    });
    $('.js-addTask').on('click', function (e) {
        e.preventDefault();

        var $this = $(this),
            $form = $this.closest('form'),
            csrfToken = $('meta[name="csrf-token"]').attr('content'),
            error = validate($form);

        if (error) {
            $('body').append('<div class="t-overflow_preload"><img src="/img/bonus1.png"></div>');

            var data = new FormData();

            $form.find('input[type=file].success').each(function(i, item) {
                if($(item)[0].files.length)
                    data.append('files['+ i +']', $(item)[0].files[0]);
            });
            data.append('_csrf', csrfToken);

            data.append('fd[status]', $this.data('type'));

            $form.find('input:not([type=file])').each(function (i, item){
                if($(item).attr('name') === 'fd[authored_visible]'){
                    if($(item).prop('checked')){
                        data.append($(item).attr('name'), 1);
                    } else{
                        data.append($(item).attr('name'), 0);
                    }
                } else{
                    if($(item).attr('name') === 'fd[deleted_files]' && $(item).val() !== ''){
                        var del_file = $(item).val().split(',');
                        data.append($(item).attr('name'), del_file);
                    } else{
                        if($(item).attr('name') && !$(item).closest('.t-input').hasClass('hidden'))
                            data.append($(item).attr('name'), $(item).val());
                    }
                }
            });

            var $tags = $form.find('.t-tag-list__box'),
                skills = [];
            $tags.find('p').each(function (i, item) {
                var tags = {},
                    id = $(item).data('value') || '';

                if(id !== '')
                    tags['id'] = id;
                tags['name'] = $(item).text();

                skills.push(tags)
            });
            skills = JSON.stringify(skills);
            data.append('fd[skills]', skills);

            $form.find('select').each(function (i, item){
                console.log($(this), $(this).val())
                if($(item).val() !== null && !$(item).closest('.t-input').hasClass('hidden')){
                    data.append($(item).attr('name'), $(item).val());
                }
            });
            $form.find('textarea').each(function (i, item){
                data.append($(item).attr('name'), $(item).val());
            });



            $.ajax({
                url: $form.attr('action'),
                data: data,
                type: $form.attr('method'),
                contentType: false,
                processData: false,
                complete: function (data) {
                    setTimeout(function () {
                        $('.t-overflow_preload').remove();
                        if(data.responseJSON.success){
                            location.href = '/profile/customer/tasks';
                        } else{
                            console.log('Error: ', data.responseJSON.msg);
                        }
                    }, 500);
                },
                error: function (error) {
                    console.log('Error: ', error)
                }
            });
        } else{
            var error_item = $form.find('div.error:first');

            var top = error_item.offset().top - 150;

            $('body,html').animate({scrollTop: top}, 500);
        }
    });
    $('.js-privilege').change(function(){
        $('.js-privilege__box').addClass('hidden');

        var this_value = $(this).val();

        $('.js-privilege__box[data-type="'+this_value+'"]').removeClass('hidden');

        CalculatePrice();
    });
    $('.js-taskType').change(function(){
        var this_value = $(this).val();

        var $form = $(this).closest('form');

        if(this_value === 'team'){
            var pr_value = $form.find('select.js-privilege').val();

            if(pr_value === '3'){
                var index_type = $form.find('select.js-privilege').find('option:first').index() + 1;
                $form.find('select.js-privilege').parent().find('li:nth-child(' + index_type + ')').click();
            }

            $form.find('select.js-privilege').find('option[value=3]').prop('disabled', true).addClass('hidden_option');
            $form.find('select[name="fd[points_participant]"]').closest('.t-input').addClass('hidden');

            $form.find('[name="fd[points_win]"]').closest('.t-input_30').find('span').text('Очки для исполнителей');

            $('.js-styler').trigger('refresh');
        } else{
            $form.find('select.js-privilege').find('option[value=3]').prop('disabled', false).removeClass('hidden_option');
            $form.find('select[name="fd[points_participant]"]').closest('.t-input').removeClass('hidden');

            $form.find('[name="fd[points_win]"]').closest('.t-input_30').find('span').text('Очки для победителя');

            $('.js-styler').trigger('refresh');
        }
    });
    $('.js_price').change(function () {
        CalculatePrice();
    });

    $('.js-del_task').on('click', function (e) {
        e.preventDefault();

        var csrfToken = $('meta[name="csrf-token"]').attr('content'),
            $task = $(this).closest('form'),
            id = $task.find('input[name="id"]').val();

        var data = {
            '_csrf': csrfToken,
            'id': id
        };

        $.ajax({
            url: '/tasks/remove',
            data: data,
            type: 'post',
            complete: function (data) {
                //console.log('complete: ', data);
                if(data.responseJSON.success){
                    location.href = '/profile/customer/tasks';
                }
            },
            error: function (error) {
                console.log('Error: ', error)
            }
        });
    });
    var reject_dialog = $('#reject_dialog').dialog({
        autoOpen: false,
        width: 896,
        modal: true,
        show: { effect: "fadeIn", duration: 300 },
        hide: { effect: "fadeOut", duration: 300 },
        open: function () {
            bodyOverflow();
            $('.ui-widget-overlay, .t-dialog-close').on('click', function (e) {
                e.preventDefault();

                reject_dialog.dialog( "close" );
            });
        },
        close: function () {
            bodyOverflow();
        }
    });
    $('body').on('click', '.js-setModeration', function (e) {
        e.preventDefault();

        var csrfToken = $('meta[name="csrf-token"]').attr('content'),
            $task = $(this).closest('.t-task-custumer'),
            id = $task.data('id');

        var data = {
            '_csrf': csrfToken,
            'id': id
        };

        $.ajax({
            url: '/tasks/set-moderation',
            data: data,
            type: 'post',
            complete: function (data) {
                if(data.status === 200){
                    $task.find('.t-task-custumer__info').remove();
                    $task.append(data.responseText);
                    $task.attr('data-type', 'moderation');
                }
            },
            error: function (error) {
                console.log('Error: ', error)
            }
        });
    }).on('click', '.js-getInfoReject', function (e) {
        e.preventDefault();

        reject_dialog.dialog( "open" );
    }).on('click', '.js-closeTask', function (e) {
        e.preventDefault();

        var csrfToken = $('meta[name="csrf-token"]').attr('content'),
            id = $(this).data('task');

        var data = {
            '_csrf': csrfToken,
            'task_id': id
        };

        $.ajax({
            url: '/tasks/view/close-task',
            data: data,
            type: 'post',
            complete: function (data) {
                if(data.status === 200){
                    window.location.reload();
                }
            },
            error: function (error) {
                console.log('Error: ', error)
            }
        });
    }).on('click', '.js-become-performer', function (e) {
        e.preventDefault();

        var csrfToken = $('meta[name="csrf-token"]').attr('content'),
            id = $(this).data('task'),
            participant_id = $(this).data('id');

        var data = {
            '_csrf': csrfToken,
            'task_id': id,
            'participant_id': participant_id
        };

        $.ajax({
            url: '/tasks/view/become-performer',
            data: data,
            type: 'post',
            complete: function (data) {
                if(data.status === 200){
                    window.location.reload();
                }
            },
            error: function (error) {
                console.log('Error: ', error)
            }
        });
    }).on('click', '.js-become-winner', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var csrfToken = $('meta[name="csrf-token"]').attr('content'),
            id = $(this).data('task'),
            participant_id = $(this).data('id');

        var data = {
            '_csrf': csrfToken,
            'task_id': id,
            'participant_id': participant_id
        };

        $.ajax({
            url: '/tasks/view/become-winner',
            data: data,
            type: 'post',
            complete: function (data) {
                if(data.status === 200){
                    window.location.reload();
                }
            },
            error: function (error) {
                console.log('Error: ', error)
            }
        });
    }).on('click', '.js-del-performer', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var csrfToken = $('meta[name="csrf-token"]').attr('content'),
            id = $(this).data('task'),
            participant_id = $(this).data('id');

        var data = {
            '_csrf': csrfToken,
            'task_id': id,
            'participant_id': participant_id
        };

        $.ajax({
            url: '/tasks/view/discard-performer',
            data: data,
            type: 'post',
            complete: function (data) {
                if(data.status === 200){
                    window.location.reload();
                }
            },
            error: function (error) {
                console.log('Error: ', error)
            }
        });
    }).on('click', '.js-del-winner', function (e) {
        e.preventDefault();

        var csrfToken = $('meta[name="csrf-token"]').attr('content'),
            id = $(this).data('task'),
            participant_id = $(this).data('id');

        var data = {
            '_csrf': csrfToken,
            'task_id': id,
            'participant_id': participant_id
        };

        $.ajax({
            url: '/tasks/view/discard-winner',
            data: data,
            type: 'post',
            complete: function (data) {
                if(data.status === 200){
                    window.location.reload();
                }
            },
            error: function (error) {
                console.log('Error: ', error)
            }
        });
    }).on('click', '.js-participant__item', function(e){
        e.preventDefault();

        var $this = $(this);

        $('.js-participant__item.active').removeClass('active');

        $this.addClass('active');

        participantLoad();
    }).on('click', '.js-lock', function (e) {
        e.preventDefault();

        var $this = $(this),
            active = $('.js-participant__item.active'),
            lockBox = $('.t-participant__list[data-type="lock"]');

        var url = '';

        if($this.hasClass('active')){
            $this.removeClass('active').text('Закрепить');
            active.find('.js-lock').removeClass('active').text('Закрепить');

            $('.t-participant__list[data-type="unlock"]').append(active);

            var l = lockBox.find('.js-participant__item').length;

            if(!l)
                lockBox.addClass('hidden');

            url = '/profile/customer/tasks/unpin-participant';
        } else{
            $this.addClass('active').text('Открепить');
            active.find('.js-lock').addClass('active').text('Открепить');

            lockBox.removeClass('hidden').append(active);

            url = '/profile/customer/tasks/pin-participant';
        }

        var uid = active.data('id'),
            task_id = active.data('task');

        var data = {
            '_csrf': $('meta[name="csrf-token"]').attr('content'),
            'task_id': task_id,
            'uid': uid
        };

        $.ajax({
            url: url,
            data: data,
            type: 'post',
            complete: function (data) {
                //console.log('complete: ', data);
            },
            error: function (error) {
                console.log('Error: ', error)
            }
        })
    }).on('click', '.js-participantBack', function (e) {
        e.preventDefault();

        $('.t-participant__collumn_left').removeClass('fade')
        $('.t-participant__collumn_right').removeClass('open');
    });


    $('body').on('change', '.js-load input[type="file"]', function (event) {
        var $this = $(this);

        $this.parent().addClass('load');
        setTimeout(function(){
            var file = event.target.files[0];
            var max = 15;

            var size = parseFloat(parseFloat($('.js-fileWeight').text()).toFixed(1));
            var new_file = file.size/1024/1024;
            new_file = parseFloat(new_file.toFixed(1));
            new_file += 0.1;
            new_file = parseFloat(new_file.toFixed(1));

            size += new_file;

            size = parseFloat(size.toFixed(1));

            if(size > max){
                $this.parent().removeClass('load');

                var raz = size - max;

                raz = parseFloat(raz.toFixed(1));

                $( '<div class="t-dialog_succes"><div class="t-dialog-close"><span></span><span></span></div>' +
                    '<h2>Превышен лимит</h2>' +
                    '<p>Общий объём файлов не должен превышать 15 МБ.<br> \n' +
                    'Текущий файл превышает лимит на '+ raz +' МБ.</p>' +
                    '</div>' ).dialog({
                    modal: true,
                    autoOpen: true,
                    width: 480,
                    open: function(){
                        $( '.t-dialog-close' ).on('click', function(e){
                            e.preventDefault();
                            $( '.t-dialog_succes' ).dialog( 'close' );
                        });
                    }
                });

                return false;
            } else{
                $('.js-fileWeight').html(size);
                $this.addClass('success');
                var file_name = file.name;
                var html = '<div class="t-load-file__item"><img src="/img/file.png">' + file_name + ' — <span class="js-fileWeight_load">' + new_file + '</span> MB' + '<a class="js-del-file"><svg width="10" height="15" viewBox="0 0 10 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path fill="#CCCCCC" d="M 5.0001 0C 5.55283 0 6.0001 0.447693 6.0001 1L 6.0001 2L 9.0001 2C 9.55283 2 10.0001 2.44769 10.0001 3C 10.0001 3.55231 9.55283 4 9.0001 4L 5.0001 4L 1.0001 4C 0.447362 4 9.62968e-05 3.55231 9.62968e-05 3C 9.62968e-05 2.44769 0.447362 2 1.0001 2L 4.0001 2L 4.0001 1C 4.0001 0.447693 4.44736 0 5.0001 0ZM 0.250096 5.3382C 0.440526 5.12317 0.712987 5 1.0001 5L 9.0001 5C 9.28721 5 9.55967 5.12317 9.7501 5.3382C 9.93955 5.55322 10.0284 5.83942 9.99228 6.12402L 8.99228 14.124C 8.92978 14.6245 8.504 15 8.0001 15L 2.0001 15C 1.49619 15 1.07041 14.6245 1.00791 14.124L 0.0079088 6.12402C -0.028224 5.83942 0.0606432 5.55322 0.250096 5.3382ZM 2.13291 7L 2.88291 13L 7.11728 13L 7.86728 7L 2.13291 7Z"></path></svg></a></div>'
                $(event.target).closest('li').addClass('load').prepend(html);
            }

            var $list = $this.closest('.js-file-list');

            if($list.data('max')){
                if($list.data('max') <= $list.find('li').length)
                    return false
            }
            var html2 = '<li>\n' +
                '                  <div class="t-load-file__controls">\n' +
                '                      <label class="js-load t-btn t-btn_border">Прикрепить файл\n' +
                '                          <input type="file" name="files">\n' +
                '                      </label>\n' +
                '                      <span>doc, .xls, .ppt, .odt, .pdf и .odp</span>\n' +
                '                  </div>\n' +
                '              </li>';
            $list.append(html2);
        }, 100); // setTimeout не нужен, но заказчик хочет видеть прелодер...........
    }).on('click', '.js-del-file', function (e) {
        e.preventDefault();

        var $this = $(this),
            $form = $this.closest('form');

        var id = $this.data('id'),
            old_val = $form.find('[name="fd[deleted_files]"]').val();

        if(old_val === ''){
            $form.find('[name="fd[deleted_files]"]').val(id);
        } else{
            $form.find('[name="fd[deleted_files]"]').val(old_val + ',' + id);
        }

        var size = parseFloat(parseFloat($('.js-fileWeight').text()).toFixed(1));
        var del_file = parseFloat(parseFloat($this.closest('li').find('.js-fileWeight_load').text()).toFixed(1));
        size -= del_file;
        size = parseFloat(size.toFixed(1));
        $('.js-fileWeight').html(size);

        var $list = $this.closest('.js-file-list');

        $this.closest('li').remove();

        if(($list.data('max') - 1) === $list.find('li.load').length){
            var html2 = '<li>\n' +
                '                  <div class="t-load-file__controls">\n' +
                '                      <label class="js-load t-btn t-btn_border">Прикрепить файл\n' +
                '                          <input type="file" name="files">\n' +
                '                      </label>\n' +
                '                      <span>doc, .xls, .ppt, .odt, .pdf и .odp</span>\n' +
                '                  </div>\n' +
                '              </li>';
            $list.append(html2);
        }
    });

    $(document).on('click', function(e) {
        if (!$(e.target).closest(".js-custom-select").length) {
            $('.js-custom-select').removeClass('open');
        }
        e.stopPropagation();
    });
    $('body').on('change', '.js-custom-select input:checkbox', function () {
        var select = $(this).closest('.js-custom-select');

        if( $(this).data('type') === 'all' ){
            select.find('input:checkbox:not([data-type="all"])').prop('checked', false);

            select.find('.t-custom-select__result').text('Любые');
        } else{
            select.find('input[data-type="all"]').prop('checked', false);

            var length = select.find('input:checked').length;

            select.find('.t-custom-select__result').text('Выбрано ' + length);
        }

        if(select.hasClass('js-react_select')){
            var react = select.data('react');
            select.closest('form').find('.js-react_bin[data-react="' + react + '"]').text(length);
        }
    });

    $(".various").fancybox({
        maxWidth	: 800,
        maxHeight	: 600,
        fitToView	: false,
        width		: '70%',
        height		: '70%',
        autoSize	: false,
        closeClick	: false,
        openEffect	: 'none',
        closeEffect	: 'none'
    });

    //setting
    $('#js-noticeUpdate').on('change', 'input:checkbox', function () {
        var $form = $(this).closest('form'),
            csrfToken = $('meta[name="csrf-token"]').attr('content');

        var data = {
            '_csrf': csrfToken
        };

        $form.find('input:checkbox').each(function (i, item) {
            if($(item).attr('name') !== 'fd[email_important]'){
                var value;
                if($(item).prop('checked')){
                    value = '1';
                } else{
                    value = '0';
                }
                data[$(item).attr('name')] = value;
            }
        });

        $.ajax({
            url: $form.attr('action'),
            data: data,
            type: 'post',
            success: function (data) {
                //console.log('complete: ', data)
            },
            error: function (error) {
                console.log('Error: ', error)
            }
        });
    });

    //respond
    var addAccount = $('#addAccount').dialog({
        autoOpen: false,
        width: 320,
        modal: true,
        show: { effect: "fadeIn", duration: 300 },
        hide: { effect: "fadeOut", duration: 300 },
        open: function () {
            bodyOverflow();
            $('.ui-widget-overlay, .t-dialog-close').on('click', function (e) {
                e.preventDefault();

                addAccount.dialog( "close" );
            });
        },
        close: function () {
            bodyOverflow();

            $('.t-dialog_error').remove();
        }
    });
    $('body').on('click', '.js-respond', function (e) {
        e.preventDefault();

        var csrfToken = $('meta[name="csrf-token"]').attr('content'),
            $task = $(this).closest('.t-task-custumer'),
            id = $task.data('id');

        var data = {
            '_csrf': csrfToken,
            'task_id': id
        };

        $.ajax({
            url: '/tasks/view/assign-me',
            data: data,
            type: 'post',
            complete: function (data) {
                //console.log(data)
                if(data.status === 200){
                    window.location.reload();
                } else {
                    addAccount.dialog( "open" );
                }
            },
            error: function (error) {
                console.log('Error: ', error);
            }
        });
    });
    var addAccount_customer= $('#addAccount_customer').dialog({
        autoOpen: false,
        width: 320,
        modal: true,
        show: { effect: "fadeIn", duration: 300 },
        hide: { effect: "fadeOut", duration: 300 },
        open: function () {
            bodyOverflow();
            $('.ui-widget-overlay, .t-dialog-close').on('click', function (e) {
                e.preventDefault();

                addAccount_customer.dialog( "close" );
            });
        },
        close: function () {
            bodyOverflow();

            $('.t-dialog_error').remove();
        }
    });
    $('body').on('click', '.js-pay', function (e) {
        e.preventDefault();
        addAccount_customer.dialog( "open" );
    }).on('change', '.js-setFile', function (e) {
        e.preventDefault();

        var $this = $(this),
            $form = $this.closest('form'),
            files = this.files,
            id = $this.closest('.t-task-custumer').data('id') || $form.data('id'),
            csrfToken = $('meta[name="csrf-token"]').attr('content');

        var data = new FormData();

        $.each(files, function(i, item) {
            data.append('files['+ i +']', item);
        });
        data.append('_csrf', csrfToken);
        data.append('task_id', id);

        $form.find('.t-error-msg').remove();

        $.ajax({
            url: $form.attr('action'),
            data: data,
            type: $form.attr('method'),
            contentType: false,
            processData: false,
            beforeSend: function(  ) {
                $this.parent().addClass('load');
            },
            complete: function (data) {
                if(data.status === 200){
                    if(data.responseText === '1'){
                        window.location.reload();
                    }
                    else
                        $form.append('<div class="t-error-msg">Произошла ошибка загрузки. Попробуйте позже или связитесь с заказчиком, для передачи результатов работы лично.</div>');
                }
                $this.parent().removeClass('load');
                console.log('complete: ', data);
            },
            error: function (error) {
                $this.parent().removeClass('load');
                $form.append('<div class="t-error-msg">Произошла ошибка загрузки. Попробуйте позже или связитесь с заказчиком, для передачи результатов работы лично.</div>');
                console.log('Error: ', error)
            }
        });
    }).on('click', '.js-del-file_2', function (e) {
        e.preventDefault();

        var $this = $(this),
            id = $this.data('id'),
            csrfToken = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            url: '/tasks/files/remove',
            data: {file_id: id, _csrf: csrfToken},
            type: 'post',
            complete: function (data) {
                if(data.responseText){
                    $this.closest('li').remove();
                }
                //console.log('complete: ', data);
            },
            error: function (error) {
                console.log('Error: ', error)
            }
        });
    });

    $('.js-moreProject').on('click', function(e){
        e.preventDefault();
        e.stopPropagation();

        if( $(this).hasClass('open') ){
            $(this).removeClass('open').text('Показать').closest('.t-portfolio-box').find('.t-portfolio-box__block').slideUp(200);
        } else{
            $(this).addClass('open').text('Свернуть').closest('.t-portfolio-box').find('.t-portfolio-box__block').slideDown(200);
        }
    });

    //filter work
    $('#search-filter').on('input change', 'input:not([id="more_options"]), select', function (e) {
        console.log('sss')
        filter($(this).closest('form'));
    }).submit(function (e) {
        e.preventDefault();
    });
    $('.js-clearFilter').on('click', function(e){
        e.preventDefault();
        e.stopPropagation();

        var $form = $(this).closest('form');

        $form.addClass('clear');

        $form.find('.js-slider').each(function (i, item) {
            $( item ).slider({
                range: true,
                min: $(item).data('min'),
                max: $(item).data('max'),
                values: [ $(item).data('min'), $(item).data('max') ],
                slide: function( event, ui ) {
                    var slider = $(item).closest('.t-input__slider');
                    slider.find('.js-min').val(ui.values[ 0 ]);
                    slider.find('.js-max').val(ui.values[ 1 ]);
                },
                change: function () {
                    filter($(this).closest('form'));
                }
            });
        });

        $form.find('.js-datepicker').html('Любая дата');
        $form.find('[name="fd[date_end]"]').attr('value', '');
        $form.find('[name="fd[date_end]"]').val('');

        $form.find('.js-custom-select').each(function (i, item) {
            $(item).find('.t-custom-select__result').text('Любые');
        });

        $form.trigger('reset');

        $form.find('.js-more-options').slideUp(200);

        $('.js-styler').trigger('refresh');

        setTimeout(function () {
            $form.removeClass('clear');
            filter($form);
        }, 200);
    });

    $('.js-tabs').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $('.t-task-list').find('.t-defaultMsg_file').remove();

        $(this).closest('ul').find('.active').removeClass('active');
        $(this).addClass('active');

        var  type = $(this).attr('href');

        if(type === 'all'){
            $('.t-task-custumer').removeClass('hidden');
        }else{
            $('.t-task-custumer').addClass('hidden');
            $('.t-task-custumer[data-'+ type +'="1"]').removeClass('hidden');
        }

        if($('.t-task-custumer:not(.hidden)').length === 0){
            $('.t-task-list').append('<div class="t-defaultMsg_file">\n' +
                '            <h2>Сейчас здесь пусто</h2>\n' +
                '            <p>Задач такого типа нет, пожалуйста выберите другой</p>\n' +
                '          </div>')
        }
    });
    $('.js-big-select_tabs').on('change', function () {
        var  type = $(this).val();

        if(type === 'all'){
            $('.t-task-custumer').show();
        }else{
            $('.t-task-custumer').hide();
            $('.t-task-custumer[data-'+ type +'="1"]').show();
        }


    });
    $('.js-tabs_cust').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $('.t-task-list').find('.t-defaultMsg_file').remove();

        $(this).closest('ul').find('.active').removeClass('active');
        $(this).addClass('active');

        var  type = $(this).attr('href');

        if(type === 'all'){
            $('.t-task-custumer').removeClass('hidden');
        }else{
            $('.t-task-custumer').addClass('hidden');
            $('.t-task-custumer[data-type="'+ type +'"]').removeClass('hidden');
        }

        if($('.t-task-custumer:not(.hidden)').length === 0){
            $('.t-task-list').append('<div class="t-defaultMsg_file">\n' +
                '            <h2>Сейчас здесь пусто</h2>\n' +
                '            <p>Задач такого типа нет, пожалуйста выберите другой</p>\n' +
                '          </div>')
        }
    });


    $('.js-newMsg').on('change', '.js-dialogFile__load input[type="file"]', function (event) {
        var $this = $(this);

        var item = $this.closest('.js-dialogFile').addClass('preload');
        $('.js-fileList').append(item);
        item.find('.js-dialogFile__load').addClass('hidden');

        var file = event.target.files[0];
        var max = 15;

        var size = parseFloat(parseFloat($('.js-fileList__weight').text()).toFixed(1));
        var new_file = file.size/1024/1024;
        new_file = parseFloat(new_file.toFixed(1));
        new_file += 0.1;
        new_file = parseFloat(new_file.toFixed(1));

        size += new_file;

        size = parseFloat(size.toFixed(1));

        if(size > max){
            $this.parent().removeClass('load');

            var raz = size - max;

            raz = parseFloat(raz.toFixed(1));

            $( '<div class="t-dialog_succes"><div class="t-dialog-close"><span></span><span></span></div>' +
                '<h2>Превышен лимит</h2>' +
                '<p>Общий объём файлов не должен превышать 15 МБ.<br> \n' +
                'Текущий файл превышает лимит на '+ raz +' МБ.</p>' +
                '</div>' ).dialog({
                modal: true,
                autoOpen: true,
                width: 480,
                open: function(){
                    $( '.t-dialog-close' ).on('click', function(e){
                        e.preventDefault();
                        $( '.t-dialog_succes' ).dialog( 'close' );
                    });
                }
            });
            item.remove();
        } else{
            var index = file.name.lastIndexOf(".");
            var file_name = file.name.substring(0,index);
            var file_type = file.name.substring(index,file.name.length);

            if(file_type === '.exe' || file_type === '.com'){
                $( '<div class="t-dialog_succes"><div class="t-dialog-close"><span></span><span></span></div>' +
                    '<h2>Не удалось загрузить файл</h2>' +
                    '<p>Файл не должен быть исполняемым</p>' +
                    '</div>' ).dialog({
                    modal: true,
                    autoOpen: true,
                    width: 480,
                    open: function(){
                        $( '.t-dialog-close' ).on('click', function(e){
                            e.preventDefault();
                            $( '.t-dialog_succes' ).dialog( 'close' );
                        });
                    }
                });
                item.remove();
            } else{
                $('.js-fileList__weight').html(size);

                var html = '<div>' +
                    '               <img src="/img/file.png">' + file_name + '<span>'+ file_type +' — <span class="js-dialogFile_weight">' + new_file + '</span> MB</span>' +
                    '               <a class="js-dialogFile__del">' +
                    '                   <svg width="10" height="15" viewBox="0 0 10 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path fill="#CCCCCC" d="M 5.0001 0C 5.55283 0 6.0001 0.447693 6.0001 1L 6.0001 2L 9.0001 2C 9.55283 2 10.0001 2.44769 10.0001 3C 10.0001 3.55231 9.55283 4 9.0001 4L 5.0001 4L 1.0001 4C 0.447362 4 9.62968e-05 3.55231 9.62968e-05 3C 9.62968e-05 2.44769 0.447362 2 1.0001 2L 4.0001 2L 4.0001 1C 4.0001 0.447693 4.44736 0 5.0001 0ZM 0.250096 5.3382C 0.440526 5.12317 0.712987 5 1.0001 5L 9.0001 5C 9.28721 5 9.55967 5.12317 9.7501 5.3382C 9.93955 5.55322 10.0284 5.83942 9.99228 6.12402L 8.99228 14.124C 8.92978 14.6245 8.504 15 8.0001 15L 2.0001 15C 1.49619 15 1.07041 14.6245 1.00791 14.124L 0.0079088 6.12402C -0.028224 5.83942 0.0606432 5.55322 0.250096 5.3382ZM 2.13291 7L 2.88291 13L 7.11728 13L 7.86728 7L 2.13291 7Z"></path></svg>' +
                    '               </a>' +
                    '           </div>';

                item.append(html);
                $this.parent().removeClass('preload');
            }
        }

        var html2 = '<div class="t-newMsg-files__item js-dialogFile">' +
            '           <label class="js-dialogFile__load">' +
            '               <svg width="11" height="18" viewBox="0 0 11 18" fill="none" xmlns="http://www.w3.org/2000/svg">' +
            '                   <path d="M1 17C1 17 1 7.98528 1 5.5C1 3.01472 3.01472 1 5.5 1C7.98528 1 10 3.01472 10 5.5C10 6.42507 10 11 10 14C10 17 7.5 17 7.5 17C7.5 17 5 17 5 14C5 11 5 8 5 8" stroke="#E0E0E0" stroke-width="2" stroke-linecap="round"/>' +
            '               </svg>' +
            '               <input type="file" name="files">' +
            '           </label>' +
            '        </div>';

        $('.t-newMsg-files').html(html2);
        $('.t-fileList').removeClass('hidden')
    }).on('submit', function(e){
        e.preventDefault();
        if($(this).find('textarea').val() !== '' || $('.js-fileList').children().length){
            var $form = $(this),
                csrfToken = $('meta[name="csrf-token"]').attr('content');

            var data = new FormData();

            $form.find('input[type=file]').each(function(i, item) {
                if($(item)[0].files.length)
                    data.append('files['+ i +']', $(item)[0].files[0]);
            });
            data.append('_csrf', csrfToken);

            data.append('text', $(this).find('textarea').val());

            $.ajax({
                url: $form.attr('action'),
                data: data,
                type: $form.attr('method'),
                contentType: false,
                processData: false,
                complete: function (data) {
                    console.log(data)
                },
                error: function (error) {
                    console.log('Error: ', error)
                }
            });
        }
        return false;
    }).on('keydown', 'textarea', function (e) {
        if (e.ctrlKey && e.keyCode == 13) {
            $(this).closest('form').submit();
        }
    });
    $('.js-fileList').on('click', '.js-dialogFile__del', function () {
        $(this).closest('.js-dialogFile ').remove();

        var weight = 0;
        $('.js-fileList').find('.js-dialogFile_weight').each(function () {
            weight += parseFloat(parseFloat($(this).text()).toFixed(1));
        });

        if(weight === 0){
            $('.t-fileList').addClass('hidden');
        }

        $('.js-fileList__weight').html(weight);
    });
    $('.js-userItemArrow').on('click', function(e){
        e.preventDefault();
        if($(this).hasClass('_close')){
            $(this).removeClass('_close');
            $('.js-slide').slideDown();
        } else{
            $(this).addClass('_close');
            $('.js-slide').slideUp();
        }
    });

    if( device.mobile() || $(window).width() < 769){
        $('.js-datepicker').each(function (i, item) {
            $(item).hide().parent().find('input').attr('type', 'date');
        });

        new Swiper('.t-new-task .swiper-container', {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            spaceBetween: 40
        });
        new Swiper('.t-advantage-list', {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            spaceBetween: 40
        });
        new Swiper('.t-reviews-slider', {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            spaceBetween: 40
        });

        //menu
        $('.t-link_open-menu').on('click', function (e) {
            e.preventDefault();

            if( $(this).hasClass('open') ){
                $(this).removeClass('open');
                $('.t-menu-box').slideUp(300);
                $('body').removeClass('overflow');
            } else {
                $(this).addClass('open');
                $('.t-menu-box').slideDown(300);
                $('body').addClass('overflow');
            }
        });

        //select
        $('.js-big-select').change(function () {
            window.location.replace($(this).val());
        });

        Header();
    } else {
        //date
        var date = new Date();
        var currentMonth = date.getMonth();
        var currentDate = date.getDate();
        var currentYear = date.getFullYear();
        pickmeup.defaults.locales['ru'] = {
            days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
            daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
        };
        $('.js-datepicker').each(function (i, item) {
            var value = $(item).parent().find('input').val();
            if(value){
                value = value.split('-');
                value = value[2] + '.' + value[1] + '.' + value[0];
                $(item).text(value);
            }

            var max_date = $(item).data('max') == true ? new Date(currentYear, currentMonth, currentDate) : '';

            pickmeup(item, {
                locale: 'ru',
                format: 'd.m.Y',
                position: 'bottom',
                hide_on_select: true,
                date: value,
                max: max_date
            });
            item.addEventListener('pickmeup-change', function (e) {
                var value = e.detail.formatted_date;

                $(this).text(value);

                value = value.split('.');
                value = value[2] + '-' + value[1] + '-' + value[0];

                $(item).parent().find('input').val(value);

                if($(item).closest('#search-filter').length){
                    filter($('#search-filter'));
                }
            })
        });
        if($('.t-templates').length && $('.t-templates').find('.js-datepicker').length){
            pickmeup('.t-templates .js-datepicker').destroy();
        }

        $('.t-reviews-slider').reviewsSlider();

        $('.js-styler').styler({
            onSelectOpened: function () {
                $(this).parent().removeClass('error');
                $('.jq-selectbox__dropdown ul').scrollbar();
            }
        });
        if($('.t-templates').length) {
            $('.t-templates .js-styler').styler('destroy');
        }

        $('.js-selectSearch').styler({
            selectSearch: true
        });
        if($('.t-templates').length){
            $('.t-templates .js-selectSearch').styler('destroy');
        }
    }
});

$(window).scroll(function () {
    if( $(this).scrollTop() > 0 ){
        $('header[data-theme="yeallow-white"]').addClass('white');
    } else{
        $('header[data-theme="yeallow-white"]').removeClass('white');
    }
});

function validate( $form ){
    var error = true;
    $form.find( '.validate' ).each( function(){
        if(!$(this).hasClass('jq-selectbox')) {
            if ($(this).hasClass('checkbox')) {
                if (!$(this).prop('checked')) {
                    $(this).parent().addClass('error');
                    error = false;
                }
            } else {
                var $block = $(this).closest('.t-input');
                $block.removeClass('error');
                if($(this)[0].tagName === 'SELECT'){
                    if ($(this).val() === null && !$(this).hasClass('hidden')) {
                        $block.addClass('error');
                        error = false;
                    }
                } else{
                    if ($(this).val() === '' && !$(this).hasClass('hidden')) {
                        $block.addClass('error');
                        error = false;
                    }
                }

            }
        } else{
            $(this).removeClass('error');

            var $sel = $(this).find('li.selected');

            if( $sel.hasClass('hidden_option') && !$sel.closest('.t-input').hasClass('hidden') ){
                $(this).addClass('error');
                error = false;
            }
        }
    });
    return error;
}

function identical( name ){
    var val_arr = [];
    var error = true;
    $('input[data-name="' + name + '"]').each(function (i, item) {
        if( val_arr.length > 0 ){
            $.each(val_arr, function (i, arr_item) {
                if (arr_item !== $(item).val() || $(item).val() === ''){
                    error = false;
                    $('input[data-name="' + name + '"]').closest('.t-input').addClass('error');
                }
            })
        }
        val_arr.push($(item).val());
    });

    return error;
}

function bodyOverflow(){
    $('body').toggleClass('overflow');
}

function handleFileSelect(evt,list) {
    evt.stopPropagation();
    evt.preventDefault();

    var files = evt.target.files;

    var type = ['image/jpg','image/jpeg','image/png'];
    var size = 5 * 1024 * 1024;

    for (var i = 0, f = files[i]; i < files.length; i++) {
        if (type.indexOf(f.type) == -1 || f.size > size) {
            return false;
        }

        var reader = new FileReader();

        reader.onload = (function() {
            return function(e) {
                $(list).find('img').attr('src', e.target.result);
                var $percent_line = $('.js-percent_line'),
                    $percent = $('.js-percent');

                var $percent_val = parseInt( $percent.text() ) + $percent_line.data('percent');
                if( $percent_val > 100 )
                    $percent_val = 100;
                $percent.text( $percent_val + '%' );

                var width = $percent_line.width() + $percent_line.parent().width() / 100 * $percent_line.data('percent');
                if( $percent_val > 100 )
                    width = '100%';
                $percent_line.css('width', width);
            };
        })(f);

        reader.readAsDataURL(f);
    }
}

function split_m( val ) {
    return val.split( /,\s*/ );
}
function extractLast( term ) {
    return split_m( term ).pop();
}

function filter($form){

    if($form.hasClass('clear'))
        return false;

    var csrfToken = $('meta[name="csrf-token"]').attr('content');

    var data = new FormData();

    data.append('_csrf', csrfToken);

    $form.find('input:not([type="checkbox"]):not([type="radio"])').each(function () {
        if($(this).val() !== '' && $(this).attr('name')){
            data.append($(this).attr('name'), $(this).val());
        }
    });
    $form.find('input:checked').each(function () {
        if($(this).val() !== '' && $(this).attr('name')){
            data.append($(this).attr('name'), $(this).val());
        }
    });
    $form.find('select').each(function () {
        if($(this).val() !== '' && $(this).attr('name')){
            data.append($(this).attr('name'), $(this).val());
        }
    });
    $form.find('.js-custom-select').each(function (i, select) {
        $(select).find('input:checked').each(function () {
            data.append($(select).attr('data-name'), $(this).val());
        });
    });

    $.ajax({
        url: $form.attr('action'),
        data: data,
        type: $form.attr('method'),
        contentType: false,
        processData: false,
        complete: function (data) {
            //console.log('complete: ', data);
            if(data.status === 200){
                if($('div').is('.t-task-custumer')){
                    $('.t-task-custumer').hide();

                    $.each(data.responseJSON, function (i, item) {
                        $('.t-task-custumer[data-id="' + item.id + '"]').show();
                    });
                } else{
                    $('.t-task-item').hide();

                    $.each(data.responseJSON, function (i, item) {
                        $('.t-task-item[data-id="' + item.id + '"]').show();
                    });
                }
            }
        },
        error: function (error) {
            console.log('Error: ', error)
        }
    });
}

function CalculatePrice(){
    var $tu = $('select.js_price'),
        csrfToken = $('meta[name="csrf-token"]').attr('content'),
        tu = $tu.closest('.js-privilege__box').hasClass('hidden')? 0 : $tu.val();


    var data = {
        '_csrf': csrfToken,
        'tu_quantity': tu
    };

    $.ajax({
        url: '/tasks/calculate-price',
        data: data,
        type: 'post',
        complete: function (data) {
            //console.log('complete: ', data);
            if(data.status === 200){
                $('.t-payStatus').html($(data.responseText).html());

                if($('.t-payStatus').find('.error_box').length){
                    $('.js-addTask[data-type="moderation"]').addClass('disabled');
                } else{
                    $('.js-addTask[data-type="moderation"]').removeClass('disabled');
                }
            }
        },
        error: function (error) {
            console.log('Error: ', error)
        }
    })
}

function participantLoad() {
    var $el =  $('.js-participant__item.active');

    if(!$el.length)
        return false;

    var participant_id = $el.data('id'),
        id = $el.data('task');

    var data = {
        '_csrf': $('meta[name="csrf-token"]').attr('content'),
        'id': id,
        'participant_id': participant_id
    };

    $.ajax({
        url: '/profile/customer/tasks/participants-details',
        data: data,
        type: 'post',
        complete: function (data) {
            //console.log('complete: ', data);
            if(data.status === 200){
                $('.t-participant__collumn_right').remove();

                $('.t-participant').append(data.responseText);

                if(device.mobile() || $(window).width() < 769){
                    setTimeout(function () {
                        $('.t-participant__collumn_left').addClass('fade')
                        $('.t-participant__collumn_right').addClass('open');
                    }, 1);
                }
            }
        },
        error: function (error) {
            console.log('Error: ', error)
        }
    })
}

function Header(){
    if (document.addEventListener) {
        if ('onwheel' in document) {
            document.addEventListener("wheel", function(event) {pageScroll(event);});
        } else if ('onmousewheel' in document) {
            document.addEventListener("mousewheel", function(event) {pageScroll(event);});
        } else {
            document.addEventListener("MozMousePixelScroll", function(event) {pageScroll(event);});
        }
    } else {
        document.attachEvent("onmousewheel", function(event) {pageScroll(event);});
    }


    var initialPoint;
    var finalPoint;
    document.addEventListener('touchstart', function(event) {
        initialPoint=event.changedTouches[0];
    }, false);
    document.addEventListener('touchmove', function() {
        finalPoint=event.changedTouches[0];

        try{
            var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;

            if(scrollTop > 150){
                if (finalPoint.pageY < initialPoint.pageY){
                    $('header').addClass('top');
                }
                else {
                    $('header').removeClass('top');
                }
            } else{
                $('header').removeClass('top');
            }
        }
        catch (error){
            $('header').removeClass('top');
        }

    }, true);
}

function pageScroll(event) {
    try {
        var delta = parseInt(event.wheelDelta || -event.detail);

        if (delta >= 0) {
            $('header').removeClass('top');
        }
        else {
            $('header').addClass('top');
        }
    }
    catch (error) {
    }
}