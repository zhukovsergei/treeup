$(document).ready(function () {
    //auth
    var dialog_auth = $('#auth_reg').dialog({
        autoOpen: false,
        width: 320,
        modal: true,
        show: { effect: "fadeIn", duration: 300 },
        hide: { effect: "fadeOut", duration: 300 },
        open: function () {
            bodyOverflow();
            $('body').addClass('contTrl');
            $('.ui-widget-overlay, .t-dialog-close').on('click', function (e) {
                e.preventDefault();

                dialog_auth.dialog( "close" );
            });
        },
        close: function () {
            $('body').removeClass('contTrl');
            bodyOverflow();
            dialog_auth.find('form').trigger( 'reset' );
            dialog_auth.find('.error').removeClass('error');
            $('.t-dialog_error').remove();
        }
    });
    dialog_auth.on('click', '.t-buttons a:not(.t-btn)', function (e) {
        e.preventDefault();

        if($(this).closest('.flip-container').hasClass('open')){
            $(this).closest('.flip-container').removeClass('open');
        } else{
            $(this).closest('.flip-container').addClass('open');
        }
    });
    $( ".t-link_auth, .js-link_auth" ).on( "click", function(e) {
        e.preventDefault();
        dialog_auth.dialog( "open" );
    });
    dialog_auth.on('click', '.t-btn', function (e) {
        e.preventDefault();

        var $form = $(this).closest('form'),
            csrfToken = $( 'meta[name="csrf-token"]' ).attr( 'content' ),
            error = validate( $form );

        if( error ){
            $('.t-dialog_error').remove();

            var data = {
                email: $form.find( 'input[name=login]' ).val(),
                password: $form.find( 'input[name=password]' ).val()
            };
            $.ajax({
                url: $form.attr('action'),
                data: { fd: data, _csrf: csrfToken },
                type: $form.attr('method'),
                success: function(data) {
                    if( data.email ){
                        dialog_auth.append('<div class="t-dialog_error">' + data.email[0] + '</div>');
                    } else{
                        if( data.password ){
                            dialog_auth.append('<div class="t-dialog_error">' + data.password[0] + '</div>');
                        } else{
                            if($form.hasClass('back')){
                                dialog_auth.dialog( "close" );
                                $( '<div class="t-dialog_succes"><div class="t-dialog-close"><span></span><span></span></div><h2>Верификация</h2><p>Чтобы продолжить подтвердите адрес электронной почты — на указанный адрес была отправлена ссылка для подтверждения.</p></div>' ).dialog({
                                    modal: true,
                                    autoOpen: true,
                                    width: 320,
                                    open: function(){
                                        $('.ui-widget-overlay, .t-dialog-close').on('click', function (e) {
                                            e.preventDefault();
                                            $( '.t-dialog_succes' ).dialog( "close" );
                                        });
                                    }
                                });
                            } else{
                                dialog_auth.dialog( "close" );
                                window.location.href = data;
                            }
                        }
                    }
                },
                error: function () {
                    dialog_auth.append('<div class="t-dialog_error">Неверный логин или пароль</div>');
                }
            });
        }
        return false;
    });
    // new password
    var dialog_new_password = $('#new_password').dialog({
        autoOpen: false,
        width: 320,
        modal: true,
        show: { effect: "fadeIn", duration: 300 },
        hide: { effect: "fadeOut", duration: 300 },
        open: function () {
            $('.ui-widget-overlay, .t-dialog-close').on('click', function (e) {
                e.preventDefault();
                dialog_new_password.dialog( "close" );
            });
        },
        close: function () {
            dialog_new_password.find('form').trigger( 'reset' );
            dialog_new_password.find('.error').removeClass('error');
            $('.t-dialog_error').remove();
        }
    });
    $('body').on('click', '.t-link_new-password', function (e) {
        e.preventDefault();
        dialog_auth.dialog( "close" );

        dialog_new_password.dialog( "open" );
    });
    dialog_new_password.on('click', '.t-btn', function (e) {
        e.preventDefault();

        var $form = $(this).closest('form'),
            csrfToken = $( 'meta[name="csrf-token"]' ).attr( 'content' ),
            error = validate( $form );

        if( error ){
            $('.t-dialog_error').remove();
            var data = {
                email: $form.find( 'input[name=login]' ).val()
            };
            $.ajax({
                url: $form.attr('action'),
                data: { fd: data, _csrf: csrfToken },
                type: $form.attr('method'),
                success: function() {
                    dialog_new_password.dialog( "close" );
                    $( '<div class="t-dialog_succes"><div class="t-dialog-close"><span></span><span></span></div><h2>Готово</h2><p>Мы выслали ссылку для сброса на указанный адрес. Если письмо не пришло — проверьте папку со спамом, возможно оно оказалось там.</p><a href="#close" class="t-btn t-btn_border t-dialogClose" disabled>Запросить повторно <span class="js-timer">через <span>60</span> сек.</span> </a></div>' ).dialog({
                        modal: true,
                        autoOpen: true,
                        width: 320,
                        open: function(){
                            $('.ui-widget-overlay, .t-dialog-close').on('click', function (e) {
                                e.preventDefault();
                                $( '.t-dialog_succes' ).dialog( "close" );
                            });

                            $( '.t-dialogClose' ).on('click', function(e){
                                e.preventDefault();
                                $( '.t-dialog_succes' ).dialog( 'close' );
                                dialog_new_password.dialog( "open" );
                            });

                            var timer = 60;
                            var timer_interval = setInterval(function () {
                                timer--;
                                if(timer <= 0){
                                    $('.js-timer').remove().parent().removeAttr('disabled');
                                    clearInterval(timer_interval);
                                } else{
                                    $('.js-timer').find('span').html(timer);
                                }
                            }, 1000);
                        }
                    });
                },
                error: function () {
                    dialog_new_password.append('<div class="t-dialog_error">Ошибка! Попробуйте чуть позже</div>');
                }
            });
        }
        return false;
    });
    $('.js-create_password').on('keyup input', 'input', function () {
        var value = $(this).val();
        var name = $(this).data('name');
        var $form = $(this).closest('form');

        if ( value !== '' ){
            var error = identical( name );

            if(error) {
                $form.find('.t-btn').prop('disabled', false);
            } else{
                $form.find('.t-btn').prop('disabled', true);
            }
        } else{
            $form.find('.t-btn').prop('disabled', true);
        }
    });
    $('.js-newPassword').on('click', function (e) {
        e.preventDefault();

        var csrfToken = $('meta[name="csrf-token"]').attr('content'),
            $form = $(this).closest('form'),
            error = validate($form);

        if (error) {
            var data = {
                '_csrf': csrfToken
            };

            $form.find('input').each(function (i, item) {
                data[$(item).attr('name')] = $(item).val();
            });

            $.ajax({
                url: window.location.pathname + '/make-password',
                data: data,
                type: $form.attr('method'),
                complete: function (data) {
                    if(data.responseJSON === true){
                        window.location.reload();
                    } else{
                        var msg = '';
                        $.each(data.responseJSON, function (i, response) {
                            msg = msg + response + ' ';
                        });
                        $form.find('.t-btnBox').before('<div class="t-errorMsg">' + msg + '</div>')
                    }
                },
                error: function (error) {
                    console.log('Error: ', error)
                }
            });
        }
    });
    $('.js-password_create').on('click', function (e) {
        e.preventDefault();

        var csrfToken = $('meta[name="csrf-token"]').attr('content'),
            $form = $(this).closest('form'),
            error = identical('password_doubel');

        if (error) {
            var data = {
                '_csrf': csrfToken
            };

            $form.find('input').each(function (i, item) {
                data[$(item).attr('name')] = $(item).val();
            });

            $.ajax({
                url: $form.attr('action'),
                data: data,
                type: $form.attr('method'),
                complete: function (data) {
                    if(data.responseJSON.success){
                        window.location.reload();
                    } else{
                        for(var key in data.responseJSON.msg){
                            $form.find('input[name="fd['+ key +']"]').closest('.t-input').addClass('error').append('<div class="t-error-msg">'+ data.responseJSON.msg[key] +'</div>');
                        }
                    }
                },
                error: function (error) {
                    console.log('Error: ', error)
                }
            });
        }
    })
});