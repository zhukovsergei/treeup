$(document).ready(function () {

    if($.cookie('forms') !== "null" && $.cookie('forms') && $('main').is('.t-page_profile')){
        var cook = $.evalJSON($.cookie('forms'));

        if($('div').is('#project_list')){
            var box = $('#project_list');

            setTimeout(function(){
                $.each(cook, function (i, items) {
                    var form = box.find('form')[i];

                    if(form === undefined){
                        box.find('.js-addProject').trigger('click');

                        form = box.find('form')[i];
                        items.edit = false;
                    }

                    $(form).find('[name="fd[name]"]').val(items["fd[name]"]);
                    $(form).find('h2').text(items["fd[name]"]);
                    $(form).find('[name="fd[text]"]').val(items["fd[text]"]);
                    $(form).find('.t-project__text').text(items["fd[name]"]);

                    $.each(items.custom_select, function(){
                        $(form).find('input[id="'+this+'"]').next().click();
                    });

                });
            }, 100);
        } else{
            var box = $('.js-addcontainer[data-type="' + cook.type + '"]');
            setTimeout(function(){
                box.prev().click();
                $.each(cook.form, function (i, items) {
                    var form = box.find('form')[i];

                    if(form === undefined){
                        box.find('.js-addBox').trigger('click');

                        form = box.find('form')[i];
                        items.edit = false;

                        $(form).find('.js-disabled').each(function (i, item) {
                            $(item).toggleClass('disabled');
                        });

                        $(form).find(':disabled').each(function (i, item) {
                            $(item).prop('disabled', false);
                            if ( $(item).val() === '-' )
                                $(item).val('');
                        });
                    }

                    for (var item in items) {
                        var value = items[item];

                        var input = $(form).find('[name="' + item + '"]');

                        $.each(input, function (i, item_input) {
                            var $item = $(item_input);

                            if($item[0].type === 'radio' || $item[0].type === 'checkbox'){
                                if( $item.val() === value ){
                                    $item.prop('checked', true);
                                } else{
                                    $item.prop('checked', false);
                                }
                            } else if( $item[0].type === 'select-one' ){
                                $item.find('option[value="' + value + '"]').prop('selected', true);
                            } else{
                                $item.val(value);
                                if( $item[0].type === 'textarea' ){
                                    $item.parent().find('li:not(:last)').remove();

                                    value = value.split(',');

                                    for (var li in value){
                                        var html = '<li><p>' + value[li] + '<span class="js-del_tag"></span></p></li>';
                                        $item.parent().find('li:last').before(html);
                                    }
                                }
                            }
                        });
                    }
                    if( !device.mobile() || $(window).width() >= 769 ){
                        $('.js-styler').trigger('refresh');
                        $('.js-selectSearch').trigger('refresh');
                    }

                    if(items.edit){
                        $(form).find('.js-edit').trigger('click');
                    }
                });
            }, 100);
        }
    }

    var $percent_line = $('.js-percent_line'),
        percent_line_width = $percent_line.data('width');
    $percent_line.css('width', percent_line_width + '%');

    //Установить аватар
    if($('input[type="file"]').is("#setAvatar")){
        document.getElementById('setAvatar').addEventListener('change', function(evt) {handleFileSelect(evt,'.t-avatar');}, false);
    }
    //удалить аватар
    $('.js-delAvatar').on('click', function (e) {
        e.preventDefault();

        var $this = $(this);

        $this.closest('.t-avatar').find('img').attr('src', '/img/IconEmptyAvatar.png');

        $this.closest('.t-avatar').find('input[name=file]').val('');

        $this.closest('form').find('[name="fd[remove_image]"]').val('1');
    });


    $('.js-profileForm').on('focus', 'input:not([type="radio"])', function () {
        if($(this).attr('name') === 'fd[lastname]')
            return false;

        var $perspective = $('.js-perspective');

        var value = $(this).val();
        if(value === '') {
            var width = $('.js-percent_line').width() + $perspective.parent().width() / 100 * $percent_line.data('percent');
            if (width > $perspective.parent().width())
                width = '100%';
        }
        $perspective.css('width', width);
    }).on('focusout', 'input:not([type="radio"])', function () {
        var $perspective = $('.js-perspective'),
            $percent = $('.js-percent');

        var value = $(this).val();
        if(value !== ''){
            $percent_line.css('width', $perspective.width());

            var $percent_val = parseInt( $percent.text() ) + $percent_line.data('percent');
            if( $percent_val > 100 )
                $percent_val = 100;

            $percent.text( $percent_val  + '%');
        }
        $perspective.css('width', $percent_line.width());
    }).on('submit', function (e) {

        var error = validate($(this));

        if(!error){
            e.preventDefault();
            return false;
        }

    });

    $('.js-addBox').on('click', function (e) {
        e.preventDefault();

        var $this = $(this),
            type = $this.data('type');

        var html = $('.t-templates form[data-type="'+type+'"]').clone();

        var count = $this.closest('.js-addcontainer').find('.t-work-area_blur__box').length + 1;

        html.find(':radio').each(function (i, item) {
            var id = $(item).attr('id') + count;
            $(item).attr('id', id).next().attr('for', id);
        });

        html.find(':checkbox').each(function (i, item) {
            var id = $(item).attr('id') + count;
            $(item).attr('id', id).next().attr('for', id);
        });

        $this.parent().before(html);

        if( !device.mobile() || $(window).width() >= 769 ){
            $('.js-styler').styler({
                onSelectOpened: function () {
                    $(this).parent().removeClass('error');
                    $('.jq-selectbox__dropdown ul').scrollbar();
                }
            });
            if($('.t-templates').length) {
                $('.t-templates .js-styler').styler('destroy');
            }

            $('.js-selectSearch').styler({
                selectSearch: true
            });
            if($('.t-templates').length) {
                $('.t-templates .js-selectSearch').styler('destroy');
            }

            var date = new Date();
            var currentMonth = date.getMonth();
            var currentDate = date.getDate();
            var currentYear = date.getFullYear();
            $('.js-datepicker').each(function (i, item) {
                var value = $(item).parent().find('input').val();
                if(value){
                    value = value.split('-');
                    value = value[2] + '.' + value[1] + '.' + value[0];
                    $(item).text(value);
                }

                var max_date = $(item).data('max') ? new Date(currentYear, currentMonth, currentDate) : '';

                pickmeup(item, {
                    locale: 'ru',
                    format: 'd.m.Y',
                    position: 'bottom',
                    hide_on_select: true,
                    date: value,
                    max: max_date
                });
                item.addEventListener('pickmeup-change', function (e) {
                    var value = e.detail.formatted_date;

                    $(this).text(value);

                    value = value.split('.');
                    value = value[2] + '-' + value[1] + '-' + value[0];

                    $(item).parent().find('input').val(value).trigger('input');
                })
            });
            if($('.t-templates').length && $('.t-templates').find('.js-datepicker').length){
                pickmeup('.t-templates .js-datepicker').destroy();
            }
        }

        $('.js-autocomplete').each(function (i, item) {
            var availableTags = $(item).data('source').split(',');
            $(item).autocomplete({
                source: availableTags,
                appendTo: $(item).parent()
            });
        });
        $('.js-autocomplete_multi').each(function (i, item) {
            var availableTags = $(item).data('source').split(',');
            $(item).on( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                    $( this ).autocomplete( "instance" ).menu.active ) {
                    event.preventDefault();
                }
            }).autocomplete({
                minLength: 0,
                appendTo: $(item).parent(),
                source: function( request, response ) {
                    // delegate back to autocomplete, but extract the last term
                    response( $.ui.autocomplete.filter(
                        availableTags, extractLast( request.term ) ) );
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split_m( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( ", " );
                    return false;
                }
            });
        });
        if( $this.data('blocked') === 'blocked' ){
            $('.js-blocked').addClass('blocked');
            html.removeClass('blocked')
        }
    });
    $('.js-addProject').on('click', function(e){
        e.preventDefault();

        var $this = $(this),
            type = $this.data('type');

        var html = $('.t-templates form[data-type="'+type+'"]').clone();

        var count = $this.closest('.t-work-area').find('.t-project').length + 1;

        html.find(':checkbox').each(function (i, item) {
            var id = $(item).attr('id') + count;
            $(item).attr('id', id).next().attr('for', id);
        });

        $(this).parent().before(html);

        if( $this.data('blocked') === 'blocked' ){
            $('.js-blocked').addClass('blocked');
            html.removeClass('blocked')
        }
        autosize(document.querySelectorAll('textarea'));
    });
    $('.js-addProject_portfolio').on('click', function (e) {
        e.preventDefault();
    });
    $('body').on('click', '.js-del', function (e) {
        e.preventDefault();

        var $this = $(this),
            $form = $this.closest('form'),
            csrfToken = $('meta[name="csrf-token"]').attr('content');

        var type = $this.data('type');

        var url = '';
        switch (type){
            case 'education':
                url = '/profile/business/cv/education/remove';
                break;
            case 'experience':
                url = '/profile/business/cv/experience/remove';
                break;
            case 'specialization':
                url = '/profile/business/cv/specialization/remove';
                break;
            case 'project':
                url = '/profile/customer/projects/remove';
                break;
            case 'portfolio':
                url = '/profile/business/portfolio/remove';
                break;
            default:
                break;
        }

        var data = {};

        data['_csrf'] = csrfToken;
        data['id'] = $form.find('input[name=id]').val();

        $.ajax({
            url: url,
            data: data,
            type: "POST",
            dataType: 'json',
            succes: function (data) {
                $form.remove();
            },
            complete: function (data) {
                $form.remove();
            },
            error: function (error) {
                console.log('Error: ', error)
            }
        });

        $('.js-blocked').removeClass('blocked');
    }).on('click', '.js-clear-form', function (e) {
        e.preventDefault();

        var $form = $(this).closest('form');

        if($(this).data('del') === 'on'){
            $form.remove();
        } else{
            if( !device.mobile() || $(window).width() >= 769 ) {
                $form.find('select').each(function (i, item) {
                    var value = $(item).attr('data-value');

                    var index = $(item).find('option[value="' + value + '"]').index() + 1;

                    $(item).parent().find('.jq-selectbox__dropdown').find('li:nth-child(' + index + ')').click();
                });
            }
            $form.find('[name="fd[remove_image]"]').val('0');
            $form.find('input[type="file"]').each(function (i, item) {
                var value = $(item).attr('data-value'),
                    id = $(item).attr('id');

                $form.find('img[data-input="' + id + '"]').attr('src', value);
            });

            $form.trigger( 'reset' );

            $form.find('.js-hidden_box').each(function (i, item) {
                $(item).toggleClass('hidden');
            });

            $form.find('.js-disabled').each(function (i, item) {
                $(item).toggleClass('disabled');
            });

            $form.find('input:not([type="file"])').each(function (i, item) {
                $(item).prop('disabled', true);
            });
            $form.find('select').each(function (i, item) {
                $(item).prop('disabled', true);
            });
            $form.find('textarea').each(function (i, item) {
                $(item).prop('disabled', true);
            });
            $form.find('.t-errorMsg').remove();
            if( !device.mobile() || $(window).width() >= 769 ){
                $('.js-styler').trigger('refresh');
                $('.js-selectSearch').trigger('refresh');
            }
        }
        if( $(this).data('blocked') === 'blocked' ){
            $('.js-blocked').removeClass('blocked');
        }

        $.cookie('forms', null);
    }).on('click', '.js-edit', function (e) {
        e.preventDefault();

        var $this = $(this),
            $form = $this.closest('form');

        $form.find('.js-hidden_box').each(function (i, item) {
            $(item).toggleClass('hidden');
        });

        $form.find('.js-disabled').each(function (i, item) {
            $(item).toggleClass('disabled');
        });

        $form.find(':disabled').each(function (i, item) {
            $(item).prop('disabled', false);
            if ( $(item).val() === '-' )
                $(item).val('');
        });
        if( !device.mobile()  || $(window).width() >= 769){
            $('.js-styler').trigger('refresh');
            $('.js-selectSearch').trigger('refresh');
        }

        if( $this.data('blocked') === 'blocked' ){
            $('.js-blocked').addClass('blocked');

            $this.closest('form').removeClass('blocked');
            autosize.update(_autosize);
        }
    }).on('keyup', '.js-tags-set', function (e) {
        var yes = false;
        //if(device.mobile() || device.tablet()){
            var len = $(this).val().length,
                val = $(this).val();
            if(val[len-1] === ','){
                yes = true;
            }
        // } else{
        //     console.log(e.keyCode)
        //     if (e.keyCode === 188){
        //         yes = true;
        //     }
        // }
        if(yes){
            var $this = $(this),
                tag = '<li><p>' + $this.val().replace(',','') + '<span class="js-del_tag"></span></p></li>';

            $this.val('');
            $this.parent().before(tag);
            e.preventDefault();
        }
    }).on('keydown', '.js-tags-set', function(e){
        if(e.keyCode === 8){
            if($(this).val() === ''){
                $(this).parent().prev().remove();
            }
        }
    }).on('click', '.js-del_tag', function (e) {
        e.preventDefault();

        $(this).closest('li').remove();
    }).on('input', 'input.js-showButton_input', function () {
        var $form = $(this).closest('form');

        var show = true;

        $form.find('input.js-showButton_input').each(function (i, item) {
            console.log($(item).val(), item)
            if( $(item).val() === '' && !$(item).parent().hasClass('hidden') ){
                console.log($(item).val() )
                show = false;
                return false;
            }
        });
        $form.find('select.js-showButton_select').each(function (i, item) {
            if( $(item).val() < 0 || $(item).val() === '' ){
                show = false;
                return false;
            }
        });

        if(show){
            $form.find('button').prop('disabled', false);
        } else{
            $form.find('button').prop('disabled', true);
        }
    });
    $('.js-addcontainer').on('change', 'select.js-showButton_select', function () {
        var $form = $(this).closest('form');

        var show = true;

        $form.find('select.js-showButton_select').each(function (i, item) {
            //console.log($(item).val())
            if( $(item).val() < 0 || $(item).val() === '' || $(item).val() === null ){
                show = false;
                return false;
            }
        });

        if(show){
            $form.find('button').prop('disabled', false);
        } else{
            $form.find('button').prop('disabled', true);
        }
    }).on('focus', '.js-autocomplete', function () {
        $(this).parent().css('z-index','7');
    }).on('focusout', '.js-autocomplete', function () {
        $(this).parent().css('z-index','5');
    }).on('input', '.js-autocomplete', function () {
        var $form = $(this).closest('form');

        if( $(this).val() === "" ){
            $form.find('select').each(function (i, item) {
                $(item).prop('disabled', true);
            });
            $form.find('textarea').each(function (i, item) {
                $(item).prop('disabled', true);
            });
            $form.find('.t-btn').each(function (i, item) {
                $(item).prop('disabled', true);
            });

            if($(this).attr('name') === 'fd[industry]'){
                $form.find('.js-disabled').each(function (i, item) {
                    $(item).addClass('disabled');
                });
                $form.find('input[name="fd[specialization]"]').prop('disabled', true);
            }

            if( !device.mobile()  || $(window).width() >= 769){
                $('.js-styler').trigger('refresh');
                $('.js-selectSearch').trigger('refresh');
            }

        } else{
            $form.find(':disabled:not(.t-btn)').each(function (i, item) {
                $(item).prop('disabled', false);
            });
            $form.find('.js-disabled').each(function (i, item) {
                $(item).removeClass('disabled');
            });

            if( !device.mobile()  || $(window).width() >= 769){
                $('.js-styler').trigger('refresh');
                $('.js-selectSearch').trigger('refresh');
            }

            var show = true;

            $form.find('input.js-showButton_input').each(function (i, item) {
                if( $(item).val() === '' ){
                    show = false;
                    return false;
                }
            });
            $form.find('select.js-showButton_select').each(function (i, item) {
                if( $(item).val() < 0 || $(item).val() === '' ){
                    show = false;
                    return false;
                }
            });

            if(show){
                $form.find('button').prop('disabled', false);
            } else{
                $form.find('button').prop('disabled', true);
            }
        }
    }).on('change', '.radio', function () {
        var text = $(this).next().text() + ' отрасль';

        $(this).parent().find('.js-radioText').html(text);
    }).on('change', '.js-until-now', function(){
        if( $(this).prop('checked') ){
            $(this).parent().find('[name="fd[date_to]"]').parent().addClass('hidden');
            $(this).val('1');
        } else{
            $(this).parent().find('[name="fd[date_to]"]').parent().removeClass('hidden');
            $(this).val('0');
        }
        var $form = $(this).closest('form');

        var show = true;

        $form.find('input.js-showButton_input').each(function (i, item) {
            if( $(item).val() === '' && !$(item).parent().hasClass('hidden') ){
                show = false;
                return false;
            }
        });
        $form.find('select.js-showButton_select').each(function (i, item) {
            if( $(item).val() < 0 || $(item).val() === '' ){
                show = false;
                return false;
            }
        });

        if(show){
            $form.find('button').prop('disabled', false);
        } else{
            $form.find('button').prop('disabled', true);
        }
    }).on('click', '.js-add_info', function (e) {
        e.preventDefault();

        var $form = $(this).closest('form'),
            $cancel = $(this).parent().find('.js-clear-form'),
            csrfToken = $('meta[name="csrf-token"]').attr('content'),
            error = validate($form);

        if (error) {
            var data = {};

            data['_csrf'] = csrfToken;
            $form.find('input:not([type=file])').each(function (i, item) {
                if($(item).attr('name')){
                    data[$(item).attr('name')] = $(item).val();
                }
            });
            $form.find('select').each(function (i, item) {
                data[$(item).attr('name')] = $(item).val();
            });
            $form.find('textarea').each(function (i, item) {
                data[$(item).attr('name')] = $(item).val();
            });

            var $tags = $form.find('.t-tag-list__box');
            if($tags.length > 0){
                var skills = [];

                $tags.find('p').each(function (i, item) {
                    var tags = {},
                        id = $(item).data('value') || '';

                    if(id !== '')
                        tags['id'] = id;
                    tags['name'] = $(item).text();

                    skills.push(tags)
                });

                data['fd[skills]'] = skills;
            }

            $.ajax({
                url: $form.attr('action'),
                data: data,
                type: $form.attr('method'),
                dataType: 'json',
                complete: function (data) {
                    if(data.status === 200){
                        var id = data.responseJSON.id;
                        $form.find('input[name=id]').val(id);

                        var $tags = $form.find('.t-tag-list__box');
                        if($tags.length > 0) {
                            var arr_skills = data.responseJSON.arr_skills;

                            $form.find('.t-tag-list__box').find('li:not(:last-child)').remove();
                            $.each(arr_skills, function (i,item) {
                                var html = '<li><p data-value="' + item.id + '">' + item.name + '<span class="js-del_tag"></span></p></li>';

                                $form.find('.t-tag-list__box').find('li:last').before(html);
                            })
                        }

                        var action = $form.attr('action').split('/');
                        action[action.length-1] = 'update';
                        action = action.join('/');
                        $form.attr('action', action);

                        $form.find('.js-hidden_box').each(function (i, item) {
                            $(item).toggleClass('hidden');
                        });
                        $form.find('.js-hidden_box_2').each(function (i, item) {
                            $(item).removeClass('hidden');
                        });
                        $form.find('.js-disabled').each(function (i, item) {
                            $(item).toggleClass('disabled');
                        });

                        $form.find('input:not([type="file"])').each(function (i, item) {
                            $(item).prop('disabled', true);
                        });
                        $form.find('select').each(function (i, item) {
                            $(item).prop('disabled', true);
                        });
                        $form.find('textarea').each(function (i, item) {
                            $(item).prop('disabled', true);
                        });
                        if( !device.mobile()  || $(window).width() >= 769){
                            $('.js-styler').trigger('refresh');
                            $('.js-selectSearch').trigger('refresh');
                        }

                        $.cookie('forms', null);

                        $cancel.removeAttr('data-del');
                    }
                },
                error: function (error) {
                    console.log('Error: ', error)
                }
            });
        }
        if( $(this).data('blocked') === 'blocked' ){
            $('.js-blocked').removeClass('blocked');
        }
    }).on('blur', 'input, select', function () {
        var box = $(this).closest('.js-addcontainer');

        var forms = {
            'type': box.data('type'),
            'form': []
        };
        box.find('form').each(function (i, form) {
            var $form = $(form);

            var form_val = {};

            if($form.hasClass('blocked'))
                form_val['edit'] = false;
            else
                form_val['edit'] = true;

            $form.find("input,select,textarea").not('[type="submit"]').each(function() {
                if($(this).attr('name') !== undefined){
                    if($(this).attr('type') === 'radio' || $(this).attr('type') === 'checkbox'){
                        if( $(this).prop('checked') )
                            form_val[$(this).attr('name')] = $(this).val();
                    } else{
                        form_val[$(this).attr('name')] = $(this).val();
                    }
                }
            });

            forms.form.push(form_val);
        });

        forms = $.toJSON(forms);
        $.cookie('forms', forms, { expires: 0.5});
    });

    $('#project_list').on('input', '.js-react', function () {
        var this_value = $(this).val(),
            react = $(this).data('react');

        $(this).closest('form').find('.js-react_bin[data-react="' + react + '"]').text(this_value);
    }).on('click', '.js-add_info', function (e) {
        e.preventDefault();

        var $form = $(this).closest('form'),
            $cancel = $(this).parent().find('.js-clear-form'),
            csrfToken = $('meta[name="csrf-token"]').attr('content'),
            error = validate($form);

        if (error) {
            $('body').append('<div class="t-overflow_preload"><img src="/img/bonus1.png"></div>');
            var data = {};

            data['_csrf'] = csrfToken;
            $form.find('input:not([type=file])').each(function (i, item) {
                if($(item).attr('name')){
                    data[$(item).attr('name')] = $(item).val();
                }
            });
            $form.find('textarea').each(function (i, item) {
                data[$(item).attr('name')] = $(item).val();
            });

            var $select = $form.find('.t-custom-select'),
                tasks = [];
            $select.find('input:checkbox:checked').each(function (i, item) {
                tasks.push($(item).val());
            });

            data[$select.data('name')] = tasks;

            $.ajax({
                url: $form.attr('action'),
                data: data,
                type: $form.attr('method'),
                dataType: 'json',
                complete: function (data) {
                    if(data.status === 200){
                        $('.t-overflow_preload').remove();

                        var id = data.responseJSON.id;
                        $form.find('input[name=id]').val(id);
                        $form.find('input[name="fd[name]"]').attr('value',data.responseJSON.name);
                        $form.find('[name="fd[text]"]').html(data.responseJSON.text);

                        var action = $form.attr('action').split('/');
                        action[action.length-1] = 'update';
                        action = action.join('/');
                        $form.attr('action', action);

                        $form.find('.js-hidden_box').each(function (i, item) {
                            $(item).toggleClass('hidden');
                        });
                        $form.find('.js-hidden_box_2').each(function (i, item) {
                            $(item).removeClass('hidden');
                        });
                        $form.find('.js-newTask').attr('data-project', id);

                        $.cookie('forms', null);

                        $cancel.removeAttr('data-del');

                        $('.t-new-task-form').find('[name="fd[project_id]"]').append(
                            '<option value="' + data.responseJSON.id + '">' + data.responseJSON.name + '</option>');
                        $('.t-new-task-form').find('[name="fd[project_id]"]').trigger('refresh');
                    }
                },
                error: function (error) {
                    console.log('Error: ', error)
                }
            });
        }
        if( $(this).data('blocked') === 'blocked' ){
            $('.js-blocked').removeClass('blocked');
        }
    }).on('blur', 'input, textarea', function () {
        var box = $(this).closest('#project_list');

        var forms = [];
        box.find('form').each(function (i, form) {
            var $form = $(form);

            var form_val = {};

            if($form.hasClass('blocked'))
                form_val['edit'] = false;
            else
                form_val['edit'] = true;

            $form.find("input,textarea").not('[type="submit"]').each(function() {
                if($(this).attr('name') !== undefined){
                    if($(this).attr('type') === 'radio' || $(this).attr('type') === 'checkbox'){
                        if( $(this).prop('checked') )
                            form_val[$(this).attr('name')] = $(this).val();
                    } else{
                        form_val[$(this).attr('name')] = $(this).val();
                    }
                }
            });
            var select = [];
            $form.find('.js-custom-select').find(':checkbox:checked').each(function(){
                select.push($(this).attr('id'));
            });
            form_val['custom_select'] = select;

            forms.push(form_val);
        });

        forms = $.toJSON(forms);
        $.cookie('forms', forms, { expires: 0.5});
    });
    $('.t-portfolio').on('click', '.js-add_info', function (e) {
        e.preventDefault();

        var $form = $(this).closest('form'),
            $cancel = $(this).parent().find('.js-clear-form'),
            csrfToken = $('meta[name="csrf-token"]').attr('content'),
            error = validate($form);

        if (error) {
            $('body').append('<div class="t-overflow_preload"><img src="/img/bonus1.png"></div>');
            var data = new FormData();

            data.append('_csrf', csrfToken);
            data.append('fd[name]', $form.find('[name="fd[name]"]').val());
            data.append('fd[text]', $form.find('[name="fd[text]"]').val());

            if($form.find('[name="id"]').val()){
                data.append('id', $form.find('[name="id"]').val());
            }
            if($form.find('[name="fd[deleted_blocks]"]').val()){
                data.append('fd[deleted_blocks]', $form.find('[name="fd[deleted_blocks]"]').val().split(','));
            }
            if($form.find('[name="fd[deleted_files]"]').val()){
                data.append('fd[deleted_files]', $form.find('[name="fd[deleted_files]"]').val().split(','));
            }

            $form.find('.t-portfolio-box__block').each(function (i, item) {
                var block_item = {
                    "name": $(item).find('[name="fd[block_name]"]').val(),
                    "text": $(item).find('[name="fd[block_text]"]').val(),
                    "type": $(item).attr('data-type')
                };

                if($(item).find('[name="fd[block_id]"]').val() !== '')
                    block_item['id'] = $(item).find('[name="fd[block_id]"]').val();

                switch ($(item).data('type')){
                    case 'image':
                        block_item['quantity'] = $(item).find('input[type="file"]').length-1;
                        break;
                    case 'video':
                        block_item['quantity'] = $(item).find('.js-video-input').length;
                        break;
                    case 'audio':
                        block_item['quantity'] = $(item).find('input[type="file"]').length-1;
                        break;
                    default:
                        break;
                }

                block_item = JSON.stringify(block_item);
                data.append('fd[blocks][]', block_item);


            });

            $form.find('[type=file]').each(function (i, item) {
                if($(item)[0].files.length) {
                    data.append('files[]', $(item)[0].files[0]);
                }
            });

            $form.find('.js-video-input').each(function(i, item) {
                if($(item).val() !== '') {
                    data.append('fd[video_urls][]', $(item).val());
                }
            });

            $.ajax({
                url: $form.attr('action'),
                data: data,
                type: $form.attr('method'),
                dataType: 'json',
                contentType: false,
                processData: false,
                complete: function (data) {
                    //console.log(data);
                    if(data.status === 200){
                        $('.t-overflow_preload').remove();

                        var id = data.responseJSON.id;
                        $form.find('input[name=id]').val(id);
                        $form.find('input[name="fd[name]"]').attr('value',data.responseJSON.name);
                        $form.find('[name="fd[text]"]').html(data.responseJSON.text);

                        $form.find('.t-portfolio-box__block').each(function (i ,item) {
                            $(item).find('[name="fd[block_id]"]').val(data.responseJSON.d_blocks[i].id);
                            $(item).find('[name="fd[block_name]"]').attr('value',data.responseJSON.d_blocks[i].name);
                            $(item).find('[name="fd[block_text]"]').attr('value',data.responseJSON.d_blocks[i].text);

                            $(item).find('input:not([type="file"])').prop('disabled', true);
                        });

                        var action = $form.attr('action').split('/');
                        action[action.length-1] = 'update';
                        action = action.join('/');
                        $form.attr('action', action);



                        $form.find('.js-hidden_box').each(function (i, item) {
                            $(item).toggleClass('hidden');
                        });

                        $form.find('[name="fd[name]"]').prop('disabled', true);
                        $form.find('[name="fd[text]"]').prop('disabled', true);

                        $.cookie('forms', null);

                        $cancel.removeAttr('data-del');
                    }
                },
                error: function (error) {
                    console.log('Error: ', error)
                }
            });
        }
        if( $(this).data('blocked') === 'blocked' ){
            $('.js-blocked').removeClass('blocked');
        }
    }).on('change', '.js-load_img input[type="file"]', function (event) {
        var $this = $(this);

        $this.parent().addClass('load');

        var files = event.target.files;
        for (var i = 0, f = files[i]; i < files.length; i++) {
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            reader.onload = (function(theFile) {
                return function(e) {
                    var html = '<div class="t-input_file__preview"><a data-fancybox="img" href="'+ e.target.result +'">\n' +
                        '                                    <img src="'+ e.target.result +'">\n' +
                        '                                </a></div>';

                    $(event.target).closest('.t-input_file').append(html);
                    $this.parent().removeClass('load');
                };
            })(f);
            reader.readAsDataURL(f);
        }

        $(event.target).closest('label').removeClass('t-fileLoad_add').removeClass('js-load_img').addClass('t-fileLoad_del').addClass('js-delFile');

        var html2 = '<div class="t-input_file">\n' +
            '                            <label class="t-fileLoad t-fileLoad_add js-load_img">\n' +
            '                                <input type="file" name="img" class="t-addFile">\n' +
            '                            </label>\n' +
            '                        </div>';

        $this.closest('.js-file-list_img').append(html2);
    }).on('click', '.js-add_video', function(e){
        e.preventDefault();

        var html = '<div class="t-input_video">\n' +
            '                            <div class="t-input_video__static-box hidden js-hidden_box">\n' +
            '                                <a class="various fancybox.iframe" href="">\n' +
            '                                    <svg width="24" height="24" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\n' +
            '                                        <path fill="#BDBDBD" fill-rule="evenodd" d="M 6.00095 1.60982C 11.7389 -1.70394 19.0771 0.26236 22.3902 6.00092C 25.7043 11.7391 23.737 19.0766 17.9985 22.3905C 12.2604 25.7039 4.92246 23.7373 1.60984 17.9988C -1.70398 12.2604 0.262403 4.92252 6.00095 1.60982ZM 16.3191 11.5123L 10.0367 7.84817C 9.46416 7.51392 9.00238 7.78114 9.00552 8.44338L 9.03775 15.7163C 9.04059 16.3784 9.50804 16.6479 10.0822 16.3163L 16.3167 12.7168C 16.8903 12.3857 16.8915 11.8466 16.3191 11.5123Z"/>\n' +
            '                                    </svg>\n' +
            '                                </a>\n' +
            '                            </div>\n' +
            '                            <div class="t-input_video__edit-box  js-hidden_box">\n' +
            '                                <svg width="24" height="24" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\n' +
            '                                    <path fill="#BDBDBD" fill-rule="evenodd" d="M 6.00095 1.60982C 11.7389 -1.70394 19.0771 0.26236 22.3902 6.00092C 25.7043 11.7391 23.737 19.0766 17.9985 22.3905C 12.2604 25.7039 4.92246 23.7373 1.60984 17.9988C -1.70398 12.2604 0.262403 4.92252 6.00095 1.60982ZM 16.3191 11.5123L 10.0367 7.84817C 9.46416 7.51392 9.00238 7.78114 9.00552 8.44338L 9.03775 15.7163C 9.04059 16.3784 9.50804 16.6479 10.0822 16.3163L 16.3167 12.7168C 16.8903 12.3857 16.8915 11.8466 16.3191 11.5123Z"/>\n' +
            '                                </svg>\n' +
            '                                <input value="" placeholder="Вставьте ссылку на YouTube-ролик" class="js-video-input">\n' +
            '                                <a href="#del" class="t-del js-del-video" >\n' +
            '                                    <svg width="10" height="15" viewBox="0 0 10 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\n' +
            '                                        <path fill="#CCCCCC" d="M 5.0001 0C 5.55283 0 6.0001 0.447693 6.0001 1L 6.0001 2L 9.0001 2C 9.55283 2 10.0001 2.44769 10.0001 3C 10.0001 3.55231 9.55283 4 9.0001 4L 5.0001 4L 1.0001 4C 0.447362 4 9.62968e-05 3.55231 9.62968e-05 3C 9.62968e-05 2.44769 0.447362 2 1.0001 2L 4.0001 2L 4.0001 1C 4.0001 0.447693 4.44736 0 5.0001 0ZM 0.250096 5.3382C 0.440526 5.12317 0.712987 5 1.0001 5L 9.0001 5C 9.28721 5 9.55967 5.12317 9.7501 5.3382C 9.93955 5.55322 10.0284 5.83942 9.99228 6.12402L 8.99228 14.124C 8.92978 14.6245 8.504 15 8.0001 15L 2.0001 15C 1.49619 15 1.07041 14.6245 1.00791 14.124L 0.0079088 6.12402C -0.028224 5.83942 0.0606432 5.55322 0.250096 5.3382ZM 2.13291 7L 2.88291 13L 7.11728 13L 7.86728 7L 2.13291 7Z"></path>\n' +
            '                                    </svg>\n' +
            '                                </a>\n' +
            '                            </div>\n' +
            '                        </div>';

        $(this).parent().before(html);
    }).on('input', '.js-video-input', function(){
        var value = $(this).val();

        $(this).closest('.t-input_video').find('a.various').attr('href', value);

        $(".various").fancybox({
            maxWidth	: 800,
            maxHeight	: 600,
            fitToView	: false,
            width		: '70%',
            height		: '70%',
            autoSize	: false,
            closeClick	: false,
            openEffect	: 'none',
            closeEffect	: 'none'
        });
    }).on('change', '.js-load_audio input[type="file"]', function (event) {
        $(this).parent().addClass('load');

        setTimeout(function(){
            var files = event.target.files;
            for (var i = 0, f = files[i]; i < files.length; i++) {
                if (!f.type.match('audio.*')) {
                    continue;
                }

                var reader = new FileReader();

                reader.onload = (function(theFile) {
                    return function(e) {
                        var html = '<div class="t-load-file__item">\n' +
                            '                                        <div class="t-input_video__static-box hidden js-hidden_box">\n' +
                            '                                            <a href="'+ e.target.result +'" target="_blank" class="audio">\n' +
                            '                                                <svg width="22" height="24" viewBox="0 0 22 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\n' +
                            '                                                    <path fill="#BDBDBD" fill-rule="evenodd" d="M 14.2191 0C 14.5703 0 14.8732 0.125029 15.1294 0.375086C 15.3842 0.625057 15.5122 0.921301 15.5122 1.26338L 15.5122 22.7371C 15.5122 23.079 15.3842 23.3752 15.128 23.6253C 14.8732 23.8748 14.5703 24 14.2191 24C 13.8693 24 13.5663 23.8748 13.3102 23.6253L 6.58416 17.0528L 1.29315 17.0528C 0.943349 17.0528 0.640376 16.9276 0.384225 16.6775C 0.128075 16.4276 0 16.1315 0 15.7895L 0 8.21062C 0 7.86854 0.128075 7.57264 0.384225 7.32232C 0.638998 7.07244 0.943349 6.94741 1.29315 6.94741L 6.58416 6.94741L 13.3102 0.375086C 13.5663 0.125029 13.8693 0 14.2191 0ZM 19.8117 7.25606C 19.2182 6.75052 18.3285 6.82186 17.8231 7.41547C 17.3191 8.00908 17.3907 8.90013 17.9828 9.40568C 18.7169 10.0311 19.1796 10.9601 19.1796 12C 19.1796 13.0399 18.7169 13.9688 17.9828 14.5943C 17.3907 15.0999 17.3191 15.9909 17.8231 16.5844C 18.3285 17.1781 19.2182 17.2495 19.8117 16.7439C 21.1489 15.604 22 13.9006 22 12C 22 10.0994 21.1489 8.39597 19.8117 7.25606Z"/>\n' +
                            '                                                </svg>\n' +
                            '                                            </a>\n' +
                            '                                            <div class="audio-name">'+ theFile.name +'</div>\n' +
                            '                                        </div>\n' +
                            '                                        <div class="t-input_video__edit-box js-hidden_box">\n' +
                            '                                            <img src="/img/audio.png">\n' +
                            '                                            <div class="audio-name">'+ theFile.name +'</div>\n' +
                            '                                            <a class="js-del-audio">\n' +
                            '                                                <svg width="10" height="15" viewBox="0 0 10 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path fill="#CCCCCC" d="M 5.0001 0C 5.55283 0 6.0001 0.447693 6.0001 1L 6.0001 2L 9.0001 2C 9.55283 2 10.0001 2.44769 10.0001 3C 10.0001 3.55231 9.55283 4 9.0001 4L 5.0001 4L 1.0001 4C 0.447362 4 9.62968e-05 3.55231 9.62968e-05 3C 9.62968e-05 2.44769 0.447362 2 1.0001 2L 4.0001 2L 4.0001 1C 4.0001 0.447693 4.44736 0 5.0001 0ZM 0.250096 5.3382C 0.440526 5.12317 0.712987 5 1.0001 5L 9.0001 5C 9.28721 5 9.55967 5.12317 9.7501 5.3382C 9.93955 5.55322 10.0284 5.83942 9.99228 6.12402L 8.99228 14.124C 8.92978 14.6245 8.504 15 8.0001 15L 2.0001 15C 1.49619 15 1.07041 14.6245 1.00791 14.124L 0.0079088 6.12402C -0.028224 5.83942 0.0606432 5.55322 0.250096 5.3382ZM 2.13291 7L 2.88291 13L 7.11728 13L 7.86728 7L 2.13291 7Z"></path>\n' +
                            '                                                </svg>\n' +
                            '                                            </a>\n' +
                            '                                        </div>\n' +
                            '                                    </div>';

                        $(event.target).closest('li').addClass('load').append(html);
                    };
                })(f);
                reader.readAsDataURL(f);
            }

            var html2 = '<li class="js-hidden_box">\n' +
                '                                    <div class="t-load-file__controls">\n' +
                '                                        <label class="js-load_audio t-btn_img">Добавить аудио-файл\n' +
                '                                            <input type="file" name="audio">\n' +
                '                                        </label>\n' +
                '                                    </div>\n' +
                '                                </li>';

            $(event.target).closest('.js-file-list').append(html2);
        }, 200); // setTimeout не нужен, но заказчик хочет видеть прелодер...........
    }).on('click', '.js-addPortfolio', function (e) {
        e.preventDefault();

        var $this = $(this),
            type = $this.data('type');

        var html = $('.t-templates').find('.t-portfolio-box__block[data-type="'+type+'"]').clone();

        $this.closest('.t-addPortfolioBlock').before(html);
    }).on('click', '.js-delBlock', function(e){
        e.preventDefault();

        var $block = $(this).closest('.t-portfolio-box__block');

        if($block.find('input[name="fd[block_id]"]').val() !== ''){
            var old_val = $(this).closest('.t-portfolio-box').find('[name="fd[deleted_blocks]"]').val(),
                id = $block.find('input[name="fd[block_id]"]').val();
            if(old_val === ''){
                $(this).closest('.t-portfolio-box').find('[name="fd[deleted_blocks]"]').val(id);
            } else{
                $(this).closest('.t-portfolio-box').find('[name="fd[deleted_blocks]"]').val(old_val + ',' + id);
            }
        }

        $block.remove();
    }).on('click', '.js-delFile', function(e){
        e.preventDefault();

        var id = $(this).find('input').val(),
            $block = $(this).closest('.t-portfolio-box'),
            old_val = $block.find('[name="fd[deleted_files]"]').val();

        if($(this).find('input').attr('type') !== 'file'){
            if(old_val === ''){
                $block.find('[name="fd[deleted_files]"]').val(id + ':image');
            } else{
                $block.find('[name="fd[deleted_files]"]').val(old_val + ',' + id + ':image');
            }
        }

        $(this).closest('.t-input_file').remove();
    }).on('click', '.js-del-video', function (e) {
        e.preventDefault();

        if($(this).data('id') !== ''){

        }
        $(this).closest('.t-input_video').remove();
    }).on('click', '.js-del-audio', function (e) {
        e.preventDefault();

        var id = $(this).data('id'),
            $block = $(this).closest('.t-portfolio-box'),
            old_val = $block.find('[name="fd[deleted_files]"]').val();

        if(id !== ''){
            if(old_val === ''){
                $block.find('[name="fd[deleted_files]"]').val(id + ':audio');
            } else{
                $block.find('[name="fd[deleted_files]"]').val(old_val + ',' + id + ':audio');
            }
        }

        $(this).closest('li.load').remove();
    });


    $( "#js-sliderDuration" ).slider({
        range: "min",
        min: 1,
        max: 12,
        value: 1,
        slide: function( event, ui ) {
            $( this ).closest('.t-input').find('input').val( ui.value );

            var end_date = getDurationEnd(ui.value);

            $( this ).closest('.t-input').find('.js-durationEnd').html(end_date);
        }
    });
    if($('p').is('.js-durationEnd')){
        var end_date = getDurationEnd(1);

        $('.js-durationEnd').html(end_date);
    }
});


function getDurationEnd(month){
    var date = new Date();
    date.setMonth(date.getMonth() + month);

    var day = date.getDate() > 9 ? date.getDate() : '0' + date.getDate();
    var month_end = date.getMonth() > 9 ? date.getMonth() : '0' + date.getMonth();

    var text = wordend(month, ['месяц','месяца','месяцев']);

    var html = '';

    return html = month + ' ' + text + '<span>— до '+ day +'.'+ month_end +'.'+ date.getFullYear() +'</span>';
}

function wordend(num, words){
    return words[ ((num=Math.abs(num%100)) > 10 && num < 15 || (num%=10) > 4 || num === 0) ? 2 : num === 1 ? 0 : 1 ];
}