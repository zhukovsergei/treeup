$(document).ready(function(){
  <!-- INITIALIZE IOS SWITCHERY -->
  // =================================================================
  $('.nicePrettyCheckbox').each(function(ind, val){
    new Switchery(val);
  });
  // =================================================================
  <!-- end initialize ios switchery -->

  <!-- SEO CHECKBOX READONLY -->
  // =================================================================
  $('#seoCheckbox').on('change', function(){
    if(!$('#seoCheckbox').prop('checked'))
    {
      $('input[name*="title"]').attr('readonly', 'readonly');
      $('input[name*="keywords"]').attr('readonly', 'readonly');
      $('textarea[name*="description"]').attr('readonly', 'readonly');
    }
    else
    {
      $('input[name*="title"]').removeAttr('readonly');
      $('input[name*="keywords"]').removeAttr('readonly');
      $('textarea[name*="description"]').removeAttr('readonly');
    }
  });
  // =================================================================
  <!-- end seo checkbox readonly -->


  //$('#actionDate').datepicker({autoclose:true});


  <!-- CHANGE PRICE TABLE -->
  // =================================================================
  $('body').on('change', '.tablePrice', function(){
    var $this = $(this);
    var newPrice = $(this).val();
    var productID = $(this).data('id');
    var productName = $this.parents('td').siblings('.tableRowName').text();

    $.ajax({
        type: 'POST',
        url : '/admin/products/changePrice',
        data: {
            'fd[productID]':  productID,
            'fd[newPrice]':  newPrice
        },
        dataType: 'json',
        success: function(res){
          $.niftyNoty({
            type: 'success',
            title: 'Цена',
            icon: 'fa fa-info fa-lg',
            message: 'Цена для твоара "'+ productName +'" успешно изменена',
            container: 'floating',
            timer: 5500
          });
        }
    });
  });
  // =================================================================
  <!-- end change price table -->


  <!-- DEL ROW CONFIRM TABLE -->
  // =================================================================
  $('body').on('click', '.del-confirm', function(){
    var tableRowName = $(this).parents('td').siblings('.mark-name').text();

    if(tableRowName)
      return confirm('Удалить "'+ tableRowName +'"?');
    else
      return confirm('Удалить эту запись?');

  });


  <!-- COPYING TABLE ROWS -->
  // =================================================================
  $('#doCopyTblRows').on('click', function(){
    $(this).addClass('disabled');

    var catID = $('#catIDforCopy').val();
    var IDS = [];

    $("table input:checkbox:checked").each(function(ind, val)
    {
      IDS.push($(this).val());
    });

    $.ajax({
      type: 'POST',
      url : '/admin/products/copyingTableRows',
      data: {
        'catID':  catID,
        'IDS':  IDS
      },
      dataType: 'json',
      success: function(res){
        location.reload();
      }
    });
  });
  // =================================================================
  <!-- end copying table rows  -->

  <!-- DO ACTION TABLE -->
  <!--===================================================-->
  $('#doActionTable').on('click', function(){

    if(!confirm('Подтверждение выполнения действия.') ) return false;

    $(this).addClass('disabled');

    var IDS = [];
    var action_type = $('select[name*="action_type"]').val();

    $("table input:checkbox:checked").each(function(ind, val)
    {
      IDS.push($(this).val());
    });

    $.ajax({
      type: 'POST',
      url : '/admin/products/doActionTable',
      data: {
        'IDS':  IDS,
        'action_type': action_type
      },
      dataType: 'json',
      success: function(res){
        location.reload();
      }
    });
  });
  <!--===================================================-->
  <!-- end do action table  -->

});
