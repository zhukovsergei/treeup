$(document).ready(function(){


  <!-- DEL TREE NODE CATEGORIES INDEX -->
  // =================================================================
  $('#fullTreeCats').on('click', '.delTreeNode', function(e){
    e.preventDefault();
    var result = confirm('Удалить эту запись?');
    var that = $(this);
    if(result)
    {
      var id = that.data('id');
      $.ajax({
        type: 'POST',
        url : '/categories/del-tree-node',
        data: {
          'id':  id
        },
        dataType: 'json',
        success: function(res){
          if(res.success)
          {
            $.niftyNoty({
              type: 'success',
              title: 'Системное сообщение',
              icon: 'fa fa-info fa-lg',
              message: 'Категория удалена',
              container: 'floating',
              timer: 5500
            });
            that.parent('li').slideUp(150);
          }
        }
      });
    }
  });
  <!--===================================================-->
  <!-- end del tree node categories index  -->



});
