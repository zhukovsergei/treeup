<?php
namespace aiur\helpers;

use common\models\TaskFile;

class TaskFileUploaderHelper
{

  public static function handle($files, $task_id, $uid) {

    foreach ($files as $file) {
      $fm = new TaskFile();
      $fm->task_id = $task_id;
      $fm->name = $file;
      $fm->size = $file->size;
      $fm->uid = $uid;
      $fm->save();
    }
  }

}