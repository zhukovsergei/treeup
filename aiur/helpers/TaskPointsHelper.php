<?php
namespace aiur\helpers;


use common\models\Task;
use common\models\TaskFile;
use common\models\TaskParticipant;
use common\models\TaskPoint;
use common\models\User;

class TaskPointsHelper
{

  public static function getMinPointsParticipants() :int
  {
    $row = Task::find()->min('points_participant');

    return (int) $row;
  }

  public static function getMaxPointsParticipants() :int
  {
    $row = Task::find()->max('points_participant');

    return (int) $row;
  }

  public static function getMinPointsWinners() :int
  {
    $row = Task::find()->min('points_win');

    return (int) $row;
  }

  public static function getMaxPointsWinners() :int
  {
    $row = Task::find()->max('points_win');

    return (int) $row;
  }

}