<?php
namespace aiur\helpers;

use common\models\PortfolioBlock;
use common\models\PortfolioBlockFileAudio;
use common\models\PortfolioBlockFileImage;
use yii\web\UploadedFile;

class PortfolioFileSaver
{

  public static function onCreate($block_ids, $quantities, $files) {

    $ball = 0;

    foreach ($block_ids as $block_id ) {

      $block = PortfolioBlock::find()->where(['id' => $block_id])->one();

      $quantity = array_shift($quantities);
      foreach(array_slice($files, $ball, $quantity) as $file)
      {
        if( $block->type === 'audio' ){
          $fa = new PortfolioBlockFileAudio();
          $fa->block_id = $block_id;
          $fa->filename = $file;
          $fa->save();
        }elseif( $block->type === 'image' ){
          $fa = new PortfolioBlockFileImage();
          $fa->block_id = $block_id;
          $fa->filename = $file;
          $fa->save();
        }
      }

      $ball += $quantity;
    }
  }

  public static function onUpdate($block_ids, $quantities, $files) {

    $ball = 0;

    foreach ($block_ids as $block_id ) {

      $block = PortfolioBlock::find()->where(['id' => $block_id])->one();

      $quantity = array_shift($quantities);

      foreach(array_slice($files, $ball, $quantity) as $file)
      {
        if( $block->type === 'audio' ){
          $fa = new PortfolioBlockFileAudio();
          $fa->block_id = $block_id;
          $fa->filename = $file;
          $fa->save();
        }elseif( $block->type === 'image' ){
          $fa = new PortfolioBlockFileImage();
          $fa->block_id = $block_id;
          $fa->filename = $file;
          $fa->save();
        }

      }
      $ball += $quantity;
    }
  }


}