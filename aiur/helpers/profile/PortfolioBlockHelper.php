<?php
namespace aiur\helpers\profile;

use common\models\PortfolioBlock;
use common\models\PortfolioBlockFileAudio;
use common\models\PortfolioBlockFileImage;

class PortfolioBlockHelper
{

  public static function delete( $array ) {
    return PortfolioBlock::deleteAll(['id' => explode(',', $array) ]);
  }

  public static function deleteFiles( $array ) {
    foreach($array as $type => $ids)
    {
      if($type === 'image')
      {
        PortfolioBlockFileImage::deleteAll(['id' => $ids]);
      }
      elseif($type === 'audio')
      {
        PortfolioBlockFileAudio::deleteAll(['id' => $ids]);
      }

    }
  }

}