<?php
namespace aiur\helpers;


use aiur\helpers\user\BalanceHelper;


class TaskPricePostHelper
{

  public static function isEnough($user_id, $tu_quantity = 0) :bool
  {
    $userBalance = BalanceHelper::getMoney($user_id);

    $total = self::calculate($tu_quantity);

    return $total <= $userBalance;
  }

  public static function calculate($tu_quantity = 0) :int
  {
    $tu_price = \Yii::$app->settings->get('price.tu');
    $task_price = \Yii::$app->settings->get('price.task');

    return ($tu_quantity * $tu_price) + $task_price;
  }
}