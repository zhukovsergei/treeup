<?php
namespace aiur\helpers;

use common\models\PortfolioBlock;
use common\models\PortfolioBlockFileAudio;
use common\models\PortfolioBlockFileImage;
use common\models\PortfolioBlockFileVideo;
use yii\web\UploadedFile;

class PortfolioUrlSaver
{

  public static function onCreate($block_ids, $quantities, $urls) {

    $ball = 0;

    foreach ($block_ids as $block_id ) {

      $quantity = array_shift($quantities);
      foreach(array_slice($urls, $ball, $quantity) as $url)
      {
        $fa = new PortfolioBlockFileVideo();
        $fa->block_id = $block_id;
        $fa->url = $url;
        $fa->save();
      }

      $ball += $quantity;
    }
  }

  public static function onUpdate($block_ids, $quantities, $urls) {

    $ball = 0;

    foreach ($block_ids as $block_id ) {

      $quantity = array_shift($quantities);
      foreach(array_slice($urls, $ball, $quantity) as $url)
      {
        $fa = new PortfolioBlockFileVideo();
        $fa->block_id = $block_id;
        $fa->url = $url;
        $fa->save();
      }

      $ball += $quantity;
    }
  }


}