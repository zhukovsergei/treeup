<?php
namespace aiur\helpers\user;

use common\models\User;

class ProfileUrlHelper
{
  public static function make( \common\models\User $user )
  {
    switch($user->profile_type)
    {
      case User::PROFILE['customer']:
        return '/profile/customer/feed';

      case User::PROFILE['business']:
        return '/profile/business/feed';

      default:
        return '/registration/choice/role';
    }
  }
}