<?php
namespace aiur\helpers\user;

use common\models\BalanceAccount;
use common\models\BalanceAccountCake;
use common\models\BalanceAccountPoint;

class BalanceHelper
{
  public static function getMoney($user_id) :int
  {
    $row = BalanceAccount::find()->where(['userId' => $user_id])->one();

    return $row->balance ?? 0;
  }

  public static function getMoneyFormatted($user_id)
  {
    return number_format(self::getMoney($user_id), null, null, ' ');
  }

  public static function getCakes($user_id) :int
  {
    $row = BalanceAccountCake::find()->where(['userId' => $user_id])->one();
    return $row->balance ?? 0;
  }

  public static function getCakesFormatted($user_id)
  {
    return number_format(self::getCakes($user_id), null, null, ' ');
  }

  public static function getPoints($user_id) :int
  {
    $row = BalanceAccountPoint::find()->where(['userId' => $user_id])->one();
    return $row->balance ?? 0;
  }

  public static function getPointsFormatted($user_id)
  {
    return number_format(self::getPoints($user_id), null, null, ' ');
  }
}