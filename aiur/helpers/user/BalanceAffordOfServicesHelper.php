<?php
namespace aiur\helpers\user;

use common\models\BalanceAccount;
use common\models\BalanceAccountCake;
use common\models\BalanceAccountPoint;

class BalanceAffordOfServicesHelper
{
  public static function getCountTasks($user_id)
  {
    $task_price = \Yii::$app->settings->get('price.task');
    $userBalance = BalanceHelper::getMoney($user_id);

    if( $userBalance > 0 )
    {
      return floor($userBalance / $task_price);
    }

    return 0;
  }

  public static function getCountCakes($user_id)
  {
    $tu_price = \Yii::$app->settings->get('price.tu');
    $userBalance = BalanceHelper::getMoney($user_id);

    if( $userBalance > 0 )
    {
      return floor($userBalance / $tu_price);
    }

    return 0;
  }

}