<?php
namespace aiur\helpers\user;

use common\models\User;

class LogoHelper
{
  public static function getUserAvatar( \common\models\User $user )
  {
    if(!isset($user->profile) || empty($user->profile))
    {
      return '/img/IconEmptyAvatar.png';
    }

    return $user->profile->getThumbFileUrl('image', 'thumb', '/img/IconEmptyAvatar.png');
  }

  public static function getUserLogoUrl( \common\models\User $user )
  {
    switch($user->profile_type)
    {
      case User::PROFILE['customer']:
        return self::getCustomerLogoUrl($user);

      case User::PROFILE['business']:
        return self::getBusinessLogoUrl($user);

      default: throw new \Exception('Wrong user type profile');
    }
  }

  public static function getCustomerLogoUrl( \common\models\User $user )
  {
    if( !empty($user->company->image) )
    {
      return $user->company->getThumbFileUrl('image', 'thumb');
    }
    elseif( !empty($user->profile->image) )
    {
      return $user->profile->getThumbFileUrl('image', 'thumb');
    }

    return '/img/IconEmptyAvatar.png';
  }

  public static function getBusinessLogoUrl( \common\models\User $user )
  {
    if(empty($user->profile))
    {
      return '/img/IconEmptyAvatar.png';
    }

    return $user->profile->getThumbFileUrl('image', 'thumb', '/img/IconEmptyAvatar.png');
  }

}