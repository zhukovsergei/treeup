<?php
namespace aiur\helpers\user;


use common\models\SettingsNotify;

class UserSettingsHelper
{
  public static function saveOrUpdate( $uid, $fd )
  {
    if(SettingsNotify::find()->where(['uid' => $uid])->exists())
    {
      return SettingsNotify::updateAll($fd, ['uid' => $uid]);
    }
    else
    {
      $sn = new SettingsNotify();
      $sn->attributes = $fd;
      $sn->uid = $uid;
      return $sn->save();
    }

  }

}