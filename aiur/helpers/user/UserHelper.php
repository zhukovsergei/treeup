<?php
namespace aiur\helpers\user;

class UserHelper
{
  public static function getUserName( \common\models\User $user )
  {
    if( !empty($user->company->name) )
      return $user->company->name;

    if( !empty($user->profile->name) )
      return $user->profile->name . ' ' . $user->profile->surname;

    return $user->username;
  }

  public static function getSkills( \common\models\User $user )
  {
    if( !empty($row->skills) )
    {
      return implode(', ', \yii\helpers\ArrayHelper::getColumn($row->skills, 'name'));
    }

    return '';
  }

}