<?php
namespace aiur\helpers\user;

use common\models\User;

class TypingProfileHelper
{
  public static function getTypeCompany( \common\models\User $user )
  {
    if(empty($user->company))
      return '';

    return \common\models\Company::JUR_TYPE[$user->company->jur_type];
  }

  public static function getBusinessCompany( \common\models\User $user )
  {
    if(empty($user->company))
      return '';

    return \common\models\Company::BUSINESS[$user->company->business];
  }

  public static function getRating( \common\models\User $user )
  {
    return $user->rating?:0;
  }

  public static function getProfileName( \common\models\User $user )
  {
    if(empty($user->profile))
      return '';

    return $user->profile->name . ' ' . $user->profile->surname;
  }

  public static function getCompanyKind( \common\models\User $user )
  {
    return $user->company->industry->name??'';
  }

  public static function setFinishedSignUp( int $user_id )
  {
    return User::updateAll(['finished_signup' => 1],['id' => $user_id]);
  }
}