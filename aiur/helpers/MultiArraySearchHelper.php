<?php
namespace aiur\helpers;

class MultiArraySearchHelper
{

  public static function getIdByName($array, $needle) {
//    $allSkills[array_search('ajax', array_column($allSkills, 'name'))]
    foreach ($array as $key => $val) {
      $val['name'] = strtolower(trim($val['name']));
      $needle = strtolower(trim($needle));

      if ($val['name'] === $needle) {
        return $val['id'];
      }
    }

    return null;
  }

}