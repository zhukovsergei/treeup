<?php
namespace aiur\helpers;


use common\models\Task;
use common\models\TaskFile;
use common\models\TaskParticipant;
use common\models\TaskPoint;
use common\models\TaskUserPin;
use common\models\User;

class TaskHelper
{

  public static function getPoints()
  {
    return json_encode(TaskPoint::find()->asArray()->all(), JSON_UNESCAPED_UNICODE);
  }

  public static function isUserPerformer($task_id, $participant_id)
  {
    $row = TaskParticipant::find()->where(['task_id' => $task_id, 'participant_id' => $participant_id])->one();

    if(isset($row) && $row->performer)
    {
      return 1;
    }

    return 0;
  }

  public static function isUserWinner($task_id, $participant_id) :int
  {
    $row = TaskParticipant::find()->where(['task_id' => $task_id, 'participant_id' => $participant_id])->one();

    if(isset($row) && $row->winner)
    {
      return 1;
    }

    return 0;
  }

  public static function isUserUploadedFile($task_id, $uid) :int
  {
    $task = Task::findOne($task_id);

    return $task->status == 'published' && TaskFile::find()->where(['task_id' => $task_id, 'uid' => $uid])->exists();
  }

  public static function isWon($task_id, $participant_id) :int
  {
    $task = Task::findOne($task_id);

    $row = TaskParticipant::find()->where(['task_id' => $task_id, 'participant_id' => $participant_id])->one();

    if(isset($row) && $row->winner && in_array($task->type_id, ['quest', 'event']) && $task->status === 'finished')
    {
      return 1;
    }

    if(isset($row) && $row->performer && $task->isTeam() && $task->status === 'finished')
    {
      return 1;
    }

    return 0;
  }

  public static function isUserDenied($task_id, $uid) :int
  {
    $task = Task::findOne($task_id);

    if(in_array($task->type_id, ['quest', 'event']))
    {
      if($task->status === 'finished' && ! self::isUserWinner($task_id, $uid) )
      {
        return 1;
      }
    }

    if($task->isTeam())
    {
      if($task->status === 'finished' && ! self::isUserPerformer($task_id, $uid) )
      {
        return 1;
      }
    }

    return 0;
  }

  public static function getDateParticipant($task_id, $participant_id)
  {
    $row = TaskParticipant::find()->where(['task_id' => $task_id, 'participant_id' => $participant_id])->one();

    if(isset($row) && $row->date_update)
    {
      return $row->date_update;
    }

    return '';
  }

  public static function isUserPinned($task_id, $participant_id) :int
  {
    return TaskUserPin::find()->where(['task_id' => $task_id, 'uid' => $participant_id])->exists();
  }
}