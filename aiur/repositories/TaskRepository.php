<?php

namespace aiur\repositories;

use common\dispatchers\EventDispatcher;
use common\models\Task;
use common\models\TaskFile;
use yii\data\ActiveDataProvider;

class TaskRepository
{
  private $dispatcher;

  public function __construct(EventDispatcher $dispatcher)
  {
    $this->dispatcher = $dispatcher;
  }

  public function getAll(): array
  {
    $rows = Task::find()->all();
    if ( empty($rows) )
    {
      throw new NotFoundException('Not found.');
    }

    return $rows;
  }

  public function get($id): Task
  {
    $row = Task::findOne($id);
    if ( empty($row) )
    {
      throw new NotFoundException('Not found.');
    }
    return $row;
  }

  public function getDataProvider(): ActiveDataProvider
  {
    return new ActiveDataProvider([
      'query' => Task::find()->orderBy('id DESC'),
      'pagination' => [
        'pageSize' => 20,
      ],
    ]);
  }

  public function save(Task $row)
  {
    if ( ! $row->save() )
    {
      throw new \RuntimeException('Saving error.');
    }

    $this->dispatcher->dispatchAll($row->releaseEvents());
  }

  public function remove(Task $row)
  {
    if ( ! $row->delete() )
    {
      throw new \RuntimeException('Removing error.');
    }
  }

}