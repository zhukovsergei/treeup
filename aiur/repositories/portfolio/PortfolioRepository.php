<?php

namespace aiur\repositories\portfolio;

use aiur\repositories\NotFoundException;
use common\dispatchers\EventDispatcher;
use common\models\Portfolio;
use yii\data\ActiveDataProvider;

class PortfolioRepository
{
//  private $dispatcher;

/*  public function __construct(EventDispatcher $dispatcher)
  {
    $this->dispatcher = $dispatcher;
  }*/

  public function getAll(): array
  {
    $rows = Portfolio::find()->all();
    if ( empty($rows) )
    {
      throw new NotFoundException('Not found.');
    }

    return $rows;
  }

  public function get($id): Portfolio
  {
    $row = Portfolio::findOne($id);
    if ( empty($row) )
    {
      throw new NotFoundException('Not found.');
    }
    return $row;
  }

  public function getDataProvider(): ActiveDataProvider
  {
    return new ActiveDataProvider([
      'query' => Portfolio::find()->orderBy('id DESC'),
      'pagination' => [
        'pageSize' => 20,
      ],
    ]);
  }

  public function save(Portfolio $row)
  {
    if ( ! $row->save() )
    {
      throw new \RuntimeException('Saving error.');
    }
//    $this->dispatcher->dispatchAll($row->releaseEvents());
  }

  public function remove(Portfolio $row)
  {
    if ( ! $row->delete() )
    {
      throw new \RuntimeException('Removing error.');
    }
  }
}