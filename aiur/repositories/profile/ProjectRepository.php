<?php

namespace aiur\repositories\profile;

use aiur\repositories\NotFoundException;
use common\models\Project;

class ProjectRepository
{
  private $dispatcher;

/*  public function __construct(EventDispatcher $dispatcher)
  {
    $this->dispatcher = $dispatcher;
  }*/

  public function getAll(): array
  {
    $rows = Project::find()->all();
    if ( empty($rows) )
    {
      throw new NotFoundException('Not found.');
    }

    return $rows;
  }

  public function get($id): Project
  {
    $row = Project::findOne($id);
    if ( empty($row) )
    {
      throw new NotFoundException('Not found.');
    }
    return $row;
  }

  public function save(Project $row)
  {
    if ( ! $row->save() )
    {
      throw new \RuntimeException('Saving error.');
    }
//    $this->dispatcher->dispatchAll($row->releaseEvents());
  }

  public function remove(Project $row)
  {
    if ( ! $row->delete() )
    {
      throw new \RuntimeException('Removing error.');
    }
  }
}