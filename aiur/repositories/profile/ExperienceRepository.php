<?php

namespace aiur\repositories\profile;

use aiur\repositories\NotFoundException;
use common\dispatchers\EventDispatcher;
use common\models\Experience;

class ExperienceRepository
{
  private $dispatcher;

/*  public function __construct(EventDispatcher $dispatcher)
  {
    $this->dispatcher = $dispatcher;
  }*/

  public function getAll(): array
  {
    $rows = Experience::find()->all();
    if ( empty($rows) )
    {
      throw new NotFoundException('Not found.');
    }

    return $rows;
  }

  public function get($id): Experience
  {
    $row = Experience::findOne($id);
    if ( empty($row) )
    {
      throw new NotFoundException('Not found.');
    }
    return $row;
  }

  public function save(Experience $row)
  {
    if ( ! $row->save() )
    {
      throw new \RuntimeException('Saving error.');
    }
//    $this->dispatcher->dispatchAll($row->releaseEvents());
  }

  public function remove(Experience $row)
  {
    if ( ! $row->delete() )
    {
      throw new \RuntimeException('Removing error.');
    }
  }
}