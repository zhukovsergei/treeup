<?php

namespace aiur\repositories\profile;

use aiur\repositories\NotFoundException;
use common\models\Education;

class EducationRepository
{
  private $dispatcher;

/*  public function __construct(EventDispatcher $dispatcher)
  {
    $this->dispatcher = $dispatcher;
  }*/

  public function getAll(): array
  {
    $rows = Education::find()->all();
    if ( empty($rows) )
    {
      throw new NotFoundException('Not found.');
    }

    return $rows;
  }

  public function get($id): Education
  {
    $row = Education::findOne($id);
    if ( empty($row) )
    {
      throw new NotFoundException('Not found.');
    }
    return $row;
  }

  public function save(Education $row)
  {
    if ( ! $row->save() )
    {
      throw new \RuntimeException('Saving error.');
    }
//    $this->dispatcher->dispatchAll($row->releaseEvents());
  }

  public function remove(Education $row)
  {
    if ( ! $row->delete() )
    {
      throw new \RuntimeException('Removing error.');
    }
  }
}