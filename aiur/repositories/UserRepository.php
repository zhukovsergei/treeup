<?php

namespace aiur\repositories;

use common\dispatchers\EventDispatcher;
use common\models\User;
use yii\data\ActiveDataProvider;

class UserRepository
{
  private $dispatcher;

  public function __construct(EventDispatcher $dispatcher)
  {
    $this->dispatcher = $dispatcher;
  }

  public function getAll(): array
  {
    $rows = User::find()->all();
    if ( empty($rows) )
    {
      throw new NotFoundException('Not found.');
    }

    return $rows;
  }

  public function get($id): User
  {
    $row = User::findOne($id);
    if ( empty($row) )
    {
      throw new NotFoundException('Not found.');
    }
    return $row;
  }

  public function getByEmail($email): User
  {
    $row = User::findByEmail($email);
    if ( empty($row) )
    {
      throw new NotFoundException('Not found.');
    }
    return $row;
  }

  public function getByPasswordResetToken($token): User
  {
    $row = User::findByPasswordResetToken($token);
    if ( empty($row) )
    {
      throw new NotFoundException('Not found.');
    }
    return $row;
  }

  public function getByEmailConfirmToken($token): User
  {
    return User::find()->where(['email_confirm_token' => $token])->one();
  }

  public function getDataProvider(): ActiveDataProvider
  {
    return new ActiveDataProvider([
      'query' => User::find()->joinWith('profile'),
      'pagination' => [
        'pageSize' => 20,
      ],
      'sort' => [
        'attributes' => [
          'id',
          'email',
          'username',
          'profile.email',
        ]
      ],
//      'sort' => false
    ]);
  }

  public function save(User $row)
  {
    if ( ! $row->save() )
    {
      throw new \RuntimeException('Saving error.');
    }
    $this->dispatcher->dispatchAll($row->releaseEvents());
  }

  public function remove(User $row)
  {
    if ( ! $row->delete() )
    {
      throw new \RuntimeException('Removing error.');
    }
  }
}