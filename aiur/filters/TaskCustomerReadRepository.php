<?php
namespace aiur\filters;

use common\models\Task;

class TaskCustomerReadRepository
{
  public function search(TaskFilterCriteria $criteria): array
  {
    $query = Task::find();
    $query->joinWith(['creator', 'creator.profile', 'creator.company']);

    if(mb_strlen($criteria->q) >= 3) {
      $query->andFilterWhere(['OR',
        ['LIKE', 'task.name', $criteria->q],
        ['LIKE', 'task.text', $criteria->q],
        ['LIKE', 'users.username', $criteria->q],
        ['LIKE', "CONCAT(profile.name, ' ', profile.surname, ' ', profile.patronymic)", $criteria->q],
        ['LIKE', 'company.name', $criteria->q],
      ]);
    }

    $query->andFilterWhere(['task.type_id' => $criteria->type_id]);
    $query->andFilterWhere(['task.project_id' => $criteria->project_id]);

    $query->andFilterWhere(['IN','task.status', $criteria->statuses]);


    return $query->all();
  }
}