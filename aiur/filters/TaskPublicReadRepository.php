<?php
namespace aiur\filters;

use common\models\Task;

class TaskPublicReadRepository
{
  public function search(TaskFilterCriteria $criteria): array
  {
    $query = Task::find();
    $query->joinWith(['creator', 'creator.profile', 'creator.company']);
    $query->where(['task.status' => ['published']]);
    if(\Yii::$app->getUser()->getIsGuest())
    {
      $query->andWhere(['task.authored_visible' => 0]);
    }

    if(mb_strlen($criteria->q) >= 3) {
      $query->andFilterWhere(['OR',
        ['LIKE', 'task.name', $criteria->q],
        ['LIKE', 'task.text', $criteria->q],
        ['LIKE', 'users.username', $criteria->q],
        ['LIKE', "CONCAT(profile.name, ' ', profile.surname, ' ', profile.patronymic)", $criteria->q],
        ['LIKE', 'company.name', $criteria->q],
      ]);
    }

    $query->andFilterWhere(['task.type_id' => $criteria->type_id]);

    $query->andFilterWhere(['task.town_id' => $criteria->town_id]);

    $query->andFilterWhere(['complexity' => $criteria->complexity]);

    $query->andFilterWhere(['BETWEEN', 'task.points_win', $criteria->points_win_start, $criteria->points_win_end ]);

    $query->andFilterWhere(['BETWEEN', 'task.points_participant', $criteria->points_participant_start, $criteria->points_participant_end ]);

    $query->andFilterWhere(['task.date_end' => $criteria->date_end]);

    $query->andFilterWhere(['IN','task.specialization_id', $criteria->specializations]);

    $query->andFilterWhere(['IN','task.industry_id', $criteria->industries]);


/*    if($criteria->status && in_array($criteria->status, Products::getApproveStatus())) {
      $query->andWhere(['status' => $criteria->status]);
    }*/

    return $query->all();
  }
}