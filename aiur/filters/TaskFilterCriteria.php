<?php
namespace aiur\filters;

use common\models\Task;

class TaskFilterCriteria
{
  public $q;
  public $type_id;
  public $town_id;
  public $project_id;
  public $complexity;
  public $points_win_start;
  public $points_win_end;
  public $points_participant_start;
  public $points_participant_end;
  public $date_end;
  public $specializations;
  public $industries;
  public $statuses;

  public $hide_performed;

  public $hide_answered;

  public function __construct( array $fd )
  {
    $specializations = isset($fd['specializations']) ? array_filter($fd['specializations']) : null;
    $industries = isset($fd['industries']) ? array_filter($fd['industries']) : null;
    $statuses = isset($fd['statuses']) ? array_filter($fd['statuses']) : null;

    $this->q = !empty($fd['q'])? $fd['q'] : null;
    $this->type_id = !empty($fd['type_id'])? $fd['type_id'] : null;
    $this->town_id = !empty($fd['town_id'])? $fd['town_id'] : null;
    $this->project_id = !empty($fd['project_id'])? $fd['project_id'] : null;
    $this->complexity = !empty($fd['complexity'])? $fd['complexity'] : null;

    $this->points_win_start = !empty($fd['points_win_start'])? $fd['points_win_start'] : null;
    $this->points_win_end = !empty($fd['points_win_end'])? $fd['points_win_end'] : null;

    $this->points_participant_start = !empty($fd['points_participant_start'])? $fd['points_participant_start'] : null;
    $this->points_participant_end = !empty($fd['points_participant_end'])? $fd['points_participant_end'] : null;

    $this->date_end = !empty($fd['date_end'])? $fd['date_end'] : null;
    $this->specializations = $specializations;
    $this->industries = $industries;
    $this->statuses = $statuses;

    $this->hide_performed = !empty($fd['hide_performed'])? $fd['hide_performed'] : null;
    $this->hide_answered = !empty($fd['hide_answered'])? $fd['hide_answered'] : null;
  }
}