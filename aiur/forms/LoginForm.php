<?php
namespace aiur\forms;

use common\models\User;
use yii\base\Model;

class LoginForm extends Model
{
  public $email;
  public $password;
  public $user;

  public function rules()
  {
    return [
//      [['remember'], 'default'],

      [['email', 'password'], 'required'],
      [['email'], 'trim'],
      ['email', 'exist', 'targetClass' => User::class, 'filter' => ['banned' => 0]],

      ['email', 'string', 'max' => 191],

      ['password', 'string', 'min' => 6],
      ['password', 'validatePassword'],
    ];
  }

  public function validatePassword($attr, $params)
  {
    $this->user = User::findByEmail($this->email);

    if ( !empty($this->user) && ! $this->user->validatePassword($this->password) ) {
      $this->addError($attr, 'Wrong password');
    }
  }

  public function getAuthoredUser()
  {
    return $this->user;
  }

}