<?php

namespace aiur\forms;

use common\models\User;
use yii\base\Model;

class ResetPasswordSettingsForm extends Model
{
    public $user;

    public $old_password;
    public $password;
    public $password_repeat;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->user = \Yii::$app->getUser()->getIdentity();
    }

    public function rules()
    {
        return [
            [['password','password_repeat'], 'required'],
            ['password', 'string', 'min' => 6],
            [['password_repeat'], 'validatePasswordEqual'],
            ['old_password', 'required', 'when' => [$this, 'isUpdatePassword']],
            ['password', 'validatePassword', 'when' => [$this, 'isUpdatePassword']],

        ];
    }

    public function isUpdatePassword()
    {
        return empty($this->user->password_hash) ? false : true;
    }

    public function validatePasswordEqual($attr, $params)
    {
        if ($this->password != $this->password_repeat) {
            $this->addError($attr, 'Пароли не совпадают.');
        }
    }

    public function validatePassword($attr, $params)
    {
        if (!$this->user->validatePassword($this->old_password)) {
            $this->addError($attr, 'неверный старый пароль.');
        }
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Новый пароль',
            'repeat_password' => 'Повтор нового пароля',
            'old_password' => 'Старый пароль'
        ];
    }

}