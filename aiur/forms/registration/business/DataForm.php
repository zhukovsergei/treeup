<?php

namespace aiur\forms\registration\business;

use common\models\Profile;
use yii\base\Model;
use common\models\Town;

class DataForm extends Model
{
    public $name;
    public $surname;
    public $patronymic;
    public $birthday;
    public $town_id;
    public $email;
    public $phone;
    public $jur_status;

    public $image;
    public $remove_image;

    public function __construct($profile = null, $config = [])
    {
        if ($profile) {
            $this->name = $profile->name;
            $this->surname = $profile->surname;
            $this->patronymic = $profile->patronymic;
            $this->birthday = $profile->birthday;
            $this->town_id = $profile->town_id;
            $this->jur_status = $profile->jur_status;
            $this->email = $profile->email;
            $this->phone = $profile->phone;
            $this->image = $profile->getThumbFileUrl('image');
        }

        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name', 'surname', 'birthday', 'town_id', 'email', 'phone'], 'required'],
            [['name', 'surname', 'patronymic', 'birthday', 'town_id', 'jur_status', 'email', 'phone'], 'safe'],
            [['name', 'surname'], 'string', 'max' => 255],
            ['birthday', 'date', 'format' => 'php:Y-m-d'],
            ['birthday', 'validateBirthday'],
            ['town_id', 'exist', 'targetClass' => Town::class, 'targetAttribute' => 'id'],
            ['email', 'email'],
            ['phone', 'string', 'max' => 255],
            [['remove_image'], 'default'],
            [['town_id'], 'default', 'value' => 0],
            ['image', 'file', 'skipOnEmpty' => true, 'extensions' => ' jpg, jpeg, png', 'maxSize' => 5 * 1024 * 1024]
        ];
    }

    public function validateBirthday($attribute, $params)
    {
        if ($this->birthday >= date('Y-m-d')) {
            $this->addError($attribute, '«Дата рождения» не может быть больше текущей даты.');
        }
    }

    public function getImage()
    {
        return $this->image ? $this->image : '/img/IconEmptyAvatar.png';
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'patronymic' => 'Отчество',
            'birthday' => 'Дата рождения',
            'town_id' => 'Город проживания',
            'email' => 'Электронная почта',
            'phone' => 'Номер телефона'
        ];
    }
}