<?php

namespace aiur\forms\registration\customer;

use common\models\Profile;
use common\models\Town;
use yii\base\Model;

class DataForm extends Model
{
    public $name;
    public $surname;
    public $patronymic;
    public $birthday;
    public $email;
    public $town_id;
    public $phone;
    public $jur_status;


    public $image;
    public $remove_image;

    public function __construct($profile = null, $config = [])
    {
        if ($profile) {
            $this->name = $profile->name;
            $this->surname = $profile->surname;
            $this->patronymic = $profile->patronymic;
            $this->birthday = $profile->birthday;
            $this->town_id = $profile->town_id;
            $this->jur_status = $profile->jur_status;
            $this->email = $profile->email;
            $this->phone = $profile->phone;
            $this->image = $profile->getThumbFileUrl('image');
        }

        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name', 'surname', 'birthday', 'town_id', 'jur_status', 'email', 'phone'], 'required'],
            [['name', 'surname', 'patronymic', 'birthday', 'town_id', 'jur_status', 'email', 'phone'], 'safe'],
            [['name', 'surname'], 'string', 'max' => 255],
            ['birthday', 'date', 'format' => 'php:Y-m-d'],
            ['birthday', 'validateBirthday'],
            ['town_id', 'exist', 'targetClass' => Town::class, 'targetAttribute' => 'id'],
            ['jur_status', 'in', 'range' => Profile::JUR_STATUS_DB],
            ['email', 'email'],
            ['phone', 'string', 'max' => 255],
            [['remove_image'], 'default'],
            [['jur_status'], 'default', 'value' => Profile::JUR_STATUS_DB['personal']],
            [['town_id'], 'default', 'value' => 0],
            ['image', 'file', 'skipOnEmpty' => true, 'extensions' => ' jpg, jpeg, png', 'maxSize' => 5 * 1024 * 1024]
        ];
    }

    public function validateBirthday($attribute, $params)
    {
        if ($this->birthday >= date('Y-m-d')) {
            $this->addError($attribute, '«Дата рождения» не может быть больше текущей даты.');
        }
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'patronymic' => 'Отчество',
            'birthday' => 'Дата рождения',
            'town_id' => 'Город проживания',
            'jur_status' => 'Тип учётной записи',
            'email' => 'Электронная почта',
            'phone' => 'Номер телефона'
        ];
    }

    public function getImage()
    {
        return $this->image ? $this->image : '/img/IconEmptyAvatar.png';
    }
}