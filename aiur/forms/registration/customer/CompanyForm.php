<?php

namespace aiur\forms\registration\customer;

use common\models\User;
use yii\base\Model;
use yii\web\UploadedFile;

class CompanyForm extends Model
{
    public $name;
    public $jur_type;
    public $business;
    public $industry_id;
    public $text;

  public $image;

  public function __construct($company = null, $config = [])
  {
    if ($company) {
      $this->name = $company->name;
      $this->jur_type = $company->jur_type;
      $this->business = $company->business;
      $this->industry_id = $company->industry_id;
      $this->text = $company->text;
      $this->image = $company->getThumbFileUrl('image');
    }

    parent::__construct($config);
  }


    public function rules(): array
    {
      return [
        [['name'], 'required'],
//        ['email', 'email'],
//        ['email', 'string', 'max' => 191],

//        [['email'], 'unique', 'targetClass' => User::class, 'filter' => ['<>', 'id', $this->email]],

        [['jur_type', 'business', 'industry_id', 'text', 'image'], 'default'],
//        [['jur_status'], 'default', 'value' => 1],

      ];
    }

  public function beforeValidate(): bool
  {
    if (parent::beforeValidate()) {
      $this->image = UploadedFile::getInstanceByName('file');
      return true;
    }
    return false;
  }

  public function getImage()
  {
    return $this->image ? $this->image : '/img/IconEmptyAvatar.png';
  }

}