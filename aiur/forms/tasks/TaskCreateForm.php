<?php

namespace aiur\forms\tasks;

use aiur\helpers\MultiArraySearchHelper;
use aiur\helpers\TaskPricePostHelper;
use common\models\Industry;
use common\models\Skill;
use common\models\Specialization;
use common\models\Task;
use common\models\TaskPoint;
use common\models\Town;
use common\models\Project;
use yii\base\Model;
use common\helpers\FileHelper;

class TaskCreateForm extends Model
{
    public $files = [];
    public $skills = [];

    public $status;
    public $name;
    public $text;
    public $town_id;
    public $type_id;
    public $project_id;
    public $industry_id;
    public $complexity;
    public $specialization_id;
    public $date_end;
    public $points_win;
    public $points_participant;
    public $privilege;
    public $tu_count;
    public $authored_visible;
    public $privilege_description;
    public $is_paid = 0;

    public function getAvailableStatus()
    {
        return [
            Task::STATUS_DB['draft'],
            Task::STATUS_DB['moderation']
        ];
    }

    public function __construct( $config = [])
    {
        parent::__construct($config);
    }

    //TODO  tu_count validation range by DI
    //TODO validation TU when type is TeamBuilding
    public function rules(): array
    {
        return [
            [['name', 'type_id','status', 'industry_id', 'complexity', 'specialization_id', 'text', 'date_end', 'points_win', 'privilege'], 'required'],
            [['name', 'type_id', 'status','industry_id', 'complexity', 'specialization_id', 'text', 'date_end', 'points_win', 'points_participant', 'privilege', 'files', 'skills','privilege_description','town_id'], 'safe'],

            ['status','in','range' => $this->getAvailableStatus()],

            ['name', 'string', 'max' => 255],
            ['text', 'string', 'max' => 10000],

            ['town_id','default','value' => 0],
            ['town_id', 'exist', 'targetClass' => Town::class, 'targetAttribute' => 'id', 'when' => [$this, 'isNotAnyTown']],

            ['type_id', 'in', 'range' => Task::TYPE_DB],

            ['project_id', 'exist', 'targetClass' => Project::class, 'targetAttribute' => 'id', 'when' => [$this, 'isNotAnyProject']],

            ['industry_id', 'exist', 'targetClass' => Industry::class, 'targetAttribute' => 'id'],
            ['complexity', 'in', 'range' => array_keys(Task::COMPLEXITY)],
            ['specialization_id', 'exist', 'targetClass' => Specialization::class, 'targetAttribute' => 'id'],
            ['date_end', 'date', 'format' => 'php:Y-m-d'],
            ['points_win', 'in', 'range' => function(){
                return explode(',',TaskPoint::findOne(['complexity' => $this->complexity])->winner);
            }],

            ['points_participant','required','when' => [$this,'isNotTeamBuilding']],
            ['points_participant', 'in', 'range' => function(){
                return explode(',',TaskPoint::findOne(['complexity' => $this->complexity])->participant);},'when' => [$this,'isNotTeamBuilding']],
            ['points_participant', 'setNullPointParticipant','when' => [$this,'isTeamBuilding']],

            ['privilege', 'in', 'range' => array_keys(Task::PRIVILEGE)],
            ['privilege_description', 'required', 'when' => [$this, 'typeIsPrize']],

            ['tu_count', 'required', 'when' => [$this, 'typeIsTu']],
            ['tu_count', 'in', 'range' => Task::TU_COUNT, 'when' => [$this, 'typeIsTu']],

            ['authored_visible', 'default', 'value' => 0],
            ['authored_visible', 'in', 'range' => [0, 1]],

            ['skills', 'validateSkills'],
            [['skills', 'files'], 'default'],

            [['files'], 'file', 'skipOnEmpty' => true, 'extensions' => ' jpg, jpeg, png, gif, doc, docx, xls, xlsx, ppt, pptx, pdf, txt, odt, psd, cdr, ai, zip, rar, 7z', 'maxFiles' => 10],
            ['files', 'validateFilesSize'],

        ];
    }

    public function isNotAnyTown()
    {
        return $this->town_id != 0 ? true : false;
    }

    public function isNotAnyProject()
    {
        return $this->project_id != 0 ? true : false;
    }

    public function typeIsTu()
    {
        return $this->privilege == Task::PRIVILEGE_DB['TU'] ? true : false;
    }

    public function typeIsPrize()
    {
        return $this->privilege == Task::PRIVILEGE_DB['Приз'] ? true : false;
    }

    public function isNotTeamBuilding()
    {
        return $this->type_id == Task::TYPE_DB['team'] ? false : true;
    }

    public function isTeamBuilding()
    {
        return $this->type_id === Task::TYPE_DB['team'] ? true : false;
    }

    public function setNullPointParticipant($attribute, $params)
    {
       $this->points_participant = null;
    }

    public function validateSkills($attribute, $params)
    {
        if (!empty($this->skills)) {
            foreach ($this->getNormalizeSkills($this->skills) as $skill) {
                if (!is_string($skill["name"]) || mb_strlen($skill["name"]) > 255) {
                    $this->addError($attribute, 'Наименование навыка «'.$skill["name"].'» не должно превышать 255 символов.');
                }
            }
        }
    }

    public function validateFilesSize($attribute, $params)
    {
        if (!empty($this->files)) {
            $size = 15 * 1024 * 1024;
            if (!is_array($this->files)) {
                if ($this->files->size > $size) {
                    $this->addError($attribute, 'Размер файла превышает ' . FileHelper::getFileSize($size) . '.');
                }
            } else {
                $currentSize = 0;
                foreach ($this->files as $file) {
                    $currentSize += $file->size;
                }
                if ($currentSize > $size) {
                    $this->addError($attribute, 'Размер файлов превышает ' . FileHelper::FileSizeConvert($size) . '.');
                }
            }
        }
    }

    public function afterValidate()
    {
        parent::afterValidate();
        if ($this->status == Task::STATUS_DB['moderation'] && $this->is_paid != 1 && !TaskPricePostHelper::isEnough(\Yii::$app->getUser()->getId(), $this->tu_count)) {
            $this->addError('tu_count', 'Недостаточно денежных средств на счете.');
        }
    }

    public function getNormalizeSkills()
    {
        $allSkills = Skill::find()->asArray()->all();

        $skills = json_decode($this->skills, true);

        $arr = array_map(function ($item) use ($allSkills) {

            $tmp = [];

            $tmp['name'] = $item['name'];

            $exists_id = MultiArraySearchHelper::getIdByName($allSkills, $item['name']);

            if (isset($item['id']) && is_numeric($item['id'])) {
                $tmp['id'] = $item['id'];
            } elseif (!empty($exists_id)) {
                $tmp['id'] = $exists_id;
            }

            return $tmp;
        }, $skills);

        return $arr;
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название задачи',
            'type_id' => 'Тип задачи',
            'industry_id' => 'Сфера',
            'complexity' => 'Уровень сложности',
            'specialization_id' => 'Специализация',
            'text' => 'Описание',
            'date_end' => 'Окончание приёма заявок',
            'points_win' => 'Очки для победителя',
            'points_participant' => 'Очки для участника',
            'privilege' => 'Привилегия',
            'files' => 'Прикреплённые файлы',
            'skills' => 'Навыки через запятую',
            'privilege_description' => 'Описание привилегии',
            'status' => 'Статус'
        ];
    }

}