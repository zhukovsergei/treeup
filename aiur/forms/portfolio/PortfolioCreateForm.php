<?php

namespace aiur\forms\portfolio;

use common\models\Portfolio;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class PortfolioCreateForm extends Model
{
  public $name;
  public $text;
  public $blocks;

  public $files;
  public $video_urls;

  public function rules(): array
  {
    return [
//        [['name', 'surname', 'birthday', 'town_id'], 'required'],
//        ['email', 'email'],
//        ['email', 'string', 'max' => 191],

//        [['email'], 'unique', 'targetClass' => User::class, 'filter' => ['<>', 'id', $this->email]],

      [['name', 'text', 'blocks', 'files', 'video_urls'], 'default'],

    ];
  }

  public function beforeValidate(): bool
  {
    $this->files = UploadedFile::getInstancesByName('files');
    return parent::beforeValidate();
  }

  public function getNormalizeBlocks()
  {
    $blocks = array_map(function ($json){
      $tmp = json_decode($json, JSON_OBJECT_AS_ARRAY);
      return [
        'id' => $tmp['id']??null,
        'name' => $tmp['name'],
        'text' => $tmp['text'],
        'type' => $tmp['type'],
        'quantity' => $tmp['quantity']??0,
      ];
    }, $this->blocks??[]);

    return $blocks;
  }

  public function getAllBlocks()
  {
    return $this->getNormalizeBlocks();
  }

  public function getImageAudioBlocks()
  {
    $blocks = array_filter($this->getNormalizeBlocks(), function ($item){
      return $item['type'] !== 'video';
    });

    return $blocks;
  }

  public function getVideoBlocks()
  {
    $blocks = array_filter($this->getNormalizeBlocks(), function ($item){
      return $item['type'] === 'video';
    });
    return $blocks;
  }

  public function getFiles()
  {
    return $this->files;
  }

  public function getFilesQuantity()
  {
    return ArrayHelper::getColumn($this->getImageAudioBlocks(), 'quantity');
  }

  public function getVideoUrls()
  {
    return $this->video_urls;
  }

  public function getVideoUrlsQuantity()
  {
    return ArrayHelper::getColumn($this->getVideoBlocks(), 'quantity');
  }
}