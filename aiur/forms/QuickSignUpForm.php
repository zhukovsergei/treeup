<?php
namespace aiur\forms;

use common\models\User;
use yii\base\Model;

class QuickSignUpForm extends Model
{
  public $email;
  public $password;
  public $type_id;

  public function rules()
  {
    return [
      [['email', 'password'], 'required'],
      [['email'], 'trim'],

      ['email', 'email'],
      ['email', 'string', 'max' => 191],
      ['email', 'unique', 'targetClass' => User::class, 'message' => 'This email address has already been taken.'],

      ['type_id', 'default', 'value' => 1],

/*      ['phone', 'required'],
      [['phone'], 'filter',  'filter' => function($value) {
        return trim(preg_replace('/\D/', '', $value));
      }],
      ['phone', 'unique', 'targetClass' => User::class, 'message' => 'This username has already been taken.'],*/

    ];
  }

}