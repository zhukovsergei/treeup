<?php

namespace aiur\forms;

use common\models\User;
use yii\base\Model;

class LostPasswordForm extends Model
{
    public $email;

    private $user;

    public function rules(): array
    {
        return [
            [['email'], 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 191],
            ['email', 'exist', 'targetClass' => User::class],

        ];
    }

}