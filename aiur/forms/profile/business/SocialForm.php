<?php

namespace aiur\forms\profile\business;

use yii\base\Model;

class SocialForm extends Model
{
    public $skype;
    public $telegram;
    public $icq;
    public $vk;
    public $fb;
    public $google;

  public function __construct($profile = null, $config = [])
  {
    if ($profile) {
      $this->skype = $profile->skype;
      $this->telegram = $profile->telegram;
      $this->icq = $profile->icq;
      $this->vk = $profile->vk;
      $this->fb = $profile->fb;
      $this->google = $profile->google;
    }

    parent::__construct($config);
  }


    public function rules(): array
    {
      return [
//        [['name', 'surname', 'birthday', 'town_id'], 'required'],
//        ['email', 'email'],
//        ['email', 'string', 'max' => 191],

//        [['email'], 'unique', 'targetClass' => User::class, 'filter' => ['<>', 'id', $this->email]],

        [['skype', 'telegram', 'icq', 'vk', 'fb', 'google'], 'default'],

      ];
    }

}