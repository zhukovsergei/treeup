<?php

namespace aiur\forms\profile\business;

use common\models\Education;
use common\models\Experience;
use yii\base\Model;

class ExperienceForm extends Model
{
    public $id;
    public $uid;

    public $current;
    public $date_from;
    public $date_to;
    public $company;
    public $post;
    public $duties;

  public function __construct(int $id = null, int $uid = null, $config = [])
  {
    if($id && $uid)
    {
      $this->id = $id;
      $this->uid = $uid;
    }

    parent::__construct($config);
  }


    public function rules(): array
    {
      return [
//        [['name', 'surname', 'birthday', 'town_id'], 'required'],
//        ['email', 'email'],
//        ['email', 'string', 'max' => 191],

//        [['email'], 'unique', 'targetClass' => User::class, 'filter' => ['<>', 'id', $this->email]],

        [['current', 'date_from', 'date_to', 'company', 'post', 'duties'], 'default'],

      ];
    }

  public function beforeValidate(): bool
  {
    if($this->id && $this->uid)
    {
      if ( ! Experience::find()->where(['id' => $this->id, 'uid' => $this->uid])->exists() ) {
        return false;
      }
    }

    return parent::beforeValidate();
  }
}