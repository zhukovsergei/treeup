<?php

namespace aiur\forms\profile\business;

use yii\base\Model;
use yii\web\UploadedFile;

class PersonalDataForm extends Model
{
  public $name;
  public $surname;
  public $patronymic;
  public $birthday;
  public $town_id;
  public $jur_status;
  public $email;
  public $phone;

  public $image;
  public $remove_image;

  public function __construct($profile = null, $config = [])
  {
    if ($profile) {
      $this->name = $profile->name;
      $this->surname = $profile->surname;
      $this->patronymic = $profile->patronymic;
      $this->birthday = $profile->birthday;
      $this->town_id = $profile->town_id;
      $this->jur_status = $profile->jur_status;
      $this->email = $profile->email;
      $this->phone = $profile->phone;
    }

    parent::__construct($config);
  }

  public function rules(): array
  {
    return [
      [['name', 'surname', 'birthday', 'town_id'], 'required'],
//        ['email', 'email'],
//        ['email', 'string', 'max' => 191],

//        [['email'], 'unique', 'targetClass' => User::class, 'filter' => ['<>', 'id', $this->email]],

      [['phone', 'email', 'patronymic', 'image', 'remove_image'], 'default'],
      [['jur_status'], 'default', 'value' => 1],
      [['town_id'], 'default', 'value' => 172],

//      [['image'], 'image'],
    ];
  }

  public function beforeValidate(): bool
  {
    if (parent::beforeValidate()) {
      $this->image = UploadedFile::getInstanceByName('file');
      return true;
    }
    return false;
  }
}