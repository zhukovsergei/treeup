<?php

namespace aiur\forms\profile\business;

use common\models\Education;
use yii\base\Model;

class EducationForm extends Model
{
    public $id;
    public $uid;

    public $date_from;
    public $date_to;
    public $university;
    public $town_id;
    public $spec;

  public function __construct(int $id = null, int $uid = null, $config = [])
  {
    if($id && $uid)
    {
      $this->id = $id;
      $this->uid = $uid;
    }

    parent::__construct($config);
  }


    public function rules(): array
    {
      return [
//        [['name', 'surname', 'birthday', 'town_id'], 'required'],
//        ['email', 'email'],
//        ['email', 'string', 'max' => 191],

//        [['email'], 'unique', 'targetClass' => User::class, 'filter' => ['<>', 'id', $this->email]],

        [['uid', 'date_from', 'date_to', 'university', 'town_id', 'spec'], 'default'],

      ];
    }

  public function beforeValidate(): bool
  {
    if($this->id && $this->uid)
    {
      if ( ! Education::find()->where(['id' => $this->id, 'uid' => $this->uid])->exists() ) {
        return false;
      }
    }

    return parent::beforeValidate();
  }
}