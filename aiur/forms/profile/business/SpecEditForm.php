<?php

namespace aiur\forms\profile\business;

use aiur\helpers\MultiArraySearchHelper;
use common\models\Industry;
use common\models\Skill;
use common\models\Spec;
use common\models\Specialization;
use yii\base\Model;

class SpecEditForm extends Model
{
  public $id;
  public $uid;

  public $type_industry;
  public $specialization;
  public $industry;
  public $exp;
  public $skills;

  public function __construct( $id = null,  $uid = null, $config = [])
  {
    if($id && $uid)
    {
      $this->id = $id;
      $this->uid = $uid;
    }

    parent::__construct($config);
  }


  public function rules(): array
  {
    return [
//        [['name', 'surname', 'birthday', 'town_id'], 'required'],
//        ['email', 'email'],
//        ['email', 'string', 'max' => 191],

//        [['email'], 'unique', 'targetClass' => User::class, 'filter' => ['<>', 'id', $this->email]],

      [['type_industry', 'industry', 'specialization', 'exp', 'skills'], 'default'],

    ];
  }

  public function beforeValidate(): bool
  {
    if($this->id && $this->uid)
    {
      if ( ! Spec::find()->where(['id' => $this->id, 'uid' => $this->uid])->exists() ) {
        return false;
      }
    }

    return parent::beforeValidate();
  }

  public function getNormalizeIndustry()
  {
    if(is_numeric($this->industry))
      return $this->industry;

    $tmp = [];

    if($exist_industry = Industry::find()->where(['name' => $this->industry])->one())
    {
      $tmp['id'] = $exist_industry->id;
    }

    $tmp['name'] = $this->industry;

    return $tmp;
  }

  public function getNormalizeSpecialization()
  {
    if(is_numeric($this->specialization))
      return $this->specialization;

    $tmp = [];

    if($exist_spec = Specialization::find()->where(['name' => $this->specialization])->one())
    {
      $tmp['id'] = $exist_spec->id;
    }

    $tmp['name'] = $this->specialization;

    return $tmp;
  }

  public function getNormalizeSkills()
  {
    $allSkills = Skill::find()->asArray()->all();

    $arr = array_map(function ($item) use ($allSkills) {

      $tmp = [];

      $tmp['name'] = $item['name'];

      $exists_id = MultiArraySearchHelper::getIdByName($allSkills, $item['name']);

      if( isset($item['id']) && is_numeric($item['id']) )
      {
        $tmp['id'] = $item['id'];
      }
      elseif( ! empty($exists_id) )
      {
        $tmp['id'] = $exists_id;
      }

      return $tmp;
    }, $this->skills);

    return $arr;
  }

}