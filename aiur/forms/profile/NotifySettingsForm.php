<?php

namespace aiur\forms\profile;

use common\models\Education;
use yii\base\Model;

class NotifySettingsForm extends Model
{
    public $email_new_article;
    public $email_deny_task;
    public $phone_important;
    public $phone_new_article;
    public $phone_deny_task;

  public function __construct($settings = null, $config = [])
  {
    if($settings)
    {
      $this->email_new_article = $settings->email_new_article;
      $this->email_deny_task = $settings->email_deny_task;
      $this->phone_important = $settings->phone_important;
      $this->phone_new_article = $settings->phone_new_article;
      $this->phone_deny_task = $settings->phone_deny_task;
    }

    parent::__construct($config);
  }
}