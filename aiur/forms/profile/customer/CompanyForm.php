<?php

namespace aiur\forms\profile\customer;

use common\models\Company;
use common\models\Industry;
use common\models\User;
use yii\base\Model;
use yii\web\UploadedFile;

class CompanyForm extends Model
{
    public $name;
    public $jur_type;
    public $business;
    public $industry_id;
    public $text;

    public $image;

    public function __construct($company = null, $config = [])
    {
        if ($company) {
            $this->name = $company->name;
            $this->jur_type = $company->jur_type;
            $this->business = $company->business;
            $this->industry_id = $company->industry_id;
            $this->text = $company->text;
            $this->image = $company->image;
        }

        parent::__construct($config);
    }


    public function rules(): array
    {
        return [
            [['name'], 'required'],
            ['name', 'string', 'max' => 255],
            ['jur_type','in','range' => Company::JUR_TYPE_DB],
            ['business', 'in', 'range' => Company::BUSINESS_DB],
            ['text', 'string', 'max' => 10000],
            ['industry_id', 'exist', 'targetClass' => Industry::class, 'targetAttribute' => 'id', 'when' => [$this, 'isIdIndustry']],
            ['image', 'file', 'skipOnEmpty' => true, 'extensions' => ' jpg, jpeg, png', 'maxSize' => 5 * 1024 * 1024]
        ];
    }

    public function isIdIndustry()
    {
        return $this->industry_id > 0 ? true : false;
    }
}