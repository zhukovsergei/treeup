<?php

namespace aiur\forms\profile\customer;

use common\models\Project;
use yii\base\Model;

class ProjectForm extends Model
{
    public $id;
    public $uid;

    public $name;
    public $text;
    public $tasks;

  public function __construct(int $id = null, int $uid = null, $config = [])
  {
    if($id && $uid)
    {
      $this->id = $id;
      $this->uid = $uid;
    }

    parent::__construct($config);
  }


  public function rules(): array
  {
    return [
//        [['name', 'surname', 'birthday', 'town_id'], 'required'],
//        ['email', 'email'],
//        ['email', 'string', 'max' => 191],

//        [['email'], 'unique', 'targetClass' => User::class, 'filter' => ['<>', 'id', $this->email]],

      [['name', 'text', 'tasks'], 'default'],

    ];
  }

  public function getTasksIds()
  {
    return $this->tasks;
  }

  public function beforeValidate(): bool
  {
    if($this->id && $this->uid)
    {
      if ( ! Project::find()->where(['id' => $this->id, 'uid' => $this->uid])->exists() ) {
        return false;
      }
    }

    return parent::beforeValidate();
  }
}