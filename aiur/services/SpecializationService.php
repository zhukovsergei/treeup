<?php

namespace aiur\services;

use aiur\repositories\SpecializationRepository;
use common\models\Specialization;

class SpecializationService
{
  private $repo;

  public function __construct( SpecializationRepository $repo )
  {
    $this->repo = $repo;
  }

  public function create(array $fd)
  {
    $m = new Specialization();
    $m->attributes = $fd;
    $m->save();

    return $m;
  }

  public function update($id, array $fd)
  {
    $user = $this->repo->get($id);
    $user->attributes = $fd;
    $user->update();
  }
}