<?php

namespace aiur\services;

use aiur\repositories\UserRepository;
use common\models\Company;
use common\models\Profile;

class CompanyService
{
  private $userRepository;

  public function __construct( UserRepository $userRepository )
  {
    $this->userRepository = $userRepository;
  }

  public function create($uid, \yii\base\Model $form)
  {
    $profile = Company::create(
      $uid,
      $form->name,
      $form->jur_type,
      $form->business,
      $form->industry_id,
      $form->text
    );

    if ($form->image) {
      $profile->setImage($form->image);
    }

    $profile->save();
  }

  public function update($uid, \yii\base\Model $form)
  {
    $profile = Company::find()->where(['uid' => $uid])->one();

    $profile->edit(
      $form->name,
      $form->jur_type,
      $form->business,
      $form->industry_id,
      $form->text
    );

    if ($form->image) {
      $profile->setImage($form->image);
    }

    $profile->save();
  }

  public function saveOrNew($id, \yii\base\Model $form)
  {
    $user = $this->userRepository->get($id);

    if($user->getCompany()->exists())
    {
      $this->update($user->id, $form);
    }
    else
    {
      $this->create($user->getId(), $form);
    }

    $this->userRepository->save($user);
  }

}