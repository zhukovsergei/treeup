<?php

namespace aiur\services;

use aiur\repositories\UserRepository;
use common\models\Profile;

class ProfileService
{
  private $userRepository;

  public function __construct( UserRepository $userRepository )
  {
    $this->userRepository = $userRepository;
  }

  public function create($uid, \yii\base\Model $form)
  {
    $profile = Profile::create(
      $uid,
      $form->name,
      $form->surname,
      $form->patronymic,
      $form->birthday,
      $form->email,
      $form->town_id,
      $form->phone,
      $form->jur_status
    );

    if ($form->image) {
      $profile->setImage($form->image);
    }

    $profile->save();
  }

  public function update($uid, \yii\base\Model $form)
  {
    $profile = Profile::find()->where(['uid' => $uid])->one();

    $profile->edit(
      $form->name,
      $form->surname,
      $form->patronymic,
      $form->birthday,
      $form->email,
      $form->town_id,
      $form->phone,
      $form->jur_status
    );

    if ($form->image) {
      $profile->setImage($form->image);
    }

    if ($form->remove_image) {
      $profile->updateAttributes(['image' => null]);
    }

    $profile->save();
  }

  public function saveOrNew($id, \yii\base\Model $form)
  {
    $user = $this->userRepository->get($id);

    if($user->getProfile()->exists())
    {
      $this->update($user->id, $form);
    }
    else
    {
      $this->create($user->getId(), $form);
    }

    $this->userRepository->save($user);
  }

  public function updateSocial($id, \yii\base\Model $form)
  {
    $profile = Profile::find()->where(['uid' => $id])->one();

    $profile->setSocial(
      $form->skype,
      $form->telegram,
      $form->icq,
      $form->vk,
      $form->fb,
      $form->google
    );

    $profile->save();
  }

}