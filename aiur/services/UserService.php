<?php

namespace aiur\services;

use aiur\forms\SignUpUserForm;
use aiur\forms\UserEditForm;
use aiur\repositories\UserRepository;
use common\models\User;

class UserService
{
  private $userRepository;

  public function __construct( UserRepository $userRepository )
  {
    $this->userRepository = $userRepository;
  }

  public function create(SignUpUserForm $form)
  {
    return User::create(
      $form->username,
      $form->password,
      $form->email,
      $form->root
    );
  }

  public function update($id, UserEditForm $form)
  {
    $user = $this->userRepository->get($id);

    $user->username = $form->username;
    $user->email = $form->email;
    $user->root = $form->root;
    $user->banned = $form->banned;

    if( !empty($form->password) )
    {
      $user->setPassword($form->password);
    }

    $this->userRepository->save($user);
  }

  public function setCustomerProfileFor( int $id )
  {
    $user = $this->userRepository->get($id);

    $user->profile_type = User::PROFILE['customer'];

    $this->userRepository->save($user);
  }

  public function setBusinessProfileFor( int $id )
  {
    $user = $this->userRepository->get($id);

    $user->profile_type = User::PROFILE['business'];

    $this->userRepository->save($user);
  }

  public function makeTransaction( $td, $type_transaction )
  {
    $user = User::findOne($td['user_id']);
    if($type_transaction === 'add')
    {
      switch($td['type_account']){
        case 'money':
          \Yii::$app->balanceManager->increase( ['userId' => $td['user_id']], $td['amount'], ['stock' => $user->getMoneyBalanceStock($td['amount'])] );
          break;

        case 'cake':
          \Yii::$app->cakeBalanceManager->increase( ['userId' => $td['user_id']], $td['amount'], ['stock' => $user->getMoneyBalanceStock($td['amount'])] );
          break;

        case 'points':
          \Yii::$app->pointBalanceManager->increase( ['userId' => $td['user_id']], $td['amount'], ['stock' => $user->getMoneyBalanceStock($td['amount'])] );
          break;
      }
    }
    else
    {
      switch($td['type_account']){
        case 'money':
          \Yii::$app->balanceManager->decrease( ['userId' => $td['user_id']], $td['amount'], ['stock' => $user->getMoneyBalanceStock(-$td['amount'])] );
          break;

        case 'cake':
          \Yii::$app->cakeBalanceManager->decrease( ['userId' => $td['user_id']], $td['amount'], ['stock' => $user->getMoneyBalanceStock(-$td['amount'])] );
          break;

        case 'points':
          \Yii::$app->pointBalanceManager->decrease( ['userId' => $td['user_id']], $td['amount'], ['stock' => $user->getMoneyBalanceStock(-$td['amount'])] );
          break;
      }
    }

  }
}