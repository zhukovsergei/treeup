<?php

namespace aiur\services;

use aiur\forms\tasks\TaskCreateForm;
use aiur\forms\tasks\TaskEditForm;
use aiur\helpers\TaskFileUploaderHelper;
use aiur\repositories\TaskRepository;
use common\models\Task;
use common\models\TaskFile;
use frontend\widgets\MyTaskListCustomer;

class TaskService
{
    private $taskRepository;

    public function __construct(TaskRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    public function createTask($uid, TaskCreateForm $form)
    {
        $row = Task::create(
            $uid,
            $form->status,
            $form->name,
            $form->text,
            $form->town_id,
            $form->type_id,
            $form->project_id,
            $form->industry_id,
            $form->complexity,
            $form->specialization_id,
            $form->date_end,
            $form->points_win,
            $form->points_participant,
            $form->privilege,
            $form->privilege_description,
            $form->tu_count,
            $form->authored_visible,
            $form->is_paid,
            $form->tu_count,
            $form->is_paid
    );

        if ($form->skills) {
            $row->skills = $form->getNormalizeSkills();
        }

        $this->taskRepository->save($row);

        if ($form->files) {
            TaskFileUploaderHelper::handle($form->files, $row->id, \Yii::$app->getUser()->getId());
        }

        return $row;
    }

    public function updateTask(TaskEditForm $form)
    {
        $row = $this->taskRepository->get($form->id);

        $row->edit(
            $form->status,
            $form->name,
            $form->text,
            $form->town_id,
            $form->type_id,
            $form->project_id,
            $form->industry_id,
            $form->complexity,
            $form->specialization_id,
            $form->date_end,
            $form->points_win,
            $form->points_participant,
            $form->privilege,
            $form->privilege_description,
            $form->authored_visible,
            $form->tu_count
        );

        if ($form->skills) {
            $row->skills = $form->getNormalizeSkills();
        }

        $this->taskRepository->save($row);

        if ($form->files) {
            TaskFileUploaderHelper::handle($form->files, $row->id, \Yii::$app->getUser()->getId());
        }

        if ($form->deleted_files) {
            TaskFile::deleteAll(['id' => $form->deleted_files]);
        }

        return $row;
    }

    public function setModeration($id)
    {
        $row = $this->taskRepository->get($id);
        $row->setStatus('moderation');
        $this->taskRepository->save($row);

        return MyTaskListCustomer::widget(['task' => $row]);
    }

    public function removeTask($id)
    {
        $res = false;
        $row = $this->taskRepository->get($id);
        if ($row->status == Task::STATUS_DB['draft']){
            $this->taskRepository->remove($row);
            $res = true;
        }
        return $res;
    }

}