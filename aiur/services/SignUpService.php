<?php

namespace aiur\services;

use aiur\forms\QuickSignUpForm;
use aiur\forms\SignUpUserForm;
use aiur\forms\UserEditForm;
use aiur\repositories\UserRepository;
use common\models\User;

class SignUpService
{
  private $userRepository;

  public function __construct( UserRepository $userRepository )
  {
    $this->userRepository = $userRepository;
  }

  public function request(QuickSignUpForm $form)
  {
    $user = User::createRequest(
      $form->email,
      $form->password,
      $form->type_id
    );

    $this->userRepository->save($user);
  }

  public function prepareFromSocial(array $data)
  {
    $user = User::createSocial(
      $data['email'],
      $data['first_name'] . ' ' .$data['last_name'],
      $data['identity'],
      $data['photo_big']
    );

    $this->userRepository->save($user);

    return $user;
  }


  public function confirm($token): void
  {
    if (empty($token)) {
      throw new \DomainException('Empty confirm token.');
    }
    $user = $this->userRepository->getByEmailConfirmToken($token);
    $user->confirmSignup();
    $this->userRepository->save($user);

    \Yii::$app->user->login($user);
  }
}