<?php

namespace aiur\services;

use aiur\repositories\IndustryRepository;
use common\models\Industry;
use common\models\Specialization;

class IndustryService
{
  private $repo;

  public function __construct( IndustryRepository $repo )
  {
    $this->repo = $repo;
  }

  public function create(array $fd)
  {
    $m = new Industry();
    $m->attributes = $fd;
    $m->save();

    return $m;
  }

  public function update($id, array $fd)
  {
    $user = $this->repo->get($id);
    $user->attributes = $fd;
    $user->update();
  }
}