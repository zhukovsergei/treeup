<?php

namespace aiur\services;

use aiur\repositories\TaskRepository;
use aiur\repositories\UserRepository;
use common\models\TaskParticipant;

class ParticipantService
{
  private $userRepository;
  private $taskRepository;

  public function __construct( UserRepository $userRepository, TaskRepository $taskRepository )
  {
    $this->userRepository = $userRepository;
    $this->taskRepository = $taskRepository;
  }

  public function assignUserToTask($user_id, $task_id)
  {
    $task = $this->taskRepository->get($task_id);

    if( !TaskParticipant::find()->where(['task_id' => $task->id, 'participant_id' => $user_id])->exists() )
    {
      $m = new TaskParticipant();
      $m->task_id = $task->id;
      $m->participant_id = $user_id;
      $m->loadDefaultValues();
      $m->save();
    }

    return $task->save();
  }

  public function closeTask($task_id)
  {
    $task = $this->taskRepository->get($task_id);

    $task->setStatus('finished');

    $this->taskRepository->save($task);

  }

  public function draftTask($task_id)
  {
    $task = $this->taskRepository->get($task_id);

    $task->setStatus('draft');

    $this->taskRepository->save($task);
  }

  public function moderationTask($task_id)
  {
    $task = $this->taskRepository->get($task_id);

    $task->setStatus('moderation');

    $this->taskRepository->save($task);
  }

  public function becomePerformer($task_id, $participant_id)
  {
    return TaskParticipant::updateAll(['performer' => 1, 'date_update' => date('Y-m-d H:i:s')], ['task_id' => $task_id, 'participant_id' => $participant_id]);
  }

  public function discardPerformer($task_id, $participant_id)
  {
    return TaskParticipant::updateAll(['performer' => 0, 'date_update' => date('Y-m-d H:i:s')], ['task_id' => $task_id, 'participant_id' => $participant_id]);
  }

  public function becomeWinner($task_id, $participant_id)
  {
    TaskParticipant::updateAll(['winner' => 0], ['task_id' => $task_id, 'participant_id' => $participant_id]);
    return TaskParticipant::updateAll(['winner' => 1, 'date_update' => date('Y-m-d H:i:s')], ['task_id' => $task_id, 'participant_id' => $participant_id]);
  }

  public function discardWinner($task_id, $participant_id)
  {
    TaskParticipant::updateAll(['winner' => 0], ['task_id' => $task_id, 'participant_id' => $participant_id]);
    return TaskParticipant::updateAll(['winner' => 0, 'date_update' => date('Y-m-d H:i:s')], ['task_id' => $task_id, 'participant_id' => $participant_id]);
  }

}