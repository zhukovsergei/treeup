<?php

namespace aiur\services;

use aiur\forms\LostPasswordForm;
use aiur\forms\ResetPasswordForm;
use aiur\forms\ResetPasswordSettingsForm;
use aiur\repositories\UserRepository;
use common\models\User;

class LostPasswordService
{
    private $userRepository;
    private $mailer;

    public function __construct( UserRepository $userRepository )
    {
        $this->userRepository = $userRepository;
    }

    public function request(LostPasswordForm $form)
    {
        $user = $this->userRepository->getByEmail($form->email);

        $user->generatePasswordResetToken();

        $this->userRepository->save($user);

        \Yii::$app->mailer->compose('passwordResetToken-html', ['user' => $user])
            ->setFrom([\Yii::$app->settings->get('main.supportNoReplyEmail') => \Yii::$app->settings->get('main.supportName')])
            ->setTo($user->email)
            ->setSubject('Сброс пароля на '. \Yii::$app->request->getHostInfo())
            ->send();
    }

    public function validate($token)
    {
        if (empty($token) || !is_string($token)) {
            throw new \DomainException('Password reset token cannot be blank.');
        }

        $this->userRepository->getByPasswordResetToken($token);
    }


    public function reset($token, ResetPasswordForm $form)
    {
        $user = $this->userRepository->getByPasswordResetToken($token);
        $user->removePasswordResetToken();
        $user->setPassword($form->password);
        $user->date_password_update = date('Y-m-d H:i:s');
        $this->userRepository->save($user);
    }

    public function save($uid, ResetPasswordSettingsForm $form)
    {
      $user = $this->userRepository->get($uid);
      $user->setPassword($form->password);
      $user->date_password_update = date('Y-m-d H:i:s');
      $this->userRepository->save($user);
    }

}