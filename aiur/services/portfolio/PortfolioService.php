<?php

namespace aiur\services\portfolio;

use aiur\helpers\PortfolioFileSaver;
use aiur\helpers\PortfolioUrlSaver;
use aiur\helpers\profile\PortfolioBlockHelper;
use aiur\repositories\portfolio\PortfolioRepository;
use common\models\Portfolio;
use common\models\PortfolioBlock;
use yii\helpers\ArrayHelper;

class PortfolioService
{
  private $portfolioRepository;

  public function __construct( PortfolioRepository $portfolioRepository )
  {
    $this->portfolioRepository = $portfolioRepository;
  }

  public function createPortfolio($uid, \yii\base\Model $form)
  {
    $row = Portfolio::create(
      $uid,
      $form->name,
      $form->text
    );

    if( $blocks = $form->getAllBlocks() )
    {
      $row->blocks = $blocks;
    }

    $this->portfolioRepository->save($row);

    if( $form->files && $form->imageAudioBlocks )
    {
      PortfolioFileSaver::onCreate(
        ArrayHelper::getColumn($row->imageAudioBlocks, 'id'),
        $form->getFilesQuantity(),
        $form->getFiles()
      );
    }

    if( $videoUrls = $form->getVideoUrls() )
    {
      PortfolioUrlSaver::onCreate(
        ArrayHelper::getColumn($row->videoBlocks, 'id'),
        $form->getVideoUrlsQuantity(),
        $videoUrls
      );
    }

    return $row;
  }

  public function updatePortfolio(\yii\base\Model $form)
  {
    $row = $this->portfolioRepository->get($form->id);
    $row->edit(
      $form->name,
      $form->text
    );

    if( $blocks = $form->getAllBlocks() )
    {
      $row->blocks = $blocks;
    }

    $this->portfolioRepository->save($row);

    if( $form->files && $form->imageAudioBlocks )
    {
      PortfolioFileSaver::onUpdate(
        ArrayHelper::getColumn($row->imageAudioBlocks, 'id'),
        $form->getFilesQuantity(),
        $form->getFiles()
      );
    }

    if( $videoUrls = $form->getVideoUrls() )
    {
      PortfolioUrlSaver::onUpdate(
        ArrayHelper::getColumn($row->videoBlocks, 'id'),
        $form->getVideoUrlsQuantity(),
        $videoUrls
      );
    }

    if($form->deleted_blocks)
    {
      PortfolioBlockHelper::delete($form->getDeletedBlocks());
    }

    if($form->deleted_files)
    {
      PortfolioBlockHelper::deleteFiles($form->getDeletedFiles());
    }

    return $row;
  }

  public function removePortfolio($id)
  {
    $row = $this->portfolioRepository->get($id);
    $this->portfolioRepository->remove($row);
  }
}