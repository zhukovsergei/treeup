<?php

namespace aiur\services\profile;

use aiur\repositories\profile\ProjectRepository;
use common\models\Project;
use common\models\Task;

class ProjectService
{
  private $projectRepository;

  public function __construct( ProjectRepository $projectRepository )
  {
    $this->projectRepository = $projectRepository;
  }

  public function createProject($uid, \yii\base\Model $form)
  {
    $row = Project::create(
      $uid,
      $form->name,
      $form->text
    );

    $this->projectRepository->save($row);
    return $row;
  }

  public function updateProject(\yii\base\Model $form)
  {
    $row = $this->projectRepository->get($form->id);

    $row->edit(
      $form->name,
      $form->text
    );

    if($tasks = $form->getTasksIds())
    {
      Task::updateAll(['project_id' => $row->id], ['id' => $tasks]);
    }

    $this->projectRepository->save($row);

    return $row;
  }

  public function removeProject($id)
  {
    $row = $this->projectRepository->get($id);
    $this->projectRepository->remove($row);
  }
}