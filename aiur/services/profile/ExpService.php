<?php

namespace aiur\services\profile;

use aiur\repositories\profile\ExperienceRepository;
use common\models\Experience;

class ExpService
{
  private $experienceRepository;

  public function __construct( ExperienceRepository $experienceRepository )
  {
    $this->experienceRepository = $experienceRepository;
  }

  public function createExperience($uid, \yii\base\Model $form)
  {
    $row = Experience::create(
      $uid,
      $form->current,
      $form->date_from,
      $form->date_to,
      $form->company,
      $form->post,
      $form->duties
    );

    $this->experienceRepository->save($row);

    return $row;
  }

  public function updateExperience(\yii\base\Model $form)
  {
    $row = $this->experienceRepository->get($form->id);
    $row->edit(
      $form->current,
      $form->date_from,
      $form->date_to,
      $form->company,
      $form->post,
      $form->duties
    );

    $this->experienceRepository->save($row);
  }

  public function removeExperience($id)
  {
    $row = $this->experienceRepository->get($id);
    $this->experienceRepository->remove($row);
  }

}