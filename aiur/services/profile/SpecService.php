<?php

namespace aiur\services\profile;

use aiur\repositories\profile\SpecRepository;
use common\models\Spec;

class SpecService
{
  private $specRepository;

  public function __construct( SpecRepository $specRepository )
  {
    $this->specRepository = $specRepository;
  }

  public function createSpec($uid, \yii\base\Model $form)
  {
    $row = Spec::create(
      $uid,
      $form->type_industry,
      $form->exp
    );

    if($form->specialization)
    {
      $row->specialization = $form->getNormalizeSpecialization();
    }

    if($form->industry)
    {
      $row->industry = $form->getNormalizeIndustry();
    }
    
    if($form->skills)
    {
      $row->skills = $form->getNormalizeSkills();
    }

    $this->specRepository->save($row);
    return $row;
  }

  public function updateSpec(\yii\base\Model $form)
  {
    $row = $this->specRepository->get($form->id);
    $row->edit(
      $form->type_industry,
      $form->exp
    );

    if($form->specialization)
    {
      $row->specialization = $form->getNormalizeSpecialization();
    }

    if($form->industry)
    {
      $row->industry = $form->getNormalizeIndustry();
    }

    if($form->skills)
    {
      $row->skills = $form->getNormalizeSkills();
    }

    $this->specRepository->save($row);
    return $row;
  }

  public function removeSpec($id)
  {
    $row = $this->specRepository->get($id);

    $this->specRepository->remove($row);
  }

}