<?php

namespace aiur\services\profile;

use aiur\repositories\profile\EducationRepository;
use common\models\Education;

class CvService
{
  private $educationRepository;

  public function __construct( EducationRepository $educationRepository )
  {
    $this->educationRepository = $educationRepository;
  }

  public function createEducation($uid, \yii\base\Model $form)
  {
    $row = Education::create(
      $uid,
      $form->date_from,
      $form->date_to,
      $form->university,
      $form->town_id,
      $form->spec
    );

    $this->educationRepository->save($row);
  }

  public function updateEducation($id, \yii\base\Model $form)
  {
    $row = $this->educationRepository->get($id);
    $row->edit(
      $form->date_from,
      $form->date_to,
      $form->university,
      $form->town_id,
      $form->spec
    );

    $this->educationRepository->save($row);
  }

}