<?php

namespace aiur\services\profile;

use aiur\repositories\profile\EducationRepository;
use common\models\Education;

class EducationService
{
  private $educationRepository;

  public function __construct( EducationRepository $educationRepository )
  {
    $this->educationRepository = $educationRepository;
  }

  public function createEducation($uid, \yii\base\Model $form)
  {
    $row = Education::create(
      $uid,
      $form->date_from,
      $form->date_to,
      $form->university,
      $form->town_id,
      $form->spec
    );

    $this->educationRepository->save($row);
    return $row;
  }

  public function updateEducation(\yii\base\Model $form)
  {
    $row = $this->educationRepository->get($form->id);
    $row->edit(
      $form->date_from,
      $form->date_to,
      $form->university,
      $form->town_id,
      $form->spec
    );

    $this->educationRepository->save($row);
  }

  public function removeEducation($id)
  {
    $row = $this->educationRepository->get($id);
    $this->educationRepository->remove($row);
  }
}