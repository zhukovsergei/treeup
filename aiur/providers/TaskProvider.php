<?php

namespace aiur\providers;


use common\models\Task;
use common\models\TaskPoint;
use yii\web\NotFoundHttpException;

class TaskProvider
{
    public function getAll()
    {
        $q = Task::find();

        $q->where(['status' => 'published']);

        if (\Yii::$app->getUser()->getIsGuest()) {
            $q->andWhere(['authored_visible' => 0]);
        }

        return $q->orderBy('id DESC')->all();
    }

    public function getTask($id)
    {
        $task = Task::findOne(['id' => $id]);
        if (empty($task)) {
            throw new NotFoundHttpException();
        }
        return $task;
    }

    public function getPoints($complexity)
    {
        return TaskPoint::findOne(['complexity' => $complexity]);
    }
}