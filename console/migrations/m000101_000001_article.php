<?php

use aiur\migrations\AiurMigration;

class m000101_000001_article extends AiurMigration
{
  public function up()
  {
    $this->createTable('{{%article}}', [
      'id' => $this->primaryKey(),
      'date_add' => $this->dateTime(),
      'date_pub' => $this->date(),
      'title' => $this->string(),
      'short_text' => $this->longText(),
      'long_text' => $this->longText(),
      'approve' => $this->boolean(),
      'image' => $this->string(),
    ], $this->tableOptions);

    for($i = 0; $i < 4; $i++)
    {
      $this->insert('{{%article}}', [
        'date_add' => $this->faker->dateTimeThisMonth()->format('Y-m-d H:i:s'),
        'date_pub' => $this->faker->dateTimeThisMonth()->format('Y-m-d'),
        'title' => $this->faker->sentence(rand(4,20)),
        'short_text' => $this->faker->realText(),
        'long_text' => $this->faker->realText(600),
        'approve' => 1,
        'image' => $this->faker->image(\Yii::getAlias('@uploads'), 640, 480, null, null),
      ]);
    }
  }

  public function down()
  {
    $files = $this->getDb()->createCommand('SELECT `image` FROM article')->queryColumn();
    foreach($files as $file)
    {
      @unlink(\Yii::getAlias('@uploads/'.$file));
    }

    $this->dropTable('{{%article}}');
  }
}
