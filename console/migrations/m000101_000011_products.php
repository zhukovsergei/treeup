<?php

use aiur\migrations\AiurMigration;

class m000101_000011_products extends AiurMigration
{

  public function up()
  {
    $this->createTable('{{%products}}', [
      'id' => $this->primaryKey(),
      'date_add' => $this->dateTime(),
      'name' => $this->string(),
      'short_text' => $this->longText(),
      'long_text' => $this->longText(),
      'thumb' => $this->string(),
      'image' => $this->string(),
      'category_id' => $this->integer(),
      'price' => $this->integer(),
      'qt' => $this->integer(),
      'is_new' => $this->boolean(),
      'is_rec' => $this->boolean(),
      'approve' => $this->boolean(),
    ], $this->tableOptions);

    $this->createTable('{{%products_images}}', [
      'id' => $this->primaryKey(),
      'product_id' => $this->integer(),
      'thumb' => $this->string(),
      'image' => $this->string(),
    ], $this->tableOptions);

    $this->createIndex('idx-pi-product_id', '{{%products_images}}', 'product_id');
    $this->addForeignKey(
      'fk_pi_product_id', '{{%products_images}}', 'product_id', '{{%products}}', 'id', 'CASCADE', 'CASCADE'
    );

    for($i = 0; $i < 4; $i++)
    {
      $this->insert('{{%products}}', [
        'date_add' => $this->faker->dateTimeThisMonth()->format('Y-m-d H:i:s'),
        'name' => $this->faker->sentence(rand(3,8)),
        'short_text' => $this->faker->realText(),
        'long_text' => $this->faker->realText(600),
        'thumb' => $this->faker->image(\Yii::getAlias('@uploads'), 100, 100, null, null),
        'image' => $this->faker->image(\Yii::getAlias('@uploads'), 640, 480, null, null),
        'category_id' => 2,
        'price' => rand(10,100)*10,
        'qt' => rand(1,7),
        'is_new' => $this->faker->boolean(70),
        'is_rec' => $this->faker->boolean(20),
        'approve' => 1,
      ]);
    }

  }

  public function down()
  {
    $files = $this->getDb()->createCommand('SELECT `image` FROM products')->queryColumn();
    foreach($files as $file)
    {
      @unlink(\Yii::getAlias('@uploads/'.$file));
    }

    $files = $this->getDb()->createCommand('SELECT `thumb` FROM products')->queryColumn();
    foreach($files as $file)
    {
      @unlink(\Yii::getAlias('@uploads/'.$file));
    }


    $this->dropForeignKey('fk_pi_product_id', '{{%products_images}}');

    $this->dropTable('{{%products}}');
    $this->dropTable('{{%products_images}}');
  }
}
