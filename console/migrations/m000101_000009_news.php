<?php

use aiur\migrations\AiurMigration;

class m000101_000009_news extends AiurMigration
{

  public function up()
  {
    $this->createTable('{{%news}}', [
      'id' => $this->primaryKey(),
      'date_add' => $this->dateTime(),
      'date_pub' => $this->date(),
      'name' => $this->string(),
      'short_text' => $this->longText(),
      'long_text' => $this->longText(),
      'approve' => $this->boolean(),
      'image' => $this->string(),
      'thumb' => $this->string(),
    ], $this->tableOptions);

    for($i = 0; $i < 4; $i++)
    {
      $this->insert('{{%news}}', [
        'date_add' => $this->faker->dateTimeThisMonth()->format('Y-m-d H:i:s'),
        'date_pub' => $this->faker->dateTimeThisMonth()->format('Y-m-d'),
        'name' => $this->faker->sentence(rand(4,20)),
        'short_text' => $this->faker->realText(),
        'long_text' => $this->faker->realText(600),
        'approve' => 1,
        'image' => $this->faker->image(\Yii::getAlias('@uploads'), 640, 480, null, null),
        'thumb' => $this->faker->image(\Yii::getAlias('@uploads'), 150, 150, null, null),
      ]);
    }

//    $this->execute(file_get_contents(\Yii::getAlias('@console/sql/article.sql')));
  }

  public function down()
  {
    $files = $this->getDb()->createCommand('SELECT `image` FROM news')->queryColumn();
    foreach($files as $file)
    {
      @unlink(\Yii::getAlias('@uploads/'.$file));
    }

    $files = $this->getDb()->createCommand('SELECT `thumb` FROM news')->queryColumn();
    foreach($files as $file)
    {
      @unlink(\Yii::getAlias('@uploads/'.$file));
    }

    $this->dropTable('{{%news}}');
  }
}
