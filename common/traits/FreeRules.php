<?php

namespace common\traits;

trait FreeRules
{
  public function rules()
  {
    $rules = [];
    $rules[] = [ $this->attributes(), 'safe' ];
    $rules[] = [ $this->attributes(), 'trim' ];

/*    switch(static::className())
    {
      case 'common\models\Article':
        $rules[] = [ ['title'], 'email' ];
//        $rules[] = [ ['title'], 'integer' ];
        break;
    }*/

    return $rules;
  }
}