<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'supportName' => 'Администрация сайта',
    'passwordResetTokenExpire' => 3600,
];
