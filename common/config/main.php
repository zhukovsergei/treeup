<?php
return [
  'id' => 'app-common',

  'name' => 'Common side application config',

  'timeZone' => 'Asia/Novosibirsk',
  'aliases' => [
    '@bower' => '@vendor/bower-asset',
    '@npm' => '@npm/bower-asset',
  ],
  'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',

  'bootstrap' => [
    'common\bootstrap\SetUp'
  ],

  'components' => [

    'db' => require(__DIR__ . '/db.php'),

    'assetManager' => [
      'class' => 'yii\web\AssetManager',
      'bundles' => [
        'yii\web\JqueryAsset' => [
          'js' => ['jquery.min.js'],
          'jsOptions' => ['position' => \yii\web\View::POS_HEAD]
        ],
        'yii\bootstrap\BootstrapAsset' => [
          'css' => ['css/bootstrap.min.css']
        ],
        'yii\bootstrap\BootstrapPluginAsset' => [
          'js' => ['js/bootstrap.min.js']
        ]
      ],
    ],

    'urlManager' => [
      'showScriptName' => false,
      'enablePrettyUrl' => true,
      /*'rules' => [
        ['class' => 'yii\rest\UrlRule', 'controller' => 'users'],
      ],*/
    ],

    'authManager' => [
      'class' => 'yii\rbac\DbManager',
    ],

    'i18n' => [
      'translations' => [
        '*' => [
          'class' => 'yii\i18n\PhpMessageSource',
          'basePath' => '@app/messages',
//          'sourceLanguage' => 'ru-RU',
          'fileMap' => [
            'app' => 'app.php',
          ],
        ],
      ],
    ],
    
    'formatter' => [
      'dateFormat' => 'dd.MM.yyyy',
      'timeFormat' => 'H:mm',
      'datetimeFormat' => 'd.MM.yyyy H:mm',
      'decimalSeparator' => ',',
      'thousandSeparator' => ' ',
      'currencyCode' => 'RUR',
    ],

    'settings' => [
      'class' => 'pheme\settings\components\Settings',
      'modelClass' => 'common\models\Setting',
      'cache' => null
    ],

    'mailer' => [
      'class' => 'yii\swiftmailer\Mailer',
      'viewPath' => '@common/mail',
    ],

    'cache' => [
      'class' => 'yii\caching\FileCache',
//      'class' => 'yii\caching\MemCache',
//      'useMemcached' => true,
    ],

    'balanceManager' => [
      'class' => 'yii2tech\balance\ManagerDb',
      'accountTable' => '{{%BalanceAccount}}',
      'transactionTable' => '{{%BalanceTransaction}}',
      'accountLinkAttribute' => 'accountId',
      'accountBalanceAttribute' => 'balance',
      'amountAttribute' => 'amount',
      'dataAttribute' => 'data',
    ],

    'pointBalanceManager' => [
      'class' => 'yii2tech\balance\ManagerDb',
      'accountTable' => '{{%BalanceAccountPoint}}',
      'transactionTable' => '{{%BalanceTransactionPoint}}',
      'accountLinkAttribute' => 'accountId',
      'accountBalanceAttribute' => 'balance',
      'amountAttribute' => 'amount',
      'dataAttribute' => 'data',
    ],

    'cakeBalanceManager' => [
      'class' => 'yii2tech\balance\ManagerDb',
      'accountTable' => '{{%BalanceAccountCake}}',
      'transactionTable' => '{{%BalanceTransactionCake}}',
      'accountLinkAttribute' => 'accountId',
      'accountBalanceAttribute' => 'balance',
      'amountAttribute' => 'amount',
      'dataAttribute' => 'data',
    ],
  ],
];

