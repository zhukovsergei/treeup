<?php
return yii\helpers\ArrayHelper::merge(
  require(__DIR__ . '/main.php'),
  [
    'id' => 'app-common-tests',
    'basePath' => dirname(__DIR__),
    'components' => [
      'db' => [
        'dsn' => 'mysql:host=localhost;dbname=yii2admin_test',
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8',
      ],
      'user' => [
        'class' => 'yii\web\User',
        'identityClass' => 'common\models\User',
      ],
    ],
  ]
);
