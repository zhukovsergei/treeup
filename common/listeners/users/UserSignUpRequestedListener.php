<?php

namespace common\listeners\users;

use common\events\users\UserSignUpRequestEvent;
use yii\mail\MailerInterface;

class UserSignUpRequestedListener
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function handle(UserSignUpRequestEvent $event): void
    {
        $sent = $this->mailer
            ->compose('signup/confirm-html', [
              'user' => $event->user,
            ])
            ->setFrom([\Yii::$app->settings->get('main.supportNoReplyEmail') => \Yii::$app->settings->get('main.supportName')])
            ->setTo($event->user->email)
            ->setSubject('Signup confirm')
            ->send();
        if (!$sent) {
            throw new \RuntimeException('Email sending error.');
        }
    }
}