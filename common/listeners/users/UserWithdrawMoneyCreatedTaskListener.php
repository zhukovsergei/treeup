<?php

namespace common\listeners\users;

use aiur\helpers\TaskPricePostHelper;
use common\events\tasks\TaskHaveCreatedEvent;
use common\models\Task;
use yii\mail\MailerInterface;

class UserWithdrawMoneyCreatedTaskListener
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function handle(TaskHaveCreatedEvent $event): void
    {
        if ($event->task->is_paid == 0 && $event->task->status == Task::STATUS_DB['moderation']) {
            $amount = TaskPricePostHelper::calculate($event->task->tu_count);

            \Yii::$app->balanceManager->decrease(
                ['userId' => $event->task->uid],
                $amount,
                ['stock' => $event->task->creator->getMoneyBalanceStock(-$amount)]
            );

            $event->task->updateAttributes(['is_paid' => 1]);
        }
    }
}