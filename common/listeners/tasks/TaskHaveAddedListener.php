<?php

namespace common\listeners\tasks;

use common\events\tasks\TaskHaveCreatedEvent;
use common\models\Notify;
use yii\mail\MailerInterface;

class TaskHaveAddedListener
{
/*    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }*/

    public function handle( TaskHaveCreatedEvent $event ): void
    {
      Notify::create(
        'Создана задача',
        $event->task->id,
        $event->task->creator->id
      );
    }
}