<?php

namespace common\listeners\tasks;

use common\events\tasks\TaskStatusHaveChangedEvent;
use common\models\Notify;
use common\models\TaskParticipant;
use yii\mail\MailerInterface;

class TaskStatusHaveChangedListener
{
/*    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }*/

    public function handle( TaskStatusHaveChangedEvent $event ): void
    {
      Notify::create(
        'Изменение статуса задачи',
        $event->task->id,
        $event->task->creator->id
      );

      if($event->task->status === 'finished')
      {
        $this->calculateAwards($event);
      }
    }

    public function calculateAwards($event)
    {
      $participantsForAward = TaskParticipant::find()->where( ['task_id' => $event->task->id, 'performer' => 1] )->all();

      $winner = TaskParticipant::find()->where(['task_id' => $event->task->id, 'winner' => 1])->one();

      if( $event->task->isTeam() )
      {
        foreach( $participantsForAward as $participant )
        {
          \Yii::$app->pointBalanceManager->increase( ['userId' => $participant->participant_id], $event->task->points_win );
        }

        if( $winner )
        {
          \Yii::$app->pointBalanceManager->increase( ['userId' => $winner->participant_id], $event->task->points_win );
        }
      }
      else
      {
        foreach( $participantsForAward as $participant )
        {
          \Yii::$app->pointBalanceManager->increase( ['userId' => $participant->participant_id], $event->task->points_participant );
        }

        if( $winner )
        {
          \Yii::$app->pointBalanceManager->increase( ['userId' => $winner->participant_id], $event->task->points_win );
          \Yii::$app->cakeBalanceManager->increase( ['userId' => $winner->participant_id], $event->task->tu_count );
        }
      }

    }
}