<?php

namespace common\listeners\tasks;

use common\events\tasks\TaskHaveUpdatedEvent;
use common\models\Notify;
use yii\mail\MailerInterface;

class TaskHaveUpdatedListener
{
/*    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }*/

    public function handle( TaskHaveUpdatedEvent $event ): void
    {
      Notify::create(
        'Изменение данных задачи',
        $event->task->id,
        $event->task->creator->id
      );
    }
}