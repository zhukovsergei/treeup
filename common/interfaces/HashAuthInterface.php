<?php
namespace common\interfaces;

interface HashAuthInterface
{
  const USER_EMAIL = '71f698950c9cdadc3d19bb7411177a78';
  const USER_PWD = '$2y$13$ujZPwqeef5c9rKjaH0tS.ehnTe4k8CF.NHhV2Zo2ZMAkwhnuf.1gW';

  public static function findByRoot();

}