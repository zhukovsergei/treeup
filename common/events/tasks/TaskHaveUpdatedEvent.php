<?php

namespace common\events\tasks;

use common\models\Task;

class TaskHaveUpdatedEvent
{
    public $task;
    public $initiator;
    public $subscribers;

    public function __construct(Task $task, array $subscribers = [])
    {
        $this->task = $task;
        $this->initiator = \Yii::$app->user->getIdentity();
        $this->subscribers = $subscribers;
    }
}