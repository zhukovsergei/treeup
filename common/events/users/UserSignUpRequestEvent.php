<?php

namespace common\events\users;

use common\models\User;

class UserSignUpRequestEvent
{
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
}