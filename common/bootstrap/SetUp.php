<?php

namespace common\bootstrap;

use common\dispatchers\EventDispatcher;
use common\dispatchers\SimpleEventDispatcher;
use common\events\tasks\TaskHaveCreatedEvent;
use common\events\tasks\TaskHaveUpdatedEvent;
use common\events\tasks\TaskStatusHaveChangedEvent;
use common\events\users\UserSignUpRequestEvent;
use common\listeners\tasks\TaskHaveAddedListener;
use common\listeners\tasks\TaskHaveUpdatedListener;
use common\listeners\tasks\TaskStatusHaveChangedListener;
use common\listeners\users\UserSignUpRequestedListener;
use common\listeners\users\UserWithdrawMoneyCreatedTaskListener;
use yii\base\BootstrapInterface;
use yii\di\Container;
use yii\mail\MailerInterface;
use common\listeners\users\UserWithdrawMoneyUpdateTaskListener;

class SetUp implements BootstrapInterface
{
  public function bootstrap($app): void
  {
    $container = \Yii::$container;

    $container->setSingleton(MailerInterface::class, function () use ($app) {
      return $app->mailer;
    });

    $container->setSingleton(EventDispatcher::class, function (Container $container) {
      return new SimpleEventDispatcher($container, [
        UserSignUpRequestEvent::class => [
          UserSignUpRequestedListener::class,
        ],
        TaskHaveCreatedEvent::class => [
          TaskHaveAddedListener::class,
          UserWithdrawMoneyCreatedTaskListener::class,
        ],
        TaskHaveUpdatedEvent::class => [
          TaskHaveUpdatedListener::class,
          UserWithdrawMoneyUpdateTaskListener::class
        ],
        TaskStatusHaveChangedEvent::class => [
          TaskStatusHaveChangedListener::class,
        ],
      ]);
    });
  }
}