<?php
namespace common\components;

use JBZoo\Utils\Str;
use Yii;

class Adobe
{
  public static function genName($fname, $dot = '.')
  {
    return substr(md5(microtime()), 24). '-' . Str::slug($fname) . $dot;
  }

  public static function ruDate($date, $static = FALSE)
  {
    $set = [
      "December" => "Декабрь", "Monday" => "Понедельник", "Tuesday" => "Вторник", "Wednesday" => "Среда",
      "Thursday" => "Четверг", "Friday" => "Пятница", "Saturday" => "Суббота", "Sunday" => "Воскресенье",

      "Mon" => "Пн.", "Tue" => "Вт.", "Wed" => "Ср.", "Thu" => "Чт.", "Fri" => "Пт.",
      "Sat" => "Сб.", "Sun" => "Вс.",

      "Jan" => "Янв", "Feb" => "Фев", "Mar" => "Мар", "Apr" => "Апр", "Jun" => "Июн", "Jul" => "Июл",
      "Aug" => "Авг", "Sep" => "Сен", "Oct" => "Окт", "Nov" => "Ноя", "Dec" => "Дек",
    ];

    if($static)
      $trans = [
        "January" => "Январь", "February" => "Февраль", "March" => "Март",
        "April" => "Апрель", "May" => "Мая", "June" => "Июнь", "July" => "Июль",
        "August" => "Август", "September" => "Сентябрь", "October" => "Октябрь", "November" => "Ноябрь",
      ];
    else
      $trans = [
        "January" => "Января", "February" => "Февраля", "March" => "Марта", "April" => "Апреля",
        "May" => "Май", "June" => "Июня", "July" => "Июля", "August" => "Августа",
        "September" => "Сентября", "October" => "Октября", "November" => "Ноября", "December" => "Декабря",
      ];

    return strtr( $date, array_merge($set, $trans) );
  }

  public static function cuteDate($date)
  {
    $today = date('d.m.Y', time());
    $yesterday = date('d.m.Y', time() - 86400);
    $dbDate = date('d.m.Y', strtotime($date));
    $dbTime = date('H:i', strtotime($date));
    switch ($dbDate)
    {
      case $today : $output = 'Сегодня в '. $dbTime; break;
      case $yesterday : $output = 'Вчера в '. $dbTime; break;
      default : $output = $dbDate;
    }
    return $output;
  }
}