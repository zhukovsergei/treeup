<?php

namespace common\models;

use backend\components\NotifyBehavior;
use common\behaviors\ImageFileUploader\ImageUploadBehavior;
use common\behaviors\ImageFileUploader\SimpleImage;
use common\interfaces\HashAuthInterface;
use common\traits\AssociateLabels;
use common\traits\FindByRootTrait;
use common\traits\FreeRules;
use yii\base\NotSupportedException;
use common\components\DateUpdater;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class Company extends ActiveRecord
{
    use FreeRules;
    use AssociateLabels;

    const JUR_TYPE = [
        'ooo' => 'ООО',
        'ip' => 'ИП',
        'to' => 'ТО',
        'personal' => 'Физическое лицо',
    ];

    const JUR_TYPE_DB = [
        'ooo' => 'ooo',
        'ip' => 'ip',
        'to' => 'to',
        'personal' => 'personal',
    ];

    const BUSINESS = [
        'idea' => 'Идея',
        'startup' => 'Стартап',
        'micro' => 'Микробизнес',
        'small' => 'Малый бизнес',
        'middle' => 'Средний бизнес',
    ];

    const BUSINESS_DB = [
        'idea' => 'idea',
        'startup' => 'startup',
        'micro' => 'micro',
        'small' => 'small',
        'middle' => 'middle',
    ];

    public function behaviors()
    {
        return [
            [
                'class' => ImageUploadBehavior::class,
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['compressor' => function (SimpleImage $si) {
                        return $si->thumbnail(300, 300);
                    }],
                ],
            ],
        ];
    }

    public function getIndustry()
    {
        return $this->hasOne(Industry::class, ['id' => 'industry_id']);
    }

    public static function create($uid, $name, $jur_type, $business, $industry_id, $text): self
    {
        $m = new self();
        $m->uid = $uid;
        $m->name = $name;
        $m->jur_type = $jur_type;
        $m->business = $business;
        $m->industry_id = $industry_id;
        $m->text = $text;
        return $m;
    }

    public function edit($name, $jur_type, $business, $industry_id, $text)
    {
        $this->name = $name;
        $this->jur_type = $jur_type;
        $this->business = $business;
        $this->industry_id = $industry_id;
        $this->text = $text;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

}
