<?php

namespace common\models;

use common\behaviors\ImageFileUploader\ImageUploadBehavior;
use common\behaviors\ImageFileUploader\SimpleImage;
use common\traits\AssociateLabels;
use common\traits\FreeRules;
use yii\db\ActiveRecord;

class PortfolioBlockFileImage extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;

  public function behaviors()
  {
    return [
      [
        'class' => ImageUploadBehavior::class,
        'attribute' => 'filename',
        'thumbs' => [
          'thumb' => ['compressor' => function(SimpleImage $si){
            return $si->thumbnail(300,300);
          }],
        ],
      ],
    ];
  }

}