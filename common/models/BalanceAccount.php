<?php

namespace common\models;

use common\traits\AssociateLabels;
use common\traits\FreeRules;
use yii\db\ActiveRecord;

class BalanceAccount extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;

  public static function tableName()
  {
    return 'BalanceAccount';
  }

  public function getHistory()
  {
    return $this->hasMany(BalanceTransaction::class, ['accountId' => 'id']);
  }

  public function getStockSum() :int
  {
    return $this->getHistory()->sum('amount');
  }
}