<?php

namespace common\models;

use common\traits\FreeRules;
use yii\data\ActiveDataProvider;

class TaskSearch extends Task
{
  use FreeRules;

  public static function tableName()
  {
    return 'task';
  }

  public function search($params)
  {
    $q = Task::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $q,
    ]);

    $q->joinWith(['creator']);

    if (!$this->load($params)) {
      return $dataProvider;
    }

    $q->andFilterWhere([self::tableName().'.'.'id' => $this->id]);
    $except = ['id'];
    foreach(self::getTableSchema()->columns as $column)
    {
      if(in_array($column->name, $except)) continue;
      $q->orFilterWhere(['like', self::tableName().'.'.$column->name, $this->{$column->name}]);
    }
    $q->orFilterWhere(['like', 'users.username', $this->uid]);
    $q->orFilterWhere(['like', 'users.email', $this->uid]);
//    $q->orFilterWhere(['like', 'profile.email', $this->uid]);

    return $dataProvider;
  }}