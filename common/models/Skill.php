<?php

namespace common\models;

use common\traits\AssociateLabels;
use common\traits\FreeRules;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveRecord;

class Skill extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;

  public function behaviors()
  {
    return [
      'saveRelations' => [
        'class'     => SaveRelationsBehavior::class,
        'relations' => [
          'users',
        ],
      ],
    ];
  }

  public function transactions()
  {
    return [
      self::SCENARIO_DEFAULT => self::OP_ALL,
    ];
  }

  public function getUsers()
  {
    return $this->hasMany(User::class, ['id' => 'uid'])->viaTable('users_skills', ['skill_id' => 'id']);
  }

  public function getTasks()
  {
    return $this->hasMany(Task::class, ['id' => 'task_id'])->viaTable('task_skills', ['skill_id' => 'id']);
  }
}