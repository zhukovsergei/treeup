<?php

namespace common\models;

use common\behaviors\ImageFileUploader\ImageUploadBehavior;
use common\behaviors\ImageFileUploader\SimpleImage;
use common\traits\AssociateLabels;
use common\traits\FreeRules;
use yii\db\ActiveRecord;

class Profile extends ActiveRecord
{
    use FreeRules;
    use AssociateLabels;

    const JUR_STATUS = [
        'personal' => 'Физическое лицо',
        'jur' => 'Юридическое лицо',
    ];

    const JUR_STATUS_DB = [
        'personal' => 'personal',
        'jur' => 'jur',
    ];

//  protected $image;

    public function behaviors()
    {
        return [
            [
                'class' => ImageUploadBehavior::class,
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['compressor' => function (SimpleImage $si) {
                        return $si->thumbnail(300, 300);
                    }],
                ],
            ],
        ];
    }

    public static function create($uid, $name, $surname, $patronymic, $birthday, $email, $town_id, $phone, $jur_status): self
    {
        $m = new self();
        $m->uid = $uid;
        $m->name = $name;
        $m->surname = $surname;
        $m->patronymic = $patronymic;
        $m->birthday = $birthday;
        $m->email = $email;
        $m->town_id = $town_id;
        $m->phone = $phone;
        $m->jur_status = $jur_status;
        return $m;
    }

    public function edit($name, $surname, $patronymic, $birthday, $email, $town_id, $phone, $jur_status)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->patronymic = $patronymic;
        $this->birthday = $birthday;
        $this->email = $email;
        $this->town_id = $town_id;
        $this->phone = $phone;
        $this->jur_status = $jur_status;
    }

    public function setSocial($skype, $telegram, $icq, $vk, $fb, $google)
    {
        $this->skype = $skype;
        $this->telegram = $telegram;
        $this->icq = $icq;
        $this->vk = $vk;
        $this->fb = $fb;
        $this->google = $google;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getTown()
    {
        return $this->hasOne(Town::class, ['id' => 'town_id']);
    }

}
