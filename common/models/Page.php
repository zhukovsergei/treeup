<?php

namespace common\models;

use backend\components\NotifyBehavior;
use common\traits\AssociateLabels;
use common\traits\FreeRules;
use yii\db\ActiveRecord;
use common\components\DateUpdater;

class Page extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;

  public function behaviors()
  {
    return [
/*      [
        'class' => DateUpdater::class,
      ],*/
      [
        'class' => NotifyBehavior::class,
      ]
    ];
  }
}