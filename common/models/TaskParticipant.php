<?php

namespace common\models;

use common\traits\AssociateLabels;
use common\traits\FreeRules;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveRecord;
use common\components\DateUpdater;

class TaskParticipant extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;

  public function behaviors()
  {
    return [
      [
        'class' => DateUpdater::class,
        'updatedAtAttribute' => 'date_update'
      ],
    ];
  }

  public function getUser()
  {
    return $this->hasOne(User::class, ['id' => 'participant_id']);
  }

  public function getTask()
  {
    return $this->hasOne(Task::class, ['id' => 'task_id']);
  }
}