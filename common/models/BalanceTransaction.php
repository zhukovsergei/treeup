<?php

namespace common\models;

use common\traits\AssociateLabels;
use common\traits\FreeRules;
use yii\db\ActiveRecord;

class BalanceTransaction extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;

  public static function tableName()
  {
    return 'BalanceTransaction';
  }

  public function getStack() :int
  {
    if(is_string($this->data))
    {
      return json_decode($this->data)->stock ?? 0;
    }
    return 0;
  }

}