<?php

namespace common\models;

use backend\components\NotifyBehavior;
use common\traits\AssociateLabels;
use common\traits\FreeRules;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveRecord;
use common\components\DateUpdater;

class Spec extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;

  const EXPERIENCE = [
    'no-exp' => 'Без опыта',
    'newbie' => 'Новичок',
    'middle' => 'Средний',
    'pro' => 'Профи',
  ];

  const LABEL_TYPE = [
    'praktisch' => 'Практикуемая',
    'interesse' => 'Интересующая',
  ];

  public function behaviors()
  {
    return [
      'saveRelations' => [
        'class'     => SaveRelationsBehavior::className(),
        'relations' => [
          'skills',
          'specialization',
          'industry',
        ],
      ],
    ];
  }

  public function fields()
  {
    $parent = parent::fields();

    $parent['indstr'] = function ($model) {
      return $model->industry;
    };

    $parent['spec'] = function ($model) {
      return $model->specialization;
    };

    $parent['arr_skills'] = function ($model) {
      return $model->skills;
    };
    return $parent;
  }

  public function transactions()
  {
    return [
      self::SCENARIO_DEFAULT => self::OP_ALL,
    ];
  }

  public function getIndustry()
  {
    return $this->hasOne(Industry::class, ['id' => 'industry_id']);
  }

  public function getSpecialization()
  {
    return $this->hasOne(Specialization::class, ['id' => 'specialization_id']);
  }

  public function getSkills()
  {
    return $this->hasMany(Skill::class, ['id' => 'skill_id'])->viaTable('spec_skills', ['spec_id' => 'id']);
  }

  public static function create($uid, $type_industry, $exp)
  {
    $m = new static();
    $m->uid = $uid;
    $m->type_industry = $type_industry;
    $m->exp = $exp;

    return $m;
  }

  public function edit($type_industry, $exp)
  {
    $this->type_industry = $type_industry;
    $this->exp = $exp;
  }
}