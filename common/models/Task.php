<?php

namespace common\models;

use common\events\tasks\TaskHaveCreatedEvent;
use common\events\tasks\TaskHaveUpdatedEvent;
use common\events\tasks\TaskStatusHaveChangedEvent;
use common\traits\AssociateLabels;
use common\traits\EventTrait;
use common\traits\FreeRules;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveRecord;
use common\components\DateUpdater;

class Task extends ActiveRecord
{
    use FreeRules;
    use AssociateLabels;
    use EventTrait;

    const STATUS = [
        'draft' => 'Черновик',
        'moderation' => 'Модерация',
        'reject' => 'Отклонена',
        'published' => 'Опубликовано',
        'finished' => 'Закрыта',
    ];

    const STATUS_DB = [
        'draft' => 'draft',
        'moderation' => 'moderation',
        'reject' => 'reject',
        'published' => 'published',
        'finished' => 'finished',
    ];

    const TYPE = [
        'quest' => 'Задание',
        'event' => 'Мероприятие',
        'team' => 'Тимбилдинг',
    ];
    const TYPE_DB = [
        'quest' => 'quest',
        'event' => 'event',
        'team' => 'team',
    ];

    const COMPLEXITY = [
        1 => 'Без опыта',
        2 => 'Новичок',
        3 => 'Средний',
        4 => 'Профи',
    ];

    const PRIVILEGE = [
        1 => 'Нет',
        2 => 'Приз',
        3 => 'TU',
    ];

    const PRIVILEGE_DB = [
        'Нет' => 1,
        'Приз' => 2,
        'TU' => 3,
    ];

    const TU_COUNT = [1, 2, 3, 4, 5];

    public function behaviors()
    {
        return [
            [
                'class' => DateUpdater::class,
                'updatedAtAttribute' => 'date_upd',
            ],
            'saveRelations' => [
                'class' => SaveRelationsBehavior::className(),
                'relations' => [
                    'skills',
                    /*          'participants' => [
                                'extraColumns' => function ($model) {
                                  return [
                                    'date_add' => date('Y-m-d H:i:s')
                                  ];
                                }
                              ],*/
                ],
            ],
        ];
    }

    public function fields()
    {
        $parent = parent::fields();

        $parent['d_files'] = function ($model) {
            return $model->files;
        };

        $parent['d_notifies'] = function ($model) {
            return $model->notifies;
        };

        $parent['d_industry'] = function ($model) {
            return $model->industry;
        };

        $parent['d_skills'] = function ($model) {
            return $model->skills;
        };

        return $parent;
    }


    /**
     * ========== RELATIONS =============
     */
    public function getCreator()
    {
        return $this->hasOne(User::class, ['id' => 'uid']);
    }

    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    public function getSpecialization()
    {
        return $this->hasOne(Specialization::class, ['id' => 'specialization_id']);
    }

    public function getTown()
    {
        return $this->hasOne(Town::class, ['id' => 'town_id']);
    }

    public function getNotifies()
    {
        return $this->hasMany(Notify::class, ['task_id' => 'id']);
    }

    public function getIndustry()
    {
        return $this->hasOne(Industry::class, ['id' => 'industry_id']);
    }

    public function getFiles()
    {
        return $this->hasMany(TaskFile::class, ['task_id' => 'id']);
    }

    public function getFilesSize()
    {
        $res = 0;
        $files = TaskFile::find()->select('size')->where(['task_id' => $this->id])->column();
        if (!empty($files)) {
            foreach ($files as $file) {
                $res += round($file / (1024 * 1024), 3);
            }
        }
        return $res;
    }

    public function getSkills()
    {
        return $this->hasMany(Skill::class, ['id' => 'skill_id'])
            ->viaTable('task_skills', ['task_id' => 'id']);
    }

    public function getParticipants()
    {
        return $this->hasMany(User::class, ['id' => 'participant_id'])
            ->viaTable('task_participant', ['task_id' => 'id']);
    }

    /**
     * ---
     */
    public static function create($uid, $status, $name, $text, $town_id, $type_id, $project_id, $industry_id, $complexity, $specialization_id, $date_end, $points_win, $points_participant, $privilege, $privilege_description, $tu_count, $authored_visible, $is_paid)
    {
        $m = new static();
        $m->uid = $uid;
        $m->status = $status;
        $m->name = $name;
        $m->text = $text;
        $m->town_id = $town_id;
        $m->type_id = $type_id;
        $m->project_id = $project_id;
        $m->industry_id = $industry_id;
        $m->complexity = $complexity;
        $m->specialization_id = $specialization_id;
        $m->date_end = $date_end;
        $m->points_win = $points_win;
        $m->points_participant = $points_participant;
        $m->privilege = $privilege;
        $m->privilege_description = $privilege_description;
        $m->tu_count = $tu_count;
        $m->authored_visible = $authored_visible;
        $m->is_paid = $is_paid;
        $m->recordEvent(new TaskHaveCreatedEvent($m, []));

        return $m;
    }

    public function edit($status, $name, $text, $town_id, $type_id, $project_id, $industry_id, $complexity, $specialization_id, $date_end, $points_win, $points_participant, $privilege, $privilege_description, $authored_visible, $tu_count)
    {
        $this->status = $status;
        $this->name = $name;
        $this->text = $text;
        $this->town_id = $town_id;
        $this->type_id = $type_id;
        $this->project_id = $project_id;
        $this->industry_id = $industry_id;
        $this->complexity = $complexity;
        $this->specialization_id = $specialization_id;
        $this->date_end = $date_end;
        $this->points_win = $points_win;
        $this->points_participant = $points_participant;
        $this->privilege = $privilege;
        $this->privilege_description = $privilege_description;
        $this->tu_count = $tu_count;
        $this->authored_visible = $authored_visible;

        $this->recordEvent(new TaskHaveUpdatedEvent($this, []));
    }

    public function setStatus(string $status)
    {
        $this->status = $status;

        $this->recordEvent(new TaskStatusHaveChangedEvent($this, []));
    }

    public function isTeam()
    {
        return $this->type_id === 'team';
    }

    public function isParticipant()
    {
        return !empty(TaskParticipant::findOne(['task_id' => $this->id, 'participant_id' => \Yii::$app->getUser()->getId()]));

    }
}