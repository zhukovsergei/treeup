<?php

namespace common\models;

use common\traits\AssociateLabels;
use common\traits\FreeRules;
use yii\db\ActiveRecord;

class BalanceAccountCake extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;

  public static function tableName()
  {
    return 'BalanceAccountCake';
  }

  public function getHistory()
  {
    return $this->hasMany(BalanceTransactionCake::class, ['accountId' => 'id']);
  }

}