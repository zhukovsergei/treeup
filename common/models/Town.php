<?php

namespace common\models;

use backend\components\NotifyBehavior;
use common\behaviors\ImageUploader;
use common\traits\AssociateLabels;
use common\traits\FreeRules;
use yii\db\ActiveRecord;
use common\components\DateUpdater;

class Town extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;

  public function behaviors()
  {
    return [
      [
        'class' => NotifyBehavior::class,
      ]
    ];
  }

  public function getRegion()
  {
    return $this->hasOne(Region::class, ['id' => 'region_id']);
  }

}