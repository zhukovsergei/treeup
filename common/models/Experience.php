<?php

namespace common\models;

use backend\components\NotifyBehavior;
use common\behaviors\ImageUploader;
use common\traits\AssociateLabels;
use common\traits\FreeRules;
use common\traits\ImagePathGenerator;
use yii\db\ActiveRecord;
use common\components\DateUpdater;

class Experience extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;
  use ImagePathGenerator;

  public function behaviors()
  {
    return [
      [
        'class' => NotifyBehavior::class,
      ]
    ];
  }

  public static function create($uid, $current, $date_from, $date_to, $company, $post, $duties)
  {
    $m = new static();
    $m->uid = $uid;
    $m->current = $current;
    $m->date_from = $date_from;
    $m->date_to = $date_to;
    $m->company = $company;
    $m->post = $post;
    $m->duties = $duties;

    return $m;
  }

  public function edit($current, $date_from, $date_to, $company, $post, $duties)
  {
    $this->current = $current;
    $this->date_from = $date_from;
    $this->date_to = $date_to;
    $this->company = $company;
    $this->post = $post;
    $this->duties = $duties;
  }
}