<?php

namespace common\models;

use backend\components\NotifyBehavior;
use common\traits\AssociateLabels;
use common\traits\FreeRules;
use yii\db\ActiveRecord;
use common\components\DateUpdater;
use yii\helpers\Html;

class Notify extends ActiveRecord
{
    use FreeRules;
    use AssociateLabels;

    public $date_add_formatted;

    public function behaviors()
    {
        return [
            [
                'class' => DateUpdater::className(),
            ],
        ];
    }

    public static function create($name, $task_id, $uid, $type = null, $important = 1)
    {
        $m = new static();
        $m->name = $name;
        $m->task_id = $task_id;
        $m->uid = $uid;
        $m->type = $type;
        $m->important = $important;
        $m->save();

        return $m;
    }

    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'uid']);
    }

/*    public function getName()
    {
        $this->name = str_replace(
            '<category-name>',
            Html::a($this->product->category->name, ['category/view', 'id' => $this->product->category->id]),
            $this->name
        );

        $this->name = str_replace(
            '<pool-name>',
            Html::a($this->product->name, ['pooling/view', 'id' => $this->product->id]),
            $this->name
        );

        return $this->name;
    }*/
}