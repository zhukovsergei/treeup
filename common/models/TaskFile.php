<?php

namespace common\models;

use backend\components\NotifyBehavior;
use common\behaviors\ImageFileUploader\FileUploadBehavior;
use common\traits\AssociateLabels;
use common\traits\FreeRules;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveRecord;
use common\components\DateUpdater;

class TaskFile extends ActiveRecord
{
  use FreeRules;

  public $date_add_formatted;

  public function behaviors()
  {
    return [
      [
        'class' => DateUpdater::class,
      ],
      [
        'class' => FileUploadBehavior::class,
        'attribute' => 'name',
      ],
    ];
  }

  public function getFileSizeMb()
  {
      return round($this->size/(1024*1024),3);
  }

  public function getTask()
  {
      return $this->hasOne(Task::class,['id' => 'task_id']);
  }

}