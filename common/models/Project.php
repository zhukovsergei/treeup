<?php

namespace common\models;

use common\traits\AssociateLabels;
use common\traits\FreeRules;
use yii\db\ActiveRecord;

class Project extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;

  public static function create( $uid, $name, $text)
  {
    $m = new static();
    $m->uid = $uid;
    $m->name = $name;
    $m->text = $text;

    return $m;
  }

  public function edit($name, $text)
  {
    $this->name = $name;
    $this->text = $text;
  }

  public function getCountAssignedCount()
  {
    return Task::find()->where(['project_id' => $this->id])->count();
  }
}