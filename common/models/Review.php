<?php

namespace common\models;

use common\behaviors\ImageUploader;
use common\traits\AssociateLabels;
use common\traits\FreeRules;
use yii\db\ActiveRecord;
use common\components\DateUpdater;

class Review extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;
  
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => DateUpdater::class,
//        'updatedAtAttribute' => 'date_upd',
      ],
      [
        'class' => ImageUploader::class,
        'attribute' => 'image',
      ],
    ];
  }

}