<?php

namespace common\models;

use backend\components\NotifyBehavior;
use common\behaviors\ImageUploader;
use common\traits\AssociateLabels;
use common\traits\FreeRules;
use common\traits\ImagePathGenerator;
use yii\db\ActiveRecord;
use common\components\DateUpdater;

class Article extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;
  use ImagePathGenerator;

  public function behaviors()
  {
    return [
      [
        'class' => DateUpdater::class,
      ],
      [
        'class' => ImageUploader::class,
        'attribute' => 'image',
      ],
      [
        'class' => NotifyBehavior::class,
      ]
    ];
  }
}