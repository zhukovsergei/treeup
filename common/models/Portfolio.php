<?php

namespace common\models;

use common\traits\AssociateLabels;
use common\traits\FreeRules;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveRecord;

class Portfolio extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;

  public function behaviors()
  {
    return [
      'saveRelations' => [
        'class'     => SaveRelationsBehavior::className(),
        'relations' => [
          'blocks',
        ],
      ],
    ];
  }
  public function fields()
  {
    $parent = parent::fields();

    $parent['d_blocks'] = function ($model) {
      return $model->blocks;
    };

    return $parent;
  }


  public static function create($uid, $name, $text ) :self
  {
    $m = new self();
    $m->uid = $uid;
    $m->name = $name;
    $m->text = $text;
    return $m;
  }

  public function edit($name, $text )
  {
    $this->name = $name;
    $this->text = $text;
  }

  /*
 * ========== RELATIONS =============
 */
  public function getCreator()
  {
    return $this->hasOne(User::class, ['id' => 'uid']);
  }

  public function getBlocks()
  {
    return $this->hasMany(PortfolioBlock::class, ['portfolio_id' => 'id']);
  }

  public function getImageAudioBlocks()
  {
    return $this->hasMany(PortfolioBlock::class, ['portfolio_id' => 'id'])
      ->where(['OR', ['type' => 'image'], ['type' => 'audio']]);
  }

  public function getVideoBlocks()
  {
    return $this->hasMany(PortfolioBlock::class, ['portfolio_id' => 'id'])
      ->where(['type' => 'video']);
  }
}