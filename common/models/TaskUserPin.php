<?php

namespace common\models;

use common\traits\AssociateLabels;
use common\traits\FreeRules;
use yii\db\ActiveRecord;

class TaskUserPin extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;

}