<?php

namespace common\models;

use backend\components\NotifyBehavior;
use common\events\tasks\TaskHaveCreatedEvent;
use common\events\tasks\TaskHaveUpdatedEvent;
use common\events\tasks\TaskStatusHaveChangedEvent;
use common\traits\AssociateLabels;
use common\traits\EventTrait;
use common\traits\FreeRules;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveRecord;
use common\components\DateUpdater;

class SettingsNotify extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;

  /*
  * ========== RELATIONS =============
  */
  public function getUser()
  {
    return $this->hasOne(User::class, ['id' => 'uid']);
  }

}