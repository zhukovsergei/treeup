<?php

namespace common\models;

use common\traits\AssociateLabels;
use common\traits\FreeRules;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveRecord;
use common\components\DateUpdater;

class PortfolioBlock extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;

  public function behaviors()
  {
    return [
      'saveRelations' => [
        'class'     => SaveRelationsBehavior::className(),
        'relations' => [
          'videos',
        ],
      ],
    ];
  }

  public function fields()
  {
    $parent = parent::fields();

    $parent['d_images'] = function ($model) {
      return $model->images;
    };

    $parent['d_videos'] = function ($model) {
      return $model->videos;
    };

    $parent['d_audios'] = function ($model) {
      return $model->audios;
    };

    return $parent;
  }

  public function getImages()
  {
    return $this->hasMany(PortfolioBlockFileImage::class, ['block_id' => 'id']);
  }

  public function getAudios()
  {
    return $this->hasMany(PortfolioBlockFileAudio::class, ['block_id' => 'id']);
  }

  public function getVideos()
  {
    return $this->hasMany(PortfolioBlockFileVideo::class, ['block_id' => 'id']);
  }

}