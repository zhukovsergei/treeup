<?php

namespace common\models;

use backend\components\NotifyBehavior;
use common\behaviors\ImageUploader;
use common\traits\AssociateLabels;
use common\traits\FreeRules;
use common\traits\ImagePathGenerator;
use yii\db\ActiveRecord;
use common\components\DateUpdater;

class Education extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;
  use ImagePathGenerator;

  public function behaviors()
  {
    return [
      [
        'class' => NotifyBehavior::class,
      ]
    ];
  }

  public static function create($uid, $date_from, $date_to, $university, $town_id, $spec)
  {
    $m = new static();
    $m->uid = $uid;
    $m->date_from = $date_from;
    $m->date_to = $date_to;
    $m->university = $university;
    $m->town_id = $town_id;
    $m->spec = $spec;

    return $m;
  }

  public function edit($date_from, $date_to, $university, $town_id, $spec)
  {
    $this->date_from = $date_from;
    $this->date_to = $date_to;
    $this->university = $university;
    $this->town_id = $town_id;
    $this->spec = $spec;
  }
}