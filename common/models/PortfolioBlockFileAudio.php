<?php

namespace common\models;

use common\behaviors\ImageFileUploader\FileUploadBehavior;
use common\traits\AssociateLabels;
use common\traits\FreeRules;
use yii\db\ActiveRecord;

class PortfolioBlockFileAudio extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;

  public function behaviors()
  {
    return [
      [
        'class' => FileUploadBehavior::class,
        'attribute' => 'filename',
      ],
    ];
  }

}