<?php

namespace common\models;

use common\traits\AssociateLabels;
use common\traits\FreeRules;
use yii\db\ActiveRecord;

class BalanceTransactionCake extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;

  public static function tableName()
  {
    return 'BalanceTransactionCake';
  }
}