<?php

namespace common\models;

use common\traits\AssociateLabels;
use common\traits\FreeRules;
use yii\db\ActiveRecord;

class BalanceAccountPoint extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;

  public static function tableName()
  {
    return 'BalanceAccountPoint';
  }

  public function getHistory()
  {
    return $this->hasMany(BalanceTransactionPoint::class, ['accountId' => 'id']);
  }
}