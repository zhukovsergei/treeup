<?php

namespace common\models;

use common\behaviors\ImageFileUploader\ImageUploadBehavior;
use common\behaviors\ImageFileUploader\SimpleImage;
use common\traits\AssociateLabels;
use common\traits\FreeRules;
use yii\db\ActiveRecord;
use common\components\DateUpdater;

class MainReview extends ActiveRecord
{
  use FreeRules;
  use AssociateLabels;
  
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => DateUpdater::class,
//        'updatedAtAttribute' => 'date_upd',
      ],
      [
        'class' => ImageUploadBehavior::class,
        'attribute' => 'image',
        'thumbs' => [
          'thumb' => ['compressor' => function(SimpleImage $si){
            return $si->thumbnail(300,300);
          }],
        ],
      ],
    ];
  }

}