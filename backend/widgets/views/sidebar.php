<?php
use yii\helpers\Url;
?>
<nav id="mainnav-container">
  <div id="mainnav">

    <!--Menu-->
    <!--================================-->
    <div id="mainnav-shortcut"></div>
    <div id="mainnav-menu-wrap">
      <div class="nano">
        <div class="nano-content">
          <ul id="mainnav-menu" class="list-group">

            <li class="list-header">Основная навигация</li>

            <li <?if(Yii::$app->controller->id == 'site'):?>class="active-link"<?endif;?>>
              <a href="/">
                <i class="fa fa-dashboard"></i>
                <span class="menu-title">Главная страница</span>
              </a>
            </li>

            <li <?if(Yii::$app->controller->id == 'news'):?>class="active-link"<?endif;?>>
              <a href="/news">
                <i class="fa fa-file-text"></i>
                <span class="menu-title">Новости</span>
              </a>
            </li>

            <li <?if(Yii::$app->controller->id == 'reviews'):?>class="active-link"<?endif;?>>
              <a href="/reviews">
                <i class="fa fa-commenting-o"></i>
                <span class="menu-title">
                    <span class="menu-title">Отзывы</span>
                  <?/*if(!empty($new_recs['recalls'])):*/?><!--
                      <span class="pull-right badge badge-success"><?/*=$new_recs['recalls']*/?></span>
                    --><?/*endif;*/?>
                  </span>
              </a>
            </li>

            <li <?if(Yii::$app->controller->id == 'main-reviews'):?>class="active-link"<?endif;?>>
              <a href="/main-reviews">
                <i class="fa fa-commenting-o"></i>
                <span class="menu-title">
                    <span class="menu-title">Отзывы на главной</span>
                  <?/*if(!empty($new_recs['recalls'])):*/?><!--
                      <span class="pull-right badge badge-success"><?/*=$new_recs['recalls']*/?></span>
                    --><?/*endif;*/?>
                  </span>
              </a>
            </li>

            <li <?if(Yii::$app->controller->id == 'specializations'):?>class="active-link"<?endif;?>>
              <a href="/specializations">
                <i class="fa fa-book"></i>
                <span class="menu-title">Специализации</span>
              </a>
            </li>

            <li <?if(Yii::$app->controller->id == 'industries'):?>class="active-link"<?endif;?>>
              <a href="/industries">
                <i class="fa fa-book"></i>
                <span class="menu-title">Отрасли</span>
              </a>
            </li>

            <!--Menu list item-->
            <!--<li>
              <a href="/orders">
                <i class="fa fa-shopping-cart"></i>
                  <span class="menu-title">
                    <span class="menu-title">Заказы</span>
                  </span>
              </a>
            </li>-->

            <li class="list-divider"></li>

            <!--Category name-->
            <li class="list-header">Второстепенная навигация</li>

            <!--Menu list item-->
            <li <?if(Yii::$app->controller->id == 'pages'):?>class="active-link"<?endif;?>>
              <a href="#">
                <i class="fa fa-file-code-o"></i>
                <span class="menu-title">Статичные страницы</span>
                <i class="arrow"></i>
              </a>

              <!--Submenu-->
              <ul class="collapse <?if(Yii::$app->controller->id == 'pages'):?>in<?endif;?>">
                <li <?if(Yii::$app->controller->id == 'pages' AND Yii::$app->controller->action->id == 'add'):?>class="active-link"<?endif;?>><a href="/pages/add ">
                    <i class="fa fa-plus"></i><strong>Добавить страницу</strong></a></li>
                <li class="list-divider"></li>
                <li <?if(Yii::$app->controller->id == 'pages' AND Yii::$app->controller->action->id == 'index'):?>class="active-link"<?endif;?>><a href="/pages">
                    <i class="fa fa-list-ol"></i> <strong>Все страницы</strong></a></li>
                <li class="list-divider"></li>

                <?foreach($pages as $page):?>
                  <li><a href="<?=Url::to(['pages/update', 'id' => $page->id])?>"><?=$page->name?></a></li>
                <?endforeach;?>
              </ul>
            </li>

            <li class="list-divider"></li>

            <li <?if(Yii::$app->controller->id == 'tasks'):?>class="active-link"<?endif;?>>
              <a href="/tasks">
                <i class="fa fa-list"></i>
                <span class="menu-title">
                    Задачи
                  </span>
              </a>
            </li>

            <!--Menu list item-->
            <li <?if(Yii::$app->controller->id == 'categories'):?>class="active-link"<?endif;?>>
              <a href="/categories">
                <i class="fa fa-list"></i>
                  <span class="menu-title">
                    Категории
                  </span>
              </a>
            </li>
            <li class="list-divider"></li>

            <!--Category name-->
            <li class="list-header">Конфигурация</li>

            <!--Menu list item-->
            <li <?if(Yii::$app->controller->id == 'users'):?>class="active-link"<?endif;?>>
              <a href="/users">
                <i class="fa fa-user"></i>
                <span class="menu-title">
                    <span class="menu-title">Пользователи </span></span>
              </a>
            </li>

            <li <?if(Yii::$app->controller->id == 'points'):?>class="active-link"<?endif;?>>
              <a href="/points">
                <i class="fa fa-user"></i>
                <span class="menu-title">
                    <span class="menu-title">Очки </span></span>
              </a>
            </li>

            <li <?if(Yii::$app->controller->id == 'meta'):?>class="active-link"<?endif;?>>
              <a href="/meta">
                <i class="fa fa-sellsy"></i>
                <span class="menu-title">Мета</span>
              </a>
            </li>

            <!--Menu list item-->
            <li <?if(Yii::$app->controller->id == 'settings'):?>class="active-link"<?endif;?>>
              <a href="/settings">
                <i class="fa fa-cog"></i>
                <span class="menu-title">Настройки сайта</span>
              </a>
            </li>

          </ul>


          <!--Widget-->
          <!--================================-->
          <div class="mainnav-widget">

            <!-- Show the button on collapsed navigation -->
            <div class="show-small">
              <a href="#" data-toggle="menu-widget" data-target="#demo-wg-server">
                <i class="fa fa-desktop"></i>
              </a>
            </div>

            <!-- Hide the content on collapsed navigation -->
            <!--<div id="demo-wg-server" class="hide-small mainnav-widget-content">
              <ul class="list-group">
                <li class="list-header pad-no pad-ver">Продвижение сайта</li>
                <li class="mar-btm">
                  <span class="label label-primary pull-right">53%</span>

                  <p>SEO оптимизация</p>

                  <div class="progress progress-sm">
                    <div class="progress-bar progress-bar-primary" style="width: 53%;">
                      <span class="sr-only">53%</span>
                    </div>
                  </div>
                </li>
                <li class="mar-btm">
                  <span class="label label-purple pull-right">95%</span>

                  <p>Индексация сайта</p>

                  <div class="progress progress-sm">
                    <div class="progress-bar progress-bar-purple" style="width: 95%;">
                      <span class="sr-only">95%</span>
                    </div>
                  </div>
                </li>
                <li class="mar-btm">
                  <span class="label label-pink pull-right">15%</span>

                  <p>Ключевики в топ-10</p>

                  <div class="progress progress-sm">
                    <div class="progress-bar progress-bar-pink" style="width: 15%;">
                      <span class="sr-only">15%</span>
                    </div>
                  </div>
                </li>
                <li class="pad-ver"><a href="#" class="btn btn-success btn-bock">Подробнее</a></li>
              </ul>
            </div>-->
          </div>
          <!--================================-->
          <!--End widget-->

        </div>
      </div>
    </div>
    <!--================================-->
    <!--End menu-->

  </div>
</nav>