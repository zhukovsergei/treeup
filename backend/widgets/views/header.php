<header id="navbar">
  <div id="navbar-container" class="boxed">

    <!--Brand logo & name-->
    <!--================================-->
    <?= backend\widgets\Brand::widget()?>
    <!--================================-->
    <!--End brand logo & name-->


    <!--Navbar Dropdown-->
    <!--================================-->
    <div class="navbar-content clearfix">
      <ul class="nav navbar-top-links pull-left">

        <!--Navigation toogle button-->
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <li class="tgl-menu-btn">
          <a class="mainnav-toggle" href="#">
            <i class="fa fa-navicon fa-lg"></i>
          </a>
        </li>
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <!--End Navigation toogle button-->


        <!--Messages Dropdown-->
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <!--End message dropdown-->

        <!--Notification dropdown-->
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <!--End notifications dropdown-->
      </ul>
      <ul class="nav navbar-top-links pull-right">

        <!--User dropdown-->
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <li id="dropdown-user" class="dropdown">
          <?if(Yii::$app->controller->id !== 'site'):?>
            <a href="<?=\Yii::$app->request->getHostInfo()?>" class="dropdown-toggle text-right">
              На главную [<i class="fa fa-arrow-down"></i>]
            </a>
          <?endif;?>

          <a target="_blank" href="<?=str_replace('cp.', '', \Yii::$app->request->getHostInfo())?>" class="dropdown-toggle text-right">
            Перейти на сайт [<i class="fa fa-arrow-right"></i>]
          </a>

          <a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
								<span class="pull-right">
									<img class="img-circle img-user media-object" src="/img/av1.png" alt="Profile Picture">
								</span>
            <div class="username hidden-xs">
              <?if(!\Yii::$app->getUser()->getIsGuest()):?>
                <?=\Yii::$app->getUser()->getIdentity()->username?>
              <?else:?>
                admin
              <?endif;?>
            </div>
          </a>


          <div class="dropdown-menu dropdown-menu-md dropdown-menu-right with-arrow panel-default">
            <!-- Dropdown heading  -->
            <div class="pad-all bord-btm">
              <p class="text-lg text-muted text-thin mar-btm">Добро пожаловать <?if(!Yii::$app->getUser()->getIsGuest()):?><?=\Yii::$app->user->getIdentity()->username?> <?endif;?>!</p>
            </div>


            <!-- User dropdown menu -->
            <ul class="head-list">
              <li>
                <a href="/settings">
                  <i class="fa fa-gear fa-fw fa-lg"></i> Настройки
                </a>
              </li>
            </ul>

            <!-- Dropdown footer -->
            <div class="pad-all text-right">
              <a href="/auth/logout" class="btn btn-primary">
                <i class="fa fa-sign-out fa-fw"></i> Выход
              </a>
            </div>
          </div>
        </li>
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <!--End user dropdown-->

      </ul>
    </div>
    <!--================================-->
    <!--End Navbar Dropdown-->

  </div>
</header>