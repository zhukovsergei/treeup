<div class="navbar-header">
  <a href="/" class="navbar-brand">
    <img src="/img/logo.png" alt="T-studio logo" class="brand-icon">
    <div class="brand-title">
      <span class="brand-text">— &nbsp; студия</span>
    </div>
  </a>
</div>