<footer id="footer">
  <!-- Visible when footer positions are static -->
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  <div class="hide-fixed pull-right pad-rgt">Текущая версия <a href="<?=yii\helpers\Url::to('site/change-log')?>">2.0.0</a></div>

  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  <!-- Remove the class name "show-fixed" and "hide-fixed" to make the content always appears. -->
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

  <p class="pad-lft"> &#0169; 2007 — <?=date('Y')?> Т-студия админпанель</p>

</footer>