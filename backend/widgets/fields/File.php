<?php
namespace backend\widgets\fields;

class File extends ExtendWidget
{
  public $showLoadButton = true;
  public $alt = null;
  public $width = 250;
  public $height = null;
  public $val = null;

  public $lg = [2, 3];

  public function run()
  {
    return $this->render('file', [
      'lf' => $this->lf,
      'nf' => $this->nf,
      'hint' => $this->hint,
      'showLoadButton' => $this->showLoadButton,
      'alt' => $this->alt,
      'width' => $this->width,
      'height' => $this->height,
      'val' => $this->val,
      'classModelName' => get_class($this->model),
      'idForRemove' => $this->model->id ?? null,
      'lg' => $this->lg,
    ]);
  }
}