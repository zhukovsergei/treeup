<?if( !empty($val) && file_exists(Yii::getAlias('@uploads/'.$val))):?>
  <div class="form-group">
    <label class="col-lg-<?=$lg[0]?> control-label"></label>
    <div class="col-lg-<?=$lg[1]?>">
      <?if(exif_imagetype(Yii::getAlias('@uploads/'.$val))):?>
        <img <?if($width):?>width="<?=$width?>"<?endif;?><?if($height):?>height="<?=$height?>"<?endif;?>
          src="<?=Yii::getAlias('@upl/'.$val)?>" <?if($alt):?><?=$alt?><?endif;?>>
      <?else:?>
        <a href="<?=Yii::getAlias('@upl/'.$val)?>" target="_blank"><?=$val?></a>
      <?endif;?>
      <button data-class="<?=$classModelName?>" data-idforremove="<?=$idForRemove?>" class="deleteFile btn btn-default btn-labeled icon-lg fa fa-trash" style="margin-top: 20px;">
        Удалить навсегда
      </button>
    </div>
    <br>
  </div>
<?endif;?>

<?if($showLoadButton):?>
  <div class="form-group">
    <label class="col-lg-<?=$lg[0]?> control-label"><?=$lf?></label>
    <div class="col-lg-<?=$lg[1]?>">
      <input name="<?=$nf?>" type="file" class="form-control">
      <?if($hint):?>
        <small class="help-block"><?=$hint?></small>
      <?endif;?>
    </div>
  </div>
<?endif;?>

<script>
  $('.deleteFile').on('click', function (e) {
    e.preventDefault();
    var classModelName = $(this).data('class');
    var id = $(this).data('idforremove');

    $.ajax({
      type: 'POST',
      url : '<?=\yii\helpers\Url::to(['deleteFileThroughCleaner'])?>',
      data: {
        'classModelName':  classModelName,
        'id':  id
      },
      dataType: 'json'
    });

    $(this).closest('.form-group').hide();
    $.niftyNoty({
      type: 'success',
      title: 'Системное сообщение',
      icon: 'fa fa-info fa-lg',
      message: 'Удалено',
      container: 'floating',
      timer: 5500
    });
  });
</script>
