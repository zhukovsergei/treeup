<?php
/**
 * Created by PhpStorm.
 * User: Adobe
 * Date: 14.06.2017
 * Time: 15:32
 */

namespace backend\widgets\fields;

use yii\base\Widget;

abstract class ExtendWidget extends Widget
{
  public $model;
  public $lf;
  public $nf;
  public $val;
  public $hint;

  public function beforeRun()
  {
    if ( ! parent::beforeRun() ) {
      return false;
    }

    if(empty($this->lf))
    {
      throw new \UnexpectedValueException('Label field must be exists');
    }

    if(empty($this->nf))
    {
      throw new \UnexpectedValueException('Name field must be exists');
    }

    if(\Yii::$app->controller->action->id === 'update' && is_null($this->val))
    {
      if(empty($this->model))
      {
        throw new \UnexpectedValueException('Model is empty');
      }

      preg_match_all("/\[([^\]]*)\]/", $this->nf, $matches);
      $this->val = isset($matches[1][0]) ? $this->model->{$matches[1][0]} : $this->model->{$this->nf};
    }

    return true;
  }

}