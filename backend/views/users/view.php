<?php
/* @var $this yii\web\View */
$this->title = 'Пользователи';
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
?>
<div id="content-container">

  <?php echo backend\widgets\Alert::widget()?>

  <div id="page-title">
    <h1 class="page-header text-overflow"><?=$this->title?></h1>
  </div>

  <div id="page-content">
    <div class="row">

      <div class="col-lg-12">
        <div class="panel">
          <div class="panel-heading">
            <h3 class="panel-title">Просмотр данных</h3>
          </div>

          <!--Input Size-->
          <!--===================================================-->
          <div class="panel-body">
            <?=DetailView::widget([
              'model' => $row,
              'attributes' => [
                'username',
                'email',
              ],
            ]);?>
          </div>

          <div class="panel-footer">
            <div class="row">
              <div class="col-lg-4 col-lg-offset-2">
                <a href="<?=Url::to(['index'])?>" class="btn btn-primary btn-labeled fa fa-check">OK</a>
              </div>
            </div>
          </div>
          <!--===================================================-->
          <!--End Input Size-->


        </div>
      </div>


    </div>

    <div class="row">
      <div class="col-md-6 col-lg-4">
        <div class="panel">
          <div class="panel-heading">
            <h3 class="panel-title">Деньги (<?=\aiur\helpers\user\BalanceHelper::getMoney($row->id)?>)</h3>
          </div>
          <div class="panel-body">

            <!--List Group with Badges-->
            <!--===================================================-->
            <?if(!empty($row->moneyBalance->history)):?>
              <ul class="list-group">
                <?foreach($row->moneyBalance->history as $mh):?>
                  <li class="list-group-item">
                    <?if($mh->amount > 0):?>
                      <span class="badge badge-primary">+</span>
                    <?else:?>
                      <span class="badge badge-warning">-</span>
                    <?endif;?>
                    <?=$mh->amount?>
                  </li>
                <?endforeach;?>
              </ul>
            <?endif;?>
            <!--===================================================-->

          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-4">
        <div class="panel">
          <div class="panel-heading">
            <h3 class="panel-title">TU (<?=\aiur\helpers\user\BalanceHelper::getCakes($row->id)?>)</h3>
          </div>
          <div class="panel-body">

            <!--List Group with Badges-->
            <!--===================================================-->
            <?if(!empty($row->cakeBalance->history)):?>
              <ul class="list-group">
                <?foreach($row->cakeBalance->history as $ch):?>
                  <li class="list-group-item">
                    <?if($ch->amount > 0):?>
                      <span class="badge badge-primary">+</span>
                    <?else:?>
                      <span class="badge badge-warning">-</span>
                    <?endif;?>
                    <?=$ch->amount?>
                  </li>
                <?endforeach;?>
              </ul>
            <?endif;?>

            <!--===================================================-->

          </div>
        </div>
      </div>
      <div class="col-md-12 col-lg-4">
        <div class="panel">
          <div class="panel-heading">
            <h3 class="panel-title">Очки (<?=\aiur\helpers\user\BalanceHelper::getPoints($row->id)?>)</h3>
          </div>
          <div class="panel-body">

            <!--Link Items-->
            <!--===================================================-->
            <?if(!empty($row->pointsBalance)):?>
              <ul class="list-group">
                <?foreach($row->pointsBalance->history as $ph):?>
                  <li class="list-group-item">
                    <?if($ph->amount > 0):?>
                      <span class="badge badge-primary">+</span>
                    <?else:?>
                      <span class="badge badge-warning">-</span>
                    <?endif;?>
                    <?=$ph->amount?>
                  </li>
                <?endforeach;?>
              </ul>
            <?endif;?>
            <!--===================================================-->

          </div>
        </div>
      </div>
    </div>
  </div>

</div>