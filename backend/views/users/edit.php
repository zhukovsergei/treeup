<?php
/* @var $this yii\web\View */
$this->title = 'Пользователи';
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use backend\widgets\fields\Checkbox;
use backend\widgets\fields\Password;
use backend\widgets\fields\Text;

?>
<div id="content-container">

  <div id="page-title">
    <h1 class="page-header text-overflow"><?=$this->title?></h1>
  </div>

  <?=Breadcrumbs::widget([
    'homeLink' => [
      'label' => 'Главная',
      'url' => Yii::$app->homeUrl,
    ],
    'links' => [
      ['label' => $this->title, 'url' => ['index']],
      'Редактирование записи'
    ],
  ]);
  ?>

  <!--Page content-->
  <!--===================================================-->
  <div id="page-content">
    <div class="row">

      <div class="col-lg-12">
        <div class="panel">
          <div class="panel-heading">
            <h3 class="panel-title">
              <?if( Yii::$app->controller->action->id == 'add'):?>Создание новой записи<?endif;?>
              <?if( Yii::$app->controller->action->id == 'edit'):?>Редактирование записи<?endif;?></h3>
          </div>

          <!--Input Size-->
          <!--===================================================-->
          <form class="form-horizontal" action="<?=Url::current()?>" method="POST" enctype="multipart/form-data">
            <div class="panel-body">

              <?= Text::widget([
                'model' => $row,
                'lf' => 'Имя',
                'nf' => 'fd[username]',
              ])?>

              <?= Text::widget([
                'model' => $row,
                'lf' => 'Логин (E-mail)',
                'nf' => 'fd[email]',
              ])?>

              <?= Password::widget()?>

              <?= Checkbox::widget([
                'model' => $row,
                'lf' => 'Администратор',
                'nf' => 'fd[root]',
              ])?>

              <?= Checkbox::widget([
                'model' => $row,
                'lf' => 'Заблокирован',
                'nf' => 'fd[banned]',
              ])?>

            </div>
            <div class="panel-footer">
              <div class="row">
                <div class="col-lg-4 col-lg-offset-2">
                  <button class="btn btn-primary btn-labeled fa fa-check" type="submit"><?if(Yii::$app->controller->action->id == 'add'):?>Добавить запись<?else:?>Принять изменения<?endif;?></button>
                  <a href="<?=Url::to(['index'])?>" class="btn btn-warning btn-labeled fa fa-repeat" >Отмена</a>
                </div>
              </div>
            </div>
          </form>
          <!--===================================================-->
          <!--End Input Size-->


        </div>
      </div>

      <div class="col-lg-12">
        <div class="panel">
          <div class="panel-heading">
            <h3 class="panel-title">
              <?if( Yii::$app->controller->action->id == 'add'):?>Создание новой записи<?endif;?>
              <?if( Yii::$app->controller->action->id == 'edit'):?>Редактирование записи<?endif;?></h3>
          </div>

          <!--Input Size-->
          <!--===================================================-->
          <form class="form-horizontal" action="/users/add-transaction" method="POST" enctype="multipart/form-data">
            <div class="panel-body">

              <?= \backend\widgets\fields\Select::widget([
                'lf' => 'Счет',
                'nf' => 'td[type_account]',
                'source' => ['money' => 'Деньги', 'cake' => 'TU', 'points' => 'Очки'],
                'val' => 1,
              ])?>

              <div class="form-group pad-ver">
                <label class="col-md-2 control-label"></label>
                <div class="col-md-9">

                  <div class="radio">

                    <!-- Inline Icon Radios Buttons -->
                    <label class="form-radio form-icon"><input type="radio" checked="" value="add" name="type_transaction"> Внести</label>
                    <label class="form-radio form-icon"><input type="radio" value="sub" name="type_transaction"> Списать</label>

                  </div>
                </div>
              </div>

              <?= Text::widget([
                'lf' => 'Сумма / Количество',
                'nf' => 'td[amount]',
                'lg' => [2,1],
                'val' => 1,
              ])?>

            </div>
            <div class="panel-footer">
              <div class="row">
                <div class="col-lg-4 col-lg-offset-2">
                  <input type="hidden" name="td[user_id]" value="<?=$row->id?>">
                  <button class="btn btn-primary btn-labeled fa fa-check" type="submit">Отправить</button>
                  <a href="<?=Url::to(['index'])?>" class="btn btn-warning btn-labeled fa fa-repeat" >Отмена</a>
                </div>
              </div>
            </div>
          </form>
          <!--===================================================-->
          <!--End Input Size-->


        </div>
      </div>


    </div>

  </div>
  <!--===================================================-->
  <!--End page content-->
</div>