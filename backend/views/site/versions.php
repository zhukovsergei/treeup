<div id="content-container">

  <div id="page-title">
    <h1 class="page-header text-overflow">История изменений Админпанели</h1>
  </div>

  <!--Page content-->
  <!--===================================================-->
  <div id="page-content">
    <div class="row">
      <div class="col-lg-12">
        <!-- Timeline -->
        <!--===================================================-->
        <div class="timeline">

          <!-- Timeline header -->
          <div class="timeline-header">
            <div class="timeline-header-title bg-purple">Сейчас</div>
          </div>

          <div class="timeline-entry">
            <div class="timeline-stat">
              <div class="timeline-icon bg-success"><i class="fa fa-plus fa-lg"></i></div>
              <div class="timeline-time">2.0.2</div>
            </div>
            <div class="timeline-label">
              <h4 class="text-info text-lg">5 марта 2016</h4>
              <ul>
                <li>Обновлены все модули и фрэймворк до актуальной версии</li>
                <li>Установлен часовой пояс</li>
                <li>Полностью переработана логика создания алиасов и убрано все лишнее</li>
                <li>Полностью переработан механизм подключения библиотек</li>
                <li>Подключен JUI</li>
              </ul>
            </div>
          </div>

          <div class="timeline-entry">
            <div class="timeline-stat">
              <div class="timeline-icon bg-success"><i class="fa fa-plus fa-lg"></i></div>
              <div class="timeline-time">2.0.1</div>
            </div>
            <div class="timeline-label">
              <h4 class="text-info text-lg">5 ноября 2015</h4>
              <ul>
                <li>Исправлена логика вывода виджета select.</li>
                <li>В виджете категорий добавлена глубина вложенности.</li>
                <li>Исправлен баг с именем пользователей.</li>
                <li>Исправлен баг в категориях.</li>
                <li>Исправлен баг со статусом пользователей.</li>
                <li>Добавлена URL библиотека (манимуляция URL).</li>
              </ul>
            </div>
          </div>

          <div class="timeline-entry">
            <div class="timeline-stat">
              <div class="timeline-icon bg-success"><i class="fa fa-plus fa-lg"></i></div>
              <div class="timeline-time">2.0.0</div>
            </div>
            <div class="timeline-label">
              <h4 class="text-info text-lg">24 октября 2015</h4>
              <ul>
                <li>Добавлен Asset Cookie в Frontend.</li>
                <li>Добавлен Notify JS.</li>
                <li>Улучшена проверка пользователей Use.</li>
                <li>Добавлены новые зголовки ответа.</li>
                <li>Все основные поля переведены на виджеты.</li>
                <li>Добавлены виджеты файлов и селекта.</li>
                <li>Исправлена проблема с безопасностью.</li>
              </ul>
            </div>

            <div class="timeline-label">
              <h4 class="text-info text-lg">22 октября 2015</h4>
              <ul>
                <li>Минимальная версия Bootstrap.</li>
                <li>Вынесен urlManager в общий конфиг.</li>
                <li>Добавлен генератор.</li>
                <li>Исправлен баг в категориях.</li>
                <li>Описан Bootstrap для frontend.</li>
                <li>Добавлена в настройки загрузка одиночного файла.</li>
                <li>Добавлены маршруты.</li>
                <li>Добавлены новые типы в генератор форм.</li>
              </ul>
            </div>
            <div class="timeline-label">
              <h4 class="text-info text-lg">17 октября 2015</h4>
              <ul>
                <li> Обневлен и локализован DataTables до актуальной версии.</li>
                <li> Исправлена работа Меню.</li>
                <li> Добавлен Articmodal.</li>
                <li> Добавлен Bxslider.</li>
                <li> Исправлены баги.</li>
              </ul>
            </div>
            <div class="timeline-label">
              <h4 class="text-info text-lg">14 октября 2015</h4>
              <ul>
                <li> Обневлен Switchery до актуальной версии.</li>
                <li> Обневлен Pace до актуальной версии.</li>
                <li> Обневлен jQuery / jQuery UI до актуальной версии.</li>
                <li> Исправлены баги.</li>
              </ul>
            </div>
            <div class="timeline-label">
              <h4 class="text-info text-lg">12 октября 2015</h4>
              <ul>
                <li> Допилены статические страницы, загрузги изображения и аттачей.</li>
                <li> <span class="label label-warning">NEW</span> Добавлена кнопка "На главную".</li>
                <li> Рефакторинг виджетов.</li>
                <li> Созданы виджеты для front-end.</li>
                <li> Рефакторинг моделей.</li>
                <li> Добавлена галерея и массовый загрузчик.</li>
                <li> Исправлены баги.</li>
              </ul>
            </div>

            <div class="timeline-label">
              <h4 class="text-info text-lg">11 октября 2015</h4>
              <ul>
                <li> Переписан хелпер русских дат, рефакторинг кода.</li>
                <li> Полностью перенесены и адаптированы категории.</li>
                <li> Адаптирован WYSIWYG Imperavi редактор.</li>
                <li> Перенесены статические страницы.</li>
                <li> Исправлены баги.</li>
              </ul>
            </div>

            <div class="timeline-label">
              <h4 class="text-info text-lg">10 октября 2015</h4>
              <ul>
                <li> <span class="label label-warning">NEW</span> Добавлен новый конфиг.</li>
                <li> Исправлена авторизация.</li>
                <li> Разделена структура контроллеров FR/BK.</li>
                <li> Рефакторинг кода при обращении к компоненту Yii.</li>
                <li> Исправлены баги.</li>
              </ul>
            </div>

            <div class="timeline-label">
              <h4 class="text-info text-lg">9 октября 2015</h4>
              <ul>
                <li> Перенесен модуль "Отзывы".</li>
                <li> Перенесен модуль "Вопрос-ответ".</li>
                <li> Перенесен модуль "Пользователи".</li>
                <li> Настроен модуль авторизации.</li>
                <li> Исправлены алерты.</li>
                <li> Исправлены видежты.</li>
                <li> Исправлены ошибки.</li>
              </ul>
            </div>

            <div class="timeline-label">
              <h4 class="text-info text-lg">7 октября 2015</h4>
              <ul>
                <li> Перенесена и добавлена пользовательская статистика.</li>
                <li> Разделены конифиги.</li>
              </ul>
            </div>

            <div class="timeline-label">
              <h4 class="text-info text-lg">5 октября 2015</h4>
              <ul>
                <li> <span class="label label-warning">NEW</span> Переход на Yii2!</li>
              </ul>
            </div>


          </div>

          <div class="timeline-entry">
            <div class="timeline-stat">
              <div class="timeline-icon bg-success"><i class="fa fa-plus fa-lg"></i></div>
              <div class="timeline-time">1.0.6</div>
            </div>

            <div class="timeline-label">
              <h4 class="text-info text-lg">1 сентября 2015</h4>
              <ul>
                <li> Добавлен поиск для всех таблиц.</li>
              </ul>
            </div>

            <div class="timeline-label">
              <h4 class="text-info text-lg">20 августа 2015</h4>
              <ul>
                <li> Font Awesome обновлен до версии 4.4.0.</li>
                <li> Добавлена Drag And Drop соритровка категорий.</li>
              </ul>
            </div>
            <div class="timeline-label">
              <h4 class="text-info text-lg">19 августа 2015</h4>
              <ul>
                <li><span class="label label-warning">NEW</span> Реализован Nested Sets в категориях.</li>
              </ul>
            </div>
            <div class="timeline-label">
              <h4 class="text-info text-lg">18 августа 2015</h4>
              <ul>
                <li>Оптимизированы и вычещены скрипты. Удалены неиспользуемые сценарии.</li>
              </ul>
            </div>
          </div>

          <div class="timeline-entry">
            <div class="timeline-stat">
              <div class="timeline-icon bg-success"><i class="fa fa-plus fa-lg"></i></div>
              <div class="timeline-time">1.0.5</div>
            </div>

            <div class="timeline-label">
              <h4 class="text-info text-lg">6 июня 2015</h4>
              <ul>
                <li>Изменена логика работы нагрузки на сервер.</li>
              </ul>
            </div>
          </div>

          <div class="timeline-entry">
            <div class="timeline-stat">
              <div class="timeline-icon bg-success"><i class="fa fa-plus fa-lg"></i></div>
              <div class="timeline-time">1.0.4</div>
            </div>

            <div class="timeline-label">
              <h4 class="text-info text-lg">24 мая 2015</h4>
              <ul>
                <li>Добавлены новые и оптимизированые некоторые старые функции в системе.</li>
              </ul>
            </div>

            <div class="timeline-label">
              <h4 class="text-info text-lg">22 мая 2015</h4>
              <ul>
                <li>Добавлена генерация паролей.</li>
              </ul>
            </div>
            <div class="timeline-label">
              <h4 class="text-info text-lg">16 мая 2015</h4>
              <ul>
                <li><span class="label label-warning">NEW</span> Добавлен модуль "Вопрос-ответ".</li>
              </ul>
            </div>
            <div class="timeline-label">
              <h4 class="text-info text-lg">15 мая 2015</h4>
              <ul>
                <li>Исправлен баг с отображением фотонового изображения на странице авторизации.</li>
                <li>На странице авторизации добавлен флаг "Запомнить".</li>
                <li>Модуль авторизации вынесен в админский раздел.</li>
                <li>WYSIWYG-редактор обновлен до актуальной версии.</li>
                <li>Полностью переработана логика загрузки изображений в WYSIWYG-редакторе.</li>
                <li>Теперь, превью и оригиналы картинок лежат в одной директории контроллера.</li>
              </ul>
            </div>

          </div>


          <div class="timeline-entry">
            <div class="timeline-stat">
              <div class="timeline-icon bg-success"><i class="fa fa-plus fa-lg"></i></div>
              <div class="timeline-time">1.0.3</div>
            </div>

            <div class="timeline-label">
              <h4 class="text-info text-lg">9 мая 2015</h4>
              <ul>
                <li>Оптимизирвоана работа JS-апплетов.</li>
                <li>Удалена тонна лишних, неиспользуемых JS-апплетов, CSS стилей.</li>
              </ul>
            </div>

          </div>

          <div class="timeline-entry">
            <div class="timeline-stat">
              <div class="timeline-icon bg-success"><i class="fa fa-plus fa-lg"></i></div>
              <div class="timeline-time">1.0.2</div>
            </div>

            <div class="timeline-label">
              <h4 class="text-info text-lg">7 мая 2015</h4>
              <ul>
                <li>Изменена логика работы статистики посещений на главной.</li>
                <li>Добавлена настройка "Предварительная загрузка страниц".</li>
              </ul>
            </div>

            <div class="timeline-label">
              <h4 class="text-info text-lg">1 мая 2015</h4>
              <ul>
                <li>Изменения в таблице "Отзывы".</li>
              </ul>
            </div>
            <div class="timeline-label">
              <h4 class="text-info text-lg">28 aпреля 2015</h4>
              <ul>
                <li>Теперь, при дублировании твоаров, их изображения тоже дублируются.</li>
                <li>Кнопка удаления вынесена в панель действий.</li>
                <li>Добавлена панель действий в таблице с товарами.</li>
              </ul>
            </div>
          </div>

          <div class="timeline-entry">
            <div class="timeline-stat">
              <div class="timeline-icon bg-success"><i class="fa fa-plus fa-lg"></i></div>
              <div class="timeline-time">1.0.1</div>
            </div>
            <div class="timeline-label">
              <h4 class="text-info text-lg">25 aпреля 2015</h4>
              <ul>
                <li>Добавлена статистическая информация по базам данных.</li>
                <li>Добавлена настройка отключения логотипа TDSGN.</li>
                <li>Добавлена кнопка "На сайт".</li>
              </ul>
            </div>
          </div>

          <div class="timeline-entry">
            <div class="timeline-stat">
              <div class="timeline-icon bg-success"><i class="fa fa-arrow-up fa-lg"></i></div>
              <div class="timeline-time">1.0.0</div>
            </div>
            <div class="timeline-label">
              <h4 class="text-info text-lg">24 aпреля 2015</h4>
              <ul>
                <li>Появилась история версий.</li>
              </ul>
            </div>
          </div>


        </div>
        <!--===================================================-->
        <!-- End Timeline -->

      </div>
    </div>

  </div>
  <!--===================================================-->
  <!--End page content-->
</div>