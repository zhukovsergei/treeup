<?php
/* @var $this yii\web\View */
$this->title = 'Задачи';

use backend\widgets\fields\Checkbox;
use backend\widgets\fields\File;
use backend\widgets\fields\Select;
use backend\widgets\fields\Text;
use backend\widgets\fields\Textarea;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

?>

<div id="content-container">

    <div id="page-title">
        <h1 class="page-header text-overflow"><?= $this->title ?></h1>
    </div>

    <?= Breadcrumbs::widget([
        'homeLink' => [
            'label' => 'Главная',
            'url' => Yii::$app->homeUrl,
        ],
        'links' => [
            ['label' => $this->title, 'url' => ['index']],
            'Редактирование записи'
        ],
    ]);
    ?>

    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">
        <div class="row">
            <form action="<?= Url::current() ?>" method="post" enctype="multipart/form-data">
                <div class="col-lg-12">
                    <div class="tab-base">

                        <!--Nav Tabs-->
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#demo-lft-tab-1" aria-expanded="true">Основная информация</a>
                            </li>
                            <!--<li class="">
                              <a data-toggle="tab" href="#demo-lft-tab-2" aria-expanded="false">Загружаемый контент</a>
                            </li>-->
                        </ul>

                        <!--Tabs Content-->
                        <div class="tab-content">
                            <div id="demo-lft-tab-1" class="tab-pane fade active in">
                                <div class="panel-body form-horizontal form-padding"> <!-- tab start here -->

                                    <?= Text::widget([
                                        'model' => $row,
                                        'lf' => 'Название задачи',
                                        'nf' => 'fd[name]',
                                    ]) ?>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="fd-name">Логин создателя</label>
                                        <div class="col-lg-4">
                                            <?= $row->creator->email ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="fd-name">Дата последнего
                                            изменения</label>
                                        <div class="col-lg-4">
                                            <?= $row->date_upd ?? '—' ?>
                                        </div>
                                    </div>

                                    <?= Select::widget([
                                        'model' => $row,
                                        'lf' => 'Тип задачи',
                                        'nf' => 'fd[type_id]',
                                        'source' => \common\models\Task::TYPE,
                                    ]) ?>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="fd-name">Проект</label>
                                        <div class="col-lg-4">
                                            <?= $row->project->name ?? '—' ?>
                                        </div>
                                    </div>

                                    <?= Select::widget([
                                        'model' => $row,
                                        'lf' => 'Город',
                                        'nf' => 'fd[town_id]',
                                        'notSet' => true,
                                        'source' => $towns,
                                    ]) ?>

                                    <?= Select::widget([
                                        'model' => $row,
                                        'lf' => 'Сфера',
                                        'nf' => 'fd[industry_id]',
                                        'source' => $industries,
                                    ]) ?>

                                    <?= Select::widget([
                                        'model' => $row,
                                        'lf' => 'Уровень сложности',
                                        'nf' => 'fd[complexity]',
                                        'source' => \common\models\Task::COMPLEXITY,
                                    ]) ?>

                                    <?= Select::widget([
                                        'model' => $row,
                                        'lf' => 'Специализация',
                                        'nf' => 'fd[specialization_id]',
                                        'notSet' => true,
                                        'source' => $specializations,
                                    ]) ?>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="fd-name">Скилы</label>
                                        <div class="col-lg-4">
                                            <? if ($row->skills): ?>
                                                <?= implode(', ', \yii\helpers\ArrayHelper::getColumn($row->skills, 'name')) ?>
                                            <? else: ?>
                                                —
                                            <? endif; ?>
                                        </div>
                                    </div>

                                    <?= Textarea::widget([
                                        'model' => $row,
                                        'lf' => 'Описание',
                                        'nf' => 'fd[text]',
                                    ]) ?>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="fd-name">Файлы</label>
                                        <div class="col-lg-4">
                                            <? if ($row->files): ?>
                                                <? foreach ($row->files as $file): ?>
                                                    <a target="_blank"
                                                       href="<?= $file->getUploadedFileUrl('name') ?>"><?= $file->name ?></a>
                                                    <br>
                                                <? endforeach; ?>
                                            <? else: ?>
                                                —
                                            <? endif; ?>
                                        </div>
                                    </div>

                                    <?= Text::widget([
                                        'model' => $row,
                                        'lf' => 'Окончание приема заявок',
                                        'nf' => 'fd[date_end]',
                                        'lg' => [2, 1],
                                        'date' => true,
                                    ]) ?>

                                    <?= Text::widget([
                                        'model' => $row,
                                        'lf' => 'Очки для победителя',
                                        'nf' => 'fd[points_win]',
                                        'lg' => [2, 1],

                                    ]) ?>

                                    <?= Text::widget([
                                        'model' => $row,
                                        'lf' => 'Очки для участника',
                                        'nf' => 'fd[points_participant]',
                                        'lg' => [2, 1],
                                    ]) ?>

                                    <?= Select::widget([
                                        'model' => $row,
                                        'lf' => 'Привилегия',
                                        'nf' => 'fd[privilege]',
                                        'source' => \common\models\Task::PRIVILEGE,
                                    ]) ?>

                                    <?= Select::widget([
                                        'model' => $row,
                                        'lf' => 'TU',
                                        'nf' => 'fd[tu_count]',
                                        'source' => \common\models\Task::TU_COUNT,
                                        'notSet' => true
                                    ]) ?>

                                    <?= Textarea::widget([
                                        'model' => $row,
                                        'lf' => 'Описание привелегии',
                                        'nf' => 'fd[privilege_description]',
                                    ]) ?>

                                    <?= Select::widget([
                                        'model' => $row,
                                        'lf' => 'Статус',
                                        'nf' => 'fd[status]',
                                        'source' => \common\models\Task::STATUS,
                                    ]) ?>

                                    <?= Checkbox::widget([
                                        'model' => $row,
                                        'lf' => 'Только авторизованным',
                                        'nf' => 'fd[authored_visible]',
                                    ]) ?>

                                    <?= Checkbox::widget([
                                        'model' => $row,
                                        'lf' => 'Оплачено',
                                        'nf' => 'fd[is_paid]',
                                    ]) ?>

                                    <div class="reject_reason">
                                        <?= Textarea::widget([
                                            'model' => $row,
                                            'lf' => 'Причина отказа',
                                            'nf' => 'fd[reject_reason]',
                                        ]) ?>
                                    </div>

                                    <!--<script>
                                      $('select[name*=status]').on('change', function (event) {
                                        var val = $(this).val();
                                        if(val === 'reject')
                                        {
                                          $('.reject_reason').show();
                                        }
                                        else
                                        {
                                          $('.reject_reason').hide();
                                        }
                                      });
                                    </script>-->

                                </div>
                            </div>

                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-lg-4 col-lg-offset-2">
                                    <button class="btn btn-primary btn-labeled fa fa-check"
                                            type="submit"><? if (Yii::$app->controller->action->id == 'add'): ?>Добавить запись<? else: ?>Принять изменения<? endif; ?></button>
                                    <a href="<?= Url::to(['index']) ?>"
                                       class="btn btn-warning btn-labeled fa fa-repeat">Отмена</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>


    <!--===================================================-->
    <!--End page content-->
</div>
