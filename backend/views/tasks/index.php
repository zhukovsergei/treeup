<?php
/* @var $this yii\web\View */
$this->title = 'Задачи';
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div id="content-container">

  <?php echo backend\widgets\Alert::widget()?>

  <div id="page-title">
    <h1 class="page-header text-overflow"><?=$this->title?></h1>
  </div>

  <div id="page-content">
    <div class="panel">
      <div class="panel-heading">
        <div class="panel-control">
          <a href="/<?=Yii::$app->controller->id?>" class="btn btn-warning btn-labeled fa fa-repeat">Сбросить</a>
          <a href="<?=Url::to(['add'])?>" class="btn btn-primary btn-labeled fa fa-plus">Добавить новую запись</a>
        </div>
        <h3 class="panel-title">Все записи </h3>
      </div>

      <div class="panel-body">
        <?= yii\grid\GridView::widget([
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          'columns' => [
            'id',
            [
              'label' => 'Название задачи',
              'class' => yii\grid\DataColumn::className(), // this line is optional
              'attribute' => 'name',
              'format' => 'text',
              'value' => function ($row) {
                return $row->name; // $data['name'] for array data, e.g. using SqlDataProvider.
              },
            ],
            [
              'label' => 'Email',
              'attribute' => 'uid',
              'value' => 'creator.email',
              'enableSorting' => false,
            ],
            [
              'label' => 'Дата последнего изменения',
              'attribute' => 'date_upd',
              'format' => ['date', 'php:d F Y в H:i'],
              'filter' => false
            ],
            [
              'label' => 'Тип задачи',
              'attribute' => 'type_id',
              'value' => function($model) {
                return \common\models\Task::TYPE[$model->type_id];
              },
              'filter' => \common\models\Task::TYPE
            ],
            [
              'label' => 'Окончание приема заявок',
              'attribute' => 'date_end',
              'format' => ['date', 'php:d F Y в H:i'],
              'filter' => false
            ],
            [
              'label' => 'Статус',
              'attribute' => 'status',
              'value' => function($model) {
                return \common\models\Task::STATUS[$model->status];
              },
              'filter' => \common\models\Task::STATUS
            ],
            [
              'class' => 'yii\grid\ActionColumn',
            ],
          ],
        ]) ?>
      </div>
    </div>
  </div>

</div>