<?php

namespace backend\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/fonts/stylesheet.css',
        'js/plugins/animate-css/animate.min.css',
        'js/plugins/bootstrap-select/bootstrap-select.min.css',
        'css/adobe.css',
    ];

    public $js = [
        'js/plugins/bootstrap-select/bootstrap-select.min.js',
        'js/jquery.query-object.js',
        'js/app.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
//    'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
//    'backend\assets\JqueryUI',
        'backend\assets\FastClick',
        'backend\assets\Nifty',
        'backend\assets\Switchery',
        'backend\assets\DateTimePicker',
        'backend\assets\Pace',
        'backend\assets\FontAwesome',
        'backend\assets\DataTables',
        'backend\assets\DropZone',
    ];
}
