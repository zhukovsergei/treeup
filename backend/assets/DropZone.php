<?php
namespace backend\assets;

use yii\web\AssetBundle;


class DropZone extends AssetBundle
{
  public $css = [
    'js/plugins/dropzone/dropzone.min.css',
  ];
  public $js = [
    'js/plugins/dropzone/dropzone.min.js',
  ];

  public $depends = [
    'yii\web\JqueryAsset',
  ];
}
