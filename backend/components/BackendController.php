<?php
namespace backend\components;

use common\controllers\BaseController;

class BackendController extends BaseController
{
  public $enableCsrfValidation = false;
  public $redirectUrl = false;

  public function actions()
  {
    return [
      'WysiwygUpload' => [
        'class' => WysiwygUpload::class,
      ],
      'deleteFileThroughCleaner' => [
        'class' => FileCleaner::class,
      ],
    ];
  }

  public function afterAction($action, $result)
  {
    $result = parent::afterAction($action, $result);

    $arr = ['update', 'approve', 'delete', 'add', 'edit'];

    if( in_array($action->id, $arr) && \Yii::$app->request->isPost )
    {
      if( $this->redirectUrl ) {
        return $this->redirect($this->redirectUrl);
      }
      else{
        return $this->redirect(['index']);
      }
    }

    return $result;
  }

}
