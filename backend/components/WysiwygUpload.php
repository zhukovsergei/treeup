<?php
namespace backend\components;

use Yii;
use common\components\Adobe;
use common\components\SimpleImage;
use yii\base\Action;
use yii\web\Response;
use yii\web\UploadedFile;

class WysiwygUpload extends Action
{
  public $enableCsrfValidation = false;

  public function init()
  {
    parent::init();
  }

  public function run()
  {
    $mode = (int) Yii::$app->request->post('mode');

    $folder = ( $mode ) ? 'WYSIWYG_IMGS' : 'WYSIWYG_FILES';

    $file = UploadedFile::getInstanceByName('file');
    $filename = Adobe::genName($file->getBaseName()).$file->getExtension();

    $path = Yii::getAlias('@uploads/') . $folder . '/'. $filename;

    if( $file->saveAs($path) )
    {
      if($mode)
      {
        $img = new SimpleImage($path);
        $img->best_fit(1280, 1280)->save($path);
      }
    }
    Yii::$app->response->format = Response::FORMAT_JSON;
    return [
      'filelink' => Yii::getAlias('@upl/') . $folder . '/'. $filename,
    ];
  }
}
