<?php
namespace backend\components;

use yii\base\Action;
use yii\db\ActiveRecord;

class FileCleaner extends Action
{
  public $enableCsrfValidation = false;

  public function run() :void
  {
    $potentialFields = ['thumb', 'image', 'file'];
    $classModelName = \Yii::$app->request->post('classModelName');
    $id = \Yii::$app->request->post('id');

    $model = \Yii::createObject(['class' => $classModelName]);
    if($model instanceof ActiveRecord)
    {
      $row = $model->findOne($id);

      foreach($potentialFields as $fileField)
      {
        if($row->hasAttribute($fileField))
        {
          self::remove($row->{$fileField});
          $row->{$fileField} = null;
        }
      }
      $row->update();
    }
  }

  public static function remove( $filename = null)
  {
    $filePath = \Yii::getAlias('@uploads/'.$filename);

    if( ! empty($filename) && is_file($filePath) )
    {
      @unlink($filePath);
    }
  }
}
