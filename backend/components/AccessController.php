<?php
namespace backend\components;

use common\models\User;
use yii\filters\AccessControl;

class AccessController extends BackendController
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
          [
            'allow' => true,
            'controllers' => ['auth'],
            'roles' => ['?', '@'],
          ],
          [
            'allow' => true,
            'roles' => ['visitAdminPanel'],
          ],
          [
            'allow' => true,
            'roles' => ['@'],
            'matchCallback' => function ($rule, $action) {
              return User::isUserAdmin(\Yii::$app->user->getIdentity()->email);
            }
          ]
        ],
        'denyCallback' => function($rule, $action) {
          \Yii::$app->response->redirect(['auth/index']);
        }
      ],
    ];
  }}
