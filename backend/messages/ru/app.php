<?php
return [
  'The row have created' => 'Запись успешно добавлена',
  'The row have updated' => 'Запись успешно изменена',
  'The row have deleted' => 'Запись успешно удалена',
];