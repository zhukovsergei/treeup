<?php

namespace backend\controllers;

use aiur\repositories\ReviewRepository;
use backend\components\AccessController;
use common\models\Review;

class ReviewsController extends AccessController
{
  private $repo;

  public function __construct( $id, $module, ReviewRepository $repo, array $config = [] )
  {
    parent::__construct( $id, $module, $config );

    $this->repo = $repo;
  }

  public function actionAdd()
  {
    $fd = \Yii::$app->request->post('fd');

    if(\Yii::$app->request->isPost)
    {
      $m = new Review();
      $m->attributes = $fd;
      $m->save();

      return $this->redirect('reviews/index');
    }

    return $this->render( 'add' , [
    ]);
  }

  public function actionUpdate($id)
  {

    if(\Yii::$app->request->isPost)
    {
      $fd = \Yii::$app->request->post('fd');

      $row = $this->repo->get($id);
      $row->attributes = $fd;
      $row->save();

      return $this->redirect('reviews/index');
    }

    return $this->render( 'edit' , [
      'row' => $this->repo->get($id),
    ]);
  }


  public function actionIndex()
  {
    $dataProvider = $this->repo->getDataProvider();

    return $this->render( 'index' , [
      'dataProvider' => $dataProvider,
    ]);
  }

  public function actionDelete( $id )
  {
    $row = $this->repo->get($id);
    $this->repo->remove($row);
  }

}
