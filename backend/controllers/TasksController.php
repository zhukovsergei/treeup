<?php

namespace backend\controllers;

use backend\components\AccessController;
use backend\components\ProductFileUploader;
use common\models\Categories;
use common\models\Industry;
use common\models\Products;
use common\models\ProductsImages;
use common\models\Specialization;
use common\models\Task;
use common\models\TaskSearch;
use common\models\Town;
use yii\helpers\Url;

class TasksController extends AccessController
{
/*  public function actions()
  {
    return array_merge(
      parent::actions(),
      [
        'upload' => ProductFileUploader::className()
      ]);
  }*/

  public function actionIndex()
  {
    $searchModel = new TaskSearch();

    $dataProvider = $searchModel->search(\Yii::$app->request->get());

    return $this->render('index' , [
      'dataProvider' => $dataProvider,
      'searchModel' => $searchModel,
    ]);
  }

  public function actionAdd()
  {
    if(\Yii::$app->request->isPost)
    {
      $fd = \Yii::$app->request->post('fd');

      $m = new Task();
      $m->attributes = $fd;
      $m->save();

      $this->redirectUrl = Url::to(['update', 'id' => $m->id]);
    }

    return $this->render('add');
  }

  public function actionUpdate($id)
  {
    if(\Yii::$app->request->isPost)
    {
      $fd = \Yii::$app->request->post('fd');

      $row = Task::findOne($id);
      $row->attributes = $fd;
      $row->update();
    }

    return $this->render('edit', [
      'row' => Task::findOne($id),
      'towns' => Town::find()->all(),
      'industries' => Industry::find()->all(),
      'specializations' => Specialization::find()->all(),
    ]);
  }

  public function actionView($id)
  {
    return $this->render('view', [
      'row' => Task::findOne($id),
      'towns' => Town::find()->all(),
      'industries' => Industry::find()->all(),
      'specializations' => Specialization::find()->all(),
    ]);
  }

  public function actionGetImgList()
  {
    $product_id = \Yii::$app->request->get('id');

    return $this->renderPartial('ajaxList', array(
      'rows' => ProductsImages::findAll(['product_id' => $product_id]),
    ));
  }

  public function actionDelImg()
  {
    $id = \Yii::$app->request->post('id');

    ProductsImages::deleteAll(['id' => $id]);
    return $this->sendOk();
  }

  public function actionDelete()
  {
    $id = \Yii::$app->request->get('id');

    Task::findOne($id)->delete();

    \Yii::$app->session->setFlash('msg', 'Запись успешно удалена');

    return $this->getBack();
  }

}
