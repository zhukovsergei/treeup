<?php
namespace backend\controllers;

use backend\components\AccessController;
use common\models\User;
use yii2tech\balance\ManagerDb;

class SiteController extends AccessController
{
  public function actionMoney()
  {
//    $id  = \Yii::$app->getUser()->getId(); //47
//    \Yii::$app->balanceManager->increase(1, 500, ['extra' => 'custom']);
//    \common\components\VarDumper::dump($asd); exit;
    \Yii::$app->cakeBalanceManager->increase( ['userId' => 65], $event->task->count_tu );

//    $manager = \Yii::$app->balanceManager;
//    $manager->accountBalanceAttribute = 'aiur';
//    $manager->decrease(['userId' => 7], 10);
//    echo $manager->calculateBalance(7);
//    echo \Yii::$app->balanceManager->calculateBalance(2);


    /*        echo \Yii::$app->balanceManager->calculateBalance(1);
            echo "<br>";
        echo \Yii::$app->balanceManager->calculateBalance(2);*/
//    $manager->revert(24,);
  }

  public function actionIndex()
  {
    $sql = "SELECT IF(COUNT(ip) = 0, 1, COUNT(ip)) as hits, IF(COUNT(DISTINCT ip) = 0, 1, COUNT(DISTINCT ip)) as hosts FROM stats WHERE DATE(date) = DATE(NOW())";
    $res = \Yii::$app->db->createCommand($sql)->queryOne();

    /*
     * WIDGET "Базы данных"
     */
    $sql = "SELECT table_schema 'name',  Round(Sum(data_length + index_length) / 1024 / 1024, 1) 'size'
            FROM   information_schema.tables
            GROUP  BY table_schema";
    $resDBInfo = \Yii::$app->db->createCommand($sql)->queryAll();

    $usersQt = User::find()->count();
    $usersBanned = User::find()->where(['banned' => 1])->count();
    $usersAdmins = User::find()->where(['root' => 1])->count();

    return $this->render( 'index', [
      'hits' => $res['hits'],
      'hosts' => $res['hosts'],
      'resDBInfo' => $resDBInfo,
      'usersQt' => $usersQt,
      'usersBanned' => $usersBanned,
      'usersAdmins' => $usersAdmins,
    ]);
  }

  public function actionGetStats()
  {
    $sql = 'SELECT TIME_FORMAT(t.time,"%H:%i") as hour, COUNT(s.ip) as hits, COUNT(DISTINCT s.ip) as hosts
        FROM times as t
        LEFT JOIN
          stats as s
        ON HOUR(t.time) = HOUR(s.date) AND DATE(s.date) = DATE(NOW())
        WHERE HOUR(t.time) <= HOUR(NOW())
        GROUP BY t.time
        ORDER BY t.time';

    $results = \Yii::$app->db->createCommand($sql)->queryAll();

    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    return $results;
  }

  public function actionChangeLog()
  {
    return $this->render('versions');
  }

  public function actionAuthInit()
  {
    $auth = \Yii::$app->authManager;
//    $role = $auth->getRole('moderator');

//    $permission = $auth->getPermission('')

//    $auth->assign($role, 2);
//    \common\components\VarDumper::dump($auth->getRole('admin'));
/*
    $admin = $auth->createRole('admin');
    $admin->description = 'Администратор';
    $auth->add($admin);

    $moderator = $auth->createRole('moderator');
    $moderator->description = 'Модератор';
    $auth->add($moderator);

    $customer = $auth->createRole('customer');
    $customer->description = 'Заказчик';
    $auth->add($customer);

    $provider = $auth->createRole('provider');
    $provider->description = 'Исполнитель';
    $auth->add($provider);

    $manageUsers = $auth->createPermission('manageUsers');
    $manageUsers->description = 'Manage users';
    $auth->add($manageUsers);*/

    $visitAdminPanel = $auth->createPermission('visitAdminPanel');
    $visitAdminPanel->description = 'Админ панель';
    $auth->add($visitAdminPanel);

    $admin = $auth->getRole('admin');
    $auth->addChild($admin, $visitAdminPanel);

//    $admin = $auth->getRole('admin');
//    $auth->addChild($admin, $visitAdminPanel);

//    \Yii::$app->authManager->removeAll();
  }
}
