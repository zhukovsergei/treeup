<?php
namespace backend\controllers;

use aiur\forms\AdminLoginForm;
use backend\components\AccessController;
use backend\components\BackendController;
use common\models\User;

class AuthController extends AccessController
{
  public $layout = 'auth';
  public $enableCsrfValidation = false;

  public function actionIndex()
  {
    $noAccounts = User::find()->count();

    return $this->render('index', [
      'noAccounts' => $noAccounts,
    ]);
  }

  public function actionLogin()
  {
    $form = new AdminLoginForm();
    if($form->load(\Yii::$app->request->post(), 'fd') && $form->validate())
    {
      \Yii::$app->user->login($form->getAuthoredUser(), $form->remember ? 3600*24*7: 0);
      return $this->goHome();
    }

    return $this->redirect(['index']);
  }

  public function actionGenFirstAccount()
  {
    $email = \Yii::$app->request->post('email');

    if(\Yii::$app->request->isAjax)
    {
      $password = \Yii::$app->security->generateRandomString(8);
      $m = new User();
      $m->username = 'Администратор';
      $m->auth_key = \Yii::$app->security->generateRandomString();
      $m->password_hash = \Yii::$app->security->generatePasswordHash($password);
      $m->email = $email;
      $m->root = 1;
      $m->save();

      \Yii::$app->mailer->compose('genFirstAccount', ['password' => $password])
        ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->params['supportName']])
        ->setTo($m->email)
        ->setSubject('Новый акаунт для '.$_SERVER["HTTP_HOST"])
        ->send();
    }
  }

  public function actionLogout()
  {
    \Yii::$app->user->logout();
    return $this->redirect('/auth');
  }
}
