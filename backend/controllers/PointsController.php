<?php

namespace backend\controllers;

use aiur\repositories\NewsRepository;
use aiur\services\NewsService;
use backend\components\AccessController;
use common\models\TaskPoint;
use yii\data\ActiveDataProvider;

class PointsController extends AccessController
{

  public function actionIndex()
  {
    $dataProvider = new ActiveDataProvider([
      'query' => TaskPoint::find(),
      'pagination' => [
        'pageSize' => 20,
      ],
    ]);;

    return $this->render( 'index', [
      'dataProvider' => $dataProvider,
    ] );
  }

  public function actionUpdate( $id )
  {
    if( \Yii::$app->request->isPost )
    {
      $fd = \Yii::$app->request->post( 'fd' );
      TaskPoint::updateAll($fd, ['id' => $id]);
    }

    return $this->render( 'edit', [
      'row' => TaskPoint::findOne($id),
    ] );
  }

}
